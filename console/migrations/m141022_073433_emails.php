<?php

use yii\db\Schema;
use yii\db\Migration;

class m141022_073433_emails extends Migration
{
    public function up()
    {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%mail}}', [
			'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			'email' => 'VARCHAR(255) NOT NULL',
			'subject' => 'VARCHAR(255) NOT NULL',
			'body' => 'TEXT NOT NULL',
			'status' => 'INT(2) NOT NULL',
			'isRead' => 'TINYINT(1) NOT NULL',
			'created_time' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ',
			'sent_time' => 'TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\'',
			0 => 'PRIMARY KEY (`id`)'
		], $tableOptions);

    }

    public function down()
    {
		$this->dropTable('{{%mail}}');
    }
}
