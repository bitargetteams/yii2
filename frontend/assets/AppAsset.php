<?php

namespace frontend\assets;

use Yii;
use yii\web\AssetBundle;
use \yii\web\View;

/**
 * Class AppAsset
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@webroot/themes/flat/assets';
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [
        'styles/bootstrap.css',
        'styles/flat-ui.css',
        'styles/index.css',
    ];
    public $js = [
        //'scripts/jquery-1.11.0.min.js',
        'scripts/jquery-ui-1.10.3.custom.min.js',
        'scripts/jquery.ui.touch-punch.min.js',
        'scripts/bootstrap.min.js',
        'scripts/bootstrap-select.js',
        'scripts/bootstrap-switch.js',
		//'scripts/require.js',
		//'scripts/registration.js',
        'scripts/main.js',
        'scripts/flatui-checkbox.js',
        'scripts/flatui-radio.js',
        'scripts/flatui-fileinput.js',
        'scripts/jquery.tagsinput.js',
        'scripts/jquery.placeholder.js',
        'scripts/typeahead.js',
        'scripts/application.js',
        'scripts/scripts.js',
        'scripts/jquery.Jcrop.min.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
		'yii\web\JqueryAsset'
    ];
}
