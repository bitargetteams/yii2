<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 09.10.14
 * Time: 12:21
 */

namespace frontend\assets;


use yii\web\AssetBundle;
use \yii\web\View;

class MainJsAsset extends AssetBundle {
    public $sourcePath = '@webroot/themes/flat/assets';
    public $jsOptions = ['position' => View::POS_HEAD,'data-main' => '/themes/flat/assets/scripts/main'];
    public $js = [
        'scripts/require.js',
    ];
} 