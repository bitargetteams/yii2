<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 22.12.14
 * Time: 11:51
 */

namespace frontend\assets;


use yii\web\AssetBundle;
use yii\web\View;

class DataTableAsset extends AssetBundle {
	public $sourcePath = '@webroot/themes/flat/assets';
	public $jsOptions = [
		'position' => View::POS_END
	];
	public $css = [
		'styles/DataTable/jquery.dataTables.css',
	];
	public $js = [
		'scripts/DataTable/jquery.dataTables.js',
		'scripts/DataTable/jquery.dataTables.columnFilter.js',
	];
	public $depends = [
		//'yii\web\JqueryAsset'
	];
}