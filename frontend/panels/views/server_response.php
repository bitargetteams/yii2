<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td colspan="2"><?=$data[0]?></td>
        </tr>
    </thead>
    <tbody>
            <tr>
                <th>Request</th>
                <td><?=isset($data[2])?$data[2]:""?></td>
            </tr>
            <tr>
                <th>Response</th>
                <td><?=isset($data[1])?$data[1]:""?></td>
            </tr>
            <tr>
                <th>Response Status Code</th>
                <td><?=isset($data[6])?$data[6]:""?></td>
            </tr>
            <tr>
                <th>Connect time</th>
                <td><?=isset($data[5])?$data[5]:""?></td>
            </tr>
            <tr>
                <th>Total time</th>
                <td><?=isset($data[3])?$data[3]:""?></td>
            </tr>
             <tr>
                <th>Method</th>
                <td><?=isset($data[4])?$data[4]:""?></td>
            </tr>
            </tbody>
</table>