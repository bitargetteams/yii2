<?php
/**
 * Description of ServerResponsePanel
 *
 * @author vl
 */
namespace app\panels;

use yii\base\Event;
use yii\base\View;
use yii\base\ViewEvent;
use yii\debug\Panel;


class ServerResponsePanel extends Panel
{
    private $_viewFiles = [];



    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Core';
    }

    /**
     * @inheritdoc
     */
    public function getSummary()
    {
       
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getDetail()
    {
        $rows=file(\Yii::getAlias("@runtime/logs/response.log"));
        $items="";
        for($i=count($rows)-1;($i>(count($rows)-10));$i--){
            $parts = explode('|', $rows[$i]);
            $items .= \Yii::$app->view->render('@app/panels/views/server_response',array('data'=>$parts));
        }
        return $items;
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        return $this->_viewFiles;
    }
}
