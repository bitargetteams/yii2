<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 11.11.14
 * Time: 19:45
 */

namespace app\modules\sections\models\audit;


use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

class JobAudit extends Model{
    public $id;
    public $status;
    public $created_time;
    public $subscribe;
    public $url;

    public static  function model($id,$pagination=null)
    {
        $model = Api::resource('audit')->get(array_merge($pagination,[
                    'id' => $id,
                    'stage' => 1,
                ]
            )
        );
        if (isset($model->response)&&isset($model->response->pdfs)) {
            $collection = [];
            foreach ($model->response->pdfs as $line) {
                $temp = new self;
                $temp->id = $line->id;
                $temp->url = $line->filename;
                $temp->status = $line->status;
                $temp->subscribe = $line->subscribe;
                $temp->created_time = $line->created_time;
                $collection[] = $temp;
            }
        }elseif(isset($model->response)&&!isset($model->response->pdfs)){
            $job = new self;
            return [$job];
        }elseif($model->validateError){
            $job = new self;
            $job->errors = $model->error->message;
            return [$job];
        }else {
            throw new HttpException($model->error->status,$model->error->message);
        }
        return $collection;
    }

    public function attributeLabels()
    {
        return[
            'id' => \Yii::t('audit','#'),
            'url' => \Yii::t('audit','PDF FILE'),
            'status' => \Yii::t('audit','Status'),
            'created_time' => \Yii::t('audit','Created time'),
        ];
    }
} 