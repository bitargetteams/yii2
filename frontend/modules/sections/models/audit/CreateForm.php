<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 05.11.14
 * Time: 20:29
 */
namespace app\modules\sections\models\audit;

use \yii\base\Model;

/**
 * Class AuditForm
 *
 * @package frontend\audit\models
 */
class CreateForm extends Model {
    /**
     * @var array
     */
    public $useAudit;
    /**
     * @var
     */
    public $url;
    /**
     * @var
     */
    public $customer_name;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $target_audience;
    /**
     * @var
     */
    public $site_competitors;
    /**
     * @var
     */
    public $promotion_problem;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['useAudit','url','customer_name'],'required'],
            ['url','url'],
            ['customer_name','string','min' => 5],
            ['customer_name','match','pattern'=>'/^(?:\w{2,}\x20\w{2,})(?:\x20\w{2,})?$/i'],
            [['products','target_audience','site_competitors','promotion_problem'],'safe'],
            //['useAudit','filter','filter'=>function($attr){ return implode('&usedAudit[]=',$attr);}]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return[
            'useAudit' => \Yii::t('audit','What you want to order an audit?'),
            'url' => \Yii::t('audit','Site URL'),
            'name' => \Yii::t('audit','Your name'),
            'products' => \Yii::t('audit','Goods and services'),
            'target_audience' => \Yii::t('audit','Target Audience'),
            'site_competitors' => \Yii::t('audit','Competitors website'),
            'promotion_problem' => \Yii::t('audit','Password'),
        ];
    }
}