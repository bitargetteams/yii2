<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 12.11.14
 * Time: 20:20
 */

namespace app\modules\sections\models\audit;


use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

class PartitionsAudits extends Model {
    public $id;
    public $price;
    public $currency;
    public $name;

    public static  function model()
    {
        $model = Api::resource('audit_partition')->get(
            [
            ]
        );
        if (isset($model->response)) {
            $collection = [];
            foreach($model->response as $line) {
                $temp = new self;
                $temp->id = $line->id;
                $temp->price = $line->price;
                $temp->currency = $line->currency;
                $temp->name = $line->name;
                $collection[] = $temp;
            }
        }elseif($model->validateError){
            $job = new self;
            $job->errors = $model->error->message;
            return $job;
        }else {
            throw new HttpException($model->error->status,$model->error->message);
        }
        return $collection;
    }

    public function attributeLabels()
    {
        return[
            'id' => \Yii::t('audit','#'),
            'price' => \Yii::t('audit','Site URL'),
            'name' => \Yii::t('audit','Your name'),
            'created_time' => \Yii::t('audit','Status'),
            'currency' => \Yii::t('audit','Created time'),
        ];
    }
}