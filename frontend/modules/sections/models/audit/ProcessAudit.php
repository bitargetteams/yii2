<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 12.11.14
 * Time: 19:49
 */

namespace app\modules\sections\models\audit;


use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

class ProcessAudit extends Model{
    public $id;
    public $status;
    public $created_time;
    public $updated_time;
    public $customer_name;
    public $url;
    public $products;
    public $target_audience;
    public $site_competitors;
    public $promotion_problem;
    public $price_total;
    public $partitions;

    public static  function model($id)
    {
        $model = Api::resource('audit')->get(['id' => $id]
        );
        if (isset($model->response)) {
            $collection = [];
            $line = $model->response;
                $temp = new self;
                $temp->id = $line->location_id;
                $temp->status = $line->status ;
                $temp->created_time = $line->created_time;
                $temp->updated_time = $line->updated_time;
                $temp->customer_name = $line->customer_name;
                $temp->url = $line->url;
                $temp->products = $line->products;
                $temp->target_audience = $line->target_audience;
                $temp->site_competitors = $line->site_competitors;
                $temp->promotion_problem = $line->promotion_problem;
                $temp->price_total = $line->price_total;
                $temp->partitions = isset($line->partitions) ? $line->partitions : [];

        }elseif($model->validateError){
            $job = new self;
            $job->errors = $model->error->message;
            return $job;
        }else {
            throw new HttpException($model->error->status,$model->error->message);
        }
        return $temp;
    }

    public function attributeLabels()
    {
        return[
            'id' => \Yii::t('audit','#'),
            'status' => \Yii::t('audit','Site URL'),
            'name' => \Yii::t('audit','Your name'),
            'created_time' => \Yii::t('audit','Status'),
            'updated_time' => \Yii::t('audit','Created time'),
            'customer_name' => \Yii::t('audit','Updated time'),
            'url' => \Yii::t('audit','Your name'),
            'products' => \Yii::t('audit','Status'),
            'target_audience' => \Yii::t('audit','Created time'),
            'promotion_problem' => \Yii::t('audit','Status'),
            'site_competitors' => \Yii::t('audit','Created time'),
            'price_total' => \Yii::t('audit','Updated time'),
            'partitions' => \Yii::t('audit','Updated time'),
        ];
    }
} 