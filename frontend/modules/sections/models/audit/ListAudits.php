<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 11.11.14
 * Time: 12:31
 */

namespace app\modules\sections\models\audit;


use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

class ListAudits extends Model{
    public $id;
    public $customer_name;
    public $url;
    public $status;
    public $created_time;
    public $updated_time;

    public static  $pagination;

    public static  function model($pagination = [])
    {
        $model = Api::resource('audit')->get($pagination);
        //var_dump($pagination);
        //var_dump($model);die;
        if (isset($model->response)) {
            $collection = [];
            foreach ($model->response as $line) {
                $temp = new self;
                $temp->id = $line->location_id;
                $temp->customer_name = $line->customer_name ;
                $temp->url = $line->url;
                $temp->status = $line->status;
                $temp->created_time = $line->created_time;
                $temp->updated_time = $line->updated_time;
                $collection[] = $temp;
            }
        }elseif($model->validateError){
            $job = new self;
            $job->errors = $model->error->message;
            return [$job];
        }else {
            throw new HttpException($model->error->status,$model->error->message);
        }
        return $collection;
    }

    public function attributeLabels()
    {
        return[
            'id' => \Yii::t('audit','#'),
            'url' => \Yii::t('audit','Site URL'),
            'name' => \Yii::t('audit','Your name'),
            'status' => \Yii::t('audit','Status'),
            'created_time' => \Yii::t('audit','Created time'),
            'updated_time' => \Yii::t('audit','Updated time'),
        ];
    }
} 