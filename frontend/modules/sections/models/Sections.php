<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 13.11.14
 * Time: 13:08
 */
namespace app\modules\sections\models;

use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

/**
 * Class Sections
 *
 * @method setClosed() setClosed() some closed
 * @package app\modules\sections\models\audit
 */
class Sections extends Model
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $slug;
    /**
     * @var
     */
    public $title;
    /**
     * @var
     */
    public $description;
    /**
     * @var
     */
    public $status;

    /**
     * @param string|null
     * @return Sections|array
     * @throws HttpException
     */
    public static function model($slug = null)
    {
        $params = ['status' => 'stable'];
        if (!is_null($slug)) {
            $params['slug'] = $slug;
        }
        $model = Api::resource('section')->get($params);
        if (isset($model->response)) {
            $collection = [];
            foreach ($model->response as $line) {
                $temp = new self;
                $temp->id = $line->id;
                $temp->slug = $line->slug;
                $temp->title = $line->title;
                $temp->description = $line->description;
                $temp->status = $line->status;
                $collection[] = $temp;
            }
        } elseif ($model->validateError) {
            $job = new self;
            $job->errors = $model->error->message;
            return $job;
        } else {
            throw new HttpException($model->error->status, $model->error->message);
        }
        return $collection;
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        return ['url' => '/section/' . $this->slug, 'label' => $this->title];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('audit', '#'),
            'slug' => \Yii::t('audit', 'Text identity'),
            'title' => \Yii::t('audit', 'Title'),
            'description' => \Yii::t('audit', 'Description'),
            'status' => \Yii::t('audit', 'Status'),
        ];
    }
}
