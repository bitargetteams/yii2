<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 22.12.14
 * Time: 18:02
 */

namespace app\modules\sections\models\social\optimizer;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;


class SearchGroup extends Model
{

	protected $api = 'social/group';
	protected $topic_api = 'social/topic';
	protected $network_api = 'social/network';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $platform_location_id;

	public $client_location_id;

	public $topic_id;
	public $member_cnt_min;
	public $member_cnt_max;
	public $day_hits_min;
	public $day_hits_max;
	public $price_text_min;
	public $price_text_max;
	public $price_image_min;
	public $price_image_max;
	public $price_video_min;
	public $price_video_max;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'topic_id', 'member_cnt_min', 'member_cnt_max', 'day_hits_min', 'day_hits_max',
			'price_text_min', 'price_text_max', 'price_image_min', 'price_image_max', 'price_video_min', 'price_video_max'], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'client_location_id' => Yii::t('social', 'Client Location ID'),
			'topic_id' => Yii::t('social', 'Topic ID'),
			'member_cnt_min' => Yii::t('social', 'Member Cnt Min'),
			'member_cnt_max' => Yii::t('social', 'Member Cnt Max'),
			'day_hits_min' => Yii::t('social', 'Day Hits Min'),
			'day_hits_max' => Yii::t('social', 'Day Hits Max'),
			'price_text_min' => Yii::t('social', 'Price Text Min'),
			'price_text_max' => Yii::t('social', 'Price Text Max'),
			'price_image_min' => Yii::t('social', 'Price Image Min'),
			'price_image_max' => Yii::t('social', 'Price Image Max'),
			'price_video_min' => Yii::t('social', 'Price Video Min'),
			'price_video_max' => Yii::t('social', 'Price Video Max'),
		];
	}

	/**
	 * Return power
	 * @return int
	 */
	public function getPower()
	{
		return $this->power;
	}

	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}

	public function getTopics()
	{
		$res = Api::resource($this->topic_api)->get();
		$topics = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $topics[$item->id] = $item;
		}
		return	$topics;
	}

	public function getNetworks()
	{
		$res = Api::resource($this->network_api)->get();
		$networks = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $networks[$item->id] = $item;
		}
		return	$networks;
	}

	public function prepareForDropDown($data = [], $default = ['0' => 'Выберите'])
	{
		$res = $default;
		foreach ($data as $k=>$v) {
			$res[$k] =  $v->title;
		}
		return $res;

	}

	public function getTableData($params = [], $columns = [], $order = [], $draw = 0)
	{

		$topics = $this->getTopics();
		$networks = $this->getNetworks();


		foreach ($columns as $col) {
			if (strlen($col['search']['value'])) $params[$col['data']] = $col['search']['value'];
		}
		foreach ($order as $o) {
			if (!empty($columns[$o['column']]['data'])) {
				$params['order'][$columns[$o['column']]['data']] = $o['dir'];
			}
		}

		$data = [];
		foreach($this->getAllItems($params) as $k=>$item) {
			$data[] = [
				'DT_RowId'=>'row_' . $k,
				'location_id'=>$item->location_id,
				'title'=>$item->title,
				'url'=>$item->url,
				'topic_id'=>$topics[$item->topic_id]->title,
				'network_id'=>$networks[$item->network_id]->title,
				'member_cnt'=>$item->member_cnt,
				'day_hits'=>$item->day_hits,
				'create_time'=>$item->create_time
			];
		}

		return [
			'data' => $data,
			'draw' => $draw,
			'recordsFiltered' => count($data),
			'recordsTotal' => $this->getPower()
		];
	}

	public function getAllItems($params = [])
	{

		$this->power = 0;

		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	public function prepareData($items = [])
	{
		$res = [];
		if (count($items)) {
			foreach ($items as $item) {
				$res[] = $item;
			}

		}
		return $res;
	}
}