<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 12.12.14
 * Time: 15:42
 */

namespace app\modules\sections\models\social\optimizer;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * Post form
 */
class Post extends Model
{

	protected $api = 'social/post';
	protected $api_attachment = 'social/post/attachment';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $isNewRecord = true;

	public $location_id;
	public $client_location_id;
	public $platform_location_id;

	public $title;
	public $description;
	public $article;

	public $link;
	public $video_link;

	public $image;

	public $moderated;
	public $moderator_id;
	public $status;
	public $status_descr;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'title', 'article'], 'required'],
			[['client_location_id'], 'integer'],
			[['description'], 'string'],
			[['title', 'link', 'video_link'], 'string', 'max' => 100],
			[['image'], 'file', 'maxFiles' => 5, 'extensions' => 'jpg, png, gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif'],

		];
	}

	public function attributeLabels()
	{
		return [
			'location_id' => Yii::t('social', 'Location ID'),
			'title' => Yii::t('social', 'Post Title'),
			'article' => Yii::t('social', 'Article'),
			'description' => Yii::t('social', 'Description'),
			'link' => Yii::t('social', 'Link'),
			'image' => Yii::t('social', 'Images'),
			'video_link' => Yii::t('social', 'Video Link'),
		];
	}

	/**
	 * Save Post data.
	 *
	 * @return Post ID|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$post = [];

			$post['client_location_id'] = $this->client_location_id;
			$post['title'] = $this->title;
			$post['description'] = $this->description;
			$post['article'] = $this->article;
			$post['link'] = $this->link;
			$post['video_link'] = $this->video_link;

			$post = Api::resource($this->api)->post($post);

			if (isset($post->power) && is_numeric($post->power) && $post->power == 1) {
				$this->location_id = $post->items[0]->location_id;
				return true;
			}
		}

		return null;
	}

	public function save_attach()
	{

		$this->image = UploadedFile::getInstances($this, 'image');

		foreach ($this->image as $item) {
			$attachment = Api::resource($this->api_attachment)->file([
				'collection' => 'image',
				'file_hash' => hash_file('sha256', $item->tempName),
				'file' => '@'.$item->tempName,
				'location_id' => $this->location_id
			]);
		}

		return true;

	}

	public function get_attach()
	{
		$res = [];

		$attachments = Api::resource($this->api_attachment)->get([
			'client_location_id' => $this->client_location_id,
			'location_id' => $this->location_id
		]);

		if (isset($attachments->power) && is_numeric($attachments->power) && $attachments->power > 0) {
			foreach ($attachments->items as $item) {
				$res[] = [
					'img_body' => $item->img_body,
					'mime' => $item->mime,
					'id' => $item->id
				];
			}
		}
		return $res;
	}

	/**
	 * Update Post data.
	 *
	 * @return Post ID|null the saved model or null if saving fails
	 */
	public function update()
	{

		if ($this->validate()) {
			$post = [];

			$post['location_id'] = $this->location_id;
			$post['client_location_id'] = $this->client_location_id;
			$post['title'] = $this->title;
			$post['description'] = $this->description;
			$post['article'] = $this->article;
			$post['link'] = $this->link;
			$post['video_link'] = $this->video_link;

			$post = Api::resource($this->api)->put($post);

			if (isset($post->error)) {
				$this->error = true;
				$this->errors = $post->error->message;

				return false;
			}

			if (isset($post->power) && is_numeric($post->power) && $post->power == 1) {
				return true;
			}
		}

		return null;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public function delete($id)
	{
		$post = Api::resource($this->api)->delete(['ids' => [$id], 'client_location_id' => $this->client_location_id]);

		if (isset($post->error)) return false;

		return true;
	}

	/**
	 * @param $id
	 * @param $location_id
	 * @return bool
	 */
	public function deleteAttachment($id, $location_id)
	{
		$attach = Api::resource($this->api_attachment)->delete([
			'ids' => [$id],
			'client_location_id' => $this->client_location_id,
			'location_id' => $location_id
		]);

		if (isset($attach->error)) return false;

		return true;
	}

	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getAllItems($params = [])
	{

		$this->power = 0;

		$params['client_location_id'] = $this->client_location_id;
		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	/**
	 * @param array $items
	 * @return array
	 *
	 */
	public function prepareData($items = [])
	{
		$res = [];
		if (count($items)) {
			foreach ($items as $item) {
				$res[] = $item;
			}

		}
		return $res;
	}

	/**
	 * Return power
	 * @return int
	 */
	public function getPower()
	{
		return $this->power;
	}

	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}


	/**
	 * @param $id
	 * @return $this|bool
	 */
	public function findByID($id)
	{
		$result = Api::resource($this->api)->get([
			'location_id' => $id,
			'client_location_id' => $this->client_location_id
		]);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else if ($result->power == 1) {
			$this->isNewRecord = false;
			foreach ($result->items[0] as $k=>$v) {
				$this->$k = $v;
			}
		}

		return $this;
	}

	public function prepareForDropDown($data = [], $default = ['0' => 'Выберите'])
	{
		$res = $default;
		foreach ($data as $k=>$v) {
			$res[$k] =  $v->title;
		}
		return $res;

	}


	/**
	 * @param array $params
	 * @param array $columns
	 * @param array $order
	 * @param int $draw
	 * @return array
	 */
	public function getTableData($params = [], $columns = [], $order = [], $draw = 0)
	{

		foreach ($columns as $col) {
			if (strlen($col['search']['value'])) $params[$col['data']] = $col['search']['value'];
		}
		foreach ($order as $o) {
			if (!empty($columns[$o['column']]['data'])) {
				$params['order'][$columns[$o['column']]['data']] = $o['dir'];
			}
		}

		$data = [];
		foreach($this->getAllItems($params) as $k=>$item) {
			$data[] = [
				'DT_RowId'=>'row_' . $k,
				'location_id'=>$item->location_id,
				'title'=>$item->title,
				'description'=>$item->description,
				'group_cnt'=>0,
				'moderated'=>($item->moderated==0)?'Ожидание':'Да'

			];
		}

		return [
			'data' => $data,
			'draw' => $draw,
			'recordsFiltered' => $this->getPower(),
			'recordsTotal' => $this->getPower()
		];
	}

}