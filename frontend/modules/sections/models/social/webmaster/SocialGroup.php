<?php

namespace app\modules\sections\models\social\webmaster;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;

/**
 * SocialGroup form
 */
class SocialGroup extends Model
{

	protected $api = 'social/group';
	protected $topic_api = 'social/topic';
	protected $network_api = 'social/network';
	protected $minprice_api = 'minprice/social';
	protected $confirm_api = 'social/group/confirm_auth';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $isNewRecord = true;

	public $location_id;
	public $platform_location_id;
	public $client_location_id;


	public $title;
	public $description;
	public $create_time;
	public $url;

	public $member_cnt;
	public $day_hits;
	public $last_check_time;
	public $topic_id;
	public $price_text;
	public $price_image;
	public $price_video;
	public $post_delete;
	public $moderated;
	public $moderator_id;
	public $network_id;
	public $status;
	public $status_descr;
	public $network_group_id;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'title', 'description', 'url', 'topic_id', 'network_id'], 'required'],
			[['client_location_id', 'member_cnt', 'day_hits', 'topic_id', 'post_delete', 'moderated', 'moderator_id', 'network_id'], 'integer'],
			[['description'], 'string'],
			[['create_time', 'last_check_time'], 'safe'],
			[['price_text', 'price_image', 'price_video'], 'number'],
			[['title'], 'string', 'max' => 100],
			[['url'], 'string', 'max' => 150],
			[['network_group_id'], 'string', 'max' => 50]
		];
	}

	public function attributeLabels()
	{
		return [
			'location_id' => Yii::t('social', 'Location ID'),
			'client_location_id' => Yii::t('social', 'Client Location ID'),
			'platform_location_id' => Yii::t('social', 'Platform Location ID'),
			'title' => Yii::t('social', 'Title'),
			'description' => Yii::t('social', 'Description'),
			'create_time' => Yii::t('social', 'Create Time'),
			'url' => Yii::t('social', 'Url'),
			'member_cnt' => Yii::t('social', 'Member Cnt'),
			'day_hits' => Yii::t('social', 'Day Hits'),
			'last_check_time' => Yii::t('social', 'Last Check Time'),
			'topic_id' => Yii::t('social', 'Topic ID'),
			'price_text' => Yii::t('social', 'Price Text'),
			'price_image' => Yii::t('social', 'Price Image'),
			'price_video' => Yii::t('social', 'Price Video'),
			'post_delete' => Yii::t('social', 'Post Delete'),
			'moderated' => Yii::t('social', 'Moderated'),
			'moderator_id' => Yii::t('social', 'Moderator ID'),
			'network_id' => Yii::t('social', 'Network ID'),
			'network_group_id' => Yii::t('social', 'Network Group ID'),
		];
	}

	/**
	 * Save Social Group data.
	 *
	 * @return Group ID|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$group = [];

			if (($network = $this->getNetwork()) !== null) {
				$group['price_text'] = $network->min_price_text;
				$group['price_image'] = $network->min_price_image;
				$group['price_video'] = $network->min_price_video;
			}


			$group['client_location_id'] = $this->client_location_id;
			$group['title'] = $this->title;
			$group['url'] = $this->url;
			$group['description'] = $this->description;
			$group['topic_id'] = $this->topic_id;
			$group['network_id'] = $this->network_id;
			//$group['post_delete'] = $this->post_delete;

			$group['group_id'] = $this->network_group_id;

			$group = Api::resource($this->api)->post($group);

			if (isset($group->error)) {
				$this->error = true;
				$this->errors[] = $group->error->message;
				return false;
			}

			if (isset($group->items)) {
				$obj = $group->items[0];

				if (isset($obj->type) && $obj->type == 'redirect') {

					setcookie('network', $obj->network, time()+120);

					header('Location: '.$obj->link);
					exit;
				} else {
					return true;
				}
			}

		}

		return null;
	}

	public function delete($id)
	{
		$post = Api::resource($this->api)->delete(['ids' => [$id], 'client_location_id' => $this->client_location_id]);

		return true;
	}

	public function confirm($params = [])
	{

		$group = Api::resource($this->confirm_api)->get(
			array_merge(
				[
					'client_location_id' => $this->client_location_id,
					'network' => $_COOKIE['network']
				],
				$params
			)
		);
		print_r($group);exit;

		return true;
	}

	public function getNetwork()
	{
		if ($this->network_id > 0) {
			$network = Api::resource($this->network_api)->get(['id' => $this->network_id]);
			if (!isset($network->error) && $network->power == 1) {
				return $network->items[0];
			}
		}

		return null;
	}


	/**
	 * Update Social Group data.
	 *
	 * @return Group ID|null the saved model or null if saving fails
	 */
	public function update()
	{

		if ($this->validate()) {
			$group = [];

			$group['location_id'] = $this->location_id;
			$group['client_location_id'] = $this->client_location_id;
			$group['title'] = $this->title;
			$group['url'] = $this->url;
			$group['description'] = $this->description;
			$group['topic_id'] = $this->topic_id;
			$group['network_id'] = $this->network_id;
			$group['post_delete'] = $this->post_delete;

			$group['network_group_id'] = $this->network_group_id;

			$group = Api::resource($this->api)->put($group);

			if (isset($group->error)) {
				$this->error = true;
				$this->errors = $group->error->message;

				return false;
			}

			if (isset($group->power) && is_numeric($group->power) && $group->power == 1) {
				return true;
			}
		}

		return null;
	}

	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getAllItems($params = [])
	{

		$this->power = 0;

		$params['client_location_id'] = $this->client_location_id;
		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	/**
	 * @param array $items
	 * @return array
	 *
	 */
	public function prepareData($items = [])
	{
		$res = [];
		if (count($items)) {
			foreach ($items as $item) {
				$res[] = $item;
			}

		}
		return $res;
	}

	/**
	 * Return power
	 * @return int
	 */
	public function getPower()
	{
		return $this->power;
	}

	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors($attribute = null)
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}


	/**
	 * @param $id
	 * @return $this|bool
	 */
	public function findByID($id)
	{
		$result = Api::resource($this->api)->get([
			'location_id' => $id,
			'client_location_id' => $this->client_location_id
		]);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else if ($result->power == 1) {
			$this->isNewRecord = false;
			foreach ($result->items[0] as $k=>$v) {
				$this->$k = $v;
			}
		}

		return $this;
	}

	public function getTopics()
	{
		$res = Api::resource($this->topic_api)->get();
		$topics = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $topics[$item->id] = $item;
		}
		return	$topics;
	}

	public function getNetworks()
	{
		$res = Api::resource($this->network_api)->get();
		$networks = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $networks[$item->id] = $item;
		}
		return	$networks;
	}

	public function prepareForDropDown($data = [], $default = ['0' => 'Выберите'])
	{
		$res = $default;
		foreach ($data as $k=>$v) {
			$res[$k] =  $v->title;
		}
		return $res;

	}


	/**
	 * @param array $params
	 * @param array $columns
	 * @param array $order
	 * @param int $draw
	 * @return array
	 */
	public function getTableData($params = [], $columns = [], $order = [], $draw = 0)
	{

		$topics = $this->getTopics();
		$networks = $this->getNetworks();


		foreach ($columns as $col) {
			if (strlen($col['search']['value'])) $params[$col['data']] = $col['search']['value'];
		}
		foreach ($order as $o) {
			if (!empty($columns[$o['column']]['data'])) {
				$params['order'][$columns[$o['column']]['data']] = $o['dir'];
			}
		}

		$data = [];
		foreach($this->getAllItems($params) as $k=>$item) {
			$data[] = [
				'DT_RowId'=>'row_' . $k,
				'location_id'=>$item->location_id,
				'title'=>$item->title,
				'url'=>$item->url,
				'topic_id'=>$topics[$item->topic_id]->title,
				'network_id'=>$networks[$item->network_id]->title,
				'moderated'=>($item->moderated==0)?'Ожидание':'Да',
				'member_cnt'=>$item->member_cnt,
				'day_hits'=>$item->day_hits,
				'create_time'=>$item->create_time
			];
		}

		return [
			'data' => $data,
			'draw' => $draw,
			'recordsFiltered' => count($data),
			'recordsTotal' => $this->getPower()
		];
	}

	/**
	 * Get Min price data for Social section by network_id
	 * @return array
	 */
	public function getMinPrice()
	{
		$result = Api::resource($this->minprice_api)->get([
			'network_id' => $this->network_id
		]);

		return [];
	}
}
