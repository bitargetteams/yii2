<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 15.01.15
 * Time: 12:35
 */

namespace app\modules\sections\models\social\webmaster;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;

class SocialAccount extends Model
{

	protected $group_api = 'social/group';
	protected $topic_api = 'social/topic';
	protected $network_api = 'social/network';
	protected $application_api = 'social/application';
	protected $client_api = 'social/client';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $isNewRecord = true;

	public $id;
	public $platform_location_id;
	public $client_location_id;


	public $access_token;
	public $token_lifetime;
	public $network_user_id;
	public $social_application_id;
	public $check_time;
	public $access_token_secret;

	/**
	 *
	 */
	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'check_time'], 'required'],
			[['client_location_id', 'token_lifetime', 'social_application_id', 'check_time'], 'integer'],
			[['access_token', 'access_token_secret'], 'string'],
			[['network_user_id'], 'string', 'max' => 50],
			[['client_location_id', 'social_application_id'], 'unique', 'targetAttribute' => ['client_location_id', 'social_application_id'], 'message' => 'The combination of Client Location ID and Social Application ID has already been taken.']
		];
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'client_location_id' => Yii::t('social', 'Client Location ID'),
			'access_token' => Yii::t('social', 'Access Token'),
			'token_lifetime' => Yii::t('social', 'Token Lifetime'),
			'network_user_id' => Yii::t('social', 'Network User ID'),
			'social_application_id' => Yii::t('social', 'Social Application ID'),
			'check_time' => Yii::t('social', 'Check Time'),
			'access_token_secret' => Yii::t('social', 'Access Token Secret'),
		];
	}

	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getAllItems($params = [])
	{

		$this->power = 0;

		$params['client_location_id'] = $this->client_location_id;
		$result = Api::resource($this->group_api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $result->items;
		}
	}

	/**
	 * @return array
	 */
	public function getTopics()
	{
		$res = Api::resource($this->topic_api)->get();
		$topics = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $topics[$item->id] = $item;
		}
		return	$topics;
	}

	/**
	 * @return array
	 */
	public function getNetworks()
	{
		$res = Api::resource($this->network_api)->get();
		$networks = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $networks[$item->id] = $item;
		}
		return	$networks;
	}

	/**
	 * Возвращает список приложений
	 *
	 * @return array
	 */
	public function getApplication()
	{
		$res = Api::resource($this->application_api)->get(['client_location_id' => $this->client_location_id]);
		$applications = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $applications[$item->id] = $item;
		}
		return	$applications;
	}

	/**
	 * Отвязать клиента от учетной записи соц.сети
	 * @param array $params
	 * @return bool
	 */
	public function unsetClient($params = [])
	{
		$res = Api::resource($this->client_api . '/' . $params['client_id'])->delete([
			'client_location_id' => $this->client_location_id
		]);

		if (isset($res->error)) {
			$this->error = true;
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Привязать клиента к учетной записи соц.сети
	 * @param array $params
	 * @return bool
	 */
	public function assignClient($params = [])
	{

		$res = Api::resource($this->client_api)->post(
			array_merge(
				$params,
				['client_location_id' => $this->client_location_id]
			)
		);

		if (isset($res->error)) {
			$this->error = true;
			$this->errors[] = $res->error->message;
			return false;
		}

		if (isset($res->items)) {
			$obj = $res->items[0];

			if (isset($obj->type) && $obj->type == 'redirect') {

				setcookie('network', $obj->network, time()+120);

				header('Location: '.$obj->link);
				exit;
			} else {
				return true;
			}
		}
	}

	/**
	 * Изменить статус группы
	 *
	 * @param array $params
	 * @return bool
	 */
	public function toggleGroup($params = [])
	{
		$res = Api::resource($this->group_api . '/' . $params['location_id'])->put([
			'client_location_id' => $this->client_location_id,
			'status' => $params['status']
		]);

		if (isset($res->error)) {
			$this->error = true;
			return false;
		}

		return true;
	}
}