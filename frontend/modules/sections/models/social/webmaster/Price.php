<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 11.12.14
 * Time: 19:04
 */

namespace app\modules\sections\models\social\webmaster;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;

/**
 * Price form
 */
class Price extends Model
{

	protected $api = 'social/group';
	protected $network_api = 'social/network';
	protected $minprice_api = 'minprice/social';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];


	public $location_id;
	public $platform_location_id;
	public $client_location_id;


	public $price_text;
	public $price_image;
	public $price_video;

	public $network_id;

	public $title;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['price_text', 'price_image', 'price_video'], 'required'],
			[['price_text', 'price_image', 'price_video'], 'number'],
		];
	}

	public function attributeLabels()
	{
		return [

			'price_text' => Yii::t('social', 'Price Text'),
			'price_image' => Yii::t('social', 'Price Image'),
			'price_video' => Yii::t('social', 'Price Video'),

			'network_id' => Yii::t('social', 'Network ID'),
		];
	}


	public function getNetwork()
	{
		if ($this->network_id > 0) {
			$network = Api::resource($this->network_api)->get(['id' => $this->network_id]);
			if (!isset($network->error) && $network->power == 1) {
				return $network->items[0];
			}
		}

		return null;
	}


	/**
	 * Update Price data.
	 *
	 * @return Group ID|null the saved model or null if saving fails
	 */
	public function update()
	{

		if ($this->validate()) {
			$group = [];

			$group['location_id'] = $this->location_id;
			$group['client_location_id'] = $this->client_location_id;


			$group['price_text'] = $this->price_text;
			$group['price_image'] = $this->price_image;
			$group['price_video'] = $this->price_video;

			$group = Api::resource($this->api)->put($group);

			if (isset($group->error)) {
				$this->error = true;
				$this->errors = $group->error->message;

				return false;
			}

			if (isset($group->power) && is_numeric($group->power) && $group->power == 1) {
				return true;
			}
		}

		return null;
	}



	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}


	/**
	 * @param $id
	 * @return $this|bool
	 */
	public function findByID($id)
	{
		$result = Api::resource($this->api)->get([
			'id' => $id,
			'client_location_id' => $this->client_location_id,
			'fields' => 'price_text,price_image,price_video,title,location_id,network_id'
		]);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else if ($result->power == 1) {
			foreach ($result->items[0] as $k=>$v) {
				$this->$k = $v;
			}
		}

		return $this;
	}

	public function getMinPrice()
	{
		$res = [];

		$result = Api::resource($this->minprice_api)->get([
			'network_id' => $this->network_id
		]);

		if (!isset($result->error)) {
			$res = $result->items[0];
		}

		return $res;
	}

}
