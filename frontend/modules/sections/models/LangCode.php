<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 24.11.14
 * Time: 19:59
 */

namespace app\modules\sections\models;


use app\components\Api;
use yii\base\Model;
use yii\web\HttpException;

class LangCode extends Model{
    public $id;
    public $title;
    public $description;
    public static  function model($id=null)
    {
        $params = is_null($id) ? [] : ['id'=>$id];
        $model = Api::resource('lang_code')->get($params);
        if (isset($model->response) || !$model->validateError && !$model->serverError ) {
            $collection = [];
            $response = is_array($model->response) ? $model->response : [$model->response];
            foreach ($response as $line) {
                $temp = new self;
                $temp->id = $line->id;
                $temp->title = $line->title;
                $temp->description = $line->description;
                $collection[] = $temp;
            }
        } elseif ($model->validateError) {
            $job = new self;
            $job->errors = $model->errors->message;
            return $job;
        } else {
            throw new HttpException($model->errors->status, $model->errors->message);
        }
        return $collection;
    }

} 