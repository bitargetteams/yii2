<?php

namespace app\modules\sections\models\jban;

use Yii;
use yii\base\Model;
use app\components\Api;
use yii\helpers\ArrayHelper;

class JbanList extends Model
{

    public $error;
    public $keys = [];
    public $type = 'domain';
    public $owner = 'webmaster';
    public $list;
    public $list_id;

    protected $api_list = 'black_list';
    protected $api_item = 'black_item';
    protected $client_id;
    protected $lists = [];
    protected $items = [];
    protected $parsed = [];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jban_id', 'owner', 'domain'], 'required'],
            [['jban_id'], 'integer'],
            [['owner'], 'string'],
            [['domain'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('jban', 'ID'),
            'jban_id' => Yii::t('jban', 'Location ID'),
            'owner' => Yii::t('jban', 'Owner'),
            'domain' => Yii::t('jban', 'Domain'),
        ];
    }
    
    /**
    * 
    * @param $client_id -- some id, not definited yet
    * @param $owner -- 'webmaster' | 'optimizer'
    * @param $type -- 'domain' | 'word'
    * @param $model -- actually 'color' of list: 'white' | 'black'
    *
    */
    public function __construct($client_id, $owner, $type, $model)
    {
        $this->client_id = $client_id;
        $this->owner = $owner;
        $this->type = $type;
        $this->api_item = 'jban/'.$model.'_item';
        $this->api_list = 'jban/'.$model.'_list';
        $this->getAllLists();
        parent::__construct();
    }

    public function getAllLists()
    {
        $result = Api::resource($this->api_list)->get([
            'client_location_id' => $this->client_id,
            'owner' => $this->owner,
        ]);
        if ($result->power) {
            $this->lists = $result->items;
            return true;
        } else {
            $result = $this->makeDefaultList();
            $this->lists = $result->items;
        }
        return true;
    }

    public function getCurrentList($lid)
    {
        foreach($this->lists as $list){
            if ($list->id == $lid) {
                $this->list_id = $lid;
                $this->list = $list;
                return $this->list_id;
            }
        }
        $this->list_id = false;
        return false;
    }

    public function getDefaultList()
    {
        $this->list_id = $this->lists[0]->id;
        return $this->list_id;
    }
    
    private function makeDefaultList()
    {
        return Api::resource($this->api_list)->post([
                            'title' => 'default',
                            'client_location_id' => $this->client_id,
                            'owner' => $this->owner,
                        ]);
    }

    public function getDropDownList()
    {
        return ArrayHelper::map($this->lists, 'id', 'title');
    }

    public function getProfileList()
    {
        $result = [];
        foreach ($this->lists as $list){
            $request = Api::resource($this->api_item)->get([
                    'list_id' => $this->list_id,
                    'per-page' => 1,
                ]);
            $result[] = ['id' => $list->id, 'title' => $list->title, 'count' => $request->power];
        }
        return $result;
    }

    public function addList($title)
    {
        $request = Api::resource($this->api_list)->post([
            'title' => $title,
            'client_location_id' => $this->client_id,
            'owner' => $this->owner,
        ]);
        return (!isset($request->error)) ? $request->items[0]->id : false;
    }

    public function getAllItems()
    {
        $result = Api::resource($this->api_item)->get([
                'type' => $this->type,
                'list_id' => $this->list_id,
                'per-page' => 1000,
            ]);
        if (!isset($result->error)) {
            $this->items = $result->items;
            return true;
        } else {
            return false;
        }
    }

    public function getItems($showMe="")
    {
        $result = [];
        $this->parse();
        if (empty($showMe)) {
            foreach($this->parsed as $item) {
                $result = array_merge($result, $item);
            }

        } else {
            $result = (isset($this->parsed[$showMe])) ? $this->parsed[$showMe] : [];
        }
        return $result;
    }

    protected function parse()
    {
        $result = [];
        foreach ($this->items as $item) {
            $first = mb_substr($item->item, 0, 1, 'UTF-8');
            if (stripos($item->item, '*') !== false) {
                $result['*'][] = ['id' => $item->id, 'item' => $item->item];
            } elseif (preg_match('/^([a-zа-я])/', $first)) {
                $result[mb_convert_case($first, MB_CASE_TITLE, 'UTF-8')][] = ['id' => $item->id, 'item' => $item->item];
            } elseif (is_numeric($first)) {
                $result['@'][] = ['id' => $item->id, 'item' => $item->item];
            } else {
                $result['語'][] = ['id' => $item->id, 'item' => $item->item];
            }
        }
        foreach ($result as $key=>$value){
            if ($result[$key]) sort($result[$key]);
        }
        ksort($result);
        $this->keys = array_keys($result);
        $this->parsed = $result;

        return true;
    }

    public function deleteItems($ids)
    {
        $result = Api::resource($this->api_item)->delete(['ids' => $ids]);
        return $result->items;
    }

    public function moveItems($items, $dest)
    {
        $result = [];
        foreach ($items as $item) {
            $result[$item] = $this->updateItem(
                $item,
                $dest);
        }
        $result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
        return $result;
    }

    public function updateItem($id, $dest = '')
    {
        $_arr = [
            'id' => $id,
            ];
        if ($dest) {
            $_arr['list_id'] = $dest;
        }
        $result = Api::resource($this->api_item)->put($_arr);
        return (isset($result->error)) ? false : true;
    }
    
    public function addItems($items)
    {
        $_array = [];
        foreach ($items as $item) {
            $_array[] = $this->addItem($item);
        }
        $result = Api::resource($this->api_item)->post(['_array' => $_array]);
        return $result->items;
    }

    public function addItem($item)
    {
        return [
            'list_id' => $this->list_id,
            'type' => $this->type,
            'item' => $item,
        ];
    }

}
