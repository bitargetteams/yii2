<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 15.12.14
 * Time: 12:05
 */

namespace app\modules\sections\models\jban\optimizer;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;

/**
 * Search form
 */
class SearchForm extends Model
{

	protected $api = 'jban/search';
	protected $topic_api = 'site_topic';
	protected $country_api = 'geo/country';
	protected $banner_api = 'jban/tmpl';
	protected $domain_zone_api = 'jban/domain_zone';
	protected $settings_api = 'jban/search_settings';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $platform_location_id;

	public $client_location_id;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'project_title'], 'required'],
			[['client_location_id', 'unset_on_site_error', 'unset_on_tic_down', 'unset_on_pr_down'], 'integer'],
			[['unset_on_hits_down', 'unset_on_price_up'], 'number'],
			[['project_description'], 'string'],
			[['project_title', 'project_url'], 'string', 'max' => 100]
		];
	}

	public function attributeLabels()
	{
		return [
			'client_location_id' => Yii::t('jban', 'Client Location ID'),
			'project_title' => Yii::t('jban', 'Project Title'),
			'project_url' => Yii::t('jban', 'Project Url'),
			'unset_on_site_error' => Yii::t('jban', 'Unset On Site Error'),
			'unset_on_hits_down' => Yii::t('jban', 'Unset On Hits Down'),
			'unset_on_tic_down' => Yii::t('jban', 'Unset On Tic Down'),
			'unset_on_pr_down' => Yii::t('jban', 'Unset On Pr Down'),
			'unset_on_price_up' => Yii::t('jban', 'Unset On Price Up'),
			'project_description' => Yii::t('jban', 'Project Description'),
		];
	}

	/**
	 * Save Jban_project data.
	 *
	 * @return Jban_website|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$jban_project = [];

			$jban_project['client_location_id'] = $this->client_location_id;
			$jban_project['project_title'] = $this->project_title;
			$jban_project['project_url'] = $this->project_url;
			$jban_project['unset_on_site_error'] = $this->unset_on_site_error;
			$jban_project['unset_on_hits_down'] = $this->unset_on_hits_down;
			$jban_project['unset_on_tic_down'] = $this->unset_on_tic_down;
			$jban_project['unset_on_pr_down'] = $this->unset_on_pr_down;
			$jban_project['unset_on_price_up'] = $this->unset_on_price_up;
			$jban_project['project_description'] = $this->project_description;

			$jban_project = Api::resource($this->api)->post($jban_project);

			if (isset($jban_project->response->id) && is_numeric($jban_project->response->id) && $jban_project->response->id > 0) {
				return $jban_project->response->id;
			}
		}

		return null;
	}


	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getAllItems($params = [])
	{

		$this->power = 0;

		$params['client_location_id'] = $this->client_location_id;
		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	/**
	 * @param array $items
	 * @return array
	 *
	 */
	public function prepareData($items= [])
	{
		$projects = [];
		if (count($items)) {
			foreach ($items as $item) {
				$projects[] = $item;
			}

		}
		return $projects;
	}

	/**
	 * Return power
	 * @return int
	 */
	public function getPower()
	{
		return $this->power;
	}

	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}

	public function getTopics()
	{
		$res = Api::resource($this->topic_api)->get();
		$topics = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $topics[$item->id] = $item;
		}
		return	$topics;
	}

	public function getCountries()
	{
		$res = Api::resource($this->country_api)->get();
		$countries = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $countries[$item->id] = $item;
		}
		return	$countries;
	}

	public function getBanners()
	{
		$res = Api::resource($this->banner_api)->get();
		$banners = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $banners[$item->id] = $item;
		}
		return	$banners;
	}

	public function getDomainZones()
	{
		return  ['ru'=>'ru','com'=>'com'];

		$res = Api::resource($this->domain_zone_api)->get();
		$domain_zones = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $domain_zones[$item->id] = $item;
		}
		return	$domain_zones;
	}

	public function getWebsiteLimitFlds()
	{
		return [
			'10' => '10',
			'20' => '20',
			'50' => '50',
			'100' => '100',
			'150' => '150',
		];
	}

	public function getPageLimitFlds()
	{
		return [
			'' => 'All',
			'3' => '3',
			'6' => '6',
			'9' => '9',
			'12' => '12',
			'15' => '15',
		];
	}

	/**
	 * @param array $data
	 * @return bool
	 */
	public function saveSettings($data = [])
	{
		$data['client_location_id'] = $this->client_location_id;

		$res = Api::resource($this->settings_api)->post($data);

		if (isset($res->response) && $res->response->is_array() && count($res->response)) {

			return $res->response[0]->id;
		}

		return 0;
	}

	public function updateSettings($data = [])
	{
		$data['client_location_id'] = $this->client_location_id;

		$res = Api::resource($this->settings_api)->put($data);

		if (isset($res->response) && $res->response->is_array() && count($res->response)) {

			return $res->response[0]->id;
		}

		return 0;
	}


	public function getSettings($data = [])
	{
		$settings = [];

		$res = Api::resource($this->settings_api)->get(array_merge($data, ['client_location_id'=>$this->client_location_id]));

		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $settings[$item->id] = $item;
		}
		return	$settings;
	}

	public function deleteSettings($id)
	{
		$res = Api::resource($this->settings_api . '/' . $id)->delete(['id' => $id]);

		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			return true;
		}

		return false;
	}

}
