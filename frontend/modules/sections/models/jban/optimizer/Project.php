<?php

namespace app\modules\sections\models\jban\optimizer;

use yii\base\Model;
use Yii;
use app\components\Api;
use yii\helpers\ArrayHelper;

/**
 * Project form
 */
class Project extends Model
{

	protected $api = 'jban/project';

	protected $power = 0;
	protected $error = false;
	protected $errors = [];

	public $location_id;
	public $platform_location_id;

	public $client_location_id;

	public $project_title;

	public $project_url;

	public $unset_on_site_error = 7;

	public $unset_on_hits_down = 50;

	public $unset_on_tic_down = 0;

	public $unset_on_pr_down = 0;

	public $isNewRecord = true;

	public $unset_on_price_up = 0;

	public $project_description;


	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['client_location_id', 'project_title'], 'required'],
			[['client_location_id', 'unset_on_site_error', 'unset_on_tic_down', 'unset_on_pr_down'], 'integer'],
			[['unset_on_hits_down', 'unset_on_price_up'], 'number'],
			[['project_description'], 'string'],
			[['project_title', 'project_url'], 'string', 'max' => 100]
		];
	}

	public function attributeLabels()
	{
		return [
			'client_location_id' => Yii::t('jban', 'Client Location ID'),
			'project_title' => Yii::t('jban', 'Project Title'),
			'project_url' => Yii::t('jban', 'Project Url'),
			'unset_on_site_error' => Yii::t('jban', 'Unset On Site Error'),
			'unset_on_hits_down' => Yii::t('jban', 'Unset On Hits Down'),
			'unset_on_tic_down' => Yii::t('jban', 'Unset On Tic Down'),
			'unset_on_pr_down' => Yii::t('jban', 'Unset On Pr Down'),
			'unset_on_price_up' => Yii::t('jban', 'Unset On Price Up'),
			'project_description' => Yii::t('jban', 'Project Description'),
		];
	}

	/**
	 * Save Jban_project data.
	 *
	 * @return Jban_website|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$jban_project = [];

			$jban_project['client_location_id'] = $this->client_location_id;
			$jban_project['project_title'] = $this->project_title;
			$jban_project['project_url'] = $this->project_url;
			$jban_project['unset_on_site_error'] = $this->unset_on_site_error;
			$jban_project['unset_on_hits_down'] = $this->unset_on_hits_down;
			$jban_project['unset_on_tic_down'] = $this->unset_on_tic_down;
			$jban_project['unset_on_pr_down'] = $this->unset_on_pr_down;
			$jban_project['unset_on_price_up'] = $this->unset_on_price_up;
			$jban_project['project_description'] = $this->project_description;

			$jban_project = Api::resource($this->api)->post($jban_project);

			if (isset($jban_project->response->id) && is_numeric($jban_project->response->id) && $jban_project->response->id > 0) {
				return $jban_project->response->id;
			}
		}

		return null;
	}

	/**
	 * Update Jban_project data.
	 *
	 * @return Jban_website|null the saved model or null if saving fails
	 */
	public function update()
	{

		if ($this->validate()) {
			$jban_project = [];


			$jban_project['location_id'] = $this->location_id;
			$jban_project['client_location_id'] = $this->client_location_id;
			$jban_project['project_title'] = $this->project_title;
			$jban_project['project_url'] = $this->project_url;
			$jban_project['unset_on_site_error'] = $this->unset_on_site_error;
			$jban_project['unset_on_hits_down'] = $this->unset_on_hits_down;
			$jban_project['unset_on_tic_down'] = $this->unset_on_tic_down;
			$jban_project['unset_on_pr_down'] = $this->unset_on_pr_down;
			$jban_project['unset_on_price_up'] = $this->unset_on_price_up;
			$jban_project['project_description'] = $this->project_description;

			$jban_project = Api::resource($this->api)->put($jban_project);

			if (isset($jban_project->response[0]->location_id) && is_numeric($jban_project->response[0]->location_id) && $jban_project->response[0]->location_id > 0) {
				return $jban_project->response[0]->location_id;
			}

			$this->error = true;
			$this->errors[] = $jban_project->error->message;
		}

		return null;
	}

	/**
	 * @param array $params
	 * @return array|bool
	 */
	public function getAllItems($params = [])
	{

		$this->power = 0;

		$params['client_location_id'] = $this->client_location_id;
		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	/**
	 * @param array $items
	 * @return array
	 *
	 */
	public function prepareData($items= [])
	{
		$projects = [];
		if (count($items)) {
			foreach ($items as $item) {
				$projects[] = $item;
			}

		}
		return $projects;
	}

	/**
	 * Return power
	 * @return int
	 */
	public function getPower()
	{
		return $this->power;
	}

	/**
	 * Return errors
	 * @return []
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Return error
	 * @return bool
	 */
	public function isError()
	{
		return $this->error;
	}


	/**
	 * @param $id
	 * @return $this|bool
	 */
	public function findByID($id)
	{
		$result = Api::resource($this->api)->get([
			'location_id'=>$id,
			'client_location_id' => $this->client_location_id
		]);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else if ($result->power == 1) {
			$this->isNewRecord = false;
			foreach ($result->items[0] as $k=>$v) {
				$this->$k = $v;
			}
		}

		return $this;
	}
}
