<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 18.11.14
 * Time: 19:21
 */

namespace app\modules\sections\models\jban\webmaster;

use app\components\Api;
use app\modules\sections\components\FormModel;
use app\modules\sections\models\LangCode;
use yii\web\HttpException;


/**
 * Class InstallSitesForm
 *
 * @package app\modules\sections\models\jban\webmaster
 */
class InstallSitesForm extends FormModel
{
    /**
     * @var
     */
    public $site;
    /**
     * @var
     */
    public $topic_id;
    /**
     * @var
     */
    public $prg_lg_id;
    /**
     * @var string
     */
    public $path = '/';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['site', 'topic_id', 'prg_lg_id', 'path'], 'required'],
            ['site', 'url'],
            ['path', 'default', 'value' => '/install.php'],
            ['path', 'match', 'pattern' => '/^\/(?:[a-zа-я0-9\.\/])*?[a-z0-9\=\&\?]*?$/i'],
            ['path', 'existsScript'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'site' => \Yii::t('jban', 'Site'),
            'topic_id' => \Yii::t('jban', 'Category'),
            'prg_lg_id' => \Yii::t('jban', 'Dev Lang'),
            'path' => \Yii::t('jban', 'Path to script'),

        ];
    }

    /**
     * @param $attribute
     *
     * @throws HttpException
     */
    public function existsScript($attribute)
    {
        if ($this->hasErrors()) {
            return;
        }
        $langCode = LangCode::model($this->prg_lg_id)[0]->title;
        $url = $this->site . $this->path . "install.{$langCode}?" . time();
        $this->preparePort();
        $site_response = @file_get_contents($url);
        $site_headers = $http_response_header;
        if ($site_headers[0] !== 'HTTP/1.1 200 OK') {
            $this->addError($attribute, \Yii::t('jban', 'Install script not found'));
            return;
        } elseif (trim($site_response) !== '6cd2422b04ead2ea9c5537260cb628ec59d48cbc2a0c1356b410cf2adac6be3d') {
            $this->addError($attribute, \Yii::t('site_answer', $site_response));
        }
    }

    /**
     * @return bool
     * @throws HttpException
     * TODO когда-нибудь все изменится..
     */
    protected function preparePort()
    {
        $domain = (substr($this->site, strpos($this->site, '//') + 2));
        $ip = gethostbyname($domain);
        $param = [
            "site" =>$domain,
            "ip"    => $ip,
            'path' => $this->path,
            'topic_id' => $this->topic_id,
            'client_location_id' => 13
        ];
        $response = Api::resource('section_script/get-hashes')->post($param);
        if (isset($response->response)) {
            $response = $response->response;
            \Yii::$app->cache->set($domain . $ip, $response->hash, 600);
            \Yii::$app->cache->set($response->hash, $this->prg_lg_id, 600);
        } elseif (isset($response->errors)) {
            throw new HttpException($response->errors->status, $response->errors->message);
        }
        return true;


    }

}