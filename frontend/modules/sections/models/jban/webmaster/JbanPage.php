<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 24.11.14
 * Time: 12:23
 */


namespace app\modules\sections\models\jban\webmaster;

use Yii;
use yii\base\Model;
use app\components\Api;

/**
 * This is the model class for table "jban_page".
 *
 * @property integer $id
 * @property integer $website_id
 * @property string $page_url
 * @property integer $level
 * @property integer $free_place_cnt
 * @property string $status
 * @property string $status_descr
 * @property integer $rate_pr
 * @property string $page_added_time
 * @property string $last_scan_time
 *
 * @property Jban_banner[] $jbanBanners
 * @property Jban_website $website
 * @property Jban_position[] $jbanPositions
 */

class JbanPage extends Model
{

	protected $website_id;
	protected $api = 'jban/page';
	protected $type;
	protected $messages = [];
	protected $power = 0;
	protected $error = false;

	public $ids = [];
	public $status;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['website_id', 'page_url', 'ids'], 'required'],
			[['website_id', 'level', 'free_place_cnt', 'rate_pr'], 'integer'],
			[['status', 'status_descr'], 'string'],
			[['page_added_time', 'last_scan_time'], 'safe'],
			[['page_url'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ids' => '',
			'website_id' => 'Website ID',
			'page_url' => 'Page Url',
			'level' => 'Level',
			'free_place_cnt' => 'Free Place Cnt',
			'all_place_cnt' => 'All Place Cnt',
			'status' => 'Status',
			'status_descr' => 'Status Descr',
			'rate_pr' => 'Rate Pr',
			'page_added_time' => 'Page Added Time',
			'last_scan_time' => 'Last Scan Time',
		];
	}

	public function __construct($website_id)
	{
		$this->website_id = $website_id;

		parent::__construct();

	}
	public function getAllItems($params = [])
	{
		$params['website_id'] = $this->website_id;

		$this->power = 0;

		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $result->items;
		}
	}

	public function updateItems($items)
	{
		$result = [];
		foreach ($items as $item) {
			$result[$item['id']] = $this->updateItem(
				$item['id'],
				$item[$this->type]);
		}
		$result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
		return $result;
	}

	public function updateItem($id, $item)
	{
		$result = Api::resource($this->api)->put([
			'id' => $id,
			'website_id' => $this->website_id,
			$this->type => $item,
		]);
		return (isset($result->error)) ? false : true;
	}

	public function updateStatus()
	{
		$result = [];
		$this->type = 'status';
		foreach ($this->ids as $id) {
			if ($id > 0) {
				$result[$id] = $this->updateItem($id, $this->status);
			}
		}
		$result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
		$this->ids= [];
		return $result;
	}

	public function addMessage($message = '')
	{
		$this->messages[] = $message;
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function getWebsiteID()
	{
		return $this->website_id;
	}

	public function getPower()
	{
		return $this->power;
	}


	/**
	 * @param array $params
	 * @param array $columns
	 * @param array $order
	 * @param $draw
	 *
	 * @return array()
	 */
	public function getTableData($params = [], $columns = [], $order= [], $draw = 1)
	{
		foreach ($columns as $col) {
			if (strlen($col['search']['value'])) $params[$col['data']] = $col['search']['value'];
		}
		foreach ($order as $o) {
			$params['order'][$columns[$o['column']]['data']] = $o['dir'];
		}

		$data = [];
		foreach($this->getAllItems($params) as $k=>$item) {
			$data[] = [
				'DT_RowId'=>'row_' . $k,
				'id'=>$item->id,
				'page_url'=>$item->page_url,
				'level'=>$item->level,
				'rate_pr'=>$item->rate_pr,
				'free_place_cnt'=>$item->free_place_cnt,
				'all_place_cnt'=>$item->free_place_cnt,
				'status'=>$item->status,
				'banner'=>'banner 1<br>baner 2',
				'price'=>'price 1<br>price 2',
				'ids'=>$item->id
			];
		}

		return [
			'data' => $data,
			'draw' => $draw,
			'recordsFiltered' => count($data),
			'recordsTotal' => $this->getPower()
		];
	}
}
