<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 18.11.14
 * Time: 16:47
 */

namespace app\modules\soap\models;


use app\components\Api;
use yii\base\Model;

class DomainHash extends Model {
    public $domain;
    public $ip;
    public $hash;
    private static $resource = 'install_script';

    protected static function prepareParams($params)
    {
        $result = [];
        foreach($params as $param)
        {
            $result["sites[{$param['domain']}]"] = gethostbyname($param['domain']);
        }
        return $result;
    }

    public static  function getHashLinks($params)
    {
        $params = self::prepareParams($params);
        $model = Api::resource(self::$resource)->post($params);
        if (isset($model->response)) {
            $collection = [];
            foreach($model->response as $domain => $hash) {
                $temp = new self;
                $temp->domain = $domain;
                $temp->ip = null;
                $temp->hash = $hash;
                    $collection[] = $temp;
            }
        }elseif($model->validateError){
            $job = new self;
            $job->errors = $model->error->message;
            return $job;
        }else {
            throw new \HttpException($model->error->status,$model->error->message);
        }
        return $collection;

    }
} 