<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 02.12.14
 * Time: 16:23
 */


namespace app\modules\sections\models\jban\webmaster;

use Yii;
use yii\base\Model;
use app\components\Api;


class JbanPrice extends Model
{

	protected $website_id;
	protected $api = 'jban/price';
	protected $type;
	protected $messages = [];
	protected $power = 0;
	protected $error;

	public $ids = [];
	public $status;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['website_id', 'ids'], 'required'],
			[['website_id'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'ids' => '',
			'website_id' => 'Website ID',
		];
	}

	public function __construct($config)
	{
                if(isset($config['website_id'])) { 
                    $this->website_id = $config['website_id'];
                    unset($config['website_id']);
                }
		parent::__construct($config);

	}
	public function getItems($params = [])
	{
		$params['website_id'] = $this->website_id;

		$this->power = 0;

		$result = Api::resource($this->api)->get($params);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $result->items;
		}
	}

	public function updateItems($items)
	{
		$result = [];
		foreach ($items as $item) {
			$result[$item['id']] = $this->updateItem(
				$item['id'],
				$item[$this->type]);
		}
		$result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
		return $result;
	}

	public function updateItem($id, $item)
	{
		$result = Api::resource($this->api)->put([
			'id' => $id,
			'website_id' => $this->website_id,
			$this->type => $item,
		]);
		return (isset($result->error)) ? false : true;
	}

	public function updatePrices()
	{
		$result = [];
		$this->type = 'price';
		foreach ($this->ids as $id) {
			if ($id > 0) {
				$result[$id] = $this->updateItem($id, $this->price);
			}
		}
		$result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
		$this->ids= [];
		return $result;
	}

	public function addMessage($message = '')
	{
		$this->messages[] = $message;
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function getWebsiteID()
	{
		return $this->website_id;
	}

	public function getPower()
	{
		return $this->power;
	}

	public function getLevelTitles()
	{
		return [
			'1' => 'Main',
			'2' => 'Level 2',
			'3' => 'Level 3',
			'4' => 'Level 4'
		];
	}
        
    public function getPrices()
    {
        $result = Api::resource('jban/website/getprice')->get(['website_id' => $this->website_id]);
        if (isset($result->error)) {
            $this->error = true;
            return false;
        } else {
            $this->power = $result->power;
            return $result->items;
        }
    }
    
    public function setPrices($data)
    {
        $array = [];
        foreach($data as $tmpl => $levels){
            foreach($levels as $level => $price){
                $array[] = [
                    'website_location_id' => $this->website_id, 
                    'tmpl_id' => $tmpl,
                    'level' => $level,
                    'price' => $price,
                ];
            }
        }
        $result = Api::resource('jban/website/setprice')->put($array);
        if (isset($result->error)) {
            $this->error = true;
            return false;
        } else {
            return true;
        }
    }

}
