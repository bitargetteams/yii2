<?php

namespace app\modules\sections\models\jban\webmaster;

use yii\base\Model;
use Yii;
use app\components\Api;

/**
 * CreateWebsiteForm form
 */
class CreateWebsiteForm extends Model
{

	public $url;
	public $country_id;
	public $region_id;
	public $city_id;
	public $topic_id;
	public $folder_id;


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'url','country_id','region_id','city_id','topic_id'
				],
				'required'
			],
			[
				[
					'url'
				],
				'filter', 'filter' => 'trim'
			],
            [
                'url','url'
            ],
			[
				[
					'country_id', 'region_id', 'city_id', 'topic_id', 'folder_id'
				],
				'number'
			],

		];
	}

	public function attributeLabels()
	{
		return [
			'url' => \Yii::t('jban', 'Url'),
			'country_id' => \Yii::t('jban', 'Country'),
			'region_id' => \Yii::t('jban', 'Region'),
			'city_id' => \Yii::t('jban', 'City'),
			'topic_id' => \Yii::t('jban', 'Site topic'),
			'folder_id' => \Yii::t('jban', 'Client folder'),

		];
	}

	/**
	 * Save Jban_website data.
	 *
	 * @return Jban_website|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$jban_website = [];

			$jban_website['url'] = $this->url;
			$jban_website['country_id'] = $this->country_id;
			$jban_website['region_id'] = $this->region_id;
			$jban_website['city_id'] = $this->city_id;
			$jban_website['topic_id'] = $this->topic_id;
			$jban_website['folder_id'] = $this->folder_id;

			$jban_website = Api::resource('jban_website')->post($jban_website);

			if (isset($jban_website->response->id) && is_numeric($jban_website->response->id) && $jban_website->response->id > 0) {
				return $jban_website->response->id;
			}
		}

		return null;
	}

	public function getCountries()
	{
		$res = Api::resource('country')->get();
		$countries = ['0' => 'Выберите страну'];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $countries[$item->id] = $item->title;
		}
		return 	$countries;
	}

	public function getFolders()
	{
		$res = Api::resource('client_folders')->get();
		$folders = ['0' => 'Выберите папку'];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $folders[$item->id] = $item->name;
		}
		return 	$folders;
	}


}
