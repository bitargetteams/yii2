<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 20:15
 */

namespace app\modules\sections\models\jban\webmaster;


use yii\base\Model;

class PrgLg extends Model{
    public $id;
    public $title;
    public $href;

    public static function model()
    {
        $collection = [];

        $model =  [
            ['id'=>0,'title'=>'PHP','href'=>'http://192.168.1.1/install.php'],
            ['id'=>1,'title'=>'ASP','href'=>'http://192.168.1.1/install.php'],
            ['id'=>2,'title'=>'DOT.NET','href'=>'http://192.168.1.1/install.php'],
            ['id'=>3,'title'=>'Python','href'=>'http://192.168.1.1/install.php'],
            ['id'=>4,'title'=>'Ruby','href'=>'http://192.168.1.1/install.php'],
        ];

        foreach($model as $line)
        {
            $t = new self;
            $t->id = $line['id'];
            $t->title = $line['title'];
            $t->href = $line['href'];
            $collection[] = $t;
        }
        return $collection;
    }

    public function getAsArray()
    {
        return [
            'id' => $this->id,
            'title' =>  $this->title,
            'href'  =>  $this->href
        ];
    }

} 