<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 27.11.14
 * Time: 12:23
 */


namespace app\modules\sections\models\jban\webmaster;

use Yii;
use yii\base\Model;
use app\components\Api;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jban_website".
 *
 * @property integer $location_id
 * @property string $url
 * @property integer $topic_id
 * @property integer $rate_pr
 * @property integer $rate_tic
 * @property string $site_created_time
 * @property string $site_added_time
 * @property integer $folder_id
 * @property string $path
 */

class JbanWebsite extends Model
{

	protected $api = 'jban/website';
	protected $power = 0;
	protected $error = false;

	public $client_location_id;

	public function init()
	{
		$this->client_location_id = ArrayHelper::getValue($_COOKIE, 'client_location_id', 0);
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['location_id', 'url'], 'required'],
			[['location_id', 'topic_id', 'rate_pr', 'rate_tic'], 'integer'],
			[['site_created_time', 'site_added_time'], 'safe'],
			[['url'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'location_id' => 'ID',
			'url' => 'Site Url',
			'status' => 'Status',
			'rate_tic' => 'Rate Tic',
			'rate_pr' => 'Rate Pr',
			'position_cnt' => 'Position Cnt',
			'pages' => 'Pages',
		];
	}

	public function getAllItems($params = [])
	{

		$this->power = 0;

		$result = Api::resource($this->api)->get(
			array_merge(
				[
					'client_location_id' => $this->client_location_id
				],
				$params
			)
		);

		if (isset($result->error)) {
			$this->error = true;
			return false;
		} else {
			$this->power = $result->power;
			return $this->prepareData($result->items);
		}
	}

	public function prepareData($items= [])
	{
		$jban_websites = [];
		if (count($items)) {
			$jban_websites = $items;
			foreach ($jban_websites as $k=>$site) {
				$jban_websites[$k]->position_cnt = 0;

				$p_info = [];
				$b_info = ['1'=>[],'2'=>[],'3'=>[],'4'=>[]];
				if (isset($site->pages)) {
					foreach ($site->pages as $page) {

						if (!isset($p_info[$page->_id->level])) $p_info[$page->_id->level] = ['all'=>0,'free_places'=>0,'all_places'=>0];
						$p_info[$page->_id->level]['all']++;
						$p_info[$page->_id->level]['free_places'] += ($page->all - $page->bnr);
						$p_info[$page->_id->level]['all_places'] += $page->all;
						$jban_websites[$k]->position_cnt += count($page->all);

						/*
						foreach ($page->positions as $position) {
							foreach ($position->tmpls as $tmpl) {
								if (!isset($b_info[$page->level][$tmpl->width.' x '.$tmpl->height])) $b_info[$page->level][$tmpl->width.' x '.$tmpl->height] = ['all'=>0,'free'=>0];
								$b_info[$page->level][$tmpl->width.' x '.$tmpl->height]['all']++;
								if ($position->status == 'free') $b_info[$page->level][$tmpl->width.' x '.$tmpl->height]['free']++;
							}
						}
						*/
					}
				}

				ksort($p_info);
				$jban_websites[$k]->pages_stat = $p_info;
				$jban_websites[$k]->banners_stat = $b_info;
			}
		}
		return $jban_websites;
	}

	public function updateItems($items)
	{
		$result = [];
		foreach ($items as $item) {
			$result[$item['location_id']] = $this->updateItem(
				$item['location_id'],
				$item[$this->type]);
		}
		$result['status'] = (in_array(false, $result, true)) ? 'ERROR' : 'OK';
		return $result;
	}

	public function updateItem($id, $item)
	{
		$result = Api::resource($this->api)->put([
			'location_id' => $id,
			$this->type => $item,
		]);
		return (isset($result->error)) ? false : true;
	}

	public function addMessage($message = '')
	{
		$this->messages[] = $message;
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function getPower()
	{
		return $this->power;
	}

	/**
	 * @param $params
	 * @param $columns
	 * @param $order
	 * @param $draw
	 * @param $controller
	 * @return array
	 */
	public function getTableData($params = [], $columns = [], $order = [], $draw = 1, $controller = null)
	{
		foreach ($columns as $col) {
			if (strlen($col['search']['value'])) $params[$col['data']] = $col['search']['value'];
		}
		foreach ($order as $o) {
			if (!empty($columns[$o['column']]['data'])) {
				$params['order'][$columns[$o['column']]['data']] = $o['dir'];
			}
		}

		$data = [];

		if (($websites = $this->getAllItems($params)) && count($websites) > 0) {
			foreach($websites as $k=>$item) {
				$data[] = [
					'DT_RowId'=>'row_' . $k,
					'location_id'=>$item->location_id,
					'url'=>$item->url,
					'status'=>$item->status,
					'rate_pr'=>$item->rate_pr,
					'rate_tic'=>$item->rate_tic,
					'page_cnt'=>(isset($item->pages)) ? count($item->pages) : 0,
					'position_cnt'=>$item->position_cnt,
					'addInfo'=>$controller->renderPartial('/jban/webmaster/website/_add_info', ['website'=>$item])
				];
			}
		}

		return [
			'data' => $data,
			'draw' => $draw,
			'recordsFiltered' => count($data),
			'recordsTotal' => $this->getPower()
		];
	}
}
