<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Description' => '',
    'Password' => '',
    'Text identity' => '',
    'Title' => '',
    'Order audit' => '@@Заказ аудита@@',
    'Problem promotion' => '@@Проблеммы продвижения@@',
    'nothing found orders audit' => '@@не найдено заказов аудита@@',
    '#' => '№',
    'Address of the site on which an audit should be performed' => 'Адресс сайта, по которому должен быть выполнен аудит',
    'Competitors website' => 'Конкуренты сайта',
    'Created time' => 'Создан',
    'Describe in free form, the problems that you have encountered' => 'Опишите в свободной форме, проблемы с которыми вы столкнулись',
    'Goods and services' => 'Товары и услуги',
    'List all of your competitors, separated by commas. Suffice 5-10 most powerful, in your opinion, competitors' => 'Перечислите всех ваших конкурентов через запятую. Достаточно 5-10 самых сильных, на ваш взгляд, конкурентов',
    'List orders audit' => 'Список заказов аудита',
    'Order!' => 'Заказать!',
    'PDF FILE' => 'PDF файл',
    'Section describing the characteristics of the services provided' => 'Раздел, описывающий характеристики предоставляемых услуг',
    'Section describing the target audience and consumers of goods and services' => 'Раздел, описывающий целевую аудиторию и потребителей товаров и услуг',
    'Site URL' => 'URL сайта',
    'Status' => 'Статус',
    'Target Audience' => 'Целевая аудитория',
    'Total:' => 'Итого:',
    'Updated time' => 'Изменён',
    'Username' => 'Имя пользователя',
    'What you want to order an audit?' => 'Какой аудит вы желаете заказать?',
    'Works performed by the Audit' => 'Выполненные работы по аудиту',
    'Your name' => 'Ваше имя',
];
