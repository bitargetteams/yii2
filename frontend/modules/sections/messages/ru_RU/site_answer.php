<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 20.11.14
 * Time: 14:06
 */

return [
    "6cd2422b04ead2ea9c5537260cb628ec59d48cbc2a0c1356b410cf2adac6be3d" => "Установка успешно завершена",
    "74604f776df8bc865ff0512be8323fb791cb4cc0ac7fbb690c14bb8096226c77" => "Ошибка прав доступа",
    "3857d42b0649048676996a715eba0406d71af0eb346f8a13c2eeb095287ea5c7" => "Временная ошибка",
    "126561490cc19e5d9012185ef1df31096a754d5346de791384ecbdc6b015ffb1" => "Не верный ключ",
];