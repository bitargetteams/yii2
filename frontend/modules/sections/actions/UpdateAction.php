<?php

namespace app\modules\sections\actions;

use Yii;
use yii\base\Action;
use app\modules\sections\models\jban\JbanList;

/**
 * Description of DomainBLBehavior
 *
 * @author user
 */
class UpdateAction extends Action
{
    public $owner;
    public $type;
    public $model;

    public function run()
    {
        $model = new JbanList(10, $this->owner, $this->type, $this->model);
        if (null !== \Yii::$app->request->post('list_id')) {
            $model->list_id = (int)\Yii::$app->request->post('list_id');
        }
        if(null !== Yii::$app->request->post('delete')) {
            $toDelete = array_filter(Yii::$app->request->post('item'),
                function($var){ return is_numeric($var);});
            $model->deleteItems($toDelete);
        } elseif (null !== Yii::$app->request->post('move')
               && $model->list_id != (int)Yii::$app->request->post('destination')) {
            $toMove = array_filter(Yii::$app->request->post('item'),
                function($var){ return is_numeric($var);});
            $model->moveItems($toMove, (int)Yii::$app->request->post('destination'));
        } elseif ('' !== trim(\Yii::$app->request->post('addItems'))){
            $arr = explode("\n", \Yii::$app->request->post('addItems'));
            $arr = array_map(function($var){
                $var = mb_convert_case(trim($var), MB_CASE_LOWER, 'UTF-8');
                /* add additional data processing as it needed */

                return $var;
            }, $arr);
            $toAdd = array_filter($arr,
                function($var){
                /* add additional data filtering as it needed */

                return $var;});
            $model->addItems($toAdd);
        } elseif ('' !== trim(\Yii::$app->request->post('addList'))) {
            $model->addList(trim(\Yii::$app->request->post('addList')));
        }
        return $this->controller->redirect(\Yii::$app->request->referrer);
    }
}
