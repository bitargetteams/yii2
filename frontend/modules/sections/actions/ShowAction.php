<?php

namespace app\modules\sections\actions;

use yii\base\Action;
use app\modules\sections\models\jban\JbanList;

/**
 * Description of DomainBLBehavior
 *
 * @author user
 */
class ShowAction extends Action
{
    public $owner;
    public $type;
    public $model;

    public function run()
    {
        $model = new JbanList(10, $this->owner, $this->type, $this->model);
        if (\Yii::$app->request->post('list_id')) {
            $model->getCurrentList((int)\Yii::$app->request->post('list_id'));
        } elseif (is_numeric(\Yii::$app->session->get('list_id'))){
            $model->getCurrentList((int)\Yii::$app->session->get('list_id'));
        } else {
            $model->getDefaultList();
        }
        \Yii::$app->session->set('list_id', $model->list_id);
        $str = '@ABCDEFGHIJKLMNOPQRSTUVWXYZ語*АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ';
        $showMe = \Yii::$app->request->get('showMe');
        if (!$showMe || strpos($str, $showMe) === false) $showMe = '';
        $model->getAllItems();
        if (!isset($model->error)) {
            return $this->controller->render('/jban/common/common', [
                    'items' => $model->getItems($showMe),
                    'keys' => $model->keys,
                    'list_id' => $model->list_id,
                    'dropDownList' => $model->getDropDownList(),
                    'show' => 'show'.$this->type.$this->model.'list',
                    'update' => 'update'.$this->type.$this->model.'list',
                                             ]);
        } else {

        }
    }

}
