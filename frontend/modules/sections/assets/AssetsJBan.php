<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 07.11.14
 * Time: 14:23
 */

namespace app\modules\sections\assets;


use yii\web\AssetBundle;

class AssetsJBan extends AssetBundle {
    public $sourcePath = '@sections/includes';
    public $jsOptions = [];
    public $css = [
        'css/j-ban.css',
        'css/j-ban-webmaster.css',
        'css/j-ban-optimizer.css'
    ];
    public $js = [
        'scripts/js/j-ban.js',
    ];
    public $depends = [
    ];
}
