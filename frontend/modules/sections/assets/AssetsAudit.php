<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 07.11.14
 * Time: 14:23
 */

namespace app\modules\sections\assets;


use yii\web\AssetBundle;
use yii\web\View;

class AssetsAudit extends AssetBundle {
    public $sourcePath = '@sections/includes';
    public $jsOptions = [];
    public $css = [
        'css/audit.css'
    ];
    public $js = [
        'scripts/js/audit.js',
    ];
    public $depends = [
    ];
} 