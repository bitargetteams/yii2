<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 26.12.14
 * Time: 15:32
 */

namespace app\modules\sections\assets\jban;

use yii\web\View;
use yii\web\AssetBundle;

class WebsiteAssets extends AssetBundle{
	public $sourcePath = '@sections/includes';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [
		'css/jban/default.css'
	];
	public $js = [
		'scripts/js/jban/website_list.js'
	];
	public $depends = [];
}