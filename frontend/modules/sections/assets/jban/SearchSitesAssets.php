<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 15.12.14
 * Time: 18:28
 */

namespace app\modules\sections\assets\jban;

use yii\web\View;
use yii\web\AssetBundle;

class SearchSitesAssets extends AssetBundle{
	public $sourcePath = '@sections/includes';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [
		'css/jban/select2.css',
		'css/jban/search.css'
	];
	public $js = [
		'scripts/js/jban/select2.js',
		'scripts/js/jban/select2_locale_ru.js',
		'scripts/js/jban/search.js'
	];
	public $depends = [];
}