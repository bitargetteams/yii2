<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 22.12.14
 * Time: 11:23
 */

namespace app\modules\sections\assets\social;

use yii\web\View;
use yii\web\AssetBundle;

class ListPostAssets extends AssetBundle{
	public $sourcePath = '@sections/includes';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [
		'css/social/post_list.css'
	];
	public $js = [
		'scripts/js/social/post_list.js'
	];
	public $depends = [];
}