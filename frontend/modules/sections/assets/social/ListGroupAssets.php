<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 26.12.14
 * Time: 11:50
 */

namespace app\modules\sections\assets\social;

use yii\web\View;
use yii\web\AssetBundle;

class ListGroupAssets extends AssetBundle{
	public $sourcePath = '@sections/includes';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [
		'css/social/post_list.css'
	];
	public $js = [
		'scripts/js/social/group_list.js'
	];
	public $depends = [];
}