<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 19.12.14
 * Time: 12:25
 */

namespace app\modules\sections\assets\social;

use yii\web\View;
use yii\web\AssetBundle;

class AddPostAssets extends AssetBundle{
	public $sourcePath = '@sections/includes';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [

	];
	public $js = [
		'scripts/js/social/add_post.js'
	];
	public $depends = [];
}