<?php

namespace app\modules\sections\assets\social;

use yii\web\View;
use yii\web\AssetBundle;

class SearchGroupAssets extends AssetBundle{
public $sourcePath = '@sections/includes';
public $jsOptions = ['position' => View::POS_END];
public $css = [
'css/social/post_list.css'
];
public $js = [
'scripts/js/social/group_search.js'
];
public $depends = [];
}

?>