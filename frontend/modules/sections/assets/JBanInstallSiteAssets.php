<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 26.11.14
 * Time: 16:02
 */

namespace app\modules\sections\assets;

use yii\web\View;
use yii\web\AssetBundle;

class JBanInstallSiteAssets extends AssetBundle{
    public $sourcePath = '@sections/includes';
    public $jsOptions = ['position' => View::POS_END];
    public $css = [
        'css/sections.css'
    ];
    public $js = [
        'scripts/js/j-ban.add.site.js'
    ];
    public $depends = [];
} 