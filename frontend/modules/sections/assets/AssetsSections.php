<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 07.11.14
 * Time: 14:22
 */
namespace app\modules\sections\assets;
use yii\web\View;
use yii\web\AssetBundle;

class AssetsSections extends AssetBundle {
    public $sourcePath = '@sections/includes/';
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [
        'css/sections.css'
    ];
    public $js = [];
    public $depends = [];
} 