<?php

/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 14.11.14
 * Time: 20:19
 */

namespace app\modules\sections\controllers;

use app\modules\sections\models\audit\ListAudits;
use app\modules\sections\models\jban\webmaster\PrgLg;
use app\modules\sections\models\LangCode;
use Yii;
use app\modules\sections\models\jban\webmaster\InstallSitesForm;
use app\modules\sections\models\jban\webmaster\ListSites;
use yii\web\Controller;
use app\components\Api;
use app\modules\sections\models\jban\webmaster\CreateWebsiteForm;
use app\modules\sections\models\jban\webmaster\JbanPage;
use app\modules\sections\models\jban\webmaster\JbanWebsite;
use app\modules\sections\models\jban\webmaster\JbanPrice;
use yii\helpers\Json;

class JbanWebmasterController extends Controller
{

    public $layout = 'jban/webmaster';

    public function beforeAction($action)
    {
        Yii::$app->session->set('section', 'jban');

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        return $this->render('/jban/webmaster/index');
    }

    public function actionInstallsite()
    {
        return $this->render('/jban/webmaster/installsite/index');
    }

    public function actionListinstallsite()
    {
        $res         = Api::resource('site_topic')->get();
        $site_topics = [];
        if (isset($res->response) && $res->response->is_array() && count($res->response)) {
            foreach ($res->response as $item) {
                $site_topics[$item->id] = $item->title;
            }
        }

        $prg_lg = [];
        foreach (LangCode::model() as $line) {
            $prg_lg[$line->id] = $line->title;
        }

        $params = \Yii::$app->request->getBodyParam("InstallSitesForm");
        $links  = ListSites::model();
        $form   = new InstallSitesForm();
        if ($params) {
            $form->load(\Yii::$app->request->getBodyParams());
            $form->validate();
        }
        return $this->renderPartial('/jban/webmaster/installsite/items_list/items.php', [
                'links'  => $links,
                'model'  => $form,
                'topics' => $site_topics,
                'prg_lg' => $prg_lg
        ]);
    }

    /**
     * Возвращает страницу списка Jban Website
     *
     *
     * @return html
     */
    public function actionListprojects()
    {
        $model = new JbanWebsite();

        return $this->render('/jban/webmaster/list-projects', ['model' => $model]);
    }

    /**
     * Возвращает набор данных Jban Website для DataTable
     *
     *
     * @return json
     */
    public function actionListwebsitedata()
    {
        $model = new JbanWebsite();

        $res = $model->getTableData(
            \Yii::$app->request->getBodyParams(), (isset($_GET['columns'])) ? $_GET['columns'] : [], (isset($_GET['order'])) ? $_GET['order'] : ['location_id' => 'asc'], $_GET['draw'], $this
        );

        return Json::encode($res);
    }

    /**
     * Возвращает страницу списка Jban Pages
     *
     *
     * @return html
     */
    public function actionListpages($id)
    {
        $model = new JbanPage($id);

        if ($model->load(Yii::$app->request->post()) && $model->updateStatus()) {
            $model->addMessage('Good');
        }

        return $this->render('/jban/webmaster/list-pages', ['model' => $model, 'items' => $model->getAllItems()]);
    }

    /**
     * Возвращает набор данных Jban Pages для DataTable
     *
     *
     * @return json
     */
    public function actionListpagesdata($id)
    {
        $model = new JbanPage($id);

        $res = $model->getTableData(
            \Yii::$app->request->getBodyParams(), (isset($_GET['columns'])) ? $_GET['columns'] : [], (isset($_GET['order'])) ? $_GET['order'] : [], $_GET['draw']
        );

        return Json::encode($res);
    }

    public function actionGetprice($id)
    {
        $model = new JbanPrice(['website_id' => $id]);

//        $dataSet = [
//            [
//                ['tmpl_id' => 1, 'width' => 250, 'height' => 250],
//                ['tmpl_id' => 1, 'level' => 1, 'min' => 10.00, 'avg' => 25.33, 'user' => 25.00],
//                ['tmpl_id' => 1, 'level' => 2, 'min' => 10.00, 'avg' => 20.33, 'user' => 20.00],
//                ['tmpl_id' => 1, 'level' => 3, 'min' => 10.00, 'avg' => 15.33, 'user' => 15.00],
//                ['tmpl_id' => 1, 'level' => 4, 'min' => 10.00, 'avg' => 10.33, 'user' => 10.88]
//            ]
//        ];

        /*
         * draw, recordsTotal & recordsFiltered -- dataTable obligatory parameters
         */
        return Json::encode(['draw' => (int) $_GET['draw'],
                             'data' => $model->getPrices(),
                             'recordsTotal' => $model->getPower(), 
                             'recordsFiltered' => $model->getPower(), 
                             ]);
    }

    /**
     * Возвращает страницу установки цен
     *
     *
     * @return html
     */
    public function actionSetprice($id)
    {
        $model = new JbanPrice(['website_id' => (int)$id]);
        
        if (null !== Yii::$app->request->post('JbanPrice')) {
            $model->setPrices(Yii::$app->request->post('JbanPrice'));
            //echo '<pre>', print_r(Yii::$app->request->post('JbanPrice'), 1);die;
        }

        if ($model->load(Yii::$app->request->post()) && $model->updatePrices()) {
            $model->addMessage('Good');
        }

        return $this->render('/jban/webmaster/set-price', ['model' => $model, 'items' => $model->getItems()]);
    }

    public function actionStatprojects()
    {
        return $this->render('/jban/webmaster/stat-projects');
    }

    public function actions()
    {
        return [
            'showdomainblacklist'   => [
                'class' => 'app\modules\sections\actions\ShowAction',
                'owner' => 'webmaster',
                'type'  => 'domain',
                'model' => 'black',
            ],
            'updatedomainblacklist' => [
                'class' => 'app\modules\sections\actions\UpdateAction',
                'owner' => 'webmaster',
                'type'  => 'domain',
                'model' => 'black',
            ],
            'showwordblacklist'     => [
                'class' => 'app\modules\sections\actions\ShowAction',
                'owner' => 'webmaster',
                'type'  => 'word',
                'model' => 'black',
            ],
            'updatewordblacklist'   => [
                'class' => 'app\modules\sections\actions\UpdateAction',
                'owner' => 'webmaster',
                'type'  => 'word',
                'model' => 'black',
            ],
        ];
    }

}
