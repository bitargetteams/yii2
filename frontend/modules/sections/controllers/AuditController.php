<?php

namespace app\modules\sections\controllers;

use app\components\Api;
use app\modules\sections\models\audit\CreateForm;
use app\modules\sections\models\audit\JobAudit;
use app\modules\sections\models\audit\ListAudits;
use app\modules\sections\models\audit\PartitionsAudits;
use app\modules\sections\models\audit\ProcessAudit;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\HttpException;

class AuditController extends Controller
{
    public function behaviors()
    {
        return [
            'isAjax' => [
                'class' => 'frontend\behaviors\AjaxBehavior',
                'actionsUsed' => ['process', 'list']
            ],
        ];
    }

    public function actionIndex()
    {
        $form = new CreateForm();
        $pagination = \Yii::$app->request->getBodyParams()
            ? \Yii::$app->request->getBodyParams()
            : [
                'per-page' => 5, 'page' => 1
            ];
        $listModel = ListAudits::model($pagination);
        $audits = PartitionsAudits::model();
        if (\Yii::$app->request->getBodyParams()) {
            $form->load(\Yii::$app->request->getBodyParams());
            if ($form->validate()) {
                $queryParams = $form->getAttributes();
                $queryParams['usedAudit'] = $queryParams['useAudit'];
                unset($queryParams['useAudit']);
                $res = Api::resource('audit')->post($queryParams);
                if (!isset($res->error)) {
                    $form = new CreateForm();
                } elseif ($res->validateError) {
                    $job = new CreateForm();
                    $job->errors = $res->error->message;
                    return $job;
                } else {
                    throw new HttpException($res->error->status, $res->error->message);
                }
            }
        }
        return $this->render(
            'index', [
                'model' => $form, 'audits' => $audits,
                'listModel' => $listModel,
                'processModel' => (new ProcessAudit())->getAttributes()
            ]
        );

    }

    public function actionList()
    {
        $params = \Yii::$app->request->getBodyParams();
        return $this->renderPartial('items', ['model' => ListAudits::model($params)]);
    }

    public function actionJobs()
    {
        $params = \Yii::$app->request->getBodyParams();
        return $this->renderPartial('job', ['model' => JobAudit::model($params['id'], $params)]);
    }

    public function actionProcess()
    {
        if (!$id = \Yii::$app->request->getBodyParam('audit')) {
            throw new InvalidParamException('Missing require param {audit}');
        }
        $model = ProcessAudit::model($id);
        return $this->renderPartial('process', ['model' => $model]);
    }
}
