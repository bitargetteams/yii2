<?php

/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 14.11.14
 * Time: 20:19
 */

namespace app\modules\sections\controllers;

use Yii;
use yii\web\Controller;
use app\modules\sections\models\jban\optimizer\Project;
use yii\helpers\Json;
use app\modules\sections\models\jban\optimizer\SearchForm;
use \app\components\Api;

class JbanOptimizerController extends Controller
{

    public $layout = 'jban/optimizer';

    public function actionIndex()
    {
        return $this->render('/jban/optimizer/index');
    }

    /**
     * Get Project list
     *
     * @return string
     */
    public function actionProjects()
    {
        $model = new Project();

        return $this->render('/jban/optimizer/projects', ['model' => $model]);
    }

    /**
     * Возвращает набор данных Jban Project для DataTable
     *
     *
     * @return json
     */
    public function actionListprojectdata()
    {
        $model = new Project();
        $params = \Yii::$app->request->getBodyParams();
        $columns = (isset($_GET['columns'])) ? $_GET['columns'] : [];
        $order = (isset($_GET['order'])) ? $_GET['order'] : ['location_id' => 'asc'];
        foreach ($columns as $col) {
            if (strlen($col['search']['value']))
                $params[$col['data']] = $col['search']['value'];
        }
        foreach ($order as $o) {
            if (!empty($columns[$o['column']]['data'])) {
                $params['order'][$columns[$o['column']]['data']] = $o['dir'];
            }
        }

        $data = [];
        foreach ($model->getAllItems($params) as $k => $item) {
            $data[] = [
                'project_title' => $item->project_title,
                'project_url' => $item->project_url,
                'location_id' => $item->location_id,
            ];
        }
        $res = [
            'data' => $data,
            'draw' => $_GET['draw'],
            'recordsFiltered' => count($data),
            'recordsTotal' => $model->getPower()
        ];
        return Json::encode($res);
    }

    /**
     * Add new Project
     *
     * @return string|\yii\web\Response
     */
    public function actionAddproject()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['projects']);
        } else {
            return $this->render('/jban/optimizer/add_project', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Update Project
     *
     * @return string|\yii\web\Response
     */
    public function actionUpdateproject($id)
    {
        $model = new Project();
        $model->findByID($id);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['projects']);
        } else {
            return $this->render('/jban/optimizer/add_project', [
                        'model' => $model,
            ]);
        }
    }

    public function actionSearch()
    {
        $model = new SearchForm();

        return $this->render('/jban/optimizer/search', [
                    'model' => $model,
                    'settings' => []
        ]);
    }

    public function actionSavesettings()
    {
        $model = new SearchForm();
        $data = Yii::$app->request->post();

        return $model->saveSettings(['title' => $data['title'], 'settings' => serialize($data['SearchForm'])]);
    }

    public function actionUpdatesettings()
    {
        $model = new SearchForm();
        $data = Yii::$app->request->post();

        return $model->updateSettings(['id' => $data['id'], 'settings' => serialize($data['SearchForm'])]);
    }

    public function actionGetsettings($id)
    {

        $model = new SearchForm();

        $search_settings = $model->getSettings(['id' => $id]);

        return Json::encode(unserialize($search_settings[$id]->settings));
    }

    public function actionDeletesettings($id)
    {

        $model = new SearchForm();

        return $model->deleteSettings($id);
    }

    public function actionCreatebanner($id)
    {
        Yii::$app->view->registerJsFile('@web/js/jban_settings.js', ['position' => \yii\web\View::POS_HEAD]);
        Yii::$app->view->registerJsFile('@web/js/raphael-min.js', ['position' => \yii\web\View::POS_HEAD]);
        Yii::$app->view->registerJsFile('@web/js/jban-light.js', ['position' => \yii\web\View::POS_HEAD]);
        Yii::$app->view->registerJsFile('@web/js/colpick.js', ['position' => \yii\web\View::POS_END]);
        Yii::$app->view->registerCssFile('@web/css/colpick.css');
        $msg = '';
        $tmpl_sizes = \app\components\Api::resource('jban/tmpl')->get();
        if(($save = \Yii::$app->request->post('save')) && isset($save['banner'])){
            $result = Api::resource('jban/banner')->post(array_merge($_POST,['project_location_id'=>$id]));
            if(isset($result->error) || isset($result->errors)){
                $msg ='Ошибка';
            } else {
                $msg = 'Баннер успешно сохранён!';
            }
        }
        if(($save = \Yii::$app->request->post('save')) && isset($save['preset'])){
            $result = Api::resource('jban/settings_preset')->post(array_merge($_POST,['project_location_id'=>$id,'client_location_id'=>$_COOKIE['client_location_id']]));
            if(isset($result->error) || isset($result->errors)){
                $msg = 'Ошибка';
            } else {
                $msg = 'Шаблон успешно сохранён!';
            }
        }
        return $this->render('/jban/optimizer/create_banner',['tmpl_sizes'=>$tmpl_sizes->items,'msg'=>$msg]);
    }
    
    public function actionGettmpl($id)
    {
        $presets = Api::resource('jban/settings_preset')->get(['client_location_id'=>$_COOKIE['client_location_id']]);
        $tmpl = Api::resource('jban/tmpl/'.$id)->get();
        return $this->renderPartial('/jban/optimizer/tmpl',['tmpl'=>$tmpl->items[0],'presets'=>(array)$presets->items->toArray()]);
    }
    
    public function actionGetpreset($id)
    {
        $preset = Api::resource('jban/settings_preset')->get(['client_location_id'=>$_COOKIE['client_location_id'],'id'=>$id]);
        echo json_encode($preset->items[0]);
    }
    
    public function actions()
    {
        return [
            'showdomainblacklist' => [
                'class' => 'app\modules\sections\actions\ShowAction',
                'owner' => 'optimizer',
                'type' => 'domain',
                'model' => 'black',
            ],
            'updatedomainblacklist' => [
                'class' => 'app\modules\sections\actions\UpdateAction',
                'owner' => 'optimizer',
                'type' => 'domain',
                'model' => 'black',
            ],
            'showdomainwhitelist' => [
                'class' => 'app\modules\sections\actions\ShowAction',
                'owner' => 'optimizer',
                'type' => 'domain',
                'model' => 'white',
            ],
            'updatedomainwhitelist' => [
                'class' => 'app\modules\sections\actions\UpdateAction',
                'owner' => 'optimizer',
                'type' => 'domain',
                'model' => 'white',
            ],
        ];
    }

}
