<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 12.12.14
 * Time: 14:11
 */

namespace app\modules\sections\controllers;


use Yii;
use yii\web\Controller;
use yii\helpers\Json;
use app\modules\sections\models\social\optimizer\Post;
use app\modules\sections\models\social\optimizer\SearchGroup;


class SocialOptimizerController extends Controller
{
	public $layout = 'social/optimizer';

	public function beforeAction($action)
	{
		Yii::$app->session->set('section', 'social');

		return parent::beforeAction($action);
	}

	public function actionIndex()
	{

		return $this->render('/social/optimizer/index');
	}


	/**
	 * Возвращает страницу списка Social Posts
	 *
	 *
	 * @return html
	 */
	public function actionListposts()
	{
		$model = new Post();

		return $this->render('/social/optimizer/list-posts', ['model' => $model]);
	}

	/**
	 * Возвращает набор данных Social Groups для DataTable
	 *
	 *
	 * @return json
	 */
	public function actionListpostdata()
	{
		$model = new Post();

		$res = $model->getTableData(
			array_merge(['page'=>($_GET['start']/$_GET['length'] + 1), 'per-page'=>$_GET['length']],\Yii::$app->request->getBodyParams()),
			((isset($_GET['columns'])) ? $_GET['columns'] : []),
			((isset($_GET['order'])) ? $_GET['order'] : ['location_id'=>'asc']),
			$_GET['draw']
		);


		return Json::encode($res);
	}

	public function actionListgroupdata()
	{
		$model = new SearchGroup(['client_location_id' => 14]);

		$res = $model->getTableData(
			\Yii::$app->request->getBodyParams(),
			((isset($_GET['columns'])) ? $_GET['columns'] : []),
			((isset($_GET['order'])) ? $_GET['order'] : ['location_id'=>'asc']),
			$_GET['draw']
		);


		return Json::encode($res);
	}

	/**
	 * Add new Post
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionAddpost()
	{
		$model = new Post();

		if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->save()) {
			$model->save_attach();

			return $this->redirect(['listposts']);
		} else {
			return $this->render('/social/optimizer/form_post.tpl', [
				'model' => $model
			]);

			/*return $this->render('/social/optimizer/form_post', [
				'model' => $model
			]);*/
		}
	}

	public function actionAddpostform($id)
	{
		$model = new Post(['network_id' => $id]);

		return $this->renderPartial('/social/optimizer/post/_form', [
			'model' => $model
		]);
	}

	/**
	 * Update Post
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionUpdatepost($id)
	{
		$model = new Post();

		$model->findByID($id);

		if ($model->load(Yii::$app->request->post()) && $model->update()) {
			$model->save_attach();

			return $this->redirect(['listposts']);
		} else {
			return $this->render('/social/optimizer/form_post.tpl', [
				'model' => $model
			]);

			/*return $this->render('/social/optimizer/form_post', [
				'model' => $model,
				'attachments' => $model->get_attach()
			]);*/

		}
	}

	/**
	 * @param $id
	 * @return string
	 */
	public function actionDeleteattachment($id)
	{
		$model = new Post();

		if ($model->deleteAttachment($id, Yii::$app->request->getQueryParam('location_id'))) {
			return json_encode(['success'=>1]);
		} else {
			return json_encode(['success'=>0]);
		}
	}

	/**
	 * @param $id
	 * @return \yii\web\Response
	 */
	public function actionDeletepost($id)
	{
		$model = new Post();

		$model->delete($id);

		return $this->redirect(['listposts']);
	}

	/**
	 * Update Post
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionGroups($id)
	{
		$post = new Post(['client_location_id' => 14]);

		$post->findByID($id);

		$model = new SearchGroup(['client_location_id' => 14]);


		return $this->render('/social/optimizer/search_group', [
			'model' => $model,
			'post' => $post
		]);

	}


}
