<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 10.12.14
 * Time: 14:01
 */

namespace app\modules\sections\controllers;


use Yii;
use yii\web\Controller;
use yii\helpers\Json;
use app\modules\sections\models\social\webmaster\SocialGroup;
use app\modules\sections\models\social\webmaster\Price;
use app\modules\sections\models\social\webmaster\SocialAccount;


class SocialWebmasterController extends Controller
{
	public $layout = 'social/webmaster';

	public function beforeAction($action)
	{
		Yii::$app->session->set('section', 'social');

		return parent::beforeAction($action);
	}

	public function actionIndex()
	{

		$model = new SocialAccount();

		return $this->render('/social/webmaster/index.tpl', [
			'model' => $model,
			'applications' => $model->getApplication()
		]);
	}

	public function actionUnset()
	{

		$model = new SocialAccount();
		$model->unsetClient(\Yii::$app->request->getBodyParam('Client'));

		return $this->render('/social/webmaster/index.tpl', [
			'model' => $model,
			'applications' => $model->getApplication()
		]);
		//return $this->redirect(['index']);
	}

	public function actionAssign()
	{

		$model = new SocialAccount();
		$model->assignClient(\Yii::$app->request->getBodyParam('Client'));

		return $this->render('/social/webmaster/index.tpl', [
			'model' => $model,
			'applications' => $model->getApplication()
		]);
		//return $this->redirect(['index']);
	}

	public function actionTogglegroup()
	{
		$model = new SocialAccount();
		$model->toggleGroup(\Yii::$app->request->getBodyParam('Client'));

		return $this->redirect(['index']);
	}


	/**
	 * Возвращает страницу списка Social Groups
	 *
	 *
	 * @return html
	 */
	public function actionListgroups()
	{
		$model = new SocialGroup();

		return $this->render('/social/webmaster/list-groups', ['model' => $model]);
	}

	/**
	 * Возвращает набор данных Social Groups для DataTable
	 *
	 *
	 * @return json
	 */
	public function actionListgroupdata()
	{
		$model = new SocialGroup();

		$res = $model->getTableData(
			\Yii::$app->request->getBodyParams(),
			((isset($_GET['columns'])) ? $_GET['columns'] : []),
			((isset($_GET['order'])) ? $_GET['order'] : ['location_id'=>'asc']),
			$_GET['draw']
		);


		return Json::encode($res);
	}

	/**
	 * Add new Group
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionAddgroup()
	{
		$model = new SocialGroup();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['listgroups']);
		} else {
			return $this->render('/social/webmaster/form_group', [
				'model' => $model,
				'topics' => $model->getTopics(),
				'networks' => $model->getNetworks()
			]);
		}
	}

	public function actionConfirmgroup() {
		$model = new SocialGroup();
		$model->confirm($_GET);

		return $this->redirect(['listgroups']);
	}

	public function actionDeletegroup($id)
	{
		$model = new SocialGroup();

		$model->delete($id);

		return $this->redirect(['listgroups']);
	}

	/**
	 * Update Group
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionUpdategroup($id)
	{
		$model = new SocialGroup();

		$model->findByID($id);

		if ($model->load(Yii::$app->request->post()) && $model->update()) {
			return $this->redirect(['listgroups']);
		} else {
			return $this->render('/social/webmaster/form_group', [
				'model' => $model,
				'topics' => $model->getTopics(),
				'networks' => $model->getNetworks()
			]);
		}
	}

	/**
	 * Set Price
	 *
	 * @return string|\yii\web\Response
	 */
	public function actionSetprice($id)
	{
		$model = new Price();

		$model->findByID($id);

		if ($model->load(Yii::$app->request->post()) && $model->update()) {
			return $this->redirect(['listgroups']);
		} else {
			return $this->render('/social/webmaster/price', [
				'model' => $model
			]);
		}
	}

	public function actionAccounts()
	{
		$model = new SocialAccount();

		return $this->render('/social/webmaster/list-accounts.tpl', ['model' => $model]);
	}

}
