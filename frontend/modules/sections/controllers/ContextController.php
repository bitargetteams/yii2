<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 06.11.14
 * Time: 16:26
 */

namespace app\modules\sections\controllers;


use app\modules\sections\models\Sections;
use yii\web\Controller;

class ContextController extends Controller{
    public function actionIndex()
    {
        return $this->render('index');
    }
} 