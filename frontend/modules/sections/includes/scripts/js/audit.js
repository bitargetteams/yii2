/**
 * Created by i_kononenko on 07.11.14.
 */
(function($){
    $(window).ready(function() {
        var $checkboxList = $("div#createform-useaudit");
        $checkboxList.on('change', function (e) {
            getTotalPriceAudits($(this));
        });
        $("#list-orders-audit").itemsList('itemsHandler', function () {
            var id = $.trim($(this).find("div.id").text());
            var $target = $("#list-orders-audit").find("span#list-item-"+ id);
            var $target_wrp = $target.parent();
            var renderProcess = function(data) {
                $target.html(data);
                $target_wrp.slideDown();
                $("#audit-job-" + id).itemsList({
                        "page": 1,
                        "pageSize": 5,
                        "itemsAction": "/audit/jobs",
                        "id": "audit-job-" + id,
                        "isAjax": true,
                        "data": {
                            "id": id
                        },
                        "itemsClass": "attach-audits",
                        "wrapperClass": "att-items-list",
                        'headerClass': "list-att-header"
                    }
                );
            };
            if($target_wrp.css('display') === "none") {
                $.post(
                    '/audit/process',
                    {audit: id},
                    function (data) {}
                ).done(function (data) {
                        renderProcess(data);
                    }
                );
                $(this).addClass("active_header");
                $target_wrp.addClass("active_body");
            }else{
                $target_wrp.fadeOut();
                $target.html("");
                $target_wrp.removeClass("active_body");
                $(this).removeClass("active_header");
            }
        });


        var getTotalPriceAudits = function ($object) {
            var summary = 0;
            $object.find('input').each(function () {
                if ($(this).prop('checked')) {
                    var price = $(this).parent().find('span.price').text();
                    summary += price * 1;
                }
            });
            $("div#summary-audit span.total-audit").text(summary);
        };
        getTotalPriceAudits($checkboxList);
    });
})(jQuery);