/* column[0] -- jban template id, banner width & height
 * ['tmpl_id' => 1, 'width' => 250, 'height' => 250],
 * 
 * column [1..4] jban template id, nesting level, min price, average price, user pirce
 * ['tmpl_id' => 1, 'level' => 1, 'min' => 10.00, 'avg' => 25.33, 'user' => 25.00],
 * 
 * 
 */


jQuery(document).ready(function() {
    var params = document.URL.split('/');
    var website_id = params[params.length-1];

    var table =
        $('#data_table').dataTable({
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': '/jban/webmaster/getprice/'+website_id,
            'dataType': 'json'

        },
        'columnDefs': [
            {
                'render': function (data, type, row) {
                    return data.width + 'x' + data.height;
                },
                'targets': 0
            },
            {
                'render': function (data, type, row) {
                    var output = '';
                    if (data.user){
                        output = '<div class="min_avg" style="width: 50%; min-height: 100%; float:left;">min: ' + data.min + '<br />avg: ' + data.avg + '</div>';
                        output += '<div class="user_price">Ваша цена:<br /><input type="text" name="' + jQuery('#data_table').dataTable.defaults.modelClass + '[' + data.tmpl_id + '][' + data.level + ']" value="' + data.user + '" style="width: 40%"></div>';
                    } else {
                        output = '<div class="text-center">X</div>';
                    }
                    return output;
                },
                'targets': [1, 2, 3, 4]
            }
        ],
        'initComplete': function () {
            var api = this.api();

            api.columns().indexes().each(function (i) {
                var input = api.$('div.user_price input[type=text]').eq(i).val();
                if (input) {
//                    console.log(input);
//                    console.log(api.column(i));
                }
            });


        }

//		"drawCallback": function ( settings ) {
//			var api = this.api();
//			var rows = api.rows( {page:'current'} ).nodes();
//			var last=null;
//			api.column(0, {page:'current'} ).data().each( function ( group, i ) {
//				if ( last !== group ) {
//					$(rows).eq( i ).before(
//						'<tr class="group"><td colspan="4">'+jQuery('#data_table').dataTable.defaults.titles[group]+'</td></tr>'
//					);
//
//					last = group;
//				}
//			} );
//		}
    });

});