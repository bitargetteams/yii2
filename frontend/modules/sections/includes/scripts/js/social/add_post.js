/**
 * Created by ddo on 19.12.14.
 */

$(document).ready(function() {

	$('#post-network_id').on('change', function(){
		var network_id = $(this).val();

		$('#post_attributes').html('');

		if (network_id == '') return false;

		$.ajax({
			url: "/social/optimizer/addpostform/"+network_id
		}).done(function(data) {
			$('#post_attributes').html(data);

		}).error(function() {

			alert('error');

		});
	});

	$('.img_div a').on('click', function(){
		var img_div = $(this).parent();
		var id = img_div.attr('id').replace('img_', '');
		var location_id = $('#location_id').val();

		$.ajax({
			url: "/social/optimizer/deleteattachment/"+id,
			data: "location_id="+location_id,
			dataType: "json"
		}).done(function(data) {
			if(data.success == '1') {
				img_div.remove('');
			} else {
				alert('error');
			}

		}).error(function() {

			alert('error');

		});

		return false;
	});

});