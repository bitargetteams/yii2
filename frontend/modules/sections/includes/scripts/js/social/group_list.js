$(document).ready(function() {

	var my_dt = $('#data_table').dataTable({
		language: {
			url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
		},
		'processing' : true,
		'serverSide' : true,
		'searching' : true,
		'ajax' : '/social/webmaster/listgroupdata',
		"columnDefs": [
			{
				"targets": [ -1 ],
				"visible": false
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/social/webmaster/updategroup/'+row['location_id']+'">'+data+'</a>';
				},
				"targets": 0
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/social/webmaster/setprice/'+row['location_id']+'">Цены</a>';
				},
				"targets": 7
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/social/webmaster/updategroup/'+row['location_id']+'" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>';
				},
				"targets": 8
			},
			{
				"render": function ( data, type, row ) {
					return ' <a href="/social/webmaster/deletegroup/'+row['location_id']+'" title="Удалить"><span class="glyphicon glyphicon-trash"></span></a>';
				},
				"targets": 9
			}
		],
		"columns" : [

			{"data":"title", "searchable": true},
			{"data":"topic_id", "orderable": false},
			{"data":"network_id", "orderable": false},
			{"data":"moderated", "orderable": false},
			{"data":"member_cnt"},
			{"data":"day_hits"},
			{"data":"create_time"},
			{"data":"location_id", "orderable": false, "searchable": false},
			{"data":null},
			{"data":null},
			{"data":null}
		]
	}).api();

	$('#topic_select').on( 'change', function () {
		my_dt
			.column( 1 )
			.search( $(this).val())
			.draw();
	});
	$('#network_select').on( 'change', function () {
		my_dt
			.column( 2 )
			.search( $(this).val())
			.draw();
	});
	$('#moderated_select').on( 'change', function () {
		my_dt
			.column( 3 )
			.search( $(this).val())
			.draw();
	});
});