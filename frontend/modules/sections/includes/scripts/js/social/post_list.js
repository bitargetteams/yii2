/**
 * Created by ddo on 12.12.14.
 */
$(document).ready(function() {

		var my_dt = $('#data_table').dataTable({
			language: {
				url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
			},
			'processing' : true,
			'serverSide' : true,
			'searching' : true,
			'ajax' : '/social/optimizer/listpostdata',

			"columnDefs": [
				{
					"targets": [ -1 ],
					"visible": false
				},
				{
					"render": function ( data, type, row ) {
						return '<a href="/social/optimizer/updatepost/'+row['location_id']+'">'+data+'</a>';
					},
					"targets": 0
				},
				{
					"render": function ( data, type, row ) {
						return '<a href="/social/optimizer/groups/'+row['location_id']+'">'+data+'</a>';
					},
					"targets": 3
				},
				{
					"render": function ( data, type, row ) {
						return '<a href="/social/optimizer/updatepost/'+row['location_id']+'" title="Редактировать"><span class="glyphicon glyphicon-pencil"></span></a>';
					},
					"targets": 4
				},
				{
					"render": function ( data, type, row ) {
						return ' <a href="/social/optimizer/deletepost/'+row['location_id']+'" title="Удалить"><span class="glyphicon glyphicon-trash"></span></a>';
					},
					"targets": 5
				}
			],
			"columns" : [
				{"data":"title", "searchable": true,  "orderable": true},
				{"data":"description", "orderable": false, "searchable": true},
				{"data":"moderated", "orderable": false, "searchable": true},
				{"data":"group_cnt", "orderable": false},
				{"data": null},
				{"data": null},
				{"data":"location_id", "orderable": false, "searchable": false}
			]
		}).api();

		$('#moderated_select').on( 'change', function () {
			my_dt
				.column( 2 )
				.search( $(this).val())
				.draw();
		});


});