/**
 * Created by ddo on 30.12.14.
 */

$(document).ready(function() {

	var my_dt = $('#data_table').dataTable({
		language: {
			url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
		},
		'processing' : false,
		'serverSide' : true,
		'searching' : true,
		'ajax' : '/social/optimizer/listgroupdata',

		"columnDefs": [
			{
				"targets": [ -1 ],
				"visible": false
			},
			{
				"render": function ( data, type, row ) {
					return '<input type="checkbox" class="group_checkbox" value="'+data['location_id']+'">';
				},
				"targets": 6
			}
		],
		"columns" : [

			{"data":"title", "searchable": true},
			{"data":"topic_id", "orderable": false},
			{"data":"network_id", "orderable": false},
			{"data":"member_cnt"},
			{"data":"day_hits"},
			{"data":"create_time"},
			{"data":null},
			{"data":"location_id", "orderable": false, "searchable": false}
		]
	}).api();

	$('#search_btn').on('click', function(){
		my_dt.column( 1 ).search( $('#topic_select').val());
		my_dt.column( 2 ).search( $('#network_select').val());

		my_dt.processing = true;
		my_dt.draw();
	});

	$(document).on('click', '.group_checkbox', function(){
		var location_id = $(this).val();
		if ($(this).is(':checked')) alert(location_id);
	});

});