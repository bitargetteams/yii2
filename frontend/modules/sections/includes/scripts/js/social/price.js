/**
 * Created by ddo on 12.12.14.
 */
jQuery(document).ready(function () {

	$('#data_table').dataTable({
		language: {
			url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
		},
		"paging": false,
		"ordering": false
	});

});