$(document).ready(function() {

	var my_dt = $('#data_table').dataTable({
		"columnDefs": [
			{
				"targets": [ -1 ],
				"visible": false
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/jban/optimizer/updateproject/'+row['location_id']+'">'+data+'</a>';
				},
				"targets": 0
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/jban/optimizer/updateproject/'+row['location_id']+'">'+data+'</a>';
				},
				"targets": 1
			}
		],
		"columns" : [

			{"data":"project_title", "searchable": true},
			{"data":"project_url"},
			{"data":"location_id", "orderable": false, "searchable": false}
		]
	}).api();
});