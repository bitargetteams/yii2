jQuery(document).ready(function () {

	var my_dt = $('#data_table').dataTable({

		"aoColumnDefs": [
			{
				"targets": [ 0 ],
				"visible": false
			},
			{
				"render": function ( data, type, row ) {
					return '<input type="checkbox" name="'+jQuery('#data_table').dataTable.defaults.modelClass+'[ids]['+row['ids']+']" value="'+row['ids']+'">';
				},
				"targets": -1
			}
		],
		"aoColumns" : [
			{"data":"id", "orderable": false, "searchable": false},
			{"data":"page_url"},
			{"data":"level", "searchable": true},
			{"data":"rate_pr"},
			{"data":"free_place_cnt", "searchable": false},
			{"data":"all_place_cnt", "orderable": false, "searchable": false},
			{"data":"status", "orderable": false, "searchable": true},
			{"data":"banner", "orderable": false, "searchable": false},
			{"data":"price", "orderable": false, "searchable": false},
			{"data":"ids", "orderable": false, "searchable": false}
		]
	}).api();

	$('#level_select').on( 'change', function () {
		my_dt
			.column( 2 )
			.search( $(this).val())
			.draw();
	});
	$('#status_select').on( 'change', function () {
		my_dt
			.column( 6 )
			.search( $(this).val())
			.draw();
	});

});