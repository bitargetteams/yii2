/**
 * Created by i_kononenko on 26.11.14.
 */
(function($){
    $(window).ready(function() {
        var target = "#install-site-jban";
        $(target).itemsList('loadNextHandler', $("#add-site"));
        $("button#install-site").on('click',function(){
            $(target).find('div.list-sites>form').each(function(){
                    var $form = $(this);
                    var formWrapper = $form.parent();
                    var url = $form.attr('action') || "/";
                    var method = $form.attr('method') || "GET";
                    var formData = $form.serialize();
                    console.log(formWrapper);
                    $.ajax({
                        type: method,
                        url: url,
                        data: formData,
                        success: function (data) {
                            formWrapper.html($(data).find("form"));
                        }
                    }).done(function () {
                    }).fail(function () {
                    });
            })
        });
    });
})(jQuery);
