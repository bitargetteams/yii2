/**
 * Created by ddo on 15.12.14.
 */
$(document).ready(function() {

	$('#country_id').select2();

	$('#topic_id').select2();

	$('#banner').select2();

	$('#domain_zone').select2();

	$('#save_settings').click(function(){
		title = prompt('Title','');

		$.ajax({
			url: "/jban/optimizer/savesettings",
			method: 'POST',
			data: 'title='+title+'&'+$('#search_form').serialize( )
		}).done(function(id) {
			$('#settings').append(
				'<div id="settings_'+id+'"><a href="#" class="saved_settings">' + title + '</a> <a href="#" class="delete_settings">Delete</a> <a href="#" class="update_settings">Update</a></div>'
			);

			//$('#myTab a:first').tab('show');

		}).error(function(){
			alert('error');
		});

		return false;
	});

	$('.saved_settings').click(function(){

		var id = $(this).parent().attr('id').replace('settings_', '');
		$.ajax({
			url: "/jban/optimizer/getsettings/"+id,
			method: 'POST',
			data: 'id='+id
		}).done(function(data) {

			insertSettings(JSON.parse(data), '');

		}).error(function(){
			alert('error');
		});

		return false;
	});

	$(document).on('click', '.delete_settings', function(){

		var parent = $(this).parent();
		var id = parent.attr('id').replace('settings_', '');

		$.ajax({
			url: "/jban/optimizer/deletesettings/"+id,
			method: 'POST',
			data: 'id='+id
		}).done(function(data) {
			parent.remove();
		}).error(function(){
			alert('error');
		});

		return false;
	});

	$(document).on('click', '.update_settings', function(){

		var parent = $(this).parent();
		var id = parent.attr('id').replace('settings_', '');

		$.ajax({
			url: "/jban/optimizer/updatesettings/"+id,
			method: 'POST',
			data: 'id=' + id + '&' + $('#search_form').serialize( )
		}).done(function(id) {
			if (id == 0) alert('error');
		}).error(function(){
			alert('error');
		});

		return false;
	});

	function insertSettings(data, parent)
	{
		//$('#banner_id').select2('data', [{id:7, text:$("#banner_id option[value='7']").text()},{id:8, text:$("#banner_id option[value='8']").text()}]);

		for(var key in data){
			if (typeof data[key] === 'object') {
				if (key == 'country_id' || key == 'topic_id' || key == 'banner' || key == 'domain_zone') {
					my_set_select(data[key], key);
				} else if (parent == 'page' && key == 'level') {
					for(var key2 in data[key]) {
						$("input:checkbox[name='SearchForm["+parent+"]["+key+"][]']").filter('[value='+data[key][key2]+']').prop('checked', true);
					}
				} else {
					insertSettings(data[key], key);
				}
			} else {
				if (parent == 'website' && (key == 'is_profitable' || key == 'domain_level')) {
					$("input:radio[name='SearchForm["+parent+"]["+key+"]']").filter('[value='+data[key]+']').prop('checked', true);
				} else if ((parent == 'website' && key == 'add_time') || (parent == 'limit')) {
					$("select[name='SearchForm["+parent+"]["+key+"]']").find("option[value='"+data[key]+"']").attr("selected", "selected");
				} else if (parent == 'website' && (key == 'cat_dmoz' || key == 'cat_yandex' || key == 'cat_setsite')) {
					if (data[key] == '1') {
						$("input[name='SearchForm["+parent+"]["+key+"]']").attr('checked', 'checked');
					}
				} else if (parent == '' && key == 'order') {
					$("select[name='SearchForm["+key+"]']").find("option[value='"+data[key]+"']").attr("selected", "selected");
				} else {
					$("input[name='SearchForm["+parent+"]["+key+"]']").val(data[key]);
				}
			}
		}
	}

	function my_set_select(data, field_id)
	{
		var vals = [];
		for(var key in data){
			vals.push({id:data[key], text:$("#"+field_id+" option[value='"+data[key]+"']").text()});
		}
		$('#'+field_id).select2('data', vals);
	}

});
