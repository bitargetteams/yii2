function format ( d ) {
	return d.addInfo;
}

$(document).ready(function() {


	var my_dt = $('#data_table').dataTable({
		language: {
			url: "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
		},
		processing: true,
		serverSide: true,
		searching: true,
		ajax: "/jban/webmaster/listwebsitedata",
		"columnDefs": [
			{
				"targets": [ -1 ],
				"visible": false
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/jban/webmaster/listpages/'+row['location_id']+'">'+data+'</a>';
				},
				"targets": 4
			},
			{
				"render": function ( data, type, row ) {
					return '<a href="/jban/webmaster/setprice/'+row['location_id']+'">Цены</a>';
				},
				"targets": 6
			}
		],
		"columns" : [

			{"data":"url"},
			{"data":"status", "searchable": true},
			{"data":"rate_pr"},
			{"data":"rate_tic", "searchable": false},
			{"data":"page_cnt", "orderable": false, "searchable": false},
			{"data":"position_cnt", "orderable": false, "searchable": false},
			{"data":null, "orderable": false, "searchable": false,"defaultContent": ""},
			{
				"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},

			{"data":"location_id", "orderable": false, "searchable": false}
		]
	}).api();

	// Array to track the ids of the details displayed rows
	var detailRows = [];

	$('#data_table tbody').on( 'click', 'tr td:last-child', function () {
		var tr = $(this).closest('tr');
		var row = my_dt.row( tr );
		var idx = $.inArray( tr.attr('id'), detailRows );

		if ( row.child.isShown() ) {
			tr.removeClass( 'details' );
			row.child.hide();

			// Remove from the 'open' array
			detailRows.splice( idx, 1 );
		}
		else {
			tr.addClass( 'details' );
			row.child( format( row.data() ) ).show();

			// Add to the 'open' array
			if ( idx === -1 ) {
				detailRows.push( tr.attr('id') );
			}
		}
	} );

	// On each draw, loop over the `detailRows` array and show any child rows
	my_dt.on( 'draw', function () {
		$.each( detailRows, function ( i, id ) {
			$('#'+id+' td:last-child').trigger( 'click' );
		} );
	} );
});