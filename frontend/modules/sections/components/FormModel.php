<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 14:36
 */
namespace app\modules\sections\components;
use yii\base\Model;

class FormModel extends Model{
    protected $formName;

    public function formName()
    {
        return !is_null($this->formName) ? $this->formName : parent::formName();
    }

    public function setFormName($value)
    {
        $this->formName = $value;
    }
} 