<?php

namespace app\modules\sections\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class AlphabeticalHeaderWidget extends Widget
{
    public $keys;
    
    protected $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ語*АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ';
    
    public function run()
    {
        $output = Html::a('ВСЕ', ['', 'showMe' => '']);
        if (in_array('@', $this->keys)){
            $output .= '&nbsp;'.Html::a('0-9', ['', 'showMe' => '@']);
        } else {
            $output .= '&nbsp;0-9';
        }
        for ($i = 0; $i <= mb_strlen($this->str); $i++){
            $char = mb_substr($this->str, $i, 1, 'UTF-8');
            if (in_array($char, $this->keys)){
                $output .= '&nbsp;'.Html::a($char,['', 'showMe' => $char]);
            } else {
                $output .= "&nbsp;{$char}";
            }
            if ($char === '*') {
                $output .= '<br />';
            }
        }
        return '<div>'.$output.'</div>';
    }
}
