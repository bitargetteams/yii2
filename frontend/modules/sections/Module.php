<?php

namespace app\modules\sections;
use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\sections\controllers';

    public function init()
    {
        parent::init();

        Yii::$app->i18n->translations['*']['basePath'] = '@sections/messages';
    }

    public function behaviors()
    {
        return [
            'role' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => true
            ],
        ];
    }

    public function redirect($url)
    {
        \Yii::$app->controller->redirect($url);
    }
}
