{include '@app/views/errors.tpl' errors=$model->getErrors()}

<ul class="nav nav-tabs">
	{foreach $applications as $k=>$application}
		<li {if $k == 1}class="active"{/if}><a href="#{$application->network->class_name}" data-toggle="tab">{$application->network->title}</a></li>
	{/foreach}
</ul>

<div class="tab-content">
	{foreach $applications as $k=>$application}
		<div class="tab-pane{if $k == 1} active{/if}" id="{$application->network->class_name}">
			<div style="float: left">
				{if isset($application->client)}
					<img src="{$application->client->avatar}" /><br>
					{$application->client->title}<br>
					<form id="form_client" action="/social/webmaster/unset" method="post">
						<input type="hidden" name="{$app->getRequest()->csrfParam}" value="{$app->getRequest()->getCsrfToken()}">
						<input type="hidden" name="Client[client_id]" value="{$application->client->id}" />
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Отвязать</button>
						</div>
					</form>
				{else}
					<form id="form_client" action="/social/webmaster/assign" method="post">
						<input type="hidden" name="{$app->getRequest()->csrfParam}" value="{$app->getRequest()->getCsrfToken()}">
						<input type="hidden" name="Client[network_id]" value="{$application->network->id}" />
						{if $application->network->class_name == 'TW'}
							Token <input type="text" name="Client[oauth_token]" value="{if isset($smarty.get.oauth_token)}{$smarty.get.oauth_token}{/if}"/>
							Verifier <input type="text" name="Client[oauth_verifier]" value="{if isset($smarty.get.oauth_verifier)}{$smarty.get.oauth_verifier}{/if}"/>
						{else}
							Code <input type="text" name="Client[code]" value="{if isset($smarty.get.code)}{$smarty.get.code}{/if}"/>
						{/if}
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Привязать</button>
						</div>
					</form>
				{/if}
			</div>
			<div style="padding-left: 20px">
				{if isset($application->client->groups) && count($application->client->groups) > 0}
					<table>
						<tr>
							<th>Аватар</th>
							<th>Заголовок</th>
							<th>Дата создания</th>
							<th>Url</th>
							<th></th>
						</tr>
						{foreach $application->client->groups as $group}
							<tr>
								<td><img src="{$group->avatar}" /></td>
								<td>{$group->title}</td>
								<td>{$group->create_time}</td>
								<td>{$group->url}</td>
								<td>
									<form id="form_client_{$group->location_id}" action="/social/webmaster/togglegroup" method="post">
										<input type="hidden" name="{$app->getRequest()->csrfParam}" value="{$app->getRequest()->getCsrfToken()}">
										<input type="hidden" name="Client[location_id]" value="{$group->location_id}" />
										{if $group->status != 'enabled'}
											<input type="hidden" name="Client[status]" value="enabled" />
											<button type="submit" class="btn btn-primary">Привязать</button>
										{else}
											<input type="hidden" name="Client[status]" value="disabled" />
											<button type="submit" class="btn btn-primary">Отвязать</button>
										{/if}
									</form>
								</td>
							</tr>
						{/foreach}
					</table>
				{/if}
			</div>
		</div>
	{/foreach}
</div>