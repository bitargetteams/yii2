<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 11.12.14
 * Time: 18:59
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\assets\DataTableAsset;
use app\modules\sections\assets\social\PriceAssets;

DataTableAsset::register($this);

$prices = $model->getMinPrice();

?>

<div class="container-fluid">
	<h1><?= $model->title ?></h1>

	<?php if ($model->isError()) print_r($model->getErrors())?>

	<?php $form = ActiveForm::begin(); ?>

	<table id="data_table" class="display" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th>Тип</th>
			<th>Минимальная цена</th>
			<th>Средняя цена</th>
			<th>Рекомендуемая цена</th>
			<th>Цена</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<th>Текст</th>
			<th><?=$prices->text->minimal?></th>
			<th><?=$prices->text->average?></th>
			<th><?=$prices->text->recommended?></th>
			<th><input min="<?=$prices->text->minimal?>" type="text" name="Price[price_text]" value="<?=$model->price_text?>"></th>
		</tr>
		<tr>
			<th>Картинка</th>
			<th><?=$prices->image->minimal?></th>
			<th><?=$prices->image->average?></th>
			<th><?=$prices->image->recommended?></th>
			<th><input min="<?=$prices->image->minimal?>" type="text" name="Price[price_image]" value="<?=$model->price_image?>"></th>
		</tr>
		<tr>
			<th>Видео</th>
			<th><?=$prices->video->minimal?></th>
			<th><?=$prices->video->average?></th>
			<th><?=$prices->video->recommended?></th>
			<th><input min="<?=$prices->video->minimal?>" type="text" name="Price[price_video]" value="<?=$model->price_video?>"></th>
		</tr>
		</tbody>
	</table>

	<br>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>

<?php
PriceAssets::register($this);
?>