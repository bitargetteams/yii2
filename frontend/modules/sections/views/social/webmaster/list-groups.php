<?php

use frontend\assets\DataTableAsset;
use app\modules\sections\assets\social\ListGroupAssets;

DataTableAsset::register($this);
?>

<div class="container-fluid">
	<div class="filter">
		<div class="field">
			<label>Тема</label>
			<select id="topic_select">
				<option value="">Все</option>
				<?php foreach($model->getTopics() as $k=>$v) {
					echo '<option value="'.$k.'">'.$v->title.'</option>';
				} ?>
			</select>
		</div>
		<div class="field">
			<label>Соцсеть</label>
			<select id="network_select">
				<option value="">Все</option>
				<?php foreach($model->getNetworks() as $k=>$v) {
					echo '<option value="'.$k.'">'.$v->title.'</option>';
				} ?>
			</select>
		</div>
		<div class="field">
			<label>Статус модерации</label>
			<select id="moderated_select">
				<option value="">Все</option>
				<option value="0">Нет</option>
				<option value="1">Да</option>
			</select>
		</div>
	</div>


	<table id="data_table" class="display" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th>Заголовок</th>
			<th>Тема</th>
			<th>Соцсеть</th>
			<th>Статус модерации</th>
			<th>Участников</th>
			<th>Посещений в день</th>
			<th>Дата создания</th>
			<th>Цены</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		</tfoot>
	</table>

</div>

<?php
ListGroupAssets::register($this);
?>