<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 09.12.14
 * Time: 16:22
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="container-fluid">
	<div class="error_text"><?php if ($model->isError()) print_r($model->getErrors())?></div>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'url')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>

	<?= $form->field($model, 'topic_id')->dropDownList($model->prepareForDropDown($topics, [''=>'Выберите тему'])) ?>

	<?= $form->field($model, 'network_id')->dropDownList($model->prepareForDropDown($networks, [''=>'Выберите сеть'])) ?>

	<?= $form->field($model, 'network_group_id')->textInput(['maxlength' => 50]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>