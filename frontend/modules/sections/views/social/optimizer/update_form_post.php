<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 09.12.14
 * Time: 16:22
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$Network = $model->getNetwork();
?>

	<div class="container-fluid">
		<?php if ($model->isError()) print_r($model->getErrors())?>

		<div><b><?=Yii::t('social', 'Network ID')?>:</b> <?=$Network->title?></div>

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

		<?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

		<?= $form->field($model, 'post_type')->dropDownList($model->getValidPost_types()) ?>

		<?= $form->field($model, 'post_text')->textarea(['rows' => 5]) ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>

		<?= $form->field($model, 'network_id')->hiddenInput() ?>
		<?php ActiveForm::end(); ?>

	</div>
