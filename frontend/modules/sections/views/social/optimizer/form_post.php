<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 09.12.14
 * Time: 16:22
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\sections\assets\social\AddPostAssets;

?>

<div class="container-fluid">
	<?php if ($model->isError()) print_r($model->getErrors())?>

	<?php $form = ActiveForm::begin([
		'options' => ['enctype'=>'multipart/form-data'],
		'action' => '/social/optimizer/' . ($model->isNewRecord ? 'addpost' : 'updatepost/' . $model->location_id)
	]); ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

	<?= $form->field($model, 'article')->textarea(['rows' => 5]) ?>

	<ul class="nav nav-tabs">
		<li class="active"><a href="#link" data-toggle="tab">Ссылка</a></li>
		<li><a href="#image" data-toggle="tab">Картинки</a></li>
		<li><a href="#video" data-toggle="tab">Видео</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="link">
			<?= $form->field($model, 'link')->textInput(['maxlength' => 100]) ?>
		</div>
		<div class="tab-pane" id="image">
			<?php if (isset($attachments)) { foreach ($attachments as $attach) {
				print '<div id="img_'.$attach['id'].'" class="img_div"><img src="data:'.$attach['mime'].';base64,'.$attach['img_body'].'" /><a class="img_a" href="#" title="Удалить"><span class="glyphicon glyphicon-trash"></span></a></div>';
			}} ?>
			<?= $form->field($model, 'image[]')->fileInput(['multiple' => 'multiple']) ?>
		</div>
		<div class="tab-pane" id="video">
			<?= $form->field($model, 'video_link')->textInput(['maxlength' => 100]) ?>
		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<input type="hidden" id="location_id" value="<?=$model->location_id?>" />

	<?php ActiveForm::end(); ?>

</div>

<?php
AddPostAssets::register($this);
?>