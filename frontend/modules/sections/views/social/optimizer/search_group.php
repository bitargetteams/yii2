<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 22.12.14
 * Time: 17:50
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\sections\assets\social\SearchGroupAssets;
use frontend\assets\DataTableAsset;

DataTableAsset::register($this);

?>
<h1><?=$post->title?></h1>
<div class="container-search">

	<?php $form = ActiveForm::begin(['id'=>'search_form']); ?>
	<div class="filter">
		<div class="field">
			<label>Тема</label>
			<select name="Search[topic_id]" id="topic_select">
				<?php foreach($model->prepareForDropDown($model->getTopics(), ['' => 'Все темы']) as $k=>$v) {
					echo '<option value="'.$k.'">'.$v.'</option>';
				} ?>
			</select>
		</div>
		<div class="field">
			<label>Соцсеть</label>
			<select name="Search[network_id]" id="network_select">
				<?php foreach($model->prepareForDropDown($model->getNetworks(), ['' => 'Все сети']) as $k=>$v) {
					echo '<option value="'.$k.'">'.$v.'</option>';
				} ?>
			</select>
		</div>
		<div class="field">
			<label>Количество участников</label>
			от <input type="number" name="Search[member_cnt_min]">
			до <input type="number" name="Search[member_cnt_max]">
		</div>
		<div class="field">
			<label>Посещений в день</label>
			от <input type="number" name="Search[day_hits_min]">
			до <input type="number" name="Search[day_hits_max]">
		</div>

		<div class="form-group">
			<?= Html::Button(Yii::t('social', 'Search'), ['class' => 'btn btn-success', 'id' => 'search_btn']) ?>
		</div>

	</div>
	<?php ActiveForm::end(); ?>


	<table id="data_table" class="display" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th>Заголовок</th>
			<th>Тема</th>
			<th>Соцсеть</th>
			<th>Участников</th>
			<th>Посещений в день</th>
			<th>Дата создания</th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		</tfoot>
	</table>

</div>


</div>

<?php
SearchGroupAssets::register($this);
?>