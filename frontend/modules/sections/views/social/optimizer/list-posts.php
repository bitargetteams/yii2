<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 12.12.14
 * Time: 15:57
 */

use frontend\assets\DataTableAsset;
use app\modules\sections\assets\social\ListPostAssets;

DataTableAsset::register($this);

?>

<div class="container-fluid">

	<div class="filter">
		<div class="field">
			<label>Статус модерации</label>
			<select id="moderated_select" class="m_select">
				<option value=""><?=Yii::t('app', 'All')?></option>
				<option value="0"><?=Yii::t('app', 'No')?></option>
				<option value="1"><?=Yii::t('app', 'Yes')?></option>
			</select>
		</div>
	</div>

	<table id="data_table" cellspacing="0" width="100%" class="display">
		<thead>
		<tr>
			<th>Заголовок</th>
			<th>Тип</th>
			<th>Статус модерации</th>
			<th>Группы</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		</tfoot>
	</table>

</div>

<?php
ListPostAssets::register($this);
?>
