
{use class="yii\helpers\Html"}


<div class="container-fluid">
	{include '@app/views/errors.tpl' errors=$model->getErrors()}

	<form id="form_post" action="/social/optimizer/{if $model->isNewRecord}addpost{else}updatepost/{$model->location_id}{/if}" method="post" enctype="multipart/form-data">

		<input type="hidden" name="{$app->getRequest()->csrfParam}" value="{$app->getRequest()->getCsrfToken()}">

		<div class="form-group field-post-title required">
			<label class="control-label" for="post-title">{Yii::t('social', 'Post Title')}</label>
			<input type="text" id="post-title" class="form-control" name="Post[title]" value="{$model->title}" maxlength="100">
			<div class="help-block"></div>
		</div>
		<div class="form-group field-post-description has-success">
			<label class="control-label" for="post-description">Описание</label>
			<textarea id="post-description" class="form-control" name="Post[description]" rows="2">{$model->description}</textarea>

			<div class="help-block"></div>
		</div>
		<div class="form-group field-post-article required">
			<label class="control-label" for="post-article">Article</label>
			<textarea id="post-article" class="form-control" name="Post[article]" rows="5">{$model->article}</textarea>

			<div class="help-block"></div>
		</div>

		<ul class="nav nav-tabs">
			<li class="active"><a href="#link" data-toggle="tab">Ссылка</a></li>
			<li><a href="#image" data-toggle="tab">Картинки</a></li>
			<li><a href="#video" data-toggle="tab">Видео</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="link">
				<div class="form-group field-post-link">
					<label class="control-label" for="post-link">Link</label>
					<input type="text" id="post-link" class="form-control" name="Post[link]" value="{$model->link}" maxlength="100">

					<div class="help-block"></div>
				</div>		</div>
			<div class="tab-pane" id="image">

				{include 'images.tpl'}

				<div class="form-group field-post-image">
					<label class="control-label" for="post-image">Images</label>
					<input type="hidden" name="Post[image][]" value="">
					<input type="file" id="post-image" name="Post[image][]" multiple="multiple">

					<div class="help-block"></div>
				</div>
			</div>
			<div class="tab-pane" id="video">
				<div class="form-group field-post-video_link">
					<label class="control-label" for="post-video_link">Video Link</label>
					<input type="text" id="post-video_link" class="form-control" name="Post[video_link]" value="{$model->video_link}" maxlength="100">

					<div class="help-block"></div>
				</div>
			</div>
		</div>

		<input type="hidden" id="location_id" value="{$model->location_id}" />

		<div class="form-group">
			<button type="submit" class="btn btn-primary">{if $model->isNewRecord}Create{else}Update{/if}</button>
		</div>

	</form>
</div>

{use class="app\modules\sections\assets\social\AddPostAssets"}
{AddPostAssets::register($this)|void}