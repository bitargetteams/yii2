<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 12:01
 */
use \yii\bootstrap\Nav;

$this->beginContent('@sections/views/layouts/jban/main.php');
    $bar = Nav::begin(['id'=>'j-ban-optimizer-nav']);
    $bar->items = [
        [
			'url' => Yii::$app->urlManager->createUrl('/jban/optimizer/projects'),
			'label'=>Yii::t('jban', 'Projects'),
			'linkOptions' => (Yii::$app->controller->action->id == 'projects') ? ['class'=>'current'] : [],
		],
		[
			'url' => Yii::$app->urlManager->createUrl('/jban/optimizer/addproject'),
			'label'=>Yii::t('jban', 'Add project'),
			'linkOptions' => (Yii::$app->controller->action->id == 'addproject') ? ['class'=>'current'] : [],
		],
        ['url' => Yii::$app->urlManager->createUrl('/jban/optimizer'),'label'=>'Some link'],
        ['url' => Yii::$app->urlManager->createUrl('/jban/optimizer'),'label'=>'Some link'],
        ['url' => Yii::$app->urlManager->createUrl('/jban/optimizer/showdomainblacklist'),'label'=>Yii::t('jban', 'showdbl')],
        ['url' => Yii::$app->urlManager->createUrl('/jban/optimizer/showdomainwhitelist'),'label'=>Yii::t('jban', 'showdwl')],
    ];
    $bar->end();
$this->params['breadcrumbs'][] = ['label' => Yii::t('sections', 'jBan'), 'url' => '/section/jban'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('jban', 'jBan_optimizer'), 'url' => '/jban/webmaster'];
    echo $content;
$this->endContent();