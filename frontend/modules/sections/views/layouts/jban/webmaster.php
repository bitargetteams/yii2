<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 12:01
 */
use \yii\bootstrap\Nav;
$this->beginContent('@sections/views/layouts/jban/main.php');
    $bar = Nav::begin(['id'=>'j-ban-webmaster-nav']);
    $bar->items = [
        [
			'url' => Yii::$app->urlManager->createUrl('/jban/webmaster/listprojects'),
			'label'=>Yii::t('jban', 'listprojects'),
			'linkOptions' => (Yii::$app->controller->action->id == 'listprojects') ? ['class'=>'current'] : [],
		],
        ['url' => Yii::$app->urlManager->createUrl('/jban/webmaster/installsite'),'label'=>Yii::t('jban', 'installsite')],
        ['url' => Yii::$app->urlManager->createUrl('/jban/webmaster/statprojects'),'label'=>Yii::t('jban', 'statprojects')],
        ['url' => Yii::$app->urlManager->createUrl('/jban/webmaster/showdomainblacklist'),'label'=>Yii::t('jban', 'showdbl')],
        ['url' => Yii::$app->urlManager->createUrl('/jban/webmaster/showwordblacklist'),'label'=>Yii::t('jban', 'showwbl')],
    ];
    $bar->end();
$visitLink = str_replace('sections/jban-webmaster/','',Yii::$app->requestedRoute);
$this->params['breadcrumbs'][] = ['label' => Yii::t('sections', 'jBan'), 'url' => '/section/jban'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('jban', 'jBan_webmaster'), 'url' => '/jban/webmaster'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('jban', $visitLink), 'url' => '/'.Yii::$app->requestedRoute];
    echo $content;
$this->endContent();