<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 12:01
 */
use \app\modules\sections\assets\AssetsJBan;
use \yii\bootstrap\Nav;

AssetsJBan::register($this);
$this->beginContent('@sections/views/layouts/main.php');
    $bar = Nav::begin(['id'=>'jban-nav']);
    $bar->items = [
    	[
			'url' => Yii::$app->urlManager->createUrl('/jban/webmaster'),
			'label'=>'Вебмастеру',
			'linkOptions' => (Yii::$app->controller->action->controller->id == 'jban-webmaster') ? ['class'=>'current'] : [],
		],
    	[
			'url' => Yii::$app->urlManager->createUrl('/jban/optimizer'),
			'label'=>'Оптимизатору',
			'linkOptions' => (Yii::$app->controller->action->controller->id == 'jban-optimizer') ? ['class'=>'current'] : [],
		],
    ];
    $bar->end();
    echo $content;
$this->endContent(); ?>