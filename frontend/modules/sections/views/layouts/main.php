
<?php $this->beginContent(Yii::$app->view->theme->baseUrl.'/views/layouts/main.php'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2 wrapper_nav">
                <?= $this->render('@app/views/layouts/nav_panel.tpl'); ?>
            </div>
            <div class="col-sm-10 wrapper_content">
                <div class="content-header">
                    <?= Yii::$app->requestedRoute?>
                </div>
                <div class=" row-content-wrapper">
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->endContent(); ?>