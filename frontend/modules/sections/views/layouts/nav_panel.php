<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 06.11.14
 * Time: 15:11
 */
use yii\bootstrap\Nav;
use \app\modules\sections\models\Sections;
?>

<div class="nav-title">Sections</div>
<?php $bar = Nav::begin(['id'=>'navigation']); ?>
<?php $bar->items = array_map(create_function('$model','{return $model->getUrl();}'),Sections::model());?>
<?php $bar->end();?>