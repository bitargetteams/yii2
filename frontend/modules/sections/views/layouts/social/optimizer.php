<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 12.12.14
 * Time: 14:22
 */

use \yii\bootstrap\Nav;
$this->beginContent('@sections/views/layouts/social/main.php');
$bar = Nav::begin(['id'=>'j-ban-webmaster-nav']);
$bar->items = [
	[
		'url' => Yii::$app->urlManager->createUrl('/social/optimizer/listposts'),
		'label'=>Yii::t('social', 'listposts'),
		'linkOptions' => (Yii::$app->controller->action->id == 'listposts') ? ['class'=>'current'] : [],
	],
	[
		'url' => Yii::$app->urlManager->createUrl('/social/optimizer/addpost'),
		'label'=>Yii::t('social', 'addpost'),
		'linkOptions' => (Yii::$app->controller->action->id == 'addpost') ? ['class'=>'current'] : [],
	],
];
$bar->end();
$visitLink = str_replace('sections/social-optimizer/','',Yii::$app->requestedRoute);
$this->params['breadcrumbs'][] = ['label' => Yii::t('sections', 'Social'), 'url' => '/section/social'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Social_optimizer'), 'url' => '/social/optimizer'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', $visitLink), 'url' => '/'.Yii::$app->requestedRoute];
echo $content;
$this->endContent();