<?php

use \app\modules\sections\assets\AssetsJBan;
use \yii\bootstrap\Nav;

AssetsJBan::register($this);
$this->beginContent('@sections/views/layouts/main.php');
    $bar = Nav::begin(['id'=>'jban-nav']);
    $bar->items = [
    	[
			'url' => Yii::$app->urlManager->createUrl('/social/webmaster'),
			'label'=>'Вебмастеру',
			'linkOptions' => (Yii::$app->controller->action->controller->id == 'social-webmaster') ? ['class'=>'current'] : [],
		],
    	[
			'url' => Yii::$app->urlManager->createUrl('/social/optimizer/listposts'),
			'label'=>'Оптимизатору',
			'linkOptions' => (Yii::$app->controller->action->controller->id == 'social-optimizer') ? ['class'=>'current'] : [],
		],
    ];
    $bar->end();
    echo $content;
$this->endContent(); ?>