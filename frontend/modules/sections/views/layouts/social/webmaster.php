<?php

use \yii\bootstrap\Nav;
$this->beginContent('@sections/views/layouts/social/main.php');

$visitLink = str_replace('sections/social-webmaster/','',Yii::$app->requestedRoute);
$this->params['breadcrumbs'][] = ['label' => Yii::t('sections', 'Social'), 'url' => '/section/social'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Social_webmaster'), 'url' => '/social/webmaster'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', $visitLink), 'url' => '/'.Yii::$app->requestedRoute];
    echo $content;
$this->endContent();