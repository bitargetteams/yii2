<?php foreach ($model as $line): ?>
<div class="row attach-audits">
    <div class="col-md-1 att-id id"><?= $line->id;?></div>
    <div class="col-md-5 att-comment "><?= $line->subscribe;?></div>
    <div class="col-md-2 att-status status"><?= $line->status;?></div>
    <div class="col-md-2 att-date created_time"><?= $line->created_time;?></div>
    <?php if ($line->url):?>
        <div class="col-md-2 att-href url"><a class="href" href="<?= $line->url;?>">PDF FILE</a></div>
    <?php endif; ?>
</div>
<?php endforeach; ?>