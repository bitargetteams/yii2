<div class="row list-audit-header">
    <div class="col-md-1 sorted" order="location_id"><?= $model['id']?></div>
    <div class="col-md-5 sorted" order="url"><?= Yii::t('audit','List orders audit')?></div>
    <div class="col-md-2 sorted" order="astatus_id"><?= $model['status']?></div>
    <div class="col-md-2 sorted" order="created_time"><?= $model['created_time']?></div>
    <div class="col-md-1 sorted" order="updated_time"><?= $model['updated_time']?></div>
    <div class="col-md-1 icon-filter">
        <span filter-status = "-90" class='glyphicon glyphicon-filter filter-list'></span>
    </div>
</div>
