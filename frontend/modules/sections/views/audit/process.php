<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 process-partition-audits">
            <label for="process-partition-audits" class="label-default">
                Список заказанных уадитов
            </label>
            <ul id="process-partition-audits">
                <?php foreach($model->partitions as $line):?>
                <li><?= $line->name?><span class="price"><?= $line->price.Yii::t('sections',$line->currency)?></span></li>
                <?php
                    $currency = $line->currency;
                endforeach;
                ?>
                <li><?= Yii::t('audit','Total:')?><span class="price"><?= $model->price_total.Yii::t('sections',$currency)?></span></li>

            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 process-about-audits">
            <?php if(isset($model->products)):?>
            <label for="process-about-audits" class="label-default">
                Товары и услуги
            </label>
            <div id="process-about-audits-extra-info" class="about-audits">
                <?=$model->products; ?>
            </div>
            <?php endif; ?>
            <?php if(isset($model->target_audience)):?>
            <label for="process-about-audits" class="label-default">
                Целевая уадитория
            </label>
            <div id="process-about-audits-extra-info" class="about-audits">
                <?=$model->target_audience; ?>
            </div>
            <?php endif; ?>
            <?php if(isset($model->site_competitors)):?>
            <label for="process-about-audits" class="label-default">
                Конкуренты сайта
            </label>
            <div id="process-about-audits-extra-info" class="about-audits">
                <?=$model->site_competitors; ?>
            </div>
            <?php endif; ?>
            <?php if(isset($model->promotion_problem)):?>
            <label for="process-about-audits" class="label-default">
                Проблеммы при продвижении
            </label>
            <div id="process-about-audits-extra-info" class="about-audits">
                <?=$model->promotion_problem; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 process-attachments-audits" id="audit-job-<?= $model->id?>">
            <?php
            $id = $model->id;
            \frontend\widgets\ActiveItemsList\ActiveItemsListWidget::begin(
                [
                    'listModel' => 'app\modules\sections\models\audit\JobAudit',
                    'template' => '@sections/views/audit/job_list/header.php',
                    'templateItem' => '@sections/views/audit/job_list/items.php',
                    'options' => [
                        'id' => '#audit-job-'.$id
                    ],
                ]
            )->end();?>
        </div>
    </div>
</div>