<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\CHtml;
use frontend\widgets\ActiveCheckboxList;
?>
<?php $form = ActiveForm::begin(['id' => 'create','method'=>'post',]); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <?= CHtml::activeLabel($model,'useAudit',[
                                'class' => '' . CHtml::cssError($model,'useAudit'),
                            ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="createform-useaudit">
                        <?php $chb = ActiveCheckboxList::begin([
                                'model'=>$audits,
                                'form'=>$model,
                                'attribute'=>'useAudit'
                            ]
                        )?>
                        <?php $chb->end();?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-8" id="summary-audit">
                        <?= Yii::t('audit','Total:')?>
                        <span>
                            <span class="total-audit">0</span>
                            <span class="currency-audit"><?= Yii::t('sections','$')?></span>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextInput($model,'url',[
                                'placeholder' => $model->getAttributeLabel('url'),
                                'class' => 'form-control input-sm contact-form__input' . CHtml::cssError($model,'url'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','Address of the site on which an audit should be performed')?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextInput($model,'customer_name',[
                                'placeholder' => $model->getAttributeLabel('name'),
                                'class' => 'form-control input-sm contact-form__input' . CHtml::cssError($model,'name'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','Username')?>
                    </div>
                </div>
            </div>
            <div class="col-md-6"><div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextarea($model,'products',[
                                'placeholder' => $model->getAttributeLabel('service'),
                                'class' => 'form-control form-control' . CHtml::cssError($model,'service'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','Section describing the characteristics of the services provided')?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextarea($model,'target_audience',[
                                'placeholder' => $model->getAttributeLabel('targetAudience'),
                                'class' => 'form-control form-control' . CHtml::cssError($model,'targetAudience'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','Section describing the target audience and consumers of goods and services')?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextarea($model,'site_competitors',[
                                'placeholder' => $model->getAttributeLabel('competitorsSites'),
                                'class' => 'form-control ' . CHtml::cssError($model,'competitorsSites'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','List all of your competitors, separated by commas. Suffice 5-10 most powerful, in your opinion, competitors')?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <?= CHtml::activeTextarea($model,'promotion_problem',[
                                'placeholder' => $model->getAttributeLabel('problemsInPromotion'),
                                'class' => 'form-control ' . CHtml::cssError($model,'problemsInPromotion'),
                            ])?>
                    </div>
                    <div class="col-md-5 subscribe">
                        <?= Yii::t('audit','Describe in free form, the problems that you have encountered')?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3">
                <?= CHtml::submitButton(Yii::t('audit','Order!'),[
                        'class' => 'btn btn-primary btn-sm'
                    ])?>
            </div>
        </div>
    </div>
<?php $form->end(); ?>