<?php foreach ($model as $line): ?>
    <div class="row exist-item-audit list-records">
        <div class="col-md-1 id-list-audit">
            <div class="unique-audit-item id"> <?= $line->id ?></div>
        </div>
        <div class="col-md-5 name-list-audit">
            <div class="name-audit-item customer_name"> <?= $line->customer_name ?></div>
            <div class="url-audit-item url"> <?= $line->url ?></div>
        </div>
        <div class="col-md-2 status-list-audit status">
            <?= $line->status ?>
        </div>
        <div class="col-md-2 date-list-audit created_time">
            <?= $line->created_time ?>
        </div>
        <div class="col-md-2 date-list-audit updated_time">
            <?= $line->updated_time ?>
        </div>
    </div>
    <div class="row process-items-audit">
        <span class="found exists-audit" id="list-item-<?= $line->id ?>"></span>
    </div>

<?php endforeach; ?>