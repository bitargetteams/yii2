<?php
use \app\modules\sections\assets\AssetsAudit;
use \frontend\widgets\ActiveItemsList\ActiveItemsListWidget;
?>
<div class="container-fluid">
    <div class="row">
        <?= $this->render('form/create',['model'=>$model,'audits'=>$audits])?>
    </div>
    <div class="row" id="content">
        <div class="col-md-12">
            <div class="container-fluid list-orders-audit" id="list-orders-audit">
                <?php ActiveItemsListWidget::begin([
                        'listModel' => '\app\modules\sections\models\audit\ListAudits',
                        'template' => '@sections/views/audit/audit_list/header.php',
                        'templateItem' => '@sections/views/audit/audit_list/items.php',
                        'isAjax' => true,
                        'options' => [
                            'id'=>'list-orders-audit'
                        ],
                        'clientOptions' => [
                            'page' =>1,
                            'pageSize' =>5,
                            'itemsAction' => Yii::$app->urlManager->createAbsoluteUrl('/audit/list'),
                            'headerClass' => "list-audit-header"
                        ]
                    ]
                )->end();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
AssetsAudit::register($this);
?>