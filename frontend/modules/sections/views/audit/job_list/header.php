<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 11.11.14
 * Time: 19:50
 */
?>
<div class="row list-att-header">
    <div class="col-md-1 sorted" order="id"><?= $model['id']?></div>
    <div class="col-md-5 sorted" order="subscribe"><?= Yii::t('audit','Works performed by the Audit')?></div>
    <div class="col-md-2 sorted" order="status"><?= $model['status']?></div>
    <div class="col-md-2 sorted" order="created_time"><?= $model['created_time']?></div>
    <div class="col-md-2 sorted" order="filename"><?= $model['url']?></div>
    <div class="icon-filter">
        <span filter-status = "-90" class='glyphicon glyphicon-filter filter-list'></span>
    </div>
</div>
