<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 05.12.14
 * Time: 17:22
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="container-fluid">
	<?php if ($model->isError()) print_r($model->getErrors())?>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'project_title')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'project_url')->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'unset_on_site_error')->textInput() ?>

	<?= $form->field($model, 'unset_on_hits_down')->textInput() ?>

	<?= $form->field($model, 'unset_on_tic_down')->checkbox() ?>

	<?= $form->field($model, 'unset_on_pr_down')->checkbox() ?>

	<?= $form->field($model, 'unset_on_price_up')->textInput() ?>

	<?= $form->field($model, 'project_description')->textarea() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>