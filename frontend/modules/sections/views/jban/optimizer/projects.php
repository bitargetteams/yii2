<?php

use frontend\widgets\DataTable\DataTableWidget;

?>

<div class="container-fluid">

	<?php DataTableWidget::begin([
			'model' => $model,
			'template' => '@sections/views/jban/optimizer/project/header.php',
			'jsFile' => '@sections/includes/scripts/js/project_list.js',
			'options' => [
				'id' => 'data_table',
				'processing' => true,
				'serverSide' => true,
				'searching' => true,
				'ajax' => Yii::$app->urlManager->createAbsoluteUrl('/jban/optimizer/listprojectdata')

			]
		]
	)->end();
	?>

</div>
