<form method="POST" id="form_tmpl_<?= $tmpl->id ?>">
    <input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken() ?>" name="_csrf">
    <? if (count($presets)): ?>
        <?= \yii\helpers\Html::dropDownList('preset_' . $tmpl->id, null, \yii\helpers\ArrayHelper::map($presets, 'id', 'title'), ['id' => 'preset_' . $tmpl->id,'prompt'=>'Не выбрано']) ?>
    <? endif; ?>
    Название: <input name="title" type="text"><br/>
    <input name="tmpl_id" type="hidden" value="<?= $tmpl->id ?>" />
    Основной цвет куба: <input name="color_main" id="colpick_cube_<?= $tmpl->id ?>" type="text"><br/>
    Цвет текста: <input name="color_text" id="colpick_banner_text_<?= $tmpl->id ?>" type="text"><br/>
    Введите адрес сайта: <input name="link" type="text">

    <hr />
    Введите заголовок: <input type="text" name="text_header"><br/>
    Введите текст баннера: <input name="text_body" type="text">

    <div id="jban-<?= $tmpl->id ?>" style="left:400px;top:130px;width: <?= $tmpl->width ?>px;height: <?= $tmpl->height ?>px;position:relative; background: url(/banner.jpg) no-repeat;">
        <div id="j-target-<?= $tmpl->id ?>" class="j-target"></div>
        <div id="j-hint-<?= $tmpl->id ?>" class="jh-block">
            <div id="j-close-<?= $tmpl->id ?>" class="j-close">&#215;</div>
            <div class="j-pointer"></div>
            <div class="j-header j-color">Header 1</div>
            <div class="j-body">
                <div class="j-color j-text">text</div>
                <img class="j-img" src="http://bitby.net/wp-content/uploads/2011/12/flash_banner_po_shirine.png" />
                <div class="j-button j-color">button</div>
            </div>
        </div>
    </div>
    <div style="padding-top: 300px;">
        <input type="submit" value="Сохранить" name="save[banner]">
        <input type="submit" value="Сохранить как шаблон" name="save[preset]">
    </div>
</form>
<script>
    var jL = new jbanLight({
        bannerId: <?= $tmpl->id ?>,
        colorMain: '#497FD4',
        colorSecondary: '#8DC2EF'
    });
    $("#colpick_cube_<?= $tmpl->id ?>").colpick({
        submit: 0,
        onChange: function (obj, hex, rgb, self) {
            jL.setColor(hex);
            $(self).val(hex);
        }
    });
    $("#colpick_banner_text_<?= $tmpl->id ?>").colpick({
        submit: 0,
        onChange: function (obj, hex, rgb, self) {
            jL.setColor(hex);
            $(self).val(hex);
        }
    });
    $("#colpick_cube_<?= $tmpl->id ?>").change(function () {
        jL.setColor($(this).val());
    });
    $("#colpick_banner_text_<?= $tmpl->id ?>").change(function () {
        jL.setColorText($(this).val());
    });

    $('#preset_<?= $tmpl->id ?>').change(function () {
        preset_id = $(this).val();
        $.ajax('/jban/optimizer/getpreset/' + preset_id, {
            method: 'GET',
            success: function (data) {
                data = $.parseJSON(data);
                jL.setColor(data.color_main);
                jL.setColorText(data.color_text);
                $("#colpick_cube_<?= $tmpl->id ?>").val(data.color_main);
                $("#colpick_banner_text_<?= $tmpl->id ?>").val(data.color_text);
            }
        });
    });

</script>
<style>
    #jban-<?= $tmpl->id ?>{position: absolute;}
    #jban-<?= $tmpl->id ?> .jh-block{float: left; box-sizing: content-box; padding-right: 15px;position: absolute;overflow: hidden; width:0;display: none;left:0;top:0;opacity: 0;}
    #jban-<?= $tmpl->id ?> .j-pointer{position: absolute; right: 4px; top: 100px; width: 0; height: 0;
                                      border-top: 16px solid transparent; border-left: 20px solid #68A1CC; border-bottom: 0;
    }
    #jban-<?= $tmpl->id ?> .j-header{box-sizing: content-box;float: left; text-align: center; padding: 10px; width: 280px; background: #5d8eb9; border-bottom: 1px solid #fff; border-radius: 4px 4px 0 0;}
    #jban-<?= $tmpl->id ?> .j-close{position: absolute; top: 3px; right: 19px; padding: 1px 3px; cursor: pointer; color: #fff;}
    #jban-<?= $tmpl->id ?> .j-body{box-sizing: content-box;float: left; padding: 10px; width: 280px; background: #68a1cc; border-radius: 0 0 4px 4px;}
    #jban-<?= $tmpl->id ?> .j-color{color: #fff;}
    #jban-<?= $tmpl->id ?> .j-text{width: 280px;}
    #jban-<?= $tmpl->id ?> .j-img{width: 280px; margin: 10px auto;}
    #jban-<?= $tmpl->id ?> .j-button{width: 100px; padding: 5px; margin: 0 auto; border: 1px solid #fff; text-align: center; cursor: pointer;}
    #jban-<?= $tmpl->id ?> .j-target{width: 90px; height: 90px; position: absolute; top: -25px; left: -25px; z-index: 100;}

</style>

