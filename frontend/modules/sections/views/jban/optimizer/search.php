<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 15.12.14
 * Time: 12:08
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\sections\assets\jban\SearchSitesAssets;

?>
<div class="container-search">

	<?php $form = ActiveForm::begin(['id'=>'search_form']); ?>
	<div class="field-search">
		<label>Site PR</label><br>
		from <input type="number" name="SearchForm[website][rate_pr_min]" value="" /> to <input type="number" name="SearchForm[website][rate_pr_max]" value="" />
	</div>
	<div class="field-search">
		<label>TIC</label><br>
		from <input type="number" name="SearchForm[website][rate_tic_min]" value="" /> to <input type="number" name="SearchForm[website][rate_tic_max]" value="" />
	</div>
	<div class="field-search">
		<label>Website older</label><br>
		<input type="number" name="SearchForm[website][older_days]" value="" /> days
	</div>

	<div class="field-search">
		<label>Site profitable</label><br>
		<input type="radio" name="SearchForm[website][is_profitable]" value="" checked /> All sites
		<input type="radio" name="SearchForm[website][is_profitable]" value="0" /> Not profitable sites
		<input type="radio" name="SearchForm[website][is_profitable]" value="1" /> Profitable sites
	</div>

	<div class="field-search">
		<label>Level</label><br>
		<input type="radio" name="SearchForm[website][domain_level]" value="0" checked /> All levels
		<input type="radio" name="SearchForm[website][domain_level]" value="2" /> Second level
		<input type="radio" name="SearchForm[website][domain_level]" value="3" /> Third level
	</div>
	<div class="field-search">
		<label>Website add time</label><br>
		<select name="SearchForm[website][add_time]">
			<option value="">All</option>
			<option value="1">Today</option>
			<option value="3">3 days</option>
			<option value="7">7 days</option>
			<option value="30">30 days</option>
		</select>
	</div>

	<div class="field-search">
		<label>Catalogues</label><br>
		<input type="checkbox" name="SearchForm[website][cat_dmoz]" value="1" /> DMOZ
		<input type="checkbox" name="SearchForm[website][cat_yandex]" value="1" /> Yandex
		<input type="checkbox" name="SearchForm[website][cat_setsite]" value="1" /> Setsite
	</div>
	<div class="field-search">
		<label>Site topics</label><br>
		<select multiple name="SearchForm[website][topic_id][]" id="topic_id" style="width: 300px;">
			<option value="">All</option>
			<?php foreach($model->getTopics() as $k=>$v) {
				echo '<option value="'.$k.'">'.$v->title.'</option>';
			} ?>
		</select>
	</div>

	<div class="field-search">
		<label>Country</label><br>
		<select multiple name="SearchForm[website][country_id][]" id="country_id" style="width: 300px;">
			<option value="">All</option>
			<?php foreach($model->getCountries() as $k=>$v) {
				echo '<option value="'.$k.'">'.$v->title.'</option>';
			} ?>
		</select>
	</div>
	<div class="field-search">
		<label>Domain zones</label><br>
		<select multiple name="SearchForm[website][domain_zone][]" id="domain_zone" style="width: 300px;">
			<?php foreach($model->getDomainZones() as $k=>$v) {
				echo '<option value="'.$k.'">' . $v . '</option>';
			} ?>
		</select>
	</div>

	<hr>

	<div class="field-search">
		<label>Page PR</label><br>
		from <input type="number" name="SearchForm[page][rate_pr_min]" value="" /> to <input type="number" name="SearchForm[page][rate_pr_max]" value="" />
	</div>
	<div class="field-search">
		<label>Page day attendance</label><br>
		from <input type="number" name="SearchForm[page][attendance_d_min]" value="" /> to <input type="number" name="SearchForm[page][attendance_d_max]" value="" />
	</div>
	<div class="field-search">
		<label>Page week attendance</label><br>
		from <input type="number" name="SearchForm[page][attendance_w_min]" value="" /> to <input type="number" name="SearchForm[page][attendance_w_max]" value="" />
	</div>
	<div class="field-search">
		<label>Page month attendance</label><br>
		from <input type="number" name="SearchForm[page][attendance_m_min]" value="" /> to <input type="number" name="SearchForm[page][attendance_m_max]" value="" />
	</div>
	<div class="field-search">
		<label>Page level</label><br>
		<input type="checkbox" name="SearchForm[page][level][]" value="1" /> Main
		<input type="checkbox" name="SearchForm[page][level][]" value="2" /> Second level
		<input type="checkbox" name="SearchForm[page][level][]" value="3" /> Third level
		<input type="checkbox" name="SearchForm[page][level][]" value="4" /> Foгrth level
	</div>
	<div class="field-search">
		<label>Banners</label><br>
		<select multiple name="SearchForm[banner][]" id="banner" style="width: 300px;">
			<?php foreach($model->getBanners() as $k=>$v) {
				echo '<option value="'.$k.'">' . $v->width . ' x '. $v->height . '</option>';
			} ?>
		</select>
	</div>

	<hr>

	<div class="field-search">
		<label>Website limit</label><br>
		<select name="SearchForm[limit][website]">
			<?php foreach($model->getWebsiteLimitFlds() as $k=>$v) {
				echo '<option value="'.$k.'">' . $v . '</option>';
			} ?>
		</select>
	</div>

	<div class="field-search">
		<label>Page limit</label><br>
		<select name="SearchForm[limit][page]">
			<?php foreach($model->getPageLimitFlds() as $k=>$v) {
				echo '<option value="'.$k.'">' . $v . '</option>';
			} ?>
		</select>
	</div>
	<hr>

	<div class="field-search">
		<label>Order</label><br>
		<select name="SearchForm[order]">
			<option value=""></option>
			<option value="rate_tic">TIC</option>
			<option value="rate_pr">PR</option>
			<option value="price">Price</option>
		</select>
	</div>
	<hr>

	<div class="field-search" id="settings">
		<label>Saved settings</label><br>
		<?php foreach($model->getSettings() as $k=>$v) {
			echo '<div id="settings_'.$k.'"><a href="#" class="saved_settings">' . $v->title . '</a> <a href="#" class="delete_settings">Delete</a> <a href="#" class="update_settings">Update</a></div>';
		} ?>
	</div>

	<br>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('jban', 'Search'), ['class' => 'btn btn-success']) ?>

		<?= Html::Button(Yii::t('jban', 'Save settings'), ['class' => 'btn btn-success', 'id' => 'save_settings']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<?php
SearchSitesAssets::register($this);
?>