<?php

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use app\modules\sections\widgets\AlphabeticalHeaderWidget;

$this->title = Yii::t('jban', $show);
?>
<div class="jban_list_show">

<h1><?= Html::encode($this->title) ?></h1>
<div class='col-sm-9'>
<?php $provider = new ArrayDataProvider([
    'allModels' => $items,
    'pagination' => [
        'pageSize' => 50,
    ],
]);
?>
<?php echo Html::beginForm('./'.$update,'post',['id'=>'AddForm']);?>
<?php echo Html::hiddenInput('list_id', $list_id); ?>
<?php echo Html::textarea('addItems', '', ['cols' => 56, 'rows' => 8]);?>
<?php echo Html::submitInput(\Yii::t('jban', 'Add domains')); ?>
<?php echo Html::endForm();?>
            <?= AlphabeticalHeaderWidget::widget(['keys' => $keys]);?>
<?php echo Html::beginForm('./'.$update,'post',['id'=>'DeleteForm']);?>
<?php echo ListView::widget([
    'dataProvider' => $provider,
    'itemView' => function($data) {
        echo '<div class="col-sm-12">',
             Html::checkbox('item[]', false, ['value' => $data['id'],'label' => $data['item'],]),
        '</div>';
    }
]); ?>

<?php echo Html::submitInput(\Yii::t('jban', 'Delete selected'), ['name' => 'delete']); ?>
<?php echo Html::submitInput(\Yii::t('jban', 'Move selected to:'), ['name' => 'move']);?>
<?php echo Html::dropDownList('destination', $list_id, $dropDownList); ?>
<?php echo Html::endForm();?>
<br />
</div>
<div class='col-sm-3'>
<?php echo Html::beginForm('./'.$show,'post',['id'=>'ListForm']);?>
<?php echo Html::radioList('list_id', $list_id, $dropDownList, ['id' => 'lid']); ?>
<?php echo Html::endForm();?>
<?php echo Html::beginForm('./'.$update,'post',['id'=>'AddListForm']);?>
<?php echo Html::label('Add new list', 'addList'); ?>
<?php echo Html::textInput('addList');?>
<?php echo Html::submitInput(\Yii::t('jban', 'Add list')); ?>
<?php echo Html::endForm();?>
</div>
</div>

<script>
$(document).ready(function() {
    $('input[name="list_id"]').on('change', function(){
        $('#ListForm').submit();
    });
});
</script>