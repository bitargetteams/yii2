<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 19.11.14
 * Time: 12:01
 */
use \frontend\helpers\CHtml;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sections\models\jban\webmaster\InstallSitesForm */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(['method' => 'POST',]);?>
<div class="container-fluid">
    <div class="row header">
        <div class="col-md-2">Сайт</div>
        <div class="col-md-2">Path</div>
        <div class="col-md-2">Категория</div>
        <div class="col-md-1">Раз.Яз.</div>
        <div class="col-md-5">Информация</div>
    </div>
    <?php $post = Yii::$app->request->getBodyParam("siteForm");?>
    <?php foreach($links as $id=>$site):?>
        <?php $model[$id]->setFormName("siteForm[$id]");?>
        <div class="row list-sites">
            <div class="col-md-2"><?= $form
                    ->field($model[$id],'site',['template' => '{input}'])
                    ->textInput(
                        [
                            'class' => 'form-control input-sm contact-form__input',
                        ]
                    )
                ?>
            </div>
            <div class="col-md-2"><?= $form
                    ->field($model[$id],'path',['template' => '{input}'])
                    ->textInput([
                            'class' => 'form-control input-sm contact-form__input',
                        ]
                    )
                ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model[$id],'topic_id',['template' => '{input}'])->dropDownList($topics,[
                        'class' => 'form-control select-sm',
                    ])?>
            </div>
            <div class="col-md-1">
                <?= $form->field($model[$id],'prg_lg_id',['template' => '{input}'])->dropDownList($prg_lg,[
                        'class' => 'form-control select-sm',
                    ])?>
            </div>
            <?php
                $info = !isset($model[$id]->errors['path']) ?
                    $post ?
                        Yii::t('site_answer','6cd2422b04ead2ea9c5537260cb628ec59d48cbc2a0c1356b410cf2adac6be3d') :
                        CHtml::activeLabel($model[$id],'path') :
                    CHtml::error($model[$id],'path')
            ?>
            <div class="col-md-5"><?=$info?></div>
        </div>

    <?php endforeach; ?>

    <button class="btn btn-primary btn-sm">Установить</button>
    <?php $form ->end()?>
</div>
