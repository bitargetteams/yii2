<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jban_website */

$this->title = Yii::t('jban', 'addwebsite');
?>
<div class="jban_website-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_website_form', [
		'model' => $model,
		'topics' => $topics,
	]) ?>

</div>
