<?php

?>
<input type="hidden" id="website_id" value="<?=$model->getWebsiteID()?>">
<table id="<?=$id?>" class="display" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th></th>
		<th>Адрес страницы</th>
		<th>Уровень</th>
		<th>PR</th>
		<th>Свободно мест</th>
		<th>Всего мест</th>
		<th>Статус</th>
		<th>Банеры</th>
		<th>Цены</th>
		<th></th>
	</tr>
	</thead>
	<tfoot>
	<tr>
		<th></th>
		<th></th>
		<th>
			<select id="level_select">
				<option value="">Все</option>
				<option value="1">Главная</option>
				<option value="2">Уровень 2</option>
				<option value="3">Уровень 3</option>
				<option value="4">Уровень 4</option>
			</select>
		</th>
		<th></th>
		<th></th>
		<th></th>
		<th>
			<select id="status_select">
				<option value="">Все</option>
				<option value="enabled">Активные</option>
				<option value="disabled">Неактивные</option>
				<option value="banned">Забаненные</option>
				<option value="archived">Удаленные</option>
				<option value="disabled_before_reindex">Неактивные до переиндексации</option>
			</select>
		</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	</tfoot>
</table>