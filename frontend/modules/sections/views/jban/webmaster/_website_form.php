<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\widgets\geodata\GeodataWidget;
use frontend\widgets\ClientFolder\ClientFolderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Jban_website */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jban_website_create-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'url')->textInput(['maxlength' => 100]) ?>

	<?php GeodataWidget::begin([
			'options' => [
				'id'=>'geodata',
				'Country'=>'Страна',
				'Region'=>'Регион',
				'City'=>'Город',
			],
			'model' => $model,
			'form' => $form,
		]
	)->end();
	?>

	<?= $form->field($model, 'topic_id')->dropDownList($topics) ?>

	<?php ClientFolderWidget::begin([
			'options' => [
				'id'=>'client_folder',
				'title'=>'Папка',
				'section'=>'jban',
			],
			'model' => $model,
		]
	)->end();
	?>


	<div class="form-group">
		<?= Html::submitButton(Yii::t('jban', 'Create'), ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>


</div>
