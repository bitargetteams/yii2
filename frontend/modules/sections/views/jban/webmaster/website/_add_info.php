<div class="col-md-8" style="font-size: 10px;">
	<div role="tabpanel">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#pages" aria-controls="pages" role="tab" data-toggle="tab">Pages</a></li>
			<li role="presentation"><a href="#banners" aria-controls="banners" role="tab" data-toggle="tab">Banners</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="pages">
				<div>
					<div class="col-md-3"></div>
					<div class="col-md-2">Всего страниц</div>
					<div class="col-md-2">Свободно мест</div>
					<div class="col-md-2">Всего мест</div>
					<div class="col-md-2">Занято мест</div>
				</div>
				<br style="clear: both">
				<?php foreach($website->pages_stat as $k=>$items): ?>
					<div>
						<div class="col-md-3"><?='Level '.$k?></div>
						<div class="col-md-2"><?=$website->pages_stat[$k]['all']?></div>
						<div class="col-md-2"><?=$website->pages_stat[$k]['free_places']?></div>
						<div class="col-md-2"><?=$website->pages_stat[$k]['all_places']?></div>
						<div class="col-md-2"><?=($website->pages_stat[$k]['all_places']-$website->pages_stat[$k]['free_places'])?></div>
					</div>
					<br style="clear: both">
				<?php endforeach ?>
			</div>
			<div role="tabpanel" class="tab-pane" id="banners">
				<div>
					<div class="col-md-3"></div>
					<div class="col-md-3">Всего мест</div>
					<div class="col-md-3">Свободно мест</div>
					<div class="col-md-3">Занято мест</div>
				</div>
				<br style="clear: both">
				<?php foreach($website->banners_stat as $k=>$items): ?>
					<?php if (count($items) > 0) { ?>
						<div>
							<div class="col-md-12" style="background-color: #f5f5f5"><?='Level '.$k?></div>
						</div>
						<br style="clear: both">
						<?php foreach($items as $name=>$item): ?>
							<div>
								<div class="col-md-3"><?=$name?></div>
								<div class="col-md-3"><?=$item['all']?></div>
								<div class="col-md-3"><?=$item['free']?></div>
								<div class="col-md-3"><?=($item['all']-$item['free'])?></div>
							</div>
							<br style="clear: both">
						<?php endforeach ?>
					<?php } ?>
				<?php endforeach ?>
			</div>
		</div>

	</div>
</div>
<div class="col-md-4" style="font-size: 14px;">
	<ul>
		<li>Тематика: <a href="#"><?=$website->site_topic->title?></a></li>
		<li>Проверка ТИЦ: <a href="#">Обновить</a></li>
		<li>Проверка Pr: <a href="#">Обновить</a></li>
		<li>Проверка Каталога: <a href="#">Обновить</a></li>
		<li>Переиндексация: (<?=$website->last_scan_time?>) <a href="#">Переиндексировать</a></li>
		<li>Проверка бана в Яндексе: <a href="#">Перепроверить</a></li>
	</ul>
</div>