<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\widgets\DataTable\DataTableWidget;

?>
<div class="container-fluid">

	<?php $form = ActiveForm::begin(); ?>



	<?php DataTableWidget::begin([
			'model' => $model,
			'template' => '@sections/views/jban/webmaster/pages/header.php',
			'jsFile' => '@sections/includes/scripts/js/page_list.js',
			'options' => [
				'id' => 'data_table',
				'processing' => true,
        		'serverSide' => true,
				'searching' => true,
				'ajax' => Yii::$app->urlManager->createAbsoluteUrl('/jban/webmaster/listpagesdata/' . $model->getWebsiteID())
			]
		]
	)->end();
	?>


	<?= $form->field($model, 'status')->dropDownList([
		'enabled'=>'Активировать',
		'disabled'=>'Деактивировать',
		'archived'=>'Удалить навсегда',
		'disabled_before_reindex'=>'Удалить до переиндексации'
	], ['style'=>'width:250px;'])?>

	<div class="form-group">
		<?= Html::submitButton('Apply', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>


</div>
