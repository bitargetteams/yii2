<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 02.12.14
 * Time: 16:27
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\widgets\DataTable\DataTableWidget;

?>
<div class="container-fluid">

	<?php $form = ActiveForm::begin(); ?>

	<?php DataTableWidget::begin([
			'model' => $model,
			'template' => '@sections/views/jban/webmaster/pages/price_header.php',
			'jsFile' => '@sections/includes/scripts/js/page_price_list.js',
			'options' => [
				'id' => 'data_table',
				'searching' => false,
				'ordering' => false,
				'titles' => $model->getLevelTitles()
			]
		])->end();
	?>

	<div class="form-group">
		<?= Html::submitButton('Apply', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
