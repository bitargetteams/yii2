<?php

//use frontend\widgets\DataTable\DataTableWidget;
use frontend\assets\DataTableAsset;
use app\modules\sections\assets\jban\WebsiteAssets;

DataTableAsset::register($this);
?>

<div class="container-fluid">

	<table id="data_table" class="display" cellspacing="0" width="100%">
		<thead>
		<tr>
			<th>Адрес сайта</th>
			<th>Статус</th>
			<th>PR</th>
			<th>ТИЦ</th>
			<th>Кол-во страниц</th>
			<th>Кол-во мест</th>
			<th>Цены</th>
			<th></th>
			<th></th>
		</tr>
		</thead>
		<tfoot>
		<tr>
			<th>Адрес сайта</th>
			<th>Статус</th>
			<th>PR</th>
			<th>ТИЦ</th>
			<th>Кол-во страниц</th>
			<th>Кол-во мест</th>
			<th>Цены</th>
			<th></th>
			<th></th>
		</tr>
		</tfoot>
	</table>
	<?php /*DataTableWidget::begin([
			'model' => $model,
			'template' => '@sections/views/jban/webmaster/website/header.php',
			'jsFile' => '@sections/includes/scripts/js/website_list.js',
			'options' => [
				'id' => 'data_table',
				'processing' => true,
				'serverSide' => true,
				'searching' => true,
				'ajax' => Yii::$app->urlManager->createAbsoluteUrl('/jban/webmaster/listwebsitedata')

			]
		]
	)->end();*/
	?>

</div>

<?php
WebsiteAssets::register($this);
?>