<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 25.11.14
 * Time: 12:55
 * @var array $prg_lg list of dev language
 * @var array $topics list of sites topics
 */
use \frontend\helpers\CHtml;
use \yii\bootstrap\ActiveForm;
$post = Yii::$app->request->getBodyParam("InstallSitesForm");?>
<div class="row list-sites">
    <?php $form = ActiveForm::begin(['method' => 'POST','action' => '/jban/webmaster/listinstallsite']);?>
    <div class="col-md-2">
        <?= $form
            ->field($model, 'site', ['template' => '{input}'])
            ->textInput(
                [
                    'class' => 'form-control input-sm contact-form__input',
                ]
            )
        ?>
    </div>
    <div class="col-md-2"><?= $form
            ->field($model, 'path', ['template' => '{input}'])
            ->textInput(
                [
                    'class' => 'form-control input-sm contact-form__input',
                ]
            )
        ?>
    </div>
    <div class="col-md-2">
        <?= $form->field($model, 'topic_id', ['template' => '{input}'])->dropDownList(
            $topics, [
                'class' => 'form-control select-sm',
            ]
        ) ?>
    </div>
    <div class="col-md-1">
        <?= $form->field($model, 'prg_lg_id', ['template' => '{input}'])->dropDownList(
            $prg_lg, [
                'class' => 'form-control select-sm',
            ]
        ) ?>
    </div>
    <div class="col-md-5">
    <?php
    if (!$model->hasErrors() && !is_null($post)) {
        echo Yii::t('site_answer', '6cd2422b04ead2ea9c5537260cb628ec59d48cbc2a0c1356b410cf2adac6be3d');
    }elseif(!$model->hasErrors()){
        echo CHtml::activeLabel($model, 'path');
    }else{
        foreach($model->getErrors() as $key=>$val)
            echo CHtml::error($model, $key);
    }
    ?>
    </div>
    <?php $form->end(); ?>
</div>