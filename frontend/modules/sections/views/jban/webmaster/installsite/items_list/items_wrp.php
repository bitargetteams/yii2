<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 25.11.14
 * Time: 12:42
 */
use \frontend\helpers\CHtml;
?>

<div class="row install-site-list">
    <div class="container-fluid items-install-site">
    </div>
</div>
<div class="row">
    <div class="col-md-12 panel-install-site">
        <button id="install-site" class="btn btn-primary btn-sm">Установить</button>
        <button id="add-site" class="btn btn-primary btn-sm">Добавить</button>
    </div>
</div>