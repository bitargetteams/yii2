<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 25.11.14
 * Time: 13:07
 */
use \frontend\widgets\ActiveItemsList\ActiveItemsListWidget;
use \app\modules\sections\assets\JBanInstallSiteAssets;

?>
<div class="container-fluid install-site-jban" id="install-site-jban">
    <?php ActiveItemsListWidget::begin(
        [
            'listModel' => '\app\modules\sections\models\jban\webmaster\InstallSitesForm',
            'template' => '@sections/views/jban/webmaster/installsite/items_list/header.php',
            'templateItem' => '@sections/views/jban/webmaster/installsite/items_list/items_wrp.php',
            'isAjax' => true,
            'options' => [
                'id' => 'install-site-jban'
            ],
            'clientOptions' => [
                'page' => 1,
                'pageSize' => 5,
                'itemsAction' => Yii::$app->urlManager->createAbsoluteUrl('jban/webmaster/listinstallsite'),
                'headerClass' => "list-install-site-header",
                'wrapperClass'    =>  'items-install-site'
            ]
        ]
    )->end();
    ?>
</div>
<?php
JBanInstallSiteAssets::register($this);
?>