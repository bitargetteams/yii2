<?php
	use yii\helpers\Html;
	use yii\widgets\LinkPager;
    use \yii\bootstrap\Alert;
    //use frontend\widgets\Alert as Alert2;

$widget_classes=[];
$widget_classes[0]="alert-danger";
$widget_classes[1]="alert-success";
$widget_classes[2]="alert-info";
?>


				<div class="items">
					<?php foreach($items as $item): ?>

						<li class="news-block__item">
<?php /* <h2 class="news-block__title">message: <?php echo $item->message ?>; color: <?php echo $item->color ?></h2> */ ?>
                        <?php
                        echo Alert::widget([
                                 'body' => $item->message,
                                'options' => ["id" => $item->id, "class" => $widget_classes[$item->color]],
                            ]); ?>
						</li>
					<?php endforeach ?>
				</div>


		<?= LinkPager::widget(['pagination' => $pagination]) ?>

