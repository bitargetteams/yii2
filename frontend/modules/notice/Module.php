<?php

namespace app\modules\notice;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\notice\controllers';

    public $layout = null;

    public $type = 'default';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
