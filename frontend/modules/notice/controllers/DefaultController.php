<?php

namespace app\modules\notice\controllers;

use yii\web\Controller;
use yii\data\Pagination;
//use frontend\models\Notice;
use app\components\Api;

class DefaultController extends Controller
{

	public function actionIndex($color = '0',$page='1',$client_id=1)
    {
        $limit='10';
        $offset=($page-1)*$limit;
        //$client_id='1';
        //$color='0';

/*
 * \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * \Yii::$app->getSession()->setFlash('info', 'This is the message');
 */
		$pagination = new Pagination([
			'defaultPageSize' => $limit,
		]);
        $pagination->setPage($page-1);
//			'totalCount' => $items->count,

        $query_params=["client_id"=>$client_id, 'page'=>$pagination->getPage()+1, 'per-page'=>$pagination->limit];
        if($color){$query_params["color"]=$color;}

        $items = Api::resource("notice")->get($query_params);

        $pagination->totalCount = $items->power;

		return $this->render('index', [
			'items' => $items->response,
			'pagination' => $pagination,
		]);
    }

}
