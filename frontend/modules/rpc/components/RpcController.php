<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 18.11.14
 * Time: 15:08
 */
namespace app\modules\rpc\components;
use \yii\web\Controller;

class RpcController extends Controller{
    public function actions()
    {
        return array(
            'rpc' => array(
                'class' => '\bitargetteams\jsonRpc\Action',
            ),
        );
    }
} 