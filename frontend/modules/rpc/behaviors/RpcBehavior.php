<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 17.11.14
 * Time: 19:11
 */
namespace app\modules\rpc\behaviors;

use yii\base\Behavior;
use yii\web\Controller;

class RpcBehavior extends Behavior {
    public $enableCsrfValidation= true;

    public function events()
    {
        return array_merge(parent::events(),
            [
                Controller::EVENT_BEFORE_ACTION => 'beforeAction'
            ]
        );
    }

    public function beforeAction($event)
    {
        $event->action->controller->enableCsrfValidation = $this->enableCsrfValidation;
    }
} 