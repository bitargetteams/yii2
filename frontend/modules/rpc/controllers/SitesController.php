<?php

namespace app\modules\rpc\controllers;

use app\components\Api;
use app\modules\rpc\components\RpcController;
use app\modules\sections\models\Sections;

class SitesController extends RpcController
{
    /**
     * @name         Install
     *
     * @param array  $serverInfo array info of requested server {hash,ip,domain}.
     *
     * @return string json encode
     * @rpc
     */
    public function getInstall($serverInfo)
    {
        $response = Api::resource('section_script/get-lib')->post(
            [
                'section_id' => Sections::model('jban')[0]->id,
                'lang_code_id' => \Yii::$app->cache->get($serverInfo['hash']),
                'hash' => $serverInfo['hash'],
                'ip' => $serverInfo['ip'],
                'site' => $serverInfo['domain']
            ]
        );
        \Yii::$app->cache->delete($serverInfo['hash']);
        if (!isset($response->response)) {
            return '3857d42b0649048676996a715eba0406d71af0eb346f8a13c2eeb095287ea5c7';
        }
        if (is_null($serverInfo['hash']) || !isset($serverInfo['hash'])) {
            return '126561490cc19e5d9012185ef1df31096a754d5346de791384ecbdc6b015ffb1';
        }
        return $response->response->codeInstall;
    }


    /**
     * @param $serverInfo
     *
     * @return bool|mixed
     * TODO почистить кэш, трансопорт рукалицо
     */
    public function success($serverInfo)
    {
        $ip = $serverInfo['ip'];
        $domain = $serverInfo['domain'];
        $success = @Api::resource('section_script/install-success')->post(
            [
                'hash' => $serverInfo['hash'],
                'ip' => $ip,
                'site' => $domain,
                'server_protocol' => $serverInfo['protocol'],
            ]
        );
        if (isset($success->response) && $success->response) {

            \Yii::$app->cache->delete($domain . $ip);
            return true;
        }
        return false;
    }
}