<?php

namespace app\modules\rpc;
use \Yii;
use \yii\base\Module as BaseModule;
class Module extends BaseModule
{
    public $controllerNamespace = 'app\modules\rpc\controllers';


    public function behaviors()
    {
        return [
            'main' => [
                'class' => 'app\modules\rpc\behaviors\RpcBehavior',
                'enableCsrfValidation' => false
            ]
        ];
    }

    public function init()
    {
        $configPath = Yii::getAlias('@frontend/modules/rpc/config/main.php');
        parent::init();
        // custom init  ialization code goes here
    }

}
