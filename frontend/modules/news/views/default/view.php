<?php
use yii\helpers\Html;
?>
<div class="content-title"><h1 class="content-title__title">Новости системы</h1></div>
<aside class="sidebar">
	<form class="search">
		<div class="form-group">
			<div class="input-group">
				<input type="text" name="text" placeholder="Поиск" class="form-control search__input">
                <span class="input-group-btn">
                    <button type="button" class="btn search__button">
						<span class="fui-search"></span>
					</button>
                </span>
			</div>
		</div>
	</form>
	<div id="datepicker" class="calendar"></div>
</aside>
<article class="news-content">
	<div class="news-block">
		<ul class="news-block__list">
			<div id="yw0" class="list-view">
				<div class="summary"></div>

				<div class="items">
						<li class="news-block__item">
							<h2 class="news-block__title"><?php echo $item->title ?></h2>
							<div class="news-block__date"><?php echo $item->news_time ?></div>
							<div class="news-block__text"><?php echo $item->text ?>
						</li>
				</div>
			</div>
		</ul>
	</div>
</article>