<?php
	use yii\helpers\Html;
	use yii\widgets\LinkPager;
?>
<div class="content-title"><h1 class="content-title__title">Новости системы</h1></div>
<aside class="sidebar">
	<!-- nav class="news-nav">
		<ul class="news-nav__list">
			<li class="news-nav__item">
				<a class="news-nav__link" href="/index.php/news/index/1">Ссылки</a>
			</li>
			<li class="news-nav__item">
				<a class="news-nav__link" href="/index.php/news/index/2">Аудит</a>
			</li>
		</ul>
	</nav -->
	<form class="search">
		<div class="form-group">
			<div class="input-group">
				<input type="text" name="text" placeholder="Поиск" class="form-control search__input">
                <span class="input-group-btn">
                    <button type="button" class="btn search__button">
						<span class="fui-search"></span>
					</button>
                </span>
			</div>
		</div>
	</form>
	<div id="datepicker" class="calendar"></div>
</aside>
<article class="news-content">
	<div class="news-block">
		<ul class="news-block__list">
			<div id="yw0" class="list-view">
				<div class="summary"></div>

				<div class="items">
					<?php foreach($news as $item): ?>
						<li class="news-block__item">
							<h2 class="news-block__title"><?php echo $item->title ?></h2>
							<div class="news-block__date"><?php echo $item->news_time ?></div>
							<div class="news-block__text"><?php echo $item->teaser ?><br />
								<a style="font-size: 16px;" href="/news/view/<?php echo $item->id ?>">Читать >> </a>
							</div>

						</li>
					<?php endforeach ?>
				</div>
			</div>
		</ul>
		<?= LinkPager::widget(['pagination' => $pagination]) ?>
	</div>
</article>