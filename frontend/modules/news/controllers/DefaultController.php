<?php

namespace app\modules\news\controllers;

use yii\web\Controller;
use frontend\models\News;
use yii\data\Pagination;

class DefaultController extends Controller
{


	public function actionIndex($date = '')
    {

		$query = News::find();

		$pagination = new Pagination([
			'defaultPageSize' => 3,
			'totalCount' => $query->count(),
		]);

		$news = $query->orderBy('news_time DESC')
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		return $this->render('index', [
			'news' => $news,
			'pagination' => $pagination,
		]);
    }

	public function actionView($id = 0)
	{
		$CurrentNews = News::findOne($id);

		return $this->render('view', [
			'item' => $CurrentNews,
		]);
	}

	public function actionBydate($date = 0)
	{
		$query = News::find();

		$pagination = new Pagination([
			'defaultPageSize' => 3,
			'totalCount' => $query->filterWhere(['news_time' => $date])->count(),
		]);

		$news = $query->orderBy('news_time DESC')
			->filterWhere(['news_time' => $date])
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		return $this->render('index', [
			'news' => $news,
			'pagination' => $pagination,
		]);
	}


}
