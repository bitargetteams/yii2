<?php

namespace app\modules\mail;

use \yii\base\Module;
/**
 * Class MailTemplate
 *
 * @package app\modules\mail
 */
class MailTemplate extends Module
{
    /**
     * @var null
     */
    public $body = null;
    /**
     * @var null|string
     */
    protected  $subject = null;
    /**
     * @var null|string
     */
    protected  $action = null;
    /**
     * @var array|null
     */
    protected  $paramsAction = null;
    /**
     * @var null|string
     */
    protected  $controller = null;
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\mail\controllers';

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return $this
     */
    protected  function Activate()
    {
        $this->runAction($this->controller . '/' . $this->action,$this->paramsAction);
        return $this;
    }

    /**
     * @param string $subject
     * @param string $action
     * @param array  $paramsAction
     * @param string $controller
     * @param string $layout
     */
    public function __construct($subject='',$action='index',$paramsAction=[],$controller = 'default',$layout='main')
    {
        $this->subject = $subject;
        $this->action = $action;
        $this->paramsAction = $paramsAction;
        $this->controller = $controller;
        $this->layout = $layout;
        
        return $this->Activate();

    }

    /**
     * @return null|string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param \yii\base\Action $controller
     * @param mixed            $action
     *
     * @return bool|mixed
     */
    public function afterAction($controller, $action)
    {
        if(parent::afterAction($controller, $action))
        {
            $this->body = $controller->controller->exView;
            return true;
        }
        else {
            return false;

        }

    }


}
