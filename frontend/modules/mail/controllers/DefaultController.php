<?php

namespace app\modules\mail\controllers;

use app\modules\mail\components\MailController;

/**
 * Class MailController
 *
 * @package app\modules\mail\controllers
 */
class DefaultController extends MailController
{
    /**
     * @param $id
     * @param $email
     *
     * @return bool
     */
    public function actionConfirmPassword($id,$email)
    {
        $this->exView = $this->render('confirmPassword',['token'=>$id,'email' => $email]);
        return true;
    }

    public function actionResetPassword($id,$hash)
    {
        $this->exView = $this->render('resetPassword',['id'=>$id,'hash' => $hash]);
        return true;
    }
    
    public function actionSendInvite($hash,$email)
    {
        $this->exView = $this->render('sendInvite',['email'=>$email,'hash' => $hash]);
        return true;
    }
}
