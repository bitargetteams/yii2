<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 24.10.14
 * Time: 16:22
 */

namespace app\modules\mail\behaviors;

use \yii\base\Behavior;
use yii\web\Controller;

class MailBehavior extends Behavior {
    public $css = null;

    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'getTemplate'
        ];
    }

    public function getTemplate($event)
    {
    }
} 