<?php
/* @var $this \yii\web\View */
/* @var $content string */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?= Yii::$app->language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Really Simple HTML Email Template</title>
    <style><?=  $this->renderFile('@app/modules/mail/css/default.php');?></style>
    <?php $this->head() ?>
</head>
<body bgcolor="#f6f6f6">
<?php $this->beginBody() ?>
<?= $this->render('_header');?>
<?= $content ?>
<?= $this->render('_footer');?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>