<!-- body -->
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <p>Notification,</p>
                            <h1>Confirm email address</h1>
                            <p>This is a really simple email template. It's sole purpose is to get you to click the button below.</p>
                            <h2>Follow the link </h2>
                            <p>To confirm email.</p>
                            <table>
                                <tr>
                                    <td class="padding">
                                        <p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['user/confirm',
                                                    'id' => $token, 'email' => $email])?>" class="btn-primary">Confirm</a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->
        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->

