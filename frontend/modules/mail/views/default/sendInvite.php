<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <!-- content -->
            <div class="content">
                <table>
                    <tr>
                        <td>
                            <p>Notification,</p>
                            <h1>You will be issued access to the project</h1>
                            <table>
                                <tr>
                                    <td class="padding">
                                        <p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['user/registration',
                                                    'email'=>$email,'hash' => $hash])?>" class="btn-primary"><?=Yii::t('registration','Register')?></a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /content -->
        </td>
        <td></td>
    </tr>
</table>
<!-- /body -->