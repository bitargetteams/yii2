<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 20.11.14
 * Time: 15:42
 */

namespace app\modules\iterator;


class ObjectsArrIterator implements \Iterator {
    private $_list;
    private $_current;
    private $_keys;

    function __construct(responseIterator $obj) {
        $this->_list = $obj;
        $this->_keys = $this->_list->keys();
    }

    function rewind() {
        $this->_current = 0;
    }

    function valid() {
        return $this->_current < $this->_list->count();
    }

    function key() {
        return $this->_keys[$this->_current];
    }

    function next() {
        $this->_current++;
    }

    function current() {
        return $this->_list->offsetGet($this->_keys[$this->_current]);
    }
}