<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 20.11.14
 * Time: 14:56
 */

namespace app\modules\iterator;
use app\modules\iterator\ObjectsArrIterator;


class responseIterator implements \IteratorAggregate, \ArrayAccess, \Countable{
    private $_response;
    private $_is_array=false;

    public function is_array(){
        return $this->_is_array;
    }

    public function __construct($response,$is_array=true){
        $this->_response=$response;
        $this->_is_array=$is_array;
        return $this;
    }

    public function __get($name){
        if(isset($this->_response[0])) {
            return $this->_response[0]->$name;
        }
        return false;
    }

    public function offsetSet($key, $value) {
        $this->_response[$key]=$value;
    }

    public function offsetUnset($key) {
        unset($this->_response[$key]);
    }

    public function offsetGet($key) {
        return $this->_response[$key];
    }

    public function offsetExists($key) {
        return isset($this->_response[$key]);
    }

    public function count() {
        return count($this->_response);
    }

    function getIterator() {
        return new ObjectsArrIterator (clone $this);
    }

    public function keys() {
        return array_keys($this->_response);
    }
    
    public function toArray(){
        return $this->_response;
    }

} 