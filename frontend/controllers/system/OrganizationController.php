<?php

namespace frontend\controllers\system;

use Yii;
use yii\web\Controller;
use frontend\models\forms\system\organization\OrganizationForm;
use frontend\models\forms\system\organization\SettingsForm;
use frontend\models\forms\system\AssistantForm;
use frontend\models\InviteMail;
use app\components\Api;
use yii\helpers\ArrayHelper;
use app\models\Country;

class OrganizationController extends Controller
{
    
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        $orgs = Api::resource('client/organization')->get(['per-page' => 1]);
        if (!$orgs->power){
            return $this->redirect('./create');
        } else {
            return $this->redirect('./settings');
        }
    }
    
    public function actionCreate()
    {
        $model = new OrganizationForm();
        if (!\Yii::$app->request->post()) {
            return $this->render('create', ['model' => $model]);
        }
        $model->load(\Yii::$app->request->post());
        if (!$model->validate()) {
            return $this->render('create', ['model' => $model]);
        }
        $clientOrganization = $model->save();
        if ($clientOrganization) {
            if (!isset($clientOrganization->error)) {
                return $this->render('../saveSuccessful');
            } else {
                return $this->render('../saveFailed', [
                            'errors' => $clientOrganization->error,
                ]);
            }
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }
    
    public function actionAssistant($id)
    {
        $model = new AssistantForm();
        $roles = Api::resource('access/getallroles')->post(['location_id'=>$id]);
        print_r($roles);
        if(isset($roles->error)){
            throw new \yii\web\HttpException(404);
        }
        if($post = Yii::$app->request->post()){
            if(isset($_POST['AssistantForm']) && $data = $_POST['AssistantForm']){
                $invite = Api::resource('invite')->post(array_merge($data,['location_id'=>$id]));
            }
            if(!$invite->response->user_exist)
            {
                $mail = new InviteMail();
                $mail->sendInvite($invite->response->email,$invite->response->hash);
            }
        }
        $assistants = Api::resource('user/invites')->post(['location_id'=>$id]);
        return $this->render('assistant',['model'=>$model,'roles'=>$roles->response,'assistants'=>$assistants->response]);
    }
        
    public function actionSettings()
    {
        $orgs = Api::resource('client/organization')->get()->items;
        $drop = ArrayHelper::merge(['0' => \Yii::t('profile', 'Choose your legal entity')], 
                    ArrayHelper::map($orgs, 'location_id', 'fullname'));
        $model = new SettingsForm();
        if (!\Yii::$app->request->post()) {
            return $this->render('settings', ['model' => $model, 'drop' => $drop]);
        } else {
            $model->load(\Yii::$app->request->post(), 'SettingsForm');
            $model->save();
            return $this->render('../saveSuccessful');
        
        }
    }
    
    
}
