<?php

/**
 * Description of BusinessmanController
 *
 * @author vl
 */

namespace frontend\controllers\system;

use Yii;
use yii\web\Controller;
use frontend\models\forms\system\businessman\BusinessmanForm;
use frontend\models\forms\system\businessman\SettingsForm;
use app\components\Api;
use frontend\models\forms\system\AssistantForm;
use \frontend\models\InviteMail;

class BusinessmanController extends \app\components\RootController
{

    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return parent::beforeAction($action);
    }

    public function actionCreate()
    {
        // Get countries
        $res = Api::resource('geo/country')->post();
        $countries = [];
        if (isset($res->response) && is_array($res->response) && count($res->response)) {
            foreach ($res->response as $item)
                $countries[$item->id] = $item->title;
        }

        $model = new BusinessmanForm();
        if (!\Yii::$app->request->post()) {
            return $this->render('create', [
                        'model' => $model,
                        'countries' => $countries,
            ]);
        }

        $model->load(\Yii::$app->request->post(), 'BusinessmanForm');

        if (!$model->validate()) {
            return $this->render('create', ['model' => $model, 'countries' => $countries,]);
        }
        $clientBusinessman = $model->save();

        if ($clientBusinessman) {
            return $this->render('../saveSuccessful');
        } else {
            return $this->render('create', ['model' => $model, 'countries' => $countries,]);
        }
    }

    public function actionAssistant($id)
    {
        $model = new AssistantForm();
        $roles = Api::resource('access/getallroles')->post(['location_id' => $id]);
        if (isset($roles->error)) {
            throw new \yii\web\HttpException(404);
        }
        if ($post = Yii::$app->request->post()) {
            if (isset($_POST['AssistantForm']) && $data = $_POST['AssistantForm']) {
                $invite = Api::resource('invite')->post(array_merge($data, ['location_id' => $id]));
            }
            if (!$invite->response->user_exist) {
                $mail = new InviteMail();
                $mail->sendInvite($invite->response->email, $invite->response->hash);
            }
        }
        $assistants = Api::resource('user/assistants')->post(['location_id' => $id]);
        Yii::$app->user->setReturnUrl('/system/businessman/assistant/' . $id);
        return $this->render('assistant', ['model' => $model, 'roles' => $roles->response, 'assistants' => $assistants->response, 'location_id' => $id]);
    }

    public function actionCancelinvite()
    {
        if (!isset($_GET['email']) || !(new \yii\validators\EmailValidator())->validate($_GET['email']) || !isset($_GET['hash']))
            throw new \yii\web\HttpException(404);
        $result = Api::resource('invite/cancel')->post(['email' => $_GET['email'], 'hash' => $_GET['hash']]);
        $this->goBack();
    }

    public function actionDeleterole($id)
    {
        if (!isset($id) || !isset($_GET['user_id']))
            throw new \yii\web\HttpException(404);
        $result = Api::resource('access/remove')->delete(['user_id' => $_GET['user_id'], 'location_id' => $id]);
        $this->goBack();
    }

    public function actionSettings($id = 0)
    {

        $model = new SettingsForm();

        $object = Api::resource('client_businessman')->get(['id' => $id]);
        $schema = Api::resource('schema/entity/client_businessman')->get();


        $attributes = (array) $object->response[0];
        $attributes['certificate_reg'] = (strlen($attributes['certificate_reg'])) ? unserialize($attributes['certificate_reg']) : [];
        $attributes['certificate_tax'] = (strlen($attributes['certificate_tax'])) ? unserialize($attributes['certificate_tax']) : [];
        $attributes['mailing_settings'] = (strlen($attributes['mailing_settings'])) ? unserialize($attributes['mailing_settings']) : [];

        $checkboxes = [];
        foreach ($schema->response->client_businessman->index->PUT->fields_output_serialized->mailing_settings as $fld) {
            $checkboxes[$fld] = (isset($attributes['mailing_settings'][$fld])) ? $attributes['mailing_settings'][$fld] : 0;
        }
        foreach ($schema->response->client_businessman->index->PUT->fields_output_serialized->certificate_reg as $fld) {
            if (!isset($attributes['certificate_reg'][$fld]))
                $attributes['certificate_reg'][$fld] = '';
        }
        foreach ($schema->response->client_businessman->index->PUT->fields_output_serialized->certificate_tax as $fld) {
            if (!isset($attributes['certificate_tax'][$fld]))
                $attributes['certificate_tax'][$fld] = '';
        }

        $model->setAttributes(array(
            'email' => $attributes['email'],
            'certificate_reg' => $attributes['certificate_reg']['series'] . ' ' . $attributes['certificate_reg']['number'],
            'certificate_tax' => $attributes['certificate_tax']['series'] . ' ' . $attributes['certificate_tax']['number'],
            'checkboxes' => $checkboxes
        ));

        if (!\Yii::$app->request->post()) {
            return $this->render('settings', ['model' => $model]);
        }

        $model->load(\Yii::$app->request->post(), 'SettingsForm');

        $res = $model->save($id);
        if ($res) {
            return $this->render('../saveSuccessful');
        } else {
            return $this->render('settings', ['model' => $model, 'attributes' => $attributes]);
        }
    }

    public function actionAgency($id)
    {
        $error = '';
        $success = '';
        $type = Api::resource('location/whois')->get(['location_id' => $id]);
        if (isset($type->error) || !$type->response->is_сommerce) {
            throw new \yii\web\HttpException(404);
        }
//                    print_r(\Yii::$app->user->isGuest());exit;

        if (($post = Yii::$app->request->post()) && isset($post['subscribe']) && isset($post['tarif_id'])) {
            $result = Api::resource('tarif')->post(['location_id' => $id, 'tarif_id' => $post['tarif_id']]);
            if (isset($result->error)) {
                $error = $result->error->message;
            } else {
                $success = 'Ok. You subscribed!';
            }
        }
        $tarifs = Api::resource('tarif')->get(['location_id' => $id]);
        return $this->render('agency', ['id' => $id, 'tarifs' => $tarifs->response, 'error' => $error, 'success' => $success]);
    }

    public function actionPartner()
    {
        $id = \yii\helpers\ArrayHelper::getValue($_COOKIE, 'client_location_id');
        $success = '';
        $error = '';
        $platforms = Api::resource('platform')->get(['location_id' => $id]);
        if (($post = Yii::$app->request->post()) && isset($post['createPlatfrom'])) {
            $result = Api::resource('platform/register-start')->post(['orgtype_location_id' => $id]);
            if (isset($result->error)) {
                if ($result->serverError) {
                    $error = 'У вас есть незавершенная регистрация платформы';
                }
                if ($result->validateError) {
                    $error = 'У вас не заполнены поля в профиле!';
                }
            } else {
                $success = 'Заявка на платформу создана. Пожалуйста проверьте e-mail!';
            }
        }
        return $this->render('partner', ['id' => $id, 'platforms' => $platforms->response, 'error' => $error, 'success' => $success]);
    }

}
