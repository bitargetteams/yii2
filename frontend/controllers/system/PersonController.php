<?php

/**
 * Description of Person
 *
 * @author vl
 */
namespace frontend\controllers\system;

use Yii;
use yii\web\Controller;
use frontend\models\forms\system\person\PersonForm;
use frontend\models\forms\system\person\SettingsForm;
use app\components\Api;

class PersonController extends Controller
{

    public function beforeAction($action)
	{
		if (\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		return parent::beforeAction($action);
	}

	public function actionCreate()
    {

		// Get countries
		$res = Api::resource('geo/country')->get();
		$countries = [];
		if (isset($res->items) && is_array($res->items) && count($res->items)) {
			foreach ($res->items as $item) $countries[$item->id] = $item->title;
		}

		$model = new PersonForm();
        if (!\Yii::$app->request->post()) {
			return $this->render('create',
				[
					'model' => $model,
					'countries' => $countries,
				]);
        }

        $model->load(\Yii::$app->request->post(), 'PersonForm');
		if (!$model->validate()) {
			return $this->render('create', ['model' => $model]);
		}

		$clientPerson = $model->save();
		if ($clientPerson) {
			return $this->render('../saveSuccessful');
		} else {
			return $this->render('create', ['model' => $model, 'countries' => $countries,]);
		}
    }

    public function actionQuick()
    {
		$client_person = Api::resource('client/person')->post([
			'firstname' => \Yii::$app->user->user->firstname,
			'lastname' => \Yii::$app->user->user->lastname,
			'middlename' => \Yii::$app->user->user->middlename,
		]);

        if (!isset($client_person->error)) {
            return $this->goHome();
        } else {
            return $this->render('../saveFailed', [
                        'error' => $client_person->error,
            ]);
        }
    }

	public function actionSettings($id = 0)
	{

		$model = new SettingsForm();
		$object = Api::resource('client/person')->get(['location_id'=>$id]);
		$schema = Api::resource('schema/entity/client_person')->get();

		$attributes = $object->items[0];
//                echo '<pre>', print_r($object->items[0], 1);die;
		//$attributes['passport'] = (strlen($attributes['passport'])) ? unserialize($attributes['passport']) : [];
//		$attributes['mailing_settings'] = (strlen($attributes['mailing_settings'])) ? unserialize($attributes['mailing_settings']) : [];

		$checkboxes = [];
//		foreach ($schema->items->client_person->index->PUT->fields_output_serialized->mailing_settings as $fld) {
//			$checkboxes[$fld] = (isset($attributes['mailing_settings'][$fld])) ? $attributes['mailing_settings'][$fld] : 0;
//		}
//		foreach ($schema->items->client_person->index->PUT->fields_output_serialized->passport as $fld) {
//			if (!isset($attributes['passport'][$fld])) $attributes['passport'][$fld] = '';
//		}
		$model->setAttributes(array(
				'email' => $attributes['email'],
				'passport' => $attributes['passport']['series'] . ' ' . $attributes['passport']['number'],
				'ipc' => $attributes['ipc'],
				'inn' => $attributes['inn'],
				'checkboxes' => $checkboxes
			));

		if (!\Yii::$app->request->post()) {
			return $this->render('settings', ['model' => $model]);
		}

		$model->load(\Yii::$app->request->post(), 'SettingsForm');

		$res = $model->save($id);
		if ($res) {
			return $this->render('../saveSuccessful');
		} else {
			return $this->render('settings', ['model' => $model, 'attributes' => $attributes]);
		}

	}

}
