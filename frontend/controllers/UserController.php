<?php

/**
 *
 * @author Valeriy Loshkaryov
 */

namespace frontend\controllers;

use app\modules\mail\MailTemplate;
use common\models\LoginForm;
use Yii;
use frontend\models\forms\PasswordResetRequestForm;
use frontend\models\forms\ResetPasswordForm;
use frontend\models\forms\SignupForm;
use frontend\models\forms\UserpicForm;
use app\models\ConfirmMail;
use frontend\models\forms\ConfirmForm;
use frontend\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use app\components\Api;
use frontend\models\Avatar;
use frontend\models\Ticket;
use \frontend\models\forms\TicketForm;

class UserController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'role' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => false,
                'allowActions' => ['registration', 'confirm', 'login', 'requestPasswordReset'],
            ],
            'authUsers' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => true,
                'allowActions' => ['ticket'],
            ],
        ];
    }
    public function actionRegistration()
    {
        $hash = isset($_GET['hash']) ? $_GET['hash'] : '';
        $email = isset($_GET['email']) ? $_GET['email'] : '';
        $model = new SignupForm();
        if ($hash) {
            $invite = Api::resource('invite/validate')->post(['hash' => $hash, 'email' => $email]);
            if (!isset($invite->error)) {
                return $this->render('signupNoEmail', ['email' => $invite->response->email]);
            }
        }
        if (\Yii::$app->request->post()) {
            $model->load(\Yii::$app->request->post(), 'SignupForm');
			$model->hash = $hash;
            $model->inviteEmail = $email;
            $model = $model->signup();
            if (!$model->hasErrors()) {
                if (!isset($model->register->error)) {
                    return $this->render('registerSuccessful');
                }
            }
        }
        return $this->render('signup', ['model' => $model]);
    }

    public function actionConfirm()
    {
        if (!\Yii::$app->request->get()) {
            return $this->render('confirmFailed', [
                        'errors' => ['message' => ['Some params are missing or invalid']],
            ]);
        }
        $model = new ConfirmForm();
        $model->load(\Yii::$app->request->get(), '');
        $model = $model->confirm();
        if ($model) {
            if (!isset($model->error)) {
                return $this->render('confirmSuccessful');
            } else {
                return $this->render('confirmFailed', [
                            'errors' => $model->error,
                ]);
            }
        } else {
            return $this->render('confirmFailed', [
                        'errors' => ['message' => ['Some params are missing or invalid']],
            ]);
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    public function actionReset($id, $token)
    {
        try {
            $model = new ResetPasswordForm($id, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        $loginAttr = Yii::$app->request->getBodyParams();
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        $model->setAttributes($loginAttr['login']);
        if ($model->login()) {
            return $this->goStartPage();
        }
        return $this->goBack();
    }

    public function goStartPage()
    {
        return Yii::$app->getResponse()->redirect('/start');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionPic()
    {
        $model = new UserpicForm();
        if (!\Yii::$app->request->post()) {
            return $this->render('pic', ['model' => $model]);
        }
        $model->load(\Yii::$app->request->post(), 'UserpicForm');

        if (\Yii::$app->request->post('UserpicForm')
            && isset(\Yii::$app->request->post('UserpicForm')['rawfile'])) { // get raw image file
            $model->saveTmp();
            return $this->render('pic', ['model' => $model]);
        } else {
            if (!$model->validate()) {
                $model->image = Img::getTmpUrl().$model->userpic;
                return $this->render('pic', ['model' => $model]);
            }
            $model->saveAvatar();
        }
        return $this->render('pic', ['model' => $model]);
    }
    
    public function actionTicket($id='')
    {
        $form = new TicketForm();

        if (Yii::$app->request->post('TicketForm')) {
            $form->attributes = Yii::$app->request->post('TicketForm');

            if ($form->validate()) {
                $apiResult = $form->save($id);
                $form->save_attach($apiResult->id);
                return $this->render('success');
            }
           // $form = new TicketForm();
        }
        $form->message = '';
        if (!$id){
            $model = $form->getParents();
            return $this->render('ticket', ['model' => $model, 'form' => $form]);
        } else {
            $model = $form->getThread($id);
            return $this->render('ticketDetail', ['model' => $model, 'form' => $form]);
        }
 
    }

}
