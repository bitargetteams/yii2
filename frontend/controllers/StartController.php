<?php

/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 24.10.14
 * Time: 18:02
 */

namespace frontend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Client_person;
use app\components\Api;

class StartController extends \app\components\RootController
{

    public function behaviors()
    {
        return [
            'role' => [
                'class'    => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => true,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->use_client_location_id = false;
        $clientBusinessman = Api::resource('client/businessman')->get(['fields' => 'id,firstname,lastname,middlename,location_id,tarif_id']);
        $clientOrganization = Api::resource('client/organization')->get(['fields' => 'location_id,fullname, tarif_id']);
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (!count($clientBusinessman->response)) {

            return $this->render('start');
        } else {
            return $this->render('clientsList', [
                    'clientBusinessman'  => $clientBusinessman->response,
//                'clientPerson'=>$clientPerson->response,
                    'clientOrganization' => $clientOrganization->response
            ]);
        }
    }

}
