<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 13:27
 */

namespace frontend\controllers;

use Yii;
use app\components\Api;
use frontend\models\forms\ConfirmPhone;
use frontend\models\forms\EditPhone;
use frontend\models\forms\UpdatePassword;
use yii\web\Controller;
use yii\web\HttpException;

class ProfileController extends Controller {

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'role' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => true
            ],
        ];
    }

    public function actionIndex()
    {
        $params = Yii::$app->request->getQueryParams();
        $alert = isset($params['edit']) && $params['edit'] === 'successful'? $params : NULL;
        $model = \Yii::$app->user->getUser();
        return $this->render('index',['model' => $model,'alert'=>$alert]);
    }

    public function actionEditPhone()
    {
        $form = new EditPhone();
        if ($params = \Yii::$app->request->getBodyParams()) {
            $form->load($params);
            if ($form->validate())
            {
                $model = Api::resource('user')->put(
                    [
                        'id' => Yii::$app->user->id,
                        'password' => $form->password,
                        'phone_number' => $form->phone_number,
                    ]
                );
                if (isset($model->response)) {
                    Yii::$app->user->reNewModel($model->response);
                    $this->redirect('/profile/index?edit=successful&target=edit_phone');
                }else{
                    throw new HttpException($model->error->status,'Server error');
                }
            }
        }
        return $this->render(
            'edit', ['view' => 'form/edit-phone', 'form' => $form]
        );
    }

    public function actionConfirmPhone()
    {
        $form = new ConfirmPhone();
        if ($params = Yii::$app->request->getBodyParams())
        {
            $form->load($params);
            if($form->validate())
            {
                $model = Api::resource('user/verify_phone')->post(
                    [
                        'id'=>  Yii::$app->user->id,
                        'password'=>  $form->password,
                        'phone_number' => Yii::$app->user->phone_number,
                        'phone_verification_pin' => $form->pin
                    ]
                );
                if (isset($model->response))
                    $this->redirect('/profile/index?edit=successful&target=confirm_phone');
                else{
                    throw new HttpException($model->error->status,'Server error');
                }
            }
        }
        return $this->render('edit',['view' => 'form/confirm_phone','form' => $form]);
    }

    /**
     * @return string
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * TODO обработчик ошибок ответа от сервера :-(
     */
    public function actionUpdatePassword()
    {
        $form = new UpdatePassword();
        if ($params = \Yii::$app->request->getBodyParams())
        {
            $form->load($params);
            if($form->validate())
            {
                $model = Api::resource('user/change')->post(
                    [
                        'old_pass'=>  $form->oldPassword,
                        'new_pass'=>  $form->password,
                        'confirm_pass' => $form->confirmPassword,
                    ]
                );
                if (isset($model->response) && $model->response[0] === true)
                    $this->redirect('/profile/index?edit=successful&target=update_password');
                else{
                    throw new HttpException($model->error->status,'Server error');
                }
            }
        }
        return $this->render('edit',['view' => 'form/update_password','form' => $form]);
    }
} 