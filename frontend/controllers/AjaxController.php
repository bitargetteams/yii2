<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 20.11.14
 * Time: 18:12
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\components\Api;
use yii\helpers\Json;


class AjaxController extends Controller
{
	public function actionCountries()
	{
		$res = Api::resource('geo/country')->get();
		$countries = [];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $countries[$item->id] = $item->title;
		}
		return $countries;
	}

	public function actionRegions()
	{
		$country_id = \Yii::$app->request->getBodyParam('country_id');
                $per_page = \Yii::$app->request->getBodyParam('per_page');

		$res = Api::resource('geo/region')->get(['country_id' => $country_id, 'per-page' => $per_page]);
		$regions = ['0' =>'Выберите регион'];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $regions[$item->id] = $item->title;
		}
		return Json::encode($regions);
	}

	public function actionCities()
	{
		$country_id = \Yii::$app->request->getBodyParam('country_id');
		$region_id = \Yii::$app->request->getBodyParam('region_id');
                $per_page = \Yii::$app->request->getBodyParam('per_page');

		$res = Api::resource('geo/city')->get(['country_id' => $country_id, 'region_id' => $region_id, 'per-page' => $per_page]);
		$cities = ['0' =>'Выберите город'];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $cities[$item->id] = $item->title;
		}
		return Json::encode($cities);
	}

	public function actionAddfolder()
	{
		$name = \Yii::$app->request->getBodyParam('name');
		$section = \Yii::$app->request->getBodyParam('section');
		$res = Api::resource('client_folder')->post(['name' => $name, 'section' => $section]);

		$res = Api::resource('client_folder')->get();
		$folders = ['0' =>'Выберите папку'];
		if (isset($res->response) && $res->response->is_array() && count($res->response)) {
			foreach ($res->response as $item) $folders[$item->id] = $item->name;
		}
		return Json::encode($folders);
	}
        
        public function actionOrganization()
        {
            $id = \Yii::$app->request->get('location_id');
            $org = Api::resource('client/organization')->get(['location_id' => $id])->items;
            $model = new \frontend\models\forms\system\organization\SettingsForm();
            $model->email = $org->email;
            $model->email_verified = $org->email_verified;
            /* mailing_settings -- a:3:{s:4:"set1";b:1;s:4:"set2";b:0;s:4:"set3";b:1;} */
            $model->checkboxes = unserialize($org->mailing_settings);
            $model->object_id = $id;

            $model->ogrn = $org->ogrn;
            $model->ogrn_date = $org->ogrn_date;
            $model->ogrn_verified = $org->ogrn_verified;

            $cert = unserialize($org->certificate_reg);
            $model->certificate_reg = $cert['number'];
            $model->certificate_reg_date = $cert['given_when'];
            $model->certificate_reg_verified = $org->certificate_reg_verified;

            $cert = unserialize($org->certificate_tax); 
            $model->certificate_tax = $cert['number'];
            $model->certificate_tax_date = $cert['given_when'];
            $model->certificate_tax_verified = $org->certificate_tax_verified;
            return $this->renderPartial('/system/organization/_settings', ['model' => $model]);
        }
}