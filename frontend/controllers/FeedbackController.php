<?php

namespace frontend\controllers;

use yii;
use frontend\models\forms\FeedBackForm;
use \yii\web\Controller;
use app\components\Api;

class FeedbackController extends Controller
{
    /**
     * Класс для обработки каптчи
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }
    /**
     * Отображение начальной страницы раздела "Обратная связь"
     * Содержит форму для отправки собщения в поддерку тех
     */
    public function actionIndex()
    {
        $form = new FeedBackForm();
        $apiResult = false;

        if (Yii::$app->request->post('FeedBackForm')) { 
            $form->attributes = Yii::$app->request->post('FeedBackForm');
            $id = $form->save();
            if ($id) {
                $form->save_attach($id);
                return $this->render('success');
            } else {
                //print_r($form);die;

            }
            $form = new FeedBackForm();
        }

        return $this->render('index', array('form' => $form, 'mailResult' => $apiResult));
    }

    /**
     * Вспомогательная функция для отправки сообщения
     *
     * @param string $subject Заголовок темы
     * @param string $message Текст сообщения
     * @param string $senderMail Адрес электронной почты отправителя
     * @param string $senderName Имя отправителя
     * @return boolean Результат оправки почты
     */
//    public static function sendMailToSuppurt($subject, $message, $senderMail, $senderName)
//    {
//        $headers = 'MIME-Version: 1.0' . "\r\n";
//        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
//        $headers .= "X-Mailer: PHP/" . phpversion();
//        $headers .= "From: {$senderName} <{$senderMail}>\r\n";
//        $headers .= "Reply-To: {$senderName} <{$senderMail}>\r\n";
//        $headers .= 'Cc: ' . "\r\n";
//        $headers .= 'Bcc: ' . "\r\n";
//        $headers .= 'Organization: Bitarget' . "\r\n";
//
//        return mail(Yii::$app->params['adminEmail'], $subject, $message, $headers);
//    }

}
