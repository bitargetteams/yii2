<?php

namespace frontend\controllers;
use \yii\web\Controller;
class DocsController extends Controller
{
    public function actionIndex(){
        $schema = \app\components\Api::resource('site/schema',true)->get();
        return $this->render('index',['schema'=>$schema]);
    }
}

