<?php

namespace frontend\controllers;


use frontend\models\User;

class PageController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'role' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => false,
                'allowActions' => ['about','index'],
                'equivActions' => ['index' => 'authorized']
            ],
            'byAuthUser' => [
                'class' => 'frontend\behaviors\RoleBehavior',
                'onlyAuth' => true,
                'allowActions' => ['authorized'],
            ],
        ];
    }

    public function actionAbout()
    {
		return $this->render('about');
    }

    public function actionIndex()
    {
		return $this->render('main_page.tpl');
    }
    
    public function actions()
    {
        return [
            'error' => ['class' => 'yii\web\ErrorAction'],
        ];
    }

    public function actionNotFound()
    {
        return $this->render('error404');
    }


    public function actionAccessDenied()
    {
        return $this->render('error403');
    }

    public function actionAuthorized()
    {
        return $this->render('authorized.tpl');
    }

}
