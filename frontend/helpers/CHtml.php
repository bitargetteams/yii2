<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 08.10.14
 * Time: 16:42
 */
namespace frontend\helpers;


use Yii;
use \yii\helpers\Html;
use yii\web\View;

class CHtml extends Html {
    public static function img($src, $options = []){
        return static::img($src, $options);
    }

    /**
     * Normalizes the input parameter to be a valid URL.
     *
     * If the input parameter is an empty string, the currently requested URL will be returned.
     *
     * If the input parameter is a non-empty string, it is treated as a valid URL and will
     * be returned without any change.
     *
     * If the input parameter is an array, it is treated as a controller route and a list of
     * GET parameters, and the {@link CController::createUrl} method will be invoked to
     * create a URL. In this case, the first array element refers to the controller route,
     * and the rest key-value pairs refer to the additional GET parameters for the URL.
     * For example, <code>array('post/list', 'page'=>3)</code> may be used to generate the URL
     * <code>/index.php?r=post/list&page=3</code>.
     * @access public
     * @static
     * @param mixed $url the parameter to be used to generate a valid URL
     * @return string the normalized URL
     */
    public static function normalizeUrl($url)
    {
        if(is_array($url))
        {
            if(isset($url[0]))
            {
                if(($c=Yii::$app->getController())!==null)
                    $url=$c->createUrl($url[0],array_splice($url,1));
                else
                    $url=Yii::$app->createUrl($url[0],array_splice($url,1));
            }
            else
                $url='';
        }
        return $url==='' ? Yii::app()->getRequest()->getUrl() : $url;
    }

    /**
     * Get errors for some fields and generate css class style to self tag;
     * @access public
     * @static
     * @param Model $model
     * @param string $field
     * @param string $class
     * @return string
     */
    public static function cssError($model,$field='',$class='error')
    {
        return $model->getErrors($field) ? ' ' . $class : '';
    }

    public static function multipleActiveCheckBoxList($model, $attribute, $items, $options = [])
    {
        $callback = function ($i) {
            foreach ($i as $key => &$j) {
                $j = "<span class='ch-col-{$key}'>{$j}</span>";
            }
            return implode("\r\n", $i);
        };

        $items = is_array($items[0]) ? array_map($callback, $items) : $items;

        return parent::activeCheckboxList($model, $attribute, $items, $options);
    }
} 