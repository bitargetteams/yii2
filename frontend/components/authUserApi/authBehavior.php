<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 23.10.14
 * Time: 12:42
 */

namespace app\components\authUserApi;


use yii\base\Behavior;

class authBehavior extends Behavior {
    public static function className()
    {
        return __NAMESPACE__.'\\'.__CLASS__;
    }

} 