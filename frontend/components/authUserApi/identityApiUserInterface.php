<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 23.10.14
 * Time: 12:20
 */
namespace app\components\authUserApi;

interface identityApiUserInterface{
    /**
     * Returned authorized user id
     * @access public
     *
     * @return integer|bool
     */
    public function getId();
}