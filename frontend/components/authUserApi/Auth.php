<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 23.10.14
 * Time: 12:17
 */

namespace app\components\authUserApi;
use frontend\models\User;
use yii;
use \yii\base\Component;

/**
 * Class Auth
 *
 * @package app\components\authUserApi
 */
class Auth extends Component {
    /**
     * @var bool
     */
    protected $isGuest = true;
    /**
     * @var integer|null
     */
    protected $lastSignUp = null;
    /**
     * @var User|null
     */
    protected $user = null;
    
    public $returnUrlParam = '__returnUrl';

    /**
     * @param identityApiUserInterface $model
     * @param                          $duration
     *
     * @return bool
     */
    
    public function __construct($config = array())
    {
        if(!$this->getIsGuest()){
            setcookie('PHPSESSID',$_COOKIE['PHPSESSID'],time()+3600*24, '/');
        }
        return parent::__construct($config);
    }
    
    
    public function login($model,$duration)
    {
        Yii::$app->session->setCookieParams(['httponly' => true,'lifetime'=>$duration,'path'=>'/']);
        Yii::$app->session->set('user',$model);
        return !$this->getIsGuest() ? true : false;
    }

    public function reNewModel($model)
    {
        $duration = Yii::$app->session->getCookieParams()['lifetime'];
        $this->login($model,$duration);
        return !$this->getIsGuest() ? true : false;
    }

    /**
     * Returned authorized user model
     * @return mixed
     */
    public function getUser()
    {
        return $this->user = Yii::$app->session->get('user');
    }

    /**
     * Returned guest user status
     * @return bool
     */
    public function getIsGuest()
    {
        $this->isGuest = $this->getUser() ? false : true;
        return $this->isGuest;
    }

    /**
     * Close session for authorized user
     * @return bool
     */
    public function logout()
    {
        Yii::$app->session->removeAll();
        return $this->getIsGuest();
    }

   
    public function getReturnUrl($defaultUrl = null)
    {
        $url = Yii::$app->getSession()->get($this->returnUrlParam, $defaultUrl);
        if (is_array($url)) {
            if (isset($url[0])) {
                return Yii::$app->getUrlManager()->createUrl($url);
            } else {
                $url = null;
            }
        }

        return $url === null ? Yii::$app->getHomeUrl() : $url;
    }

    public function setReturnUrl($url)
    {
        Yii::$app->getSession()->set($this->returnUrlParam, $url);
    }

    /**
     * @return int
     */
    public function getLastSignUp()
    {
        return $this->user->last_login;
    }
    
    public function getId()
    {
        return !$this->isGuest ? $this->user->id : -1;
    }
    
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }
        return $this->user->$name;
    }

} 