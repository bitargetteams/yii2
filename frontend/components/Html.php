<?php

namespace app\components;

class Html extends \yii\helpers\Html
{

    public static function beginForm($action = '', $method = 'post', $options = array())
    {
        $form = parent::beginForm($action, $method, $options);
        if (isset($_COOKIE['clid'])) {
            $form .= "\n" . parent::hiddenInput('__client_location_id', $_COOKIE['clid']);
        }
        return $form;
    }

    public static function clidInput($id=null)
    {
        return parent::hiddenInput('__client_location_id', $id ? $id : $_COOKIE['clid']);
    }

}
