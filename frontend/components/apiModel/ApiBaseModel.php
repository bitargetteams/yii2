<?php
namespace app\components\apiModel;

use app\components\apiModel\interfaces\ApiBaseModelInterface;
use yii\base\Model;


/**
 * Class ApiBaseModel
 *
 * @package ext\models
 */
abstract class ApiBaseModel extends Model implements ApiBaseModelInterface{
    /**
     * @var array
     */
    private static $calledActions=[];
    /**
     * @var array
     */
    private $attributes=[];
    /**
     * @var string
     */
    private $type;
    /**
     * @var array
     */
    private $rules = [[]];

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name,$value)
    {
        $this->attributes[$name] = $value;
        return;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->attributes[$name];
    }

    /**
     * @param string $className
     *
     * @return ApiQuery $apiQuery
     */
    public static function model($className=__CLASS__)
    {
        return new ApiQuery(static::entityName(),$className);
    }

    /**
     * @param $class
     * @param $actions
     * @param $lvl
     *
     * @throws \Exception
     */
    public  static function checkAccessAction($class,$actions,$lvl)
    {
        self::$calledActions[$class::entityName()][$lvl] = $actions;
        $classActions = $class::actions();
        if (($index = array_search($actions[0],array_column($classActions,0))) !== false &&
            (isset($actions[1])&&!in_array($actions[1],$classActions[$index]))){
            throw new \Exception('This action not allowed for this model');
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param array $attr
     * @param bool  $safeOnly
     */
    public function setAttributes($attr,$safeOnly = true)
    {
        foreach($attr as $key=>$val) {
            $this->$key = $val;
        }
    }

    public function rules()
    {
        return $this->rules;
    }

    /**
     * @param null  $name
     * @param array $except
     *
     * @return array
     */
    public function getAttributes($name=null,$except = [])
    {
        return is_null($name) ? $this->attributes : array_intersect_key($this->attributes,array_flip($name));
    }

    /**
     * Конструктор модели
     * @param null $type
     */
    public function __construct($type=null)
    {
        $this->type = $type;
    }

    /**
     * @param $method
     *
     * @throw BadMethodCallException
     */
    private function checkAccessObjectAction($method)
    {
        if(!preg_match('/^'.$method.'(.*?)$/',$this->type,$matches))
            throw new \BadMethodCallException('This action not allowed for this type model');
        return strtolower($matches[1]);
    }

    /**
     * @param $name
     * @param null   $attributes
     *
     * @return mixed
     */
    public function __call($name,$attributes=null)
    {

        parent::__call($name,$attributes);
    }

    public function validate($attributeNames = NULL, $clearErrors = true)
    {
        return true;
    }

    public function save()
    {
        return self::editableMethod('save');
    }

    public function update($id)
    {
        return self::editableMethod('update',['id'=>$id]);
    }

    protected function editableMethod($typeAction,$cond=[])
    {
        $action = self::checkAccessObjectAction($typeAction);
        $query = new ApiQuery(static::entityName(),$this->className());
        $attributes = array_merge($this->getAttributes(),$cond);
        return $query->$action()->$typeAction($attributes);
    }


}


