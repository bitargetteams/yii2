<?php

/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 17.10.14
 * Time: 17:30
 */

namespace app\components\apiModel;

use app\components\apiModel\itfc\selectBaseInterface;

/**
 * Class ApiQuery
 * @package frontend\models
 */
final class ApiRestTransport
{

    public $response = null;
    protected $entity;
    protected $action;
    protected $attributes;
    protected $id;
    protected static $alias = [
        'save' => 'POST',
        'update' => 'PUT',
        'select' => 'GET',
        'delete' => 'DELETE'
    ];

    /**
     * @access public
     * @static
     * @param $entity
     * @param $action
     * @param $attributes
     *
     * @return ApiRestTransport
     */
    public static function factory($entity, $action, array $attributes)
    {
        $object = new self($entity, $action, $attributes);
        return $object;
    }

    protected function __construct($entity, $action, $attributes)
    {
        $this->entity = $entity;
        $this->action = $action;
        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
            //unset($attributes['id']);
        }
        $this->attributes = is_array($attributes) ? $attributes : [];
    }

    protected function setAction($type)
    {
        if (strlen($type)) {
            $type = self::$alias[$type];
        } else {
            $type = self::$alias[$this->action];
            $this->action = '';
        };
        return $type;
    }

    private function query($type = '')
    {
		$type = $this->setAction($type);
        $ch = curl_init();
        $sig = '';
        $delimiter = ($this->action) ? '/' : '';
        $url = trim(\Yii::$app->params['core'].$this->entity . '/' . $this->action . $delimiter. $this->id,'/');
        $get = http_build_query($this->attributes);
        $bodyParams = '';
        if ($type == 'GET') {
            $url .= ($get) ? '?' . $get : '';
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $get);
            $bodyParams = $get;
        }
        $httpHeaders =  [
            'PLATFORM-ID: ' . \Yii::$app->params['platform_id'],
            'PLATFORM-TOKEN: ' . hash("sha256", \Yii::$app->params['platform_id'].$url.$bodyParams.\Yii::$app->params['secret_key']),
            'X-HTTP-Method-Override: ' . $type,
        ];
        if(!\Yii::$app->user->isGuest){
            $httpHeaders[] = 'USER-ID: ' . \Yii::$app->user->user->id;
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        /*Блок сбора информации о запросе*/
        $head = curl_exec($ch);
        $time = curl_getinfo($ch,CURLINFO_TOTAL_TIME);
        $connectTime = curl_getinfo($ch,CURLINFO_CONNECT_TIME);
        $status = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        \Yii::info("$url | $head | $bodyParams | $time | $type | $connectTime | $status","apiResponse");
        /*Блок сбора информации о запросе*/
        
        curl_close($ch);
        $this->response = $head;
    }

    /**
     * @param $name
     * @param $attr
     *
     * @return selectBaseInterface
     */
    public function __call($name, $attr)
    {
        if (in_array($name, array_keys(self::$alias))) {
            $this->attributes = $attr[0];
            if (isset($this->attributes['id'])) {
                //unset($this->attributes['id']);
            }
            $this->query($name);
            return $this;
        }
    }

}
