<?php

/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 20.10.14
 * Time: 13:46
 */

namespace app\components\apiModel;

use Yii;
use yii\base\Component;
use yii\base\Model;

/**
 * Class ApiQuery
 *
 * @package ext\models
 */
final class ApiQuery extends Component
{

    /**
     * @var string $entity Название сущности
     */
    private $entity;

    /**
     * @var ApiBaseModel
     */
    private $calledClass;

    /**
     * @var ApiRestTransport|null
     */
    private $transport = NULL;

    /**
     * @var array forbidden method to call from this object
     */
    protected static $forbiddenMethod = ['save', 'update'];

    /**
     * @var string used action for try up method
     */
    private $usedAction = '';

    /**
     * @param string $entity
     * @param string $className
     *
     * @return self
     */
    public function __construct($entity, $className)
    {
        $this->entity = $entity;
        $this->calledClass = $className;
        return $this;
    }

    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param string $name
     * @param array $attr
     *
     * @return self
     */
    public function __call($name, $attr)
    {
        $this->transport = ApiRestTransport::factory($this->entity, $name, $attr);
        if (!\Yii::$app->params['secret_key']) {
            throw new \yii\web\HttpException(400, 'Secret key not found!');
        }
        if (!\Yii::$app->params['platform_id']) {
            throw new \yii\web\HttpException(400, 'ID platform not found!');
        }
        $this->usedAction = $name;
        return $this;
    }

    /**
     * @param $condition
     *
     * @return ApiBaseModel|mixed
     */
    public function select($condition)
    {
        return $this->upQuery($condition, 'select');
    }

    /**
     * @param $attr
     *
     * @return ApiBaseModel|mixed
     */
    public function save($attr)
    {
        return $this->upQuery($attr, 'save');
    }

    /**
     * @param $attr
     *
     * @return ApiBaseModel|mixed
     */
    public function update($attr)
    {
        return $this->upQuery($attr, 'update');
    }

    protected function querySchema()
    {
        $schema = Yii::$app->cache->get('schema');
        if ($schema === false) {
            $oSch = new self('schema/fake', $this->calledClass);
            $schema = $oSch->select([]);
            Yii::$app->cache->set('schema', $schema);
        }
        return $schema;
    }

    /**
     * @param array $condition
     *
     * @return ApiBaseModel|mixed
     */
    public function delete($condition)
    {
        return $this->upQuery($condition, 'delete');
    }

    /**
     * @param array $condition
     * @param string $action
     *
     * @return ApiBaseModel|mixed
     */
    protected function upQuery($condition, $action)
    {
        if (is_null($transport = $this->transport)) {
            $transport = ApiRestTransport::factory($this->entity, '', $condition);
        }
        $transport->$action($condition);
        $response = $transport->response;
        $collection = [];
        $response = json_decode($response);
        $usedAction = self::buildUsedAction($this, $action);
        if (isset($response->error)) {
            $error = $response->error;
            $model = new $this->calledClass($usedAction);
            if (isset($error->details)) {
                $model->addError($error->details);
                unset($error->details);
            }
            $model->addError('code', $error->code);
            $model->addError('message', $error->message);
            $model->addError('status', $error->status);
            return $model;
        }
        if (isset($response->response)) {
            if (is_array($response->response)) {
                foreach ($response->response as $row) {
                    $model = new $this->calledClass($usedAction);

                    $model->setAttributes($row);
                    if (method_exists($this->calledClass, $usedAction)) {
                        $model->$usedAction();
                    }
                    $collection[] = $model;
                }
            }else{
                $model = new $this->calledClass($usedAction);
                $model->setAttributes($response->response);
                if (method_exists($this->calledClass, $usedAction)){
                    $model->$usedAction();
                }
                return $model;
            }


        }
        return $collection;
    }

    /**
     * @param ApiQuery $object
     * @param string   $action
     *
     * @return string
     */
    public static function buildUsedAction(ApiQuery $object, $action)
    {
        return $action . ucfirst($object->usedAction);
    }

}
