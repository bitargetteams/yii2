<?php

namespace app\components\apiModel\interfaces;
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 16.10.14
 * Time: 16:55
 */

use app\components\apiModel\ApiQuery;

interface ApiBaseModelInterface {
    /**
     * @param string $className
     * @static
     * @access public
     * @param $className
     * @return ApiQuery $apiQuery
     */
    public static function model($className=__CLASS__);

    /**
     * Возвращает название сущности
     *
     * @access public
     * @static
     * @return string
     */
    public static function  entityName();

    /**
     * Permissions for accessible actions. If action not be allow in this returned array
     * baseModel search full method from called model.
     * @return array
     */





}