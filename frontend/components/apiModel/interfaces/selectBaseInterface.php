<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 20.10.14
 * Time: 12:41
 */
namespace app\components\apiModel\itfc;


interface selectBaseInterface{
    public function select();
    public function delete();
}