<?php
namespace frontend\components;
use yii\base\Security;

/**
 * Description of BSecurity
 *
 * @author vl
 */

class BSecurity extends Security
{
    public function generatePasswordHash($password, $cost = 13) 
    {
        return hash("sha256", $password);
    }

    public function getSalt() 
    {
        return $this->generateRandomString(12);
    }
    
    public function validatePassword($password, $hash) 
    {
        return ($this->generatePasswordHash($password) == $hash);
    }
    
}
