<?php

namespace app\components;

use \yii\web\Controller;
use Yii;

class RootController extends Controller
{

    public $use_client_location_id = true;

    public function beforeAction($action)
    {
        parent::beforeAction($action);
        if (Yii::$app->request->isPost && ($client_location_id = Yii::$app->request->getBodyParam('__client_location_id'))) {
            setcookie('client_location_id', $client_location_id, 0, '/');
            $_COOKIE['client_location_id'] = $client_location_id;
        }
        if(isset($_COOKIE['client_location_id'])) {
            Yii::$app->view->title = 'Мой клиент: '.$_COOKIE['client_location_id'];
            Yii::$app->view->registerJs('window.client_location_id = ' . $_COOKIE['client_location_id'], \yii\web\View::POS_HEAD);
            Yii::$app->view->registerJsFile('@web/js/client_location_id.js', ['position' => \yii\web\View::POS_HEAD]);
        }
        return true;
    }

    public function afterAction($action, $result)
    {
        if ($this->use_client_location_id) {
            if (!Yii::$app->request->isPost && !isset($_COOKIE['client_location_id'])) {
                $this->redirect('/start');
            }
        }
        return parent::afterAction($action, $result);
    }

}
