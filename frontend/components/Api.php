<?php

/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 31.10.14
 * Time: 11:56
 */

namespace app\components;

use yii\base\Exception;


class Api {
    private $resource;
    private $method;
    private $flagArray;
    
    public static function resource($resourse, $flagArray = false)
    {
        return new self($resourse, $flagArray);
    }

    private function __construct($resource, $flagArray = false)
    {
        $this->flagArray = $flagArray;
        $this->resource = $resource;
    }

    public function get($params = array())
    {
        $this->method = 'GET';
        return $this->query($params);
    }

    public function post($params = array())
    {
        $this->method = 'POST';
        return $this->query($params);
    }

    public function put($params = array())
    {
        $this->method = 'PUT';
        return $this->query($params);
    }

    public function delete($params = array())
    {
        $this->method = 'DELETE';
        return $this->query($params);
    }

    public function file($params = array())
    {
        $this->method = 'POST';//'FILE';
        return $this->queryFile($params);
    }

    private function query($params = array())
    {
	$ch = curl_init();
        $url = trim(\Yii::$app->params['core'] . trim($this->resource), '/');
        $body = trim($this->data_encode($params), '&');
        if (($this->method == 'GET')) {
            $url .= ($body) ? '?' . $body : '';
            $body = '';
        } else {
            // День 36-й...http_build_query продолжает вести себя странно и неоднозначно, написал короткий цикл...Продолжаем наблюдение.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        $httpHeaders = [
            'PLATFORM-ID: ' . \Yii::$app->params['platform_id'],
            'PLATFORM-TOKEN: ' . hash("sha256", \Yii::$app->params['platform_id'] . $url . $body . \Yii::$app->params['secret_key']
            ),
            'X-HTTP-Method-Override: ' . $this->method,
        ];
        if (!\Yii::$app->user->isGuest) {
            $httpHeaders[] = 'USER-ID: ' . \Yii::$app->user->id;
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        /* Блок сбора информации о запросе */
        $head = curl_exec($ch);
        $time = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $connectTime = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        \Yii::info("$url | $head | $body | $time | $this->method | $connectTime | $status", "apiResponse");
        /* Блок сбора информации о запросе */
        curl_close($ch);

        if ($this->flagArray) {
            $result = $this->parseResponseToArray($head);
        } else {
            $result = $this->parseResponse($head);
        }
        return $result;
    }

    private function queryFile($params = array())
    {
        $ch = curl_init();
        $url = trim(\Yii::$app->params['core'] . trim($this->resource), '/');

        curl_setopt($ch, CURLOPT_POST, 1);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $httpHeaders = [
            'PLATFORM-ID: ' . \Yii::$app->params['platform_id'],
            'PLATFORM-TOKEN: ' . hash("sha256", \Yii::$app->params['platform_id'] . $url .
                    $params['file_hash'] .
                    \Yii::$app->params['secret_key']
            ),
            'X-HTTP-Method-Override: ' . $this->method,
        ];
        unset($params['file_hash']);
        if (!\Yii::$app->user->isGuest) {
            $httpHeaders[] = 'USER-ID: ' . \Yii::$app->user->id;
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        /* Блок сбора информации о запросе */
        $head = curl_exec($ch);
        $time = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $connectTime = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        \Yii::info("$url | ".print_r($httpHeaders,1)." | $head | $time | $this->method | $connectTime | $status", "apiFileResponse");
        /* Блок сбора информации о запросе */
        curl_close($ch);
        return $this->parseResponse($head);
    }
    
    private function parseResponseToArray($data){
        $data = json_decode($data, true);
        return $data;
    }

    private function parseResponse($data)
    {
		$original = $data;
        $data = json_decode($data);
        if (!is_object($data)) {
            throw new Exception(serialize($data));
        }
        $data->serverError = false;
        $data->validateError = false;
        if (isset($data->errors)) {
            $data->validateError = true;
        }
        if (isset($data->error)) {
            $data->serverError = true;
        }
        if (isset($data->items) && is_array($data->items)) {
            $data->response = new \app\modules\iterator\responseIterator($data->items);
        } elseif (isset($data->items)) {
            $data->response = new \app\modules\iterator\responseIterator([$data->items], false);
        } else {
            $data->response = new \app\modules\iterator\responseIterator([], false);
        }
        $data->items = $data->response;
        return $data;
    }

    private function data_encode($data, $keyprefix = '', $keypostfix = '')
    {
        return http_build_query($data);
    }
}
