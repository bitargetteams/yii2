<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);
$config = [
    'id' => 'app-frontend',
    'defaultRoute' => 'page',
    'language' => 'ru_RU',
    'sourceLanguage' => 'en_US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        '@sections' => '@frontend/modules/sections',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                    'weight' => 100,
                ],
            ],
        ],
        'user'=>[
            'class' => 'app\components\authUserApi\Auth'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'page/error',
        ],
        'view' => [
			'class' => 'yii\web\View',
			'renderers' => [
				'tpl' => [
					'class' => 'yii\smarty\ViewRenderer',
					//'cachePath' => '@runtime/Smarty/cache',
				],
			],
			'theme' => [
                'pathMap' => [
                    '@app/views' => [
                        '@webroot/themes/flat/views',
                    ],
                ],
                'baseUrl' => '@webroot/themes/flat'
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'page/about' => 'page/about',
                'authorized' => 'page/authorized',
                'rpc/<controller:\w+>/<id:\d+>'=>'rpc/<controller>/rpc',
                'rpc/<controller:\w+>'=>'rpc/<controller>/rpc',
                'rpc/<controller:\w+>/<action:\w+>/<id:\d+>'=>'rpc/<controller>/<action>',
                'rpc/<controller:\w+>/<action:\w+>/'=>'rpc/<controller>/<action>',
                'news/<action:\w+>/<id:\d+>' => 'news/default/<action>',
                'audit/<action:\w+>/<id:\d+>' => 'sections/audit/<action>',
                'audit/<action:\w+>' => 'sections/audit/<action>',
                'remarks/<action:\w+>/<id:\d+>' => 'sections/remarks/<action>',
                'remarks/<action:\w+>' => 'sections/remarks/<action>',
                'teletext/<action:\w+>/<id:\d+>' => 'sections/teletext/<action>',
                'teletext/<action:\w+>' => 'sections/teletext/<action>',
                'context/<action:\w+>/<id:\d+>' => 'sections/context/<action>',
                'context/<action:\w+>' => 'sections/context/<action>',

				'jban/optimizer/<action:\w+>/<id:\d+>' => 'sections/jban-optimizer/<action>',
                'jban/optimizer/<action:\w+>' => 'sections/jban-optimizer/<action>',
                'jban/webmaster/<action:\w+>/<id:\d+>' => 'sections/jban-webmaster/<action>',
                'jban/webmaster/<action:\w+>' => 'sections/jban-webmaster/<action>',
                'jban' => 'sections/jban/index',
                'jban/webmaster' => 'sections/jban-webmaster/index',
                'jban/optimizer' => 'sections/jban-optimizer/index',
                'section/jban' => 'sections/jban-webmaster/index',

				'section/social' => 'sections/social-webmaster/index',
				'social/optimizer/<action:\w+>/<id:\d+>' => 'sections/social-optimizer/<action>',
				'social/optimizer/<action:\w+>' => 'sections/social-optimizer/<action>',
				'social/webmaster/<action:\w+>/<id:\d+>' => 'sections/social-webmaster/<action>',
				'social/webmaster/<action:\w+>' => 'sections/social-webmaster/<action>',
				'social' => 'sections/social/index',
				'social/webmaster' => 'sections/social-webmaster/index',
				'social/optimizer' => 'sections/social-optimizer/index',

                'section/<controller:\w+>' => 'sections/<controller>/index',
                'profile/notice' => 'notice/default/index',
                'profile/notice/<color:\d+>' => 'notice/default/index/<color>',
                'news/<action:\w+>/<date:\d{4}-\d{2}-\d{2}>' => 'news/default/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                'system/<controller:\w+>/<action:\w+>/<id:\d+>' => 'system/<controller>/<action>',


            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'login'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages'
                ],
                'registration'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages'
                ],
                'share'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages'
                ]
            ],
        ],
    ],
    'modules' => [
        'news' => [
            'class' => 'app\modules\news\Module',
        ],
        'mail' => [
            'class' => 'app\modules\mail\Module',
        ],
        'notice' => [
            'class' => 'app\modules\notice\Module',
        ],
        'sections' => [
            'class' => 'app\modules\sections\Module',
            'layout'=>'@sections/views/layouts/main',
        ],
        'rpc' => [
            'class' => 'app\modules\rpc\Module',
        ],
        'iterator' => [
            'class' => 'app\modules\iterator\Module',
        ],
    ],
    'params' => $params,
];

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'panels' => [
            'response' => [
                'class' => 'app\panels\ServerResponsePanel'
            ],
        ],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    
    //uncomment next string if forced assets refresh is needed
    //$config['components']['assetManager']['forceCopy'] = true;
    
    $config['components']['log']['traceLevel'] = 0;
    $config['components']['log']['targets'][] = [
        'class' => 'yii\log\FileTarget',
        'levels' => ['info','warning','error','trace'],
        'prefix' => function ($message) {
            
            return "[--CORE REQUEST/RESPONSE--]";
        },
        'logVars' => [],
        'categories' => ['apiResponse'],
        'logFile' => '@runtime/logs/response.log',
        'maxFileSize' => 1024 * 2,
        'maxLogFiles' => 20,
    ];
}

return $config;
