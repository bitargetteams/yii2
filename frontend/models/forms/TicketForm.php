<?php
namespace frontend\models\forms;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\components\Api;

class TicketForm extends Model
{

    const MAXFILES = 6;
    /**
     * @var int Идентификатор выбранной тематики сообщения
     */
    public $subject;
    public $message;
    //public $text;

    public $img;
//    public $parent_id;


    /**
     * @return array Набор правил валидации объедененных с правилами родительской формы
     */

    public function rules()
    {
        $childRules = array(
            array('subject', 'integer', 'integerOnly' => true),
            array('message', 'string'),
//            array('parent_id', 'integer', 'integerOnly' => true),
            array('img', 'file', 'maxFiles' => self::MAXFILES) //,'types'=> ['rar']
        );
        return array_merge($childRules, parent::rules());
    }

    /**
     * @return array Набор меток для полей модели объедененных с метками полей радительской формы
     */
    public function attributeLabels()
    {
        $childLabels = array(
            'subject' => Yii::t('ticket', 'subject'),
            'message' => Yii::t('ticket', 'message'),
            'img' => Yii::t('ticket', 'img'),
        );
        return $childLabels + parent::attributeLabels();
    }

    /**
     * @static
     * @return array Набор предопределенных тем для отправки сообщения в техподдежрку
     */
    public static function getSubjects()
    {
        return \yii\helpers\ArrayHelper::map(Api::resource('feedback/subject')->get(['type' => 'user'])->items, 'id', 'title');
    }

    public function save_attach($id)
    {
        $attachment = null;
        $files = UploadedFile::getInstances($this, 'img');
        $i=0;
        foreach ($files as $item) {
            if ($i++ > self::MAXFILES) break;
            $attachment = Api::resource('feedback/ticket/attachment')->file([
                    'collection' => 'ticket_uploads',
                    'file_hash' => hash_file('sha256', $item->tempName),
                    'file' => '@'.$item->tempName,
                    'id' => $id
            ]);
        }
        return true;

    }
    
    public function getAttach($id)
    {
        
    }
    
    public function save($id='')
    {
        $request =  Api::resource("feedback/ticket")->post([
                        'subject' => $this->subject,
                        'status' => '0',
                        'message' => $this->message,
                        'parent_id' => ($id) ? $id : null,
                        'sender_name' => \Yii::$app->user->firstname.' '.\Yii::$app->user->lastname,
                        'sender_email' => \Yii::$app->user->email,
                    ]);
        return (!isset($request->error)) ? $request->items : false;
    }
    
    public function getThread($id)
    {
        $request = Api::resource('feedback/ticket/thread')->get(['id' => $id]);
        return (!isset($request->error)) ? $request->items : false;
    }
    
    public function getParents()
    {
        $request = Api::resource('feedback/ticket')->get(['parent_id' => 0]);
        return (!isset($request->error)) ? $request->items : false;
    }
}