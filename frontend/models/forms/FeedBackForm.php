<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 08.10.14
 * Time: 18:32
 */

namespace frontend\models\forms;

use Yii;
use frontend\models\forms\base\IdiaFrom;
use yii\web\UploadedFile;
use app\components\Api;

class FeedBackForm extends IdiaFrom
{
    /**
     * @var string Имя автора сообщения
     */
    public $name;

    /**
     * @var type Адрес электронной почты автора сообщения
     */
    public $e_mail;

    /**
     * @var int Идентификатор выбранной тематики сообщения
     */
    //public $subject;

    public $img;


    /**
     * @return array Набор правил валидации объедененных с правилами родительской формы
     */

    public function rules()
    {
        $childRules = array(
            array('e_mail', 'email', 'skipOnEmpty' => true),
            array('name', 'string', 'min' => 2, 'skipOnEmpty' => true),
            //array('integer', 'integerOnly' => true),
            array('img', 'file') //,'types'=> ['rar']
        );
        return array_merge($childRules, parent::rules());
    }

    /**
     * @return array Набор меток для полей модели объедененных с метками полей радительской формы
     */
    public function attributeLabels()
    {
        $childLabels = array(
            'name' => Yii::t('feedback', 'name'),
            'e_mail' => Yii::t('feedback', 'e_mail'),
            //'subject' => Yii::t('feedback', 'subject'),
        );
        return $childLabels + parent::attributeLabels();
    }
    
    public function save()
    {
        $result = Api::resource("feedback/ticket")->post([
                'sender_name' => $this->name,
                'sender_email' => $this->e_mail,
                'subject' => "1",
                'status' => '0',
                'message' => $this->text
            ]);
        return (!isset($result->error)) ? $result->items->id : false;
    }

    public function save_attach($id)
    {
        $attachment = null;
        $files = UploadedFile::getInstances($this, 'img');
        //print_r($_REQUEST); print_r($_FILES); print_r($_POST);die;
        $i=0;
                foreach ($files as $item) {
                $i++;
                    //if (($attach = UploadedFile::getInstance($this, $item)) && $attach->error == 0) {

                        $attachment = Api::resource('feedback/ticket/attachment')->file([
                                'number' => $i,
                                'file_hash' => hash_file('sha256', $item->tempName),
                                'file' => '@'.$item->tempName,
                                'id' => $id
                            ]);
                    //}
                }

            //if (!$attachment->hasErrors()) {}
            return true;

    }
}