<?php
namespace frontend\models\forms;

//use frontend\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;
use app\components\Api;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $confirm;
    public $token;
    public $id;

    /**
     * @var \common\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($id, $token, $config = [])
    {
        $this->id=$id;
        $this->token=$token;
       if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        /*
        //var_dump($_POST); var_dump($_GET); exit;
        $this->_user = User::model()->select(["id"=>$id,"password_reset_token"=>$token]);
        //var_dump($this->_user);exit;
        if ($this->_user->hasErrors()) {
            throw new InvalidParamException('Wrong password reset token.');
        }*/
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['confirm', 'required'],
            ['confirm', 'string', 'min' => 6],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    { //var_dump($this->password, $this->id); exit;
        if($this->password!=$this->confirm){
            Yii::$app->getSession()->setFlash('error', \Yii::t('reset_password', 'Different passwords'));
            return false;
        }

        /*$user = new User("saveVerify_reset_token");
        $user->id=$this->id;
        $user->password=$this->password;
        $user->password_reset_token=$this->token;
        $user->save();*/
        $user=Api::resource("user/verify_reset_token")->post([
                "id" => $this->id,
                "password" => $this->password,
                "password_confirm" => $this->confirm,
                "password_reset_token" => $this->token
            ]);

        return !isset($user->error);
    }
}
