<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 17:26
 */

namespace frontend\models\forms;


use common\models\User;
use yii\base\Model;

class UpdatePassword extends Model {
    /**
     * @var
     */
    public $oldPassword;
    /**
     * @var
     */
    public $password;
    /**
     * @var
     */
    public $confirmPassword;

    public function rules()
    {
        return[
            [['oldPassword','password','confirmPassword'], 'required'],
            ['password','compare','compareAttribute'=>'confirmPassword'],
            [['oldPassword','password','confirmPassword'],'safe'],
        ];
    }

    public function attributeLabels()
    {

        return array_merge(
            [
                'oldPassword' => \Yii::t('registration', 'Old Password'),
                'password' => \Yii::t('registration', 'New Password'),
                'confirmPassword' => \Yii::t('registration', 'Repeat password'),
            ],
            parent::attributeLabels()
        );
    }
}