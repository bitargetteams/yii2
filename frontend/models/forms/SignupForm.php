<?php

namespace frontend\models\forms;

use common\models\User;
use yii\base\Model;
use Yii;
use app\components\Api;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $email;
    public $password;
    public $firstname;
    public $lastname;
    public $middlename;
    public $passwordRepeat;
    public $hash;
    public $inviteEmail;
    public $register;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'passwordRepeat', 'firstname', 'lastname'], 'required'],
            [['email', 'password', 'passwordRepeat', 'firstname', 'lastname', 'middlename'], 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['password', 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'passwordRepeat'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('signup', 'Email'),
            'password' => \Yii::t('signup', 'Password'),
            'passwordRepeat' => \Yii::t('signup', 'Password to compare'),
            'firstname' => \Yii::t('signup', 'Firstname'),
            'middlename' => \Yii::t('signup', 'Middlename'),
            'lastname' => \Yii::t('signup', 'Lastname'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $this->register = Api::resource('user')->post($this->getAttributes());
            if (!isset($this->register->error)) {
                $mail = new \app\models\ConfirmMail();
                $mail->sendConfirmation($this->register);
            }
        }
        return $this;
    }

}
