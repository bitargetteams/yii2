<?php

namespace frontend\models\forms;

//use common\models\User;
use yii\base\Model;
use app\components\Api;
//use Yii;

/**
 * Signup form
 */
class ConfirmForm extends Model
{

    public $email;
    public $id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'id'], 'required'],
            [['email', 'id'], 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['id', 'string', 'length' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('signup', 'Email'),
            'id' => \Yii::t('signup', 'Email confirmation hash'),
        ];
    }
    
    public function confirm()
    {
        if ($this->validate()) {
            $user = Api::resource('user/verify_email')->post([
                'email' => $this->email,
                'email_verification_hash' => $this->id,
            ]);
            if (!isset($user->error)) {
                $mail = new \app\models\ConfirmMail();
                $mail->confirmed($user);
            }
            return $user;
        }
        return null;
    }

}
