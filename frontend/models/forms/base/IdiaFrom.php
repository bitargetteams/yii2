<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 08.10.14
 * Time: 18:48
 */
namespace  frontend\models\forms\base;

use \yii\base\Model;
use Yii;
class IdiaFrom extends captchaForm {
    /**
     * @var string Текст сообщения "Подать идею"
     */
    public $text;

    /**
     * @var string Код проверки для captcha
     */
    public $verifyCode;

    /**
     * @return array Правила валидации модели
     */
    public function rules()
    {
        return [
            ['text', 'string', 'skipOnEmpty' => false, 'min' => 10],

        ];
    }

    /**
     * @return array Имена полей модели
     */
    public function attributeLabels()
    {
        return array_merge( parent::attributeLabels(),[
                'text' => Yii::t('idea', 'Text')
            ]
        );
    }
} 