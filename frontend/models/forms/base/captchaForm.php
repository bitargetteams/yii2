<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 19:03
 */

namespace frontend\models\forms\base;


use yii\base\Model;

/**
 * Class captchaForm
 *
 * @package frontend\models\forms\base
 */
class captchaForm extends Model {
    /**
     * @var
     */
    public $verifyCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['verifyCode', 'required'],
            ['verifyCode', 'captcha']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => \Yii::t('share',"Enter verify code"),
        ];
    }
} 