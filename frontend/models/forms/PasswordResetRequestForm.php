<?php
namespace frontend\models\forms;

//use app\components\ApiQuery;
//use frontend\models\User;
use yii\base\Model;
use app\models\PasswordResetMail;
use app\components\Api;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],

        ];

/*          ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
*/
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {

    //$user=User::model()->reset()->select(["email"=>$this->email]);
    $user=Api::resource("user/reset")->post(["email"=>$this->email]);

        if(!isset($user->error)) {

            /*$mail = new Mail;
            $mail->setAttributes(['email' => $this->email, 'subject'=>'hello', 'body'=>'http://front.dev/user/reset/'.$user->id.'?'.$user->password_reset_token, 'status' => 0, 'isRead' => 0]);
           // var_dump($mail); exit;
            $mail->save();
          //  var_dump($mail->getErrors());
            return true;*/
            $user->response->email=$this->email;
            $mail = new PasswordResetMail;
            $mail->sendConfirmation($user->response);
            return true;


        }




        return false;
    }
}
