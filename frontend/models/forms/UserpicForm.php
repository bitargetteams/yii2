<?php

namespace frontend\models\forms;

use common\models\User;
use yii\base\Model;
use Yii;
use app\components\Api;
use yii\web\UploadedFile;
use yii\base\Exception;
use frontend\models\Avatar;

/**
 * Signup form
 */
class UserpicForm extends Avatar
{
    const AVATAR_W = 80;
    const AVATAR_H = 80;
    
    public $rawfile;
    public $x;
    public $y;
    public $w;
    public $h;

    public $tmpPath;
    public $tmpUrl;
    public $tmpFile;

    public function __construct($config=[])
    {
        $this->tmpPath = \Yii::$app->assetManager->basePath . '/tmp/';
        $this->tmpUrl  = \Yii::$app->assetManager->baseUrl . '/tmp/';
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['rawfile'], 'image', 'extensions' => ['gif', 'png', 'jpg', 'jpeg']],
            [['x', 'y', 'w', 'h'], 'integer'],
            [['w'], 'integer', 'min' => self::AVATAR_W],
            [['w'], 'integer', 'min' => self::AVATAR_H],
        ];
    }

    public function attributeLabels()
    {
        return [
            'rawfile' => \Yii::t('signup', 'Userpic'),
            'x' => \Yii::t('signup', 'left position'),
            'y' => \Yii::t('signup', 'upperposition'),
            'w' => \Yii::t('signup', 'crop width'),
            'h' => \Yii::t('signup', 'crop height'),
        ];
    }

    public function saveTmp()
    {
        $this->rawfile = UploadedFile::getInstance($this,'rawfile');
        if (!is_dir($this->tmpPath)) mkdir($this->tmpPath);
        $filename = $this->getFileName().'.'.$this->rawfile->extension;
        $this->tmpFile = \Yii::$app->assetManager->baseUrl . '/tmp/'.$filename;
        return ($this->rawfile->saveAs($this->tmpPath.$filename)) ? true : false;
    }

    public function saveAvatar()
    {
        $pattern = $this->tmpPath.$this->getFileName().'.*';
        foreach (glob($pattern) as $filename) {
            $fileInfo = pathinfo($filename);
        }
        $image = $this->getImage($fileInfo['dirname'].'/'.$fileInfo['basename'], $fileInfo['extension']);
        $result = ImageCreateTrueColor(self::AVATAR_W, self::AVATAR_H);
        imagecopyresampled($result, $image, 0, 0, $this->x, $this->y, self::AVATAR_W, self::AVATAR_W, $this->w, $this->h);
        if (!is_dir($this->avatarPath)) mkdir($this->avatarPath);
        @unlink($fileInfo['dirname'].'/'.$fileInfo['basename']);
        $this->setAvatarFile();
        return (imagejpeg($result, $this->avatarPath.$this->getAvatarName(), 90)) ? true : false;
    }


}