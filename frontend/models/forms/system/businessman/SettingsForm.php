<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 29.10.14
 * Time: 19:52
 */

namespace frontend\models\forms\system\businessman;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;
use app\components\Api;

class SettingsForm extends Model
{
	public $certificate_reg;
	public $certificate_tax;
	public $email;

	public $checkboxes;

	public function rules()
	{
		return [
			[['email'], 'email'],
			[['checkboxes'], 'required'],
			[['certificate_reg', 'certificate_tax',], 'image'],
		];
	}

	public function attributeLabels()
	{
		return [
			'certificate_reg' => \Yii::t('profile', 'Scan'),
			'certificate_tax' => \Yii::t('profile', 'Scan'),
			'email' => \Yii::t('profile', 'Notification email'),
			'option_1' => \Yii::t('profile', 'Mail settings 1'),
			'option_2' => \Yii::t('profile', 'Mail settings 2'),
			'option_3' => \Yii::t('profile', 'Mail settings 3'),
			'option_4' => \Yii::t('profile', 'Mail settings 4'),
			'option_5' => \Yii::t('profile', 'Mail settings 5'),
		];
	}

	public function save($object_id)
	{
		$attachment = null;
		$files = array(
			'certificate_reg' => 'snils',
			'certificate_tax' => 'tax'
		);

		if ($this->validate()) {
			$mailing_settings = array();
			foreach ($this->checkboxes as $k=>$v) {
				$mailing_settings[$k] = (in_array($v, array('1','0'))) ? $v : '0';
			}

			$client_businessman = Api::resource('client/businessman/' . $object_id)->put([
				'id' => $object_id,
				'email' => $this->email,
				'mailing_settings' => serialize($mailing_settings)
			]);

			if (!isset($client_businessman->errors)) {
				foreach ($files as $item => $collection_name) {
					if (($attach = UploadedFile::getInstance($this, $item)) && $attach->error == 0) {

						$attachment = Api::resource('client/businessman/attachment')->file([
							'scan_type' => $collection_name,
							'file_hash' => hash_file('sha256', $attach->tempName),
							'file' => '@'.$attach->tempName,
							'object_id' => $object_id
						]);
					}
				}
			}

			//if (!$attachment->hasErrors()) {}
			return true;
		}

		return null;
	}

} 