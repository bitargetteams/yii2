<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 28.10.14
 * Time: 11:22
 */

namespace frontend\models\forms\system\businessman;

use yii\base\Model;
use Yii;
use app\components\Api;



class BusinessmanForm extends Model
{
	public $lastname;
	public $firstname;
	public $middlename;
	public $contact_person;
	public $phone;

	public $a_address;
	public $a_zip;
	public $a_city_id;
	public $a_region_id;
	public $a_country_id;

	public $l_address;
	public $l_zip;
	public $l_city_id;
	public $l_region_id;
	public $l_country_id;

	public $inn;
	public $ogrnip;
	public $ogrn_date;

	public $taxation_form;

	public $certificate_reg_series;
	public $certificate_reg_number;
	public $certificate_reg_date;

	public $certificate_tax_series;
	public $certificate_tax_number;
	public $certificate_tax_date;




	public function rules()
	{
		return [
			[
				[
					'firstname', 'lastname', 'phone', 'a_zip', 'a_country_id', 'a_region_id', 'a_city_id', 'a_address',
					'l_zip', 'l_country_id', 'l_region_id', 'l_city_id', 'l_address', 'inn', 'ogrnip', 'ogrn_date',
					'taxation_form', 'certificate_reg_series', 'certificate_reg_number', 'certificate_reg_date',
					'certificate_tax_series', 'certificate_tax_number', 'certificate_tax_date',
				],
				'required'
			],
			[
				[
					'firstname', 'lastname', 'middlename', 'contact_person', 'phone', 'a_address', 'l_address', 'inn', 'ogrnip',
					'certificate_reg_series', 'certificate_reg_number', 'certificate_tax_series', 'certificate_tax_number',
				],
				'filter', 'filter' => 'trim'
			],
			[
				[
					'a_zip', 'a_country_id', 'a_region_id', 'a_city_id', 'l_zip', 'l_country_id', 'l_region_id', 'l_city_id', 'taxation_form',
				],
				'number'
			],
			['phone', 'match', 'pattern' => '/^(?:\+?)\d{11,11}$/is'],

		];
	}

	public function attributeLabels()
	{
		return [
			'lastname' => \Yii::t('profile', 'Lastname'),
			'firstname' => \Yii::t('profile', 'Firstname'),
			'middlename' => \Yii::t('profile', 'Middlename'),
			'contact_person' => \Yii::t('profile', 'Contact person'),
			'phone' => \Yii::t('profile', 'Phone number'),
			'a_zip' => \Yii::t('profile', 'Zip'),
			'a_country_id' => \Yii::t('profile', 'Country'),
			'a_region_id' => \Yii::t('profile', 'Region'),
			'a_city_id' => \Yii::t('profile', 'City'),
			'a_address' => \Yii::t('profile', 'Address'),
			'l_zip' => \Yii::t('profile', 'Zip'),
			'l_country_id' => \Yii::t('profile', 'Country'),
			'l_region_id' => \Yii::t('profile', 'Region'),
			'l_city_id' => \Yii::t('profile', 'City'),
			'l_address' => \Yii::t('profile', 'Address'),
			'inn' => \Yii::t('profile', 'Inn'),
			'ogrnip' => \Yii::t('profile', 'Ogrnip'),
			'ogrn_date' => \Yii::t('profile', 'Ogrn date'),
			'taxation_form' => \Yii::t('profile', 'Taxation form'),

			'certificate_reg_series' => \Yii::t('profile', 'Number'),
			'certificate_reg_number' => \Yii::t('profile', 'Certificate on registration'),
			'certificate_reg_date' => \Yii::t('profile', 'Given date'),

			'certificate_tax_series' => \Yii::t('profile', 'Number'),
			'certificate_tax_number' => \Yii::t('profile', 'Certificate on tax'),
			'certificate_tax_date' => \Yii::t('profile', 'Given date'),
		];
	}

	public function save()
	{

		if ($this->validate()) {
			$client_businessman = [];

			$client_businessman['firstname'] = $this->firstname;
			$client_businessman['lastname'] = $this->lastname;
			$client_businessman['middlename'] = $this->middlename;
			$client_businessman['contact_person'] = $this->contact_person;
			$client_businessman['phone'] = $this->phone;

			$client_businessman['a_address'] = $this->a_address;
			$client_businessman['a_zip'] = $this->a_zip;
			$client_businessman['a_city_id'] = $this->a_city_id;
			$client_businessman['a_region_id'] = $this->a_region_id;
			$client_businessman['a_country_id'] = $this->a_country_id;
			$client_businessman['l_address'] = $this->l_address;
			$client_businessman['l_zip'] = $this->l_zip;
			$client_businessman['l_city_id'] = $this->l_city_id;
			$client_businessman['l_region_id'] = $this->l_region_id;
			$client_businessman['l_country_id'] = $this->l_country_id;

			$client_businessman['inn'] = $this->inn;
			$client_businessman['ogrnip'] = $this->ogrnip;
			$client_businessman['ogrn_date'] =$this->ogrn_date;
			$client_businessman['taxation_form'] = $this->taxation_form;

			$client_businessman['certificate_reg'] = serialize([
				'series' => $this->certificate_reg_series,
				'number' => $this->certificate_reg_number,
				'given_when' => $this->certificate_reg_date
			]);

			$client_businessman['certificate_tax'] = serialize([
				'series' => $this->certificate_tax_series,
				'number' => $this->certificate_tax_number,
				'given_when' => $this->certificate_tax_date
			]);


			$client_businessman = Api::resource('client/businessman')->post($client_businessman);

			if (isset($client_businessman->response->id) && is_numeric($client_businessman->response->id) && $client_businessman->response->id > 0) {
				return $client_businessman->response->id;
			}
		}

		return null;
	}
} 