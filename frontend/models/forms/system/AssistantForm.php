<?php
/**
 * Description of AssistantForm
 *
 * @author vl
 */

namespace frontend\models\forms\system;
use yii\base\Model;


class AssistantForm extends Model
{

    public $email;
    public $role;

    public function rules()
    {
        return [
            [['email', 'role'],'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'email'=>'E-mail',
            'role'=>'Роль'
        ];
    }

}
