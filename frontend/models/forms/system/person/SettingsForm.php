<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 29.10.14
 * Time: 19:52
 */

namespace frontend\models\forms\system\person;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;
use app\components\Api;

class SettingsForm extends Model
{
	public $passport1_scan;
	public $passport2_scan;
	public $ipc_scan;
 	public $inn_scan;
	public $email;
	public $ipc;
	public $inn;
	public $passport;

	public $checkboxes;



	public function rules()
	{
		return [
			[['ipc','inn','passport'],'filter', 'filter' => 'trim'],
			[['email'], 'email'],
			[['checkboxes'], 'required'],
			[['passport1_scan', 'passport2_scan', 'ipc_scan', 'inn_scan',], 'image'],
		];
	}

	public function attributeLabels()
	{
		return [
			'passport1_scan' => \Yii::t('profile', 'Passport Scan 1'),
			'passport2_scan' => \Yii::t('profile', 'Passport Scan 2'),
			'ipc_scan' => \Yii::t('profile', 'Scan'),
			'inn_scan' => \Yii::t('profile', 'Scan'),
			'email' => \Yii::t('profile', 'Notification email'),
			'option_1' => \Yii::t('profile', 'Mail settings 1'),
			'option_2' => \Yii::t('profile', 'Mail settings 2'),
			'option_3' => \Yii::t('profile', 'Mail settings 3'),
			'option_4' => \Yii::t('profile', 'Mail settings 4'),
			'option_5' => \Yii::t('profile', 'Mail settings 5'),
		];
	}

	public function save($object_id)
	{
		$attachment = null;
		$files = array(
			'passport1_scan' => 'passport',
			'passport2_scan' => 'passport',
			'ipc_scan' => 'snils',
			'inn_scan' => 'tax'
		);

		if ($this->validate()) {
			$mailing_settings = array();
			foreach ($this->checkboxes as $k=>$v) {
				$mailing_settings[$k] = (in_array($v, array('1','0'))) ? $v : '0';
			}

			$client_person = Api::resource('client/person')->put([
				'id' => $object_id,
				'email' => $this->email,
				'mailing_settings' => serialize($mailing_settings)
			]);

			if (!isset($client_person->errors)) {
				foreach ($files as $item => $collection_name) {
					if (($attach = UploadedFile::getInstance($this, $item)) && $attach->error == 0) {

						$attachment = Api::resource('client/person/attachment')->file([
							'scan_type' => $collection_name,
							'file_hash' => hash_file('sha256', $attach->tempName),
							'file' => '@'.$attach->tempName,
							'object_id' => $object_id
						]);
					}
				}
			}

			//if (!$attachment->hasErrors()) {}
			return true;
		}

		return null;
	}

} 