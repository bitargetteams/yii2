<?php

namespace frontend\models\forms\system\person;

use yii\base\Model;
use Yii;
use app\components\Api;

/**
 * Profile Person form
 */
class PersonForm extends Model
{

	public $lastname;
	public $firstname;
	public $middlename;

	public $birthday;

	public $birth_place;
	public $phone;

	public $a_address;
	public $a_zip;
	public $a_city_id;
	public $a_region_id;
	public $a_country_id;

	public $l_address;
	public $l_zip;
	public $l_city_id;
	public $l_region_id;
	public $l_country_id;

	public $passport_s;
	public $passport_n;
	public $passport_w;
	public $passport_date;
	public $passport_dc;

	public $ipc;
	public $inn;


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'firstname', 'lastname', 'birth_place', 'phone', 'inn', 'ipc',
					'birthday', 'passport_date',
					'l_country_id','l_region_id','l_city_id','a_country_id','a_region_id','a_city_id', 'a_zip', 'l_zip',
					'passport_s', 'passport_n', 'passport_w', 'passport_dc'
				],
				'required'
			],
			[
				[
					'firstname', 'lastname', 'middlename', 'phone', 'inn', 'ipc', 'a_zip', 'l_zip',
					'a_address', 'l_address', 'passport_s', 'passport_n', 'passport_w', 'passport_dc'
				],
				'filter', 'filter' => 'trim'
			],
			[
				[
					'l_country_id',
					'l_region_id','l_city_id','a_country_id','a_region_id','a_city_id', 'a_zip', 'l_zip'
				],
				'number'
			],
			['phone', 'match', 'pattern' => '/^(?:\+?)\d{11,11}$/is'],

		];
	}

	public function attributeLabels()
	{
		return [
			'lastname' => \Yii::t('profile', 'Lastname'),
			'firstname' => \Yii::t('profile', 'Firstname'),
			'middlename' => \Yii::t('profile', 'Middlename'),
			'birthday' => \Yii::t('profile', 'Birthday'),
			'birth_place' => \Yii::t('profile', 'Place of birth'),
			'phone' => \Yii::t('profile', 'Phone number'),
			'passport' => \Yii::t('profile', 'Number'),
			'inn' => \Yii::t('profile', 'Inn'),
			'ipc' => \Yii::t('profile', 'IPC Number'),
			'passport_w' => \Yii::t('profile', 'Given by'),
			'passport_date' => \Yii::t('profile', 'Given date'),
			'passport_dc' => \Yii::t('profile','Subdivision code'),

			'a_zip' => \Yii::t('profile', 'Zip'),
			'a_country_id' => \Yii::t('profile', 'Country'),
			'a_region_id' => \Yii::t('profile', 'Region'),
			'a_city_id' => \Yii::t('profile', 'City'),
			'a_address' => \Yii::t('profile', 'Address'),
			'l_zip' => \Yii::t('profile', 'Zip'),
			'l_country_id' => \Yii::t('profile', 'Country'),
			'l_region_id' => \Yii::t('profile', 'Region'),
			'l_city_id' => \Yii::t('profile', 'City'),
			'l_address' => \Yii::t('profile', 'Address'),


		];
	}

	/**
	 * Save Client_person data.
	 *
	 * @return Client_person|null the saved model or null if saving fails
	 */
	public function save()
	{

		if ($this->validate()) {
			$client_person = [];

			$client_person['firstname'] = $this->firstname;
			$client_person['lastname'] = $this->lastname;
			$client_person['middlename'] = $this->middlename;
			$client_person['birth_place'] = $this->birth_place;
			$client_person['birthday'] = $this->birthday;
			$client_person['phone'] = $this->phone;
			$client_person['inn'] = $this->inn;
			$client_person['ipc'] = $this->ipc;

			$client_person['a_address'] = $this->a_address;
			$client_person['a_zip'] = $this->a_zip;
			$client_person['a_city_id'] = $this->a_city_id;
			$client_person['a_region_id'] = $this->a_region_id;
			$client_person['a_country_id'] = $this->a_country_id;
			$client_person['l_address'] = $this->l_address;
			$client_person['l_zip'] = $this->l_zip;
			$client_person['l_city_id'] = $this->l_city_id;
			$client_person['l_region_id'] = $this->l_region_id;
			$client_person['l_country_id'] = $this->l_country_id;


			$client_person['passport'] = serialize([
				'series' => $this->passport_s,
				'number' => $this->passport_n,
				'issued_by' => $this->passport_w,
				'issued_when' => $this->passport_date,
				'department_code' => $this->passport_dc
			]);

			//$client_person = $client_person->save();
			$client_person = Api::resource('client/person')->post($client_person);

			if (isset($client_person->items->location_id) && is_numeric($client_person->items->location_id) && $client_person->items->location_id > 0) {
				return $client_person->items->location_id;
			}
		}

		return null;
	}


}
