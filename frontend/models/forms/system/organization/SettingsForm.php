<?php

namespace frontend\models\forms\system\organization;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;
use app\components\Api;

class SettingsForm extends Model
{
    public $ogrn;
    public $ogrn_date;
    public $ogrn_scan;
    public $ogrn_verified;
    public $certificate_reg;
    public $certificate_reg_date;
    public $certificate_reg_scan;
    public $certificate_reg_verified;
    public $certificate_tax;
    public $certificate_tax_date;
    public $certificate_tax_scan;
    public $certificate_tax_verified;
    
    public $email;
    public $email_verified;
    public $checkboxes;
    public $object_id;


    public function rules()
    {
        return [
            [['email', 'checkboxes'], 'required'],
            [['ogrn', 'certificate_reg', 'certificate_tax'], 'string', 'max' => 128],
            [['object_id'], 'integer'],
            //[['ogrn_date', 'certificate_reg_date', 'certificate_tax_date'], 'date'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('profile', 'Organization name'),
            'fullname' => \Yii::t('profile', 'Organization full name'),

        ];
    }
    
    public function load($data, $formName = null)
    {
        parent::load($data, $formName);
        $this->certificate_reg = serialize([
                        'number' => $data['SettingsForm']['certificate_reg'],
                        'given_when' => $data['SettingsForm']['certificate_reg_date'],
                        ]);
        $this->certificate_tax = serialize([
                        'number' => $data['SettingsForm']['certificate_tax'],
                        'given_when' => $data['SettingsForm']['certificate_tax_date'],
                        ]);
        $this->checkboxes = serialize($data['SettingsForm']['checkboxes']);
//        echo '<pre>', print_r($this, 1);die;
        return true;
    }

    public function save()
    {
        $files = array(
            'ogrn_scan' => 'ogrn',
            'certificate_reg' => 'reg',
            'certificate_tax' => 'tax'
            );
        if ($this->validate()) {
            $res = Api::resource('client/organization/'.$this->object_id)->put([
                'email' => $this->email,
                'certificate_reg' => $this->certificate_reg,
                'certificate_tax' => $this->certificate_tax,
                'mailing_settings' => $this->checkboxes,
            ]);
        }
        if (!isset($res->errors)){
            foreach ($files as $item => $collection_name) {
                if (($attach = UploadedFile::getInstance($this, $item)) && $attach->error == 0) {

                    $attachment = Api::resource('client/organization/attachment')->file([
                            'scan_type' => $collection_name,
                            'file_hash' => hash_file('sha256', $attach->tempName),
                            'file' => '@'.$attach->tempName,
                            'object_id' => $this->object_id
                    ]);
                }
            }
            return true;
        }        

        return null;
    }
}