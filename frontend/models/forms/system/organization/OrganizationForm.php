<?php

namespace frontend\models\forms\system\organization;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;
use app\components\Api;
use frontend\components\validators\ValidateOgrn;

class OrganizationForm extends Model
{
    public $name;
    public $fullname;

    /* Юридический адрес */
    public $a_country_id;
    public $a_region_id;
    public $a_city_id;
    public $a_address;
    public $a_zip;

    /* Фактический адрес */
    public $l_country_id;
    public $l_region_id;
    public $l_city_id;
    public $l_address;
    public $l_zip;

    /* Реквизиты */
    public $director;
    public $accountant;
    public $contact_person;
    public $phone;

    /* Свидетельства и их сканы */
    public $inn;
    public $kpp;
    public $ogrn;
    public $ogrn_date;
    public $certificate_reg;
    public $certificate_reg_date;
    public $certificate_tax;
    public $certificate_tax_date;
    public $taxation_form;

    /* Банковская информация */
//    public $bank_name;
//    public $bank_bik;
//    public $bank_account_num;
//    public $bank_cor_num;


    public function rules()
    {
        return [
            [['name', 'fullname', /*'a_country_id', 'a_region_id', 'a_city_id',*/ 
              'a_address', 'a_zip', /*'l_country_id', 'l_region_id', 'l_city_id',*/ 
              'l_address', 'l_zip', 'director', 'accountant', 'contact_person',
              'phone', 'inn', 'kpp', 'ogrn'], 'required'],
            [['name', 'fullname', 'a_country_id', 'a_region_id', 'a_city_id', 
              'a_address', 'a_zip', 'l_country_id', 'l_region_id', 'l_city_id', 
              'l_address', 'l_zip', 'director', 'accountant', 'contact_person',
              'phone', 'inn', 'kpp', 'ogrn', 'ogrn_date', 'certificate_reg',
              'certificate_reg_date', 'certificate_tax', 'certificate_tax_date',
              'taxation_form', /*'bank_name', 'bank_bik', 'bank_account_num',
              'bank_cor_num'*/], 'filter', 'filter' => 'trim'],
            [['a_country_id', 'a_region_id', 'a_city_id', 'a_zip', 'l_country_id',
              'l_region_id', 'l_city_id', 'l_zip'], 'integer'],
            [['ogrn', 'certificate_reg', 'certificate_tax', 'phone'], 'double'],
            [['ogrn'], 'frontend\components\validators\validateOgrn'],
            [['inn'], 'frontend\components\validators\validateInn'],
            //[['ogrn_date', 'certificate_reg_date', 'certificate_tax_date'], 'date'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('profile', 'Organization name'),
            'fullname' => \Yii::t('profile', 'Organization full name'),

            /* Юридический адрес */
            'a_country_id' => \Yii::t('profile', 'Registration Country'),
            'a_region_id' => \Yii::t('profile', 'Registration Region'),
            'a_city_id' => \Yii::t('profile', 'Registration City'),
            'a_address' => \Yii::t('profile', 'Registration Address'),
            'a_zip' => \Yii::t('profile', 'Registration Zip code'),

            /* Фактический адрес */
            'l_country_id' => \Yii::t('profile', 'Real Country'),
            'l_region_id' => \Yii::t('profile', 'Real Region'),
            'l_city_id' => \Yii::t('profile', 'Real City'),
            'l_address' => \Yii::t('profile', 'Real Address'),
            'l_zip' => \Yii::t('profile', 'Real Zip code'),

            /* Реквизиты */
            'director' => \Yii::t('profile', 'Director name'),
            'accountant' => \Yii::t('profile', 'Accountant name'),
            'contact_person' => \Yii::t('profile', 'Contact person'),
            'phone' => \Yii::t('profile', 'Phone number'),

            /* Свидетельства и их сканы */
            'inn' => \Yii::t('profile', 'Inn'),
            'kpp' => \Yii::t('profile', 'KPP'),
            'ogrn' => \Yii::t('profile', 'Ogrn'),
            'ogrn_date' => \Yii::t('profile', 'Ogrn date'),
            'certificate_reg' => \Yii::t('profile', 'Registration certificate'),
            'certificate_reg_date' => \Yii::t('profile', 'Registration certificate date'),
            'certificate_tax' => \Yii::t('profile', 'Tax certificate'),
            'certificate_tax_date' => \Yii::t('profile', 'Tax certificate date'),

            'taxation_form' => \Yii::t('profile', 'Taxation form'),

            /* Банковская информация */
//            'bank_name' => \Yii::t('profile', 'Bank name'),
//            'bank_bik' => \Yii::t('profile', 'BIK'),
//            'bank_account_num' => \Yii::t('profile', 'Current account'),
//            'bank_cor_num' => \Yii::t('profile', 'Correspondent account'),
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $params = array_merge($this->attributes,
                [
                    'certificate_reg' => serialize([
                        'number' => $this->certificate_reg,
                        'given_when' => $this->certificate_reg_date,
                        ]),
                    'certificate_tax' => serialize([
                        'number' => $this->certificate_tax,
                        'given_when' => $this->certificate_tax_date,
                        ]),
                    'taxation_form' => ($this->taxation_form == true) ? 1:0,
                ]);
            $client = Api::resource('client/organization')->post($params);
            if (!isset($client->error)) {

            }
            return $client;
        }

        return null;
    }

    public function getCountries()
    {
            $res = Api::resource('geo/country')->get();
            $countries = ['0' => \Yii::t('profile', 'Choose your country')];
            if (isset($res->items) && $res->items->is_array() && count($res->items)) {
                    foreach ($res->items as $item) $countries[$item->id] = $item->title;
            }
            return 	$countries;
    }

}