<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 20:28
 */

namespace frontend\models\forms;


use yii\base\Model;

class EditPhone extends Model{
    public $phone_number;
    public $password;

    public function rules()
    {
        return[
            [['phone_number','password'], 'required'],
            ['phone_number', 'match', 'pattern' => '/^(?:\+?)\d{11,11}$/is']
        ];
    }

    public function attributeLabels()
    {

        return array_merge(
            [
                'phone_number' => \Yii::t('profile','Phone number'),
                'password' => \Yii::t('registration', 'You Password'),
            ],
            parent::attributeLabels()
        );
    }
} 