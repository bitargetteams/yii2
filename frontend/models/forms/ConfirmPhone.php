<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 18:21
 */

namespace frontend\models\forms;


use yii\base\Model;

class ConfirmPhone extends Model {
    public $phone_number;
    public $pin;
    public $password;

    public function rules()
    {
        return[
            [['phone_number','password','pin'], 'required'],
            ['pin','string','length'=>[3,4]],
            ['phone_number', 'match', 'pattern' => '/^(?:\+?)\d{11,11}$/is']
        ];
    }

    public function attributeLabels()
    {
        return[
            'phone_number' => \Yii::t('registration','Phone: '),
            'pin' => \Yii::t('registration','Pin'),
            'password' => \Yii::t('registration','Password'),
        ];
    }
} 