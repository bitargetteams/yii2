<?php

namespace frontend\models;

use app\components\apiModel\ApiBaseModel;
use app\components\apiModel\ApiQuery;

class Ticket extends ApiBaseModel{
    
    public static function entityName()
    {
        return 'ticket';
    }

    /**
     * Метод создания модели
     * @param $className
     * @return ApiQuery
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getId()
    {
        return $this->id;
    }

    public function select()
    {
        $this->id;
    }    
}