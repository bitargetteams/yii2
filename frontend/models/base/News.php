<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $teaser
 * @property string $text
 * @property string $create_time
 * @property string $update_time
 * @property string $news_time
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['create_time', 'update_time', 'news_time'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['teaser'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'teaser' => Yii::t('app', 'Teaser'),
            'text' => Yii::t('app', 'Text'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'news_time' => Yii::t('app', 'News Time'),
        ];
    }
}
