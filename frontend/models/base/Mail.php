<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "{{%mail}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property integer $status
 * @property integer $isRead
 * @property string $created_time
 * @property string $sent_time
 */
class Mail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%mail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'subject', 'body', 'status', 'isRead'], 'required'],
            [['body'], 'string'],
            [['status', 'isRead'], 'integer'],
            [['created_time', 'sent_time'], 'safe'],
            [['email', 'subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'subject' => Yii::t('app', 'Subject'),
            'body' => Yii::t('app', 'Body'),
            'status' => Yii::t('app', 'Status'),
            'isRead' => Yii::t('app', 'Is Read'),
            'created_time' => Yii::t('app', 'Created Time'),
            'sent_time' => Yii::t('app', 'Sent Time'),
        ];
    }
}
