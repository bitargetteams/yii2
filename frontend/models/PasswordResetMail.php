<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 28.10.14
 * Time: 12:41
 */

namespace app\models;

use app\modules\mail\MailTemplate;
use frontend\models\User;
use Yii;
use app\models\base\Mail;
use yii\base\Exception;


class PasswordResetMail extends Mail {

    public function sendConfirmation(User $user)
    {
        $subject = \Yii::t('login','Follow the link to reset your password');
        $params = [
            'id' => $user->id,
            'hash' => $user->password_reset_token
        ];

        $mail = new MailTemplate($subject,'reset-password',$params);
        $this->email = $user->email;
        $this->subject = $mail->getSubject();
        $this->body = $mail->body;
        $this->status = 0;
        $this->isRead = 0;
        if ($this->save()) {
            $this->sendMail($this->id);
        }
    }


    public function confirmed($email)
    {
    }

    public function sendMail($id)
    {
        $mail = self::findOne(['id'=>$id]);
        //var_dump($mail);die;
        $sender = @mail($mail->email,$mail->subject,$this->body);
        if($sender)
        {
            $mail->status = 1;
            $mail->save(false);
        }else
            throw new Exception("Mail can't bee sending");
    }


} 