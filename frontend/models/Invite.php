<?php

/**
 * Description of Access
 *
 * @author vl
 */
namespace frontend\models;

use app\components\apiModel\ApiBaseModel;
use app\components\apiModel\ApiQuery;

class Invite extends ApiBaseModel
{
    public static function entityName()
    {
        return 'invite';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function saveValidate(){}

}
