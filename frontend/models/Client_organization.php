<?php

namespace frontend\models;

use app\components\apiModel\ApiBaseModel;
use app\components\apiModel\ApiQuery;
use app\components\authUserApi\identityApiUserInterface;

//namespace app\models;
//
//use app\components\apiModel\ApiBaseModel;

class Client_organization extends ApiBaseModel
{
    public static function entityName()
    {
        return 'client_organization';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
} 