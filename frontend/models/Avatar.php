<?php

namespace frontend\models;

/* It is supposed, avatars are cached in /web/assets/avatar/ folder
 * as .jpg file, name as crc32 of user->id.
 *
 * To get avatar url just use such code:
 *
 *  $avatar = new Avatar();
 *  $avatar->getAvatarFileUrl();
 *
 * This model is extended with UserpicForm model
 */

use common\models\User;
use yii\base\Model;
use Yii;
use app\components\Api;
use yii\base\Exception;

class Avatar extends Model
{
    public $avatarPath;
    public $avatarUrl;
    public $avatarFile;

    public function __construct($config=[])
    {
        $this->avatarPath = \Yii::$app->assetManager->basePath . '/avatar/';
        $this->avatarUrl  = \Yii::$app->assetManager->baseUrl . '/avatar/';
        parent::__construct($config);
    }

    protected function getImage($path, $ext)
    {
        switch ($ext){
        case 'gif':
                $i = imagecreatefromgif($path);
                break;
        case 'jpg':
        case 'jpeg':
                $i = imagecreatefromjpeg($path);
                break;
        case 'png':
                $i = imagecreatefrompng($path);
                break;
        }
        return $i;
    }

    public function getFileName()
    {
        return crc32(\Yii::$app->user->id);
    }

    public function getAvatarName()
    {
        return $this->getFileName().'.jpg';
    }

    public function setAvatarFile()
    {
        $this->avatarFile = $this->avatarUrl.$this->getAvatarName();
        return true;
    }

    public function checkAvatarFile()
    {
            return (is_file($this->avatarPath.$this->getAvatarName())) ?
            $this->avatarUrl.$this->getAvatarName() : false;
    }
    
    protected function saveAvatarFile($file_content)
    {
        $avatar = $this->avatarPath.$this->getAvatarName();
        if ($this->checkAvatarFile()) @unlink($avatar);
        return (file_put_contents($avatar, $file_content)) ? true : false;
    }

    public function getAvatarFileUrl(){
        if($this->checkAvatarFile()) { //avatar cached
            return $this->checkAvatarFile();
        } else { //get avatar from core
            $getAttach = Api::resource('user/get_attachment')->get([
                'user_id' => \Yii::$app->user->id,
                'image_type' => 'avatar',
            ]);
            if (!isset($getAttach->errors)) {
                $this->saveAvatarFile(base64_decode($getAttach->response->img_body));
                return $this->checkAvatarFile();
            } else {
                return false;
            }
        }
    }

}