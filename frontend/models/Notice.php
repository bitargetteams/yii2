<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 29.10.14
 * Time: 13:40
 */

namespace frontend\models;
use app\components\apiModel\ApiBaseModel;
use app\components\apiModel\ApiQuery;

class Notice extends ApiBaseModel{

    public static function entityName()
    {
        return 'notice';
    }

    /**
     * Метод создания модели
     * @param $className
     * @return ApiQuery
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getId()
    {
        return $this->id;
    }

    public function select()
    {
        $this->id;
    }

} 