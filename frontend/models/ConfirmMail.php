<?php

namespace app\models;

use app\modules\mail\MailTemplate;
use frontend\models\User;
use Yii;
use app\models\base\Mail;
use yii\base\Exception;

/**
 * This is the model class for table "mail".
 *
 */
class ConfirmMail extends Mail
{
    
    public function sendConfirmation($user)
    {
        $subject = \Yii::t('login','Please confirm your e-mail');
        $params = [
            'id' => $user->response->email_verification_hash,
            'email' => $user->response->email
        ];

        $mail = new MailTemplate($subject,'confirm-password',$params);
        $this->email = $user->response->email;
        $this->subject = $mail->getSubject();
        $this->body = $mail->body;
        $this->status = 0;
        $this->isRead = 0;
        if ($this->save())
            $this->sendMail($this->id);
    }
    
    public function confirmed($email)
    {
    }
    
    public function sendMail($id)
    {
        $mail = self::findOne(['id'=>$id]);
        $sender = @mail($mail->email,$mail->subject,$this->body,'Content-type: text/html');
        if($sender)
        {
            $mail->status = 1;
            $mail->save(false);
        }//else
           // throw new Exception("Mail can't bee sending");
    }
}
