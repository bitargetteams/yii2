<?php

namespace frontend\models;

use app\modules\mail\MailTemplate;
use frontend\models\User;
use Yii;
use app\models\base\Mail;
use yii\base\Exception;

/**
 * This is the model class for table "mail".
 *
 */
class InviteMail extends Mail
{
    
    public function sendInvite($email,$hash)
    {
        $subject = \Yii::t('share','You will be issued access to the project');
        $params = [
            'hash'=>$hash,
            'email' => $email
        ];

        $mail = new MailTemplate($subject,'send-invite',$params);
        $this->email = $email;
        $this->subject = $mail->getSubject();
        $this->body = $mail->body;
        $this->status = 0;
        $this->isRead = 0;
        $this->save();
//        if ($this->save())
//            $this->sendMail($this->id);
    }
    
    public function confirmed($email)
    {
    }
    
    public function sendMail($id)
    {
        $mail = self::findOne(['id'=>$id]);
        $sender = @mail($mail->email,$mail->subject,$this->body,'Content-type: text/html');
        if($sender)
        {
            $mail->status = 1;
            $mail->save(false);
        }//else
           // throw new Exception("Mail can't bee sending");
    }
}
