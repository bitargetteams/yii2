<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 05.11.14
 * Time: 20:10
 */

namespace frontend\behaviors;


use yii\base\Behavior;
use yii\web\Controller;

class AjaxBehavior extends Behavior{

    public $actionsUsed = [];

    public function events()
    {
        return array_merge(parent::events(),
            [
                Controller::EVENT_BEFORE_ACTION => 'beforeAction'
            ]
        );
    }

    public function beforeAction($event)
    {
        if(!\Yii::$app->request->isAjax && in_array($event->action->id,$this->actionsUsed))
            $this->owner->redirect('/page/access-denied');
    }


} 