<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 13:34
 */

namespace frontend\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;

/**
 * Class RoleBehavior
 *
 * @package yii\base
 */
class RoleBehavior extends Behavior
{

    public $onlyAuth = true;
    public $allowActions = [];
    public $equivActions = [];

    public function events()
    {
        return array_merge(
            parent::events(), array(
                Controller::EVENT_BEFORE_ACTION => 'beforeAction',
            )
        );
    }

    /**
     * @param $event
     *
     * @return bool
     */
    public function allowedByAuthUsers($event)
    {
        return !Yii::$app->user->isGuest;
    }


    public function beforeAction($event)
    {
        if (($this->onlyAuth ^ $this->allowedByAuthUsers($event))
            && (count($this->allowActions) === 0 || in_array($event->action->id, $this->allowActions))
        ) {
            if (isset($this->equivActions[$event->action->id])) {
                $action = $this->equivActions[$event->action->id];
                $actionParams = $event->action->controller->actionParams;
                echo $this->owner->runAction($action, $actionParams);
                Yii::$app->end(0);
            } else {
                Yii::$app->controller->redirect('/page/access-denied');
            }

        }
    }
} 