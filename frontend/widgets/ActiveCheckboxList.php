<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 07.11.14
 * Time: 11:46
 */

namespace frontend\widgets;


use frontend\helpers\CHtml;
use yii\base\Exception;
use yii\base\Model;
use yii\base\Widget;

/**
 * Class ActiveCheckboxList
 *
 * @package frontend\widgets
 */
class ActiveCheckboxList extends Widget {
    /**
     * @var string
     * определят html аттрибут ID для враппера виджета
     * по умолчанию берется {название формы}_{название поля формы}
     */
    public $id;
    /**
     * @var Model|mixed
     * Модель данных для списка чекбоксов
     */
    public $model;
    /**
     * @var Model
     * Модель формы
     */
    public $form;
    /**
     * @var string
     * Название аттрибута для CheckboxList
     */
    public $attribute;
    /**
     * Это свойство определяет какой набор доп данных использовать.
     * По умолчанию используются все поля модели.
     * @var array
     * @default NULL
     */
    public $meta;
    /**
     * @var string
     * Это свойство определяет шаблон списка чекбоксов
     * Есть 3 основных тега которые используются для определения шаблона:
     *  +   {input} -   определят HTML тег input с типом checkbox
     *  +   {label} -   определят Лайбл к инпут тегу
     *  +   {meta}  -   определят область доплнительных данных, содержащиеся в модели
     *
     */
    public $template;
    /**
     * @var string
     * Это свойство определяет шаблон дополнительных данных из модели
     *  +   {data}  -   определят текст мета-данных
     *  +   {name}  -   название атрибута
     *  По умолчанию оборачивает в тег <span> с классом названия аттрибута модели
     */
    public $templateMeta;

    /**
     * @var array
     */
    protected $list = [];

    /**
     * @throws Exception
     */
    public function init()
    {
        parent::init();


        $this->requiredParams('form');
        $this->requiredParams('model',NULL);
        $this->requiredParams('attribute',NULL);

        $this->id = !is_null($this->id) ? $this->id : $this->form->formName(). "_" . $this->attribute;

        $this->model = (array)$this->model;

        if($this->template === NULL)
            $this->template = '{input}<span class="ch-bx-label">{label}</span><span class="ch-bx-meta">{meta}</span>';
        if($this->templateMeta === NULL)
            $this->templateMeta = '<span class="{name}">{data}</span>';
    }

    /**
     * @return string
     */
    public function run()
    {
        $attribute = $this->attribute;
        foreach($this->model as $key => $row)
        {
            $this->list[$key] = [];
            $name =  $this->form->formName(). "[" . $attribute . "][{$row['id']}]";
            $field = $this->form->$attribute;
            $checked = isset($field[$row['id']]) ? true : false;
            $this->list[$key]['input'] = CHtml::checkbox($name,$checked,['value' => $row['id']]);
            $this->list[$key]['label'] = $row['name'];
            $this->processMeta($key,$row);
        }

        echo $this->process();
    }

    /**
     * @param $key
     * @param $row
     */
    protected function processMeta($key,$row)
    {
        $this->list[$key]['meta'] = '';
        foreach($row as $labelRec => $rec)
        {
            if(
                ($labelRec === 'id' || $labelRec === 'name') ||
                (is_array($this->meta) && !in_array($labelRec,$this->meta))
            )
                continue;
            $patterns[0] = '/\{name\}/';
            $patterns[1] = '/\{data\}/';
            $replaced[0] = $labelRec;
            $replaced[1] = $rec;
            $template = $this->templateMeta;
            $this->list[$key]['meta'] .= preg_replace($patterns,$replaced,$template);
        }
    }

    /**
     * @return string
     */
    protected function process()
    {
        $result = "";
        foreach($this->list as $key => $rec) {
            $patterns[0] = '/\{input\}/';
            $patterns[1] = '/\{label\}/';
            $patterns[2] = '/\{meta\}/';
            $replaced[0] = $rec['input'];
            $replaced[1] = $rec['label'];
            $replaced[2] = $rec['meta'];
            $template = $this->template;
            $result .= "<label>" . preg_replace($patterns,$replaced,$template) . "</label>\r\n";
        }
        return $result;
    }

    /**
     * @param        $param
     * @param string|NULL $instance
     *
     * @throws Exception
     */
    protected function requiredParams($param,$instance='Yii\base\Model')
    {
        if($this->$param === null or ($instance !== NULL && !$this->$param instanceof $instance))
            throw new Exception("Param {{$param}} must be instanced of class {{$instance}} and can't be NULL");
    }
} 