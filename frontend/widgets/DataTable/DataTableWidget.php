<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 26.11.14
 * Time: 10:19
 */

namespace frontend\widgets\DataTable;

use yii\bootstrap\Widget;
use yii\helpers\Json;
use yii\web\View;
use yii\helpers\Url;


class DataTableWidget extends Widget {
	public $id = 'data_table';
	public $template;
	public $jsFile;
	public $model;
	public $model_class;

	public function init()
	{
		$arrNameClass = explode('\\',get_class($this->model));
		$this->options['modelClass'] = $this->model_class = end($arrNameClass);


		if(isset($this->options['id']))
		{
			$this->id = $this->options['id'];
		}
		if(is_null($this->template))
		{
			$this->template = '@DataTable/views/default.php';
		}


	}

	public function run()
	{
		$this->registerPlugin('dataTable');
		$this->get();
	}

	protected function registerPlugin($name)
	{
		$view = $this->view;

		DataTableAssets::register($view);
		\Yii::setAlias('@DataTable','@frontend/widgets/DataTable');

		$options = empty($this->options) ? '' : Json::encode($this->options);

		if (isset($this->jsFile) && strlen($this->jsFile)) {
			$js = "$.extend( jQuery('#$this->id').dataTable.defaults, $options);";
			$view->registerJs($js,View::POS_END);
			$view->registerJs(file_get_contents(Url::to($this->jsFile)),View::POS_READY);
		} else {
			$js = "jQuery('#$this->id').$name($options);";
			$view->registerJs($js,View::POS_END);
		}
	}

	protected function get()
	{
		echo $data = $this->renderFile(
			$this->template,
			[
				'id'=>$this->id,
				'model'=>$this->model,
				'className'=>$this->model_class,
				'options'=>$this->options
			]);
	}
}