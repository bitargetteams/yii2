<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 26.11.14
 * Time: 10:09
 */

namespace frontend\widgets\DataTable;


use yii\web\AssetBundle;
use yii\web\View;

class DataTableAssets extends AssetBundle {
	public $sourcePath = '@app/widgets/DataTable';
	public $jsOptions = ['position' => View::POS_END];
	public $css = [
		'css/jquery.dataTables.css',
		'css/default.css'
	];
	public $js = [
		'js/jquery.dataTables.js',
		'js/jquery.dataTables.columnFilter.js'
	];
	public $depends = [
		//'yii\web\JqueryAsset'
	];
} 