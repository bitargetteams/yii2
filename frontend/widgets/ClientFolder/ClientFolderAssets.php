<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 20.11.14
 * Time: 17:44
 */

namespace frontend\widgets\ClientFolder;

use yii\web\AssetBundle;

class ClientFolderAssets extends AssetBundle {
	public $sourcePath = '@app/widgets/ClientFolder';
	public $jsOptions = [];
	public $css = [
		'css/client_folder.css'
	];
	public $js = [
		'js/client_folder.js'
	];
	public $depends = [];
}