<?php
use yii\helpers\Html;
?>
<div class="<?= $id ?>">

	<div class="form-group field-<?= strtolower($className) ?>-folder_id required">
		<label class="control-label" for="<?= strtolower($className) ?>-folder_id"><?= $options['title']?></label>
		<?= Html::dropDownList($className.'[folder_id]',null, $model->getFolders(),['id'=>'folder_id', 'class' => 'form-control', 'style'=>'width:300px;'])?>
		<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			<button type="button" class="btn btn-primary">+</button>
		</a>
		<div class="help-block"></div>
		<input type="hidden" id="folder_section" value="<?=\Yii::$app->session->get('section', 'jban')?>">
		<div id="collapseOne" class="panel-collapse collapse">
			<?= Html::textInput('add_input','',['id'=>'i_add', 'class'=>'form-control','style'=>'width:300px;float:left;'])?>
			<button type="button" class="btn btn-primary" id="b_add">+</button>
		</div>
	</div>

</div>
