/**
 * Created by ddo on 20.11.14.
 */

$(document).ready(function() {
	$( document ).on( 'click', '#b_add', function(){
		select = $('#folder_id');
		select.html('');
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/ajax/addfolder",
			data: {name: $('#add_input').val(), section: $('#folder_section').val()},
			success: function(list) {
				$.each(list, function(i) {select.append('<option value="' + i + '">' + this + '</option>');});


			}
		});
		$("#collapseOne").hide();

	});

});
