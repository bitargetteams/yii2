<?php
/**
 * Created by PhpStorm.
 * User: ddo
 */
namespace frontend\widgets\ClientFolder;

use yii\bootstrap\Widget;
use frontend\widgets\ClientFolder\ClientFolderAssets;



class ClientFolderWidget extends Widget {
	public $id = 'client_folder';
	public $template;
	public $model;
	public $model_class;

	public function init()
	{
		$arrNameClass = explode('\\',get_class($this->model));
		$this->model_class = array_pop($arrNameClass);

		if(isset($this->options['id']))
		{
			$this->id = $this->options['id'];
		}
		if(is_null($this->template))
		{
			$this->template = '@ClientFolder/views/default.php';
		}

		if(!isset($this->options['title']))
		{
			$this->options['title'] = 'Client folder';
		}
		if(!isset($this->options['section']))
		{
			$this->options['section'] = 'jban';
		}

	}

	public function run()
	{
		$this->registerPlugin('ClientFolder');
		$this->get();
	}

	protected function registerPlugin($name)
	{
		$view = $this->view;
		$this->clientOptions['id'] = $this->options['id'];

		ClientFolderAssets::register($view);
		\Yii::setAlias('@ClientFolder','@frontend/widgets/ClientFolder');

	}

	protected function get()
	{
		echo $data = $this->renderFile(
			$this->template,
			[
				'id'=>$this->id,
				'model'=>$this->model,
				'className'=>$this->model_class,
				'options'=>$this->options
			]);
	}
} 