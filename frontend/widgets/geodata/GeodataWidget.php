<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * 
 * multiple widgets in one form are allowed
 * in this case unique id and prefix for each of instance SHOULD be set
 * attribute data-widget in view for each <select> SHOULD be set
 */
namespace frontend\widgets\geodata;

use yii\bootstrap\Widget;
use frontend\widgets\geodata\GeodataAssets;

class GeodataWidget extends Widget {
	public $id = 'geodata'; //should be unique for multiple widgets in one form
        public $prefix = ''; //should be unique for multiple widgets in one form
        public $template; // default for simple form, defaultActive for ActiveForm
	public $form;
	public $model;
	public $model_class;


	public function init()
	{
        $arrNameClass = explode('\\',get_class($this->model));
		$this->model_class = array_pop($arrNameClass);

		if(isset($this->options['id']))
		{
			$this->id = $this->options['id'];
		}
		if(is_null($this->template))
		{
			$this->template = '@geodata/views/default.php';
		}

		if(!isset($this->options['Country']))
		{
			$this->options['Country'] = 'Country';
		}
		if(!isset($this->options['Region']))
		{
			$this->options['Region'] = 'Region';
		}
		if(!isset($this->options['City']))
		{
			$this->options['City'] = 'City';
		}
                $this->options['country_id'] = (isset($this->prefix)) ? 
                    $this->prefix.'_country_id' : 'country_id';
                $this->options['region_id'] = (isset($this->prefix)) ? 
                    $this->prefix.'_region_id' : 'region_id';
                $this->options['city_id'] = (isset($this->prefix)) ? 
                    $this->prefix.'_city_id' : 'city_id';                
	}

	public function run()
	{
		$this->registerPlugin('geodata');
		$this->get();
	}

	protected function registerPlugin($name)
	{
		$view = $this->view;
		$this->clientOptions['id'] = $this->options['id'];

		GeodataAssets::register($view);
		\Yii::setAlias('@geodata','@frontend/widgets/geodata');


		//$js = "jQuery('#$id').$name([]);";
		//$view->registerJs($js,View::POS_END);

	}

	protected function get()
	{
		echo $data = $this->renderFile(
			$this->template,
			[
				'id'=>$this->id,
				'model'=>$this->model,
				'form'=>$this->form,
				'className'=>$this->model_class,
				'options'=>$this->options
			]);
	}
} 