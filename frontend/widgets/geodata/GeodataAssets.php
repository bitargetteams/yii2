<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 19.11.14
 * Time: 12:27
 */
namespace frontend\widgets\geodata;

use yii\web\AssetBundle;

class GeodataAssets extends AssetBundle {
	public $sourcePath = '@app/widgets/geodata';
	public $jsOptions = [];
	public $css = [
		'css/geodata.css'
	];
	public $js = [
//		'js/geodata.js'
            'js/geodata.js'
	];
	public $depends = [];
}