/**
 * Created by ddo on 19.11.14.
 */

$(document).ready(function() {
	$("[id$=country_id]").on('change', function(){
		getRegions($(this).data('widget'));
	});
	$("[id$=region_id]").on( 'change', function(){
		getCities($(this).data('widget'));
	});

});

function getRegions(data) {
        var select = $('[data-widget='+data+']')
        var country = select.filter('[id$=country_id]').val();
        var region = select.filter('[id$=region_id]');

	region.html('');

	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/ajax/regions",
		data: {'country_id': country, 'per_page': 100},
		success: function(list) {
			$.each(list, function(i) {region.append('<option value="' + i + '">' + this + '</option>');});
			getCities();
		}
	});

	select.removeAttr('readonly');
}

function getCities(data) {
	var select = $('[data-widget='+data+']');
        var country = select.filter('[id$=country_id]').val();
        var region = select.filter('[id$=region_id]').val();
        var city = select.filter('[id$=city_id]');

        city.html('');

	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/ajax/cities",
		data: {'country_id': country, 'region_id': region, 'per_page': 1000},
		success: function(list) {
			$.each(list, function(i) {city.append('<option value="' + i + '">' + this + '</option>');});
		}
	});

	select.removeAttr('readonly');
}
