<?php
use yii\helpers\Html;
?>
<div class="<?= $id ?>">
<?php $id_suffix = strtolower($className) . '-' . $options['country_id']; ?>
	<div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?=$id_suffix;?>"><?= $options['Country']?></label>
		<?= Html::dropDownList($className.'[country_id]', null, $model->getCountries(),
                    ['id'=>$id_suffix, 'class' => 'form-control', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>
<?php $id_suffix = strtolower($className) . '-' . $options['region_id']; ?>
       <div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?=$id_suffix;?>">Регион</label>
		<?= Html::dropDownList($className.'[region_id]', null, [],
                    ['id'=>$id_suffix, 'class' => 'form-control', 'readonly'=>'readonly', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>
<?php $id_suffix = strtolower($className) . '-' . $options['city_id']; ?>
        <div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?=$id_suffix;?>">Город</label>
		<?= Html::dropDownList($className.'[city_id]', null, [],
                    ['id'=>$id_suffix, 'class' => 'form-control', 'readonly'=>'readonly', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>

</div>