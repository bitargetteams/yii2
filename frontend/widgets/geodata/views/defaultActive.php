<?php
use yii\helpers\Html;
?>
<div class="<?= $id ?>">
<?php $id_suffix = strtolower($className) . '-' . $options['country_id']; ?>
	<div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?=$id_suffix;?>"><?= $options['Country']?></label>
		<?= Html::activeDropDownList($model, $options['country_id'], $model->getCountries(),
                    ['id'=>$id_suffix, 'class' => 'form-control', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>
<?php $id_suffix = strtolower($className) . '-' . $options['region_id']; ?>
        <div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?= $id_suffix;?>">Регион</label>
		<?= Html::activeDropDownList($model, $options['region_id'], [],
                    ['id'=>$id_suffix, 'class' => 'form-control', 'readonly'=>'readonly', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>
<?php $id_suffix = strtolower($className) . '-' . $options['city_id']; ?>
        <div class="form-group field-<?=$id_suffix;?> required">
		<label class="control-label" for="<?=$id_suffix;?>">Город</label>
		<?= Html::activeDropDownList($model, $options['city_id'], [],
                    ['id'=>$id_suffix, 'class' => 'form-control', 'readonly'=>'readonly', 'data-widget' => $id])?>
		<div class="help-block"></div>
	</div>

</div>