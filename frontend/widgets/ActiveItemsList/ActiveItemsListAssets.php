<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 10.11.14
 * Time: 16:09
 */

namespace frontend\widgets\ActiveItemsList;


use yii\web\AssetBundle;
use yii\web\View;

class ActiveItemsListAssets extends AssetBundle {
    public $sourcePath = '@app/widgets/assets';
    public $jsOptions = ['position' => View::POS_END];
    public $css = [
        'css/items-list.css'
    ];
    public $js = [
        'js/jquery.items.list.js'
    ];
    public $depends = [
		//'yii\web\JqueryAsset'
	];
} 