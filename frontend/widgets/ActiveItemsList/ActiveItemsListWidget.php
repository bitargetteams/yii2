<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 10.11.14
 * Time: 16:04
 */
namespace frontend\widgets\ActiveItemsList;
use yii\bootstrap\Widget;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\InputWidget;

class ActiveItemsListWidget extends Widget {
    public $id;
    public $listModel;
    public $filter=null;
    public $sort=null;
    public $pagination;
    public $fields;
    public $isAjax=false;
    public $eventOnClickItems = null;
    public $template;
    public $templateItem;
    public $noneMessage;

    public function init()
    {
        if(is_null($this->pagination))
        {
            $this->pagination = [
                'type'=>'standard',
                'items'=>20,
                'template' => '@items-list-assets/views/pagination/lazy.php',
            ];
        }

        if(!is_array($this->listModel) && !is_object($this->listModel))
            $this->listModel = [new $this->listModel((isset($this->options['initParam'])) ? $this->options['initParam'] : '')];
        if(is_null($this->template))
        {
            $this->template = '@items-list-assets/views/items_list/header.php';
        }

        if(is_null($this->templateItem))
        {
            $this->templateItem = '@items-list-assets/views/items_list/item.php';
        }
    }

    public function run()
    {
        $this->registerPlugin('itemsList');
        $this->getItemsList();
    }

    protected function registerPlugin($name)
    {
        $view = $this->view;
        $this->clientOptions['id'] = $this->options['id'];
        $this->clientOptions['isAjax'] = $this->isAjax;
        ActiveItemsListAssets::register($view);
        \Yii::setAlias('@items-list-assets','@frontend/widgets/assets');
        $id = $this->options['id'];

        if ($this->clientOptions !== false) {
            $options = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);
            $js = "jQuery('#$id').$name($options);";
            $view->registerJs($js,View::POS_END);
        }
    }

    protected function getItemsList()
    {
        //var_dump($this->listModel[0]->attributeLabels());exit;
        $header = $this->renderFile($this->template,['model'=>$this->listModel[0]->attributeLabels()]);
        $item = $this->renderFile($this->templateItem,['model'=>$this->listModel]);
        $pagination = $this->renderFile($this->pagination['template'],['model'=>$this->listModel]);
        echo $header;
        echo $item;
        echo $pagination;
    }
} 