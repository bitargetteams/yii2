/**
 * Created by i_kononenko on 10.11.14.
 */
(function($) {


    $.fn.itemsList = function( method ) {
        var params = this.params || {};
        var lazyButton = "div.wrp-more-button";
        var lazyButtonIsSet = true;
        var methods = {
            init: function (options) {
                var data = {};
                data.id = options.id;
                data.extraData=options.data || {};
                data.element = this;
                data.items = {};
                data.items.src = options.itemsAction;
                data.page = 1;
                data.pageSize = options.pageSize || 5;
                data.items.class = options.itemsClass || "list-records";
                data.wrapperClass = options.wrapperClass || "items-list";
                data.headerClass = options.headerClass || "list-header";
                params = data;
                this.__proto__.params = data;
                _private.filterHandler("div.icon-filter span.filter-list");
                _private.lazyHandler(lazyButton);
                _private.setHandlerOrders();
                if(options.isAjax === true)
                    _private.beginRender().done(function(data){
                        _private.removeLazyButton(params.element.find(lazyButton),data)
                    });
                return this;
            },
            /**
             *
             * @param callback
             */
            itemsHandler: function(callback)
            {
                var $target = params.element;
                $target.on('click',"."+params.items.class,callback);
                return this;
            },
            loadNextHandler: function ($handler) {
                console.log($handler);
                $handler.on('click', function(){
                    _private.beginRender().done(function(data){
                        _private.removeLazyButton(params.element.find(lazyButton),params)
                    });
                });
                return this;
            },
            destroy: function () {
                return false;
            }
        };

        var _private = {
            /**
             * Handler of click filter button
             * @param element
             */
            filterHandler: function (element) {
                var $target = params.element;
                $target.on('click', element, function () {
                    var $filter = $(this);
                    var val = $filter.attr('filter-status') == 0 ? 0 : -90;
                    $filter.animate({
                        some: val
                    }, {
                        step: function (now, fx) {
                            $filter.css('transform', 'rotate(' + now + 'deg)');
                        },
                        done: function () {
                            $filter.attr('filter-status', val == 0 ? -90 : 0);
                        }
                    }, 1500);
                });
            },

            setLazyButton:function(){
                params.element.append(
                    "<div class=\"row wrp-more-button\">" +
                        "<span class='glyphicon glyphicon-chevron-down' id=\"more-button\">" +
                    "</span></div>"
                );
            },

            lazyHandler: function (element) {
                var target = "#"+params.id;
                var $target = $(target+">"+element);
                $target.on('click',function () {
                    _private.beginRender().done(function(data){
                        _private.removeLazyButton($target,data)
                    });
                });
            },

            removeLazyButton: function(element,data){
                var countRows = 0;
                $.each($(data),function(){
                    countRows = $(this).hasClass(params.items.class) ? ++countRows : countRows;
                });
                if(countRows < params.pageSize) {
                    $(element).remove();
                    return false;
                }
                return true;
            },

            getOrders: function(){
                $("#"+params.id +" ."+params.headerClass).find("div.sorted").each(function(){
                    var item = $(this).attr('order');
                    var type = $(this).attr('order-type') || "";
                    if(typeof type === "string" && type.length > 0){
                        params.extraData.order = {};
                        params.extraData.order[item] = type;
                    }
                });
            },

            setHandlerOrders: function()
            {
                $("#"+params.id +" ."+params.headerClass)
                    .on('click',"div.sorted",function(){
                        if($(this).attr('order-type') === 'desc'){
                            $(this).attr('order-type','ask');
                            $(this).removeClass('desc');
                            $(this).removeClass('ask');
                            $(this).addClass('ask');
                        }else{
                            $(this).attr('order-type','desc');
                            $(this).removeClass('desc');
                            $(this).removeClass('ask');
                            $(this).addClass('desc');
                        };
                        _private.flushOtherSort($(this));
                        _private.reloadList();
                    }
                )
            },

            flushOtherSort: function($e){
                var order_attr = $e.attr('order');
                var selector = ".sorted[order=\"" + order_attr + "\"]";
                $("#"+params.id +" ."+params.headerClass)
                    .find("div.sorted")
                    .not(selector)
                    .each(function(){
                        $(this).attr('order-type',"");
                        $(this).removeClass('desc');
                        $(this).removeClass('ask');
                    }
                )
            },

            reloadList: function($plugin){
                var $target = params.element.find("." + params.wrapperClass);
                $target.empty();
                params.element.find(lazyButton).remove();
                params.page = 1;
                _private.setLazyButton();
                _private.lazyHandler(lazyButton);
                _private.beginRender().done(function(data){
                    _private.removeLazyButton(params.element.find(lazyButton),data);

                });
            },
            /**
             * Ajax query
             * @returns {*}
             */
            getModel: function()
            {
                var def = new $.Deferred();
                var data = $.extend({
                    page:params.page,
                    "per-page":params.pageSize,
                    _csrf:$("meta[name='csrf-token']").attr('content')
                    },params.extraData
                );
                $.post(
                    params.items.src,
                    data,
                    function(data){
                    }
                ).done(function(data){def.resolve(data)})
                    .error(function(){def.reject("Error query transport!")});
                return def.promise();
            },

            /**
             *
             *
             */
            beginRender: function()
            {
                var def = $.Deferred();
                _private.getOrders();
                _private.getModel().done(function(data){
                    params.page++;
                    var $target = params.element.find("." + params.wrapperClass);
                    $target.append(data);
                    def.resolve(data);
                });
                return def.promise();
            }
        };

        if ( methods[method] && typeof params.element !== "undefined") {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.itemsList' );
        }

    };
})(jQuery);