<?php
// directions_messages

return array(
    // ---------- WMSites ----------
    'Statistics'=>'Статистика',
    'Url' => 'URL',
    'Status' => 'Статус',
    'State' => 'Статус',
    'Tic' => 'ТИЦ',
    'Date of add' => 'Дата добавления',
    'PR' => 'PR',
    'Indmoz' => 'Наличие в dmoz',
    'Inyaca' => 'Наличие в yaca',
    'Inbtca' => '',
    'Theme' => 'Тематика',
    'Category' => 'Категория',
    'Language' => 'Язык сайта',
    'URL page with code' => 'URL страницы с кодом',
    'Country' => 'Страна сайта',
    'City' => 'Город сайта',
    'This price is too small. Min price is {n}$' => 'Эта цена меньше минимально допустимой. Сделайте её не менее {n} рубля|Эта цена меньше минимально допустимой. Сделайте её не менее {n} рублей',
    'id' => 'Идентификатор',
    'User' => 'Пользователь',
    'Is work' => 'Статус работы',
    'Not set' => 'Не устанавливать',
    'Average' => 'Средние',
    'Average with k' => 'Средние умноженные на коэффицент',
    'Manually set up' => 'Заданные вручную',
    'Links' => 'Ссылки',
    'Settings' => 'Настройки',
    'Pages' => 'Страницы',
    'Black list' => 'Чёрный список',
    'Prices' => 'Цены',
    'Price for 1 level page' => 'Цена страниц 1 уровня',
    'Price for 2 level page' => 'Цена страниц 2 уровня',
    'Price for 3 level page' => 'Цена страниц 3 уровня',
    'Price for 4 level page' => 'Цена страниц 4 уровня',
    'Price for {number} level pages' => 'Цена страниц {number} уровня',
    'Search' => 'Поиск',
    'Texts' => 'Тексты',
    'Must be greater than zero'=>'Должно быть больше нуля',
    'You are, so far, nothing has requested'=>'Вы пока ничего не запрашивали',
    'it\'s too small' => 'Значение слишком маленькое',
    'it\'s too big' => 'Значение слишком большое',

    // ---------- WMSitesSettings ----------
    'Automatic' => 'Автоматический режим',
    'Hide URL' => 'Прятать Url',
    'Links on other lang' => 'Ссылки на другом языке',
    'Setup prices for new pages' => 'Устанавливать цены для новых страниц',
    'Use adult stop words' => 'Использовать стоп слова Adult',
    'Use stop world profanity' => 'Использовать стоп слова ненормативной лексики',
    'Use this site stop word list' => 'Свой список стоп слов для этого сайта',
    'Do not accept applications on site' => 'Не принимать заявки на сайт',
    'Activate a new page' => 'Активировать новые страницы',
    'Separator of links' => 'Разделитель ссылок',
    'Maximum number of links in the OK status' => 'Максимальное количество ссылок в OK статусе',
    'Change the price for the current links when changing PR' => 'Менять цены для текущих ссылок при смене PR',
    'Change prices for pages by changing PR' => 'Менять цены для страниц при смене PR',
    'Coefficient for the new' => 'Коэффицент для новых',
    'Coefficient for PR' => 'Коэффицент для PR',
    'Adult stop words' => 'Сайты с материалами порнографического характера',
    // --------- LinksGroupSettings ---------

    'delimiter' => 'Разделитель ссылок',
    'auto' => 'Автоматический режим',
    'hide_url' => 'Прятать Url',
    'latin_links' => 'Ссылки на другом языке',
    'max_links_1' => 'Максимальное количество ссылок для страниц 1го уровня',
    'max_links_2' => 'Максимальное количество ссылок для страниц 2го уровня',
    'max_links_3' => 'Максимальное количество ссылок для страниц 3го уровня',
    'max_links_4' => 'Максимальное количество ссылок для страниц 4го уровня',
    'new_pages' => 'Автоматически активировать новые страницы',
    'max_links_site' => 'Максимальное количество ссылок в ок статусе',
    'auto_new_prices' => 'Способы установки новой цены',
    'change_prices_pr' => 'Менять цены для страниц при смене pr',
    'change_price_on_change_pr' => 'Менять цены для текущих ссылок при смене pr',
    'can_requested' => 'Не принимать заявки на сайт',
    'Maximum count of links for pages {num} level' => 'Максимальное количество ссылок для страниц {num}го уровня',
     
     // ---------- для модели Optlinks ---------- 
    'Date Creation' => 'Дата создания',
    'Name' => 'Название',
    'Keyword' => 'Ключевое слово',
    'Auto Mode' => 'Автоматический режим',
    'Stop Nofollow' => 'Проставлять ссылкам атрибут rel="nofollow"',
    
    // ---------- для модели OptlinksTexts ---------- 
    'Text' => 'Текст',
    'Texts identifers' => 'Идентификаторы текстов',
    'Bloked' => 'Заблокирован',
    'Incorrect definition links in a string {num}'=>'Некорректное определение ссылки в строке num',
    'The text in the {rowNum} is too short, at least the symbol {num}'=>'Текст в строке rowNum слишком короткий, минимум num символа',
    
    // ---------- для модели PlacesSearch (модель формы) ---------- 
    'from PR' => 'от',
    'From price' => 'от',
    'from TIC' => 'от',
    'to PR' => 'до',
    'To price' => 'до',
    'to TIC' => 'до',
    'Outer links' => 'Внешниx ссылок',
    'Page in Google' => 'Наличие в Google',
    'Page in Yandex' => 'Наличие в Yandex',

    // --------- purchased ------------
    'Accept' => 'Подтвердить',
    'Decline' => 'Отклонить',
    'decline_by_opt' => 'Отклонено Оптимизатором',
    'decline_by_wm' => 'Отклонено Веб-Мастером',
    'showpurchasedexpectedto' => 'Ожидает',
    'showpurchaseddeclined' => 'Отклонено',
    'showpurchasedok' => 'ОК',
    'showpurchasedsleep' => 'Спят',
    'showpurchasederror' => 'Ошибка',

    'placesrequestedexpectedto' => 'Ожидает',
    'placesrequesteddeclined' => 'Отклонено',
    'placesrequestedok' => 'ОК',
    'placesrequestedsleep' => 'Спят',
    'placesrequestederror' => 'Ошибка',

    'placesrequestedexpectedto not found' => 'Ссылок ожидающих подтверждения не найдено',
    'placesrequesteddeclined not found' => 'Отклоненных ссылок не найдено',
    'placesrequestedok not found' => 'Подтвержденных ссылок не найдено',
    'placesrequestedsleep not found' => 'Спящщих ссылок не найдено',
    'placesrequestederror not found' => 'Ссылок со статусом "Ошибка" не найдено',//

    'placesrequestedexpectedto header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'placesrequesteddeclined header' => 'Ссылки со статусом "Отклонено"',
    'placesrequestedok header' => 'Ссылки со статусом "Ок"',
    'placesrequestedsleep header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'placesrequestederror header' => 'Ссылки со статусом "Ошибка"',

    'showpurchasedexpectedto header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'showpurchaseddeclined header' => 'Ссылки со статусом "Отклонено"',
    'showpurchasedok header' => 'Ссылки со статусом "Ок"',
    'showpurchasedsleep header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'showpurchasederror header' => 'Ссылки со статусом "Ошибка"',

    'showpurchasedexpectedto not found' => 'Ссылок ожидающих подтверждения не найдено',
    'showpurchaseddeclined not found' => 'Отклоненных ссылок не найдено',
    'showpurchasedok not found' => 'Подтвержденных ссылок не найдено',
    'showpurchasedsleep not found' => 'Спящщих ссылок не найдено',
    'showpurchasederror not found' => 'Ссылок со статусом "Ошибка" не найдено',
    
    //* для модели Projects
    'Bolm1' => 'Для главных страниц',
    'Bolm2' => 'Для страниц второго уровня вложенности (УВ);',
    'Bolm3' => 'Для страниц третьего уровня вложенности (УВ);',    
    'Bolm4' => 'Для страниц четвёртого уровня вложенности (УВ);',
    'Err Links Time' => 'Автоматически снимать ссылки со статусом ERROR, находящиеся в этом статусе',
    'In Yandex' => 'Нахождение в Yandex',
    'Guest Pass' => 'Гостевой пароль',
    
    //* WmBLAddForm && WmBLFindForm
    'URL starts with http://, each new URL on a new line'=>'URL начинается с http://, Каждый новый URL с новой строки',
    'Date from' => 'от',
    'Date to' => 'до',
    'Search string' => 'строка для поиска',
    'No URL' => 'Нет ни одного URL',
    'Global blacklist' => 'Черный список',
    'Find' => 'Найти',
    'ALL' => "Сбросить фильтр",
    'Add to' => "Добавить",
    'Delete' => "Удалить",
    'Delete from black list' => 'Удалить из чёрного списка',
    'Select/search in black list' => 'Выбор/поиск в чёрном списке',
    'Search fon name' => 'Искать по названию',
    'Filter by first letter addresses' => 'Фильтр по первой букве адреса',
    'Add to black list' => 'Добавить в чёрный список'
    
    
);
