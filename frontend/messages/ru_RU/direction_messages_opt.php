<?php

//* 'direction_messages_opt'

return array(
    'Projects' => 'Проекты',
    'Trade' => 'Запросы и покупки',
    'Add project' => 'Добавить проект',
    'My projects' => 'Мои проекты',
    'To get started, you need to add to a project'=>'Для начала работы вам необходимо добавить проект',
    'Project' => 'Проект',
    'Spent' => 'Потрачено',
    'Links' => 'Ссылок',
    'Bid' => 'Заявок',
    'Texts' => 'Текстов',
    'Yesterday' => 'Вчера',
    'Week' => 'За неделю',
    'Month' => 'За месяц',
    'Total' => 'Всего',
    'Edit' => 'Редактировать',
    'Add single URL' => 'Добавить один URL',
    'Total / Available'=>'Всего/Доступно',
    'Add text' => 'Добавить тексты',
    'Search places' => 'Найти места для рекламы',
    'Choise action on selected items' => 'Выберите действие над выбранными объектами',
    'Delete entry' => 'Удалить запись',
    'Delete entry texts' => 'Удалить все тексты записи',
    'Freeze' => 'Заморозить',
    'Unfreeze' => 'Разморозить',
    'Do it' => 'Выполнить',
    'Create' => 'Создать',
    'Adding project' => 'Добавление проекта',
    
    //* Для экшона NoOptimizerProjectsAction
    'To get started, you need to add to a project'=>'Для начала работы вам необходимо добавить проект',
    
    //* для представления _search_tabs
    'Fast search' => 'Быстрый поиск',
    'Relevant selection' => 'Релевантный подбор',
    'Filters' => 'Фильтры',
    'Contextual links' => 'Контекстные ссылки',
    
    //* для представления addUrl
    'Add' => 'Добавить',
    
    //* для представления add_texts
    'Text' => 'Текст',
    'Length' => 'Длина',
    'Uses count' => 'Количество использований',
    'is Blocked' => 'Блокировка',
    'Lock' => 'Заблокирован',
    'Unlock' => 'Разблокирован',
    'Choice' => 'Выбор',
    'Delete' => 'Удалить',
    'Block|Unblock' => 'Заблокировать|Разблокировать',
    
    //* для представления editProject
    'Automatically remove links from the site, the number of external links (BC) which exceeded:' => 'Автоматически снимать ссылки со страниц сайтов, количество внешних ссылок (ВС) на которых превысило:',
    '7 to 30 days' => 'от 7 до 30 дней',
    
    //* для представленияя place_buy_status
    'You don\'t have enough money to buy selected places for your links.' => 'У вас не хватает средств для покупки выбранных мест для ваших ссылок.',
    'You can select another place for links placement' => 'Вы можете выбрать другие места размещения ссылок',
    'or' => 'или',
    'fill yor balance' => 'пополнить баланс',
    
    //* для предствления purchased_places
    'Status' => 'Статус',
    'Page' => 'Страница',
    'Link text' => 'Текст ссылки',
    'URL' => 'URL ссылки',
    'TIC' => 'ТИЦ',
    'Page Rating'=>'Рейтинг стриницы',
    'Price' => 'Цена',
    'To wake up the sleeping links replenish Ballance' => 'Чтобы разбудить спящие ссылки пополните <a href="/profile/balance/add/">балланс</a>',
    'Decline' => 'Отклонить',
    'Submit' => 'Отправить',
    
    //* для предствления search_places
    'PR' => 'Рейтинг стриницы',
    'to' => 'до',
    'No matter' => 'Не имеет значения',
    'Yes' => 'Да',
    'No' => 'Нет',
    'Search' => 'Найти',
    'For link not added any text or texts are blocked' => 'Для ссылки не добавлено ни одного текста, либо тексты заблокированы',
    
    //* для предствления searched_places
    'Yandex' => 'Яндекс',
    'Prices' => 'Цены',
    'Choose keywords' => 'Выбрать ключевые слова',
    'Buy' => 'Купить',
    'For your search nothing finded. Try again' => 'По вашему запросу ничего не найдено. Попробуйте ещё раз',
    
    //* для optimizerController
    'Settings' => 'Настройки',
    'Search' => 'Поиск',
    'Texts' => 'Тексты',
    
    
);

