<?php

return array(
    'showpurchasedexpectedto' => 'Ожидает',
    'showpurchaseddeclined' => 'Отклонено',
    'showpurchasedok' => 'ОК',
    'showpurchasedsleep' => 'Спят',
    'showpurchasederror' => 'Ошибка',
    
    'placesrequestedexpectedto' => 'Ожидает',
    'placesrequesteddeclined' => 'Отклонено',
    'placesrequestedok' => 'ОК',
    'placesrequestedsleep' => 'Спят',
    'placesrequestederror' => 'Ошибка',
    
    'placesrequestedexpectedto not found' => 'Ссылок ожидающих подтверждения не найдено',
    'placesrequesteddeclined not found' => 'Отклоненных ссылок не найдено',
    'placesrequestedok not found' => 'Подтвержденных ссылок не найдено',
    'placesrequestedsleep not found' => 'Спящщих ссылок не найдено',
    'placesrequestederror not found' => 'Ссылок со статусом "Ошибка" не найдено',//
    
    'placesrequestedexpectedto header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'placesrequesteddeclined header' => 'Ссылки со статусом "Отклонено"',
    'placesrequestedok header' => 'Ссылки со статусом "Ок"',
    'placesrequestedsleep header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'placesrequestederror header' => 'Ссылки со статусом "Ошибка"',
    
    'showpurchasedexpectedto header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'showpurchaseddeclined header' => 'Ссылки со статусом "Отклонено"',
    'showpurchasedok header' => 'Ссылки со статусом "Ок"',
    'showpurchasedsleep header' => 'Ссылки со статусом "Ожидают подтверждения"',
    'showpurchasederror header' => 'Ссылки со статусом "Ошибка"',
    
    'showpurchasedexpectedto not found' => 'Ссылок ожидающих подтверждения не найдено',
    'showpurchaseddeclined not found' => 'Отклоненных ссылок не найдено',
    'showpurchasedok not found' => 'Подтвержденных ссылок не найдено',
    'showpurchasedsleep not found' => 'Спящщих ссылок не найдено',
    'showpurchasederror not found' => 'Ссылок со статусом "Ошибка" не найдено',
);

