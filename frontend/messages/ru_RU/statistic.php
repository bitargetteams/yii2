<?php

return array(
    /**
     * StatisticFilterFormBase
     */
    'beginPeriod' => 'Начало периода',
    'endPeriod' => 'Конец периода',
    'groupBy' => 'Группировать по:',
    
    /**
     * StatisticFilterFormOpt, StatisticFilterFormWM
     */
    'Statistics summary' => 'Сводная статистика',
    
    /**
     * StatisticFilterFormOpt
     */
    '--= All projects =--' => '--= Все проекты =--',
    '--= All projects on one page =--' => '--= Все проекты на одной странице =--',
    'Project' => 'Проект',
    
    /**
     * StatisticFilterFormWM
     */
    '--= All sites =--' => '--= Все сайты =--',
    '--= All sites on one page =--' => '--= Все сайты на одной странице =--',
    'Site' => 'Сайт',
    
    /**
     * bitarget/protected/bitarget/views/statistic/statistic.php
     */
    'Projects not found' => 'Проектов не найдено',
    'Sites not found' => 'Сайтов не найдено',
    'Group by day' => 'По дням',
    'Group by month' => 'По месяцам',
    'from' => 'с',
    'to' => 'по', 
    'Apply' => 'Применить',
    'Statistic' => 'Статистика',
);

