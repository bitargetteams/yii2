<?php

return array(
    '{attribute} is too long(maximum: {num} symbols)' => 'Поле "{attribute}" слишком длинное (Максимум: num симв.).',
    'Only letters' => 'Только символы алфавита',
    'Only digits' => 'Допустимы только цифры',
    'Date less that minimum value' => 'Дата меньше минимального значения',
    'Date more than maximum value' => 'Дата больше максимального значения',
    'Must be in the format xxx-xxx' => 'Необходимо вводить в формате xxx-xxx'
    
    
);
?>
