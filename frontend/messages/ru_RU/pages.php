<?php
//Делаем импорт модели для использования констант

return array(
    'External links' => 'Внешние ссылки',
    'Nesting level page' => 'Уровень вложенности страницы',
    'Page Rank Google' => 'Рейтинг Google',
    'Number of places' => 'Всего мест',
    'Maximum links on page' => 'Максимальное количество размещаемых ссылок на странице',
    'Free places' => 'Свободных мест',
    'Count of free places on the page' => 'Количество свободных мест на странице',
    'Links' => 'Ссылки',
    'The count of placed links on the page' => 'Количество размещённых ссылок на странице',
    'Status' => 'Статус',
    'Price' => 'Цена',
    'Refresh' => 'Обновить',
    'Pages not found' => 'Страницы не найдены',
    'Url'=>'Адрес',
    'Pages' => 'Страницы',
    
    //Перевод констант для модели LinksPlaces
    Places::STATUS_ADDED => 'Добавлена',
    Places::STATUS_DELETED => 'Удалена',
    Places::STATUS_DELETED_BEFORE_INDEXING => 'Удалена до переиндексации',
    Places::STATUS_ERROR => 'Ошибка',
    Places::STATUS_EXCLUDE => 'Исключена',
    Places::STATUS_OK => 'Работает',
    Places::STATUS_SLEEP => 'Заморожена',
);
