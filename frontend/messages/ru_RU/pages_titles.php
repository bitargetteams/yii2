<?php
return array(
    'Bitarget ad system: For Optimizer' => 'Bitarget рекламная система: Оптимизатору',
    'Bitarget ad system: For Webmaster' => 'Bitarget рекламная система: Вэбмастеру',
    'Bitarget ad system: Help' => 'Bitarget рекламная система: Помощь',
    'Bitarget ad system: News' => 'Bitarget рекламная система: Новости',
    'Bitarget ad system: Balance' => 'Bitarget рекламная система: Баланс',
    'Bitarget ad system: Profile' => 'Bitarget рекламная система: Профиль',
    'Bitarget ad system: Dcuments' => 'Bitarget рекламная система: Документы',
    'Bitarget ad system: Paiment' => 'Bitarget рекламная система: Платежи',
    'Bitarget ad system: Settings' => 'Bitarget рекламная система: Настройки',
    'Bitarget ad system: Tarifs' => 'Bitarget рекламная система: Тарифы',
    'Bitarget ad system: Blog' => 'Bitarget рекламная система: Блог',
    'Bitarget ad system: Audit' => 'Bitarget рекламная система: Аудит',
    'Bitarget ad system: Activation Account' => 'Bitarget рекламная система: Акивация аккаунта',
    'Bitarget ad system: Feedback' => 'Bitarget рекламная система: Техподдержка',
    'Bitarget ad system: Global Settins' => 'Bitarget рекламная система: Глобальные насторйки',
    'Bitarget ad system: Notifications' => 'Bitarget рекламная система: Уведомления',
    'Bitarget ad system: General' => 'Bitarget рекламная система: Главная',
    'Bitarget ad system: Registration' => 'Bitarget рекламная система: Регистрация',
    'Bitarget ad system: Authorization' => 'Bitarget рекламная система: Авторизация',
);

