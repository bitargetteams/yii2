<?php

// direction_messages_wm

return array(
    'Sites' => 'Сайты',
    'Add category' => 'Добавить категорию',
    'Group prices' => 'Групповые цены',
    'Group settings' => 'Групповые настройки',
    'Requested places' => 'Запрошенные места',
    'GBL' => 'ГБЛ',
    'Add site' => 'Добавьте сайт',
    'My sites' => 'Мои сайты',
    'The number of pages' => 'Количество страниц',
    'The number of links' => 'Количество ссылок',
    'indexing' => 'Индексируется',
    'onmoderation' => 'На модерации',
    'added' => 'Добавлен',
    'success' => 'Работает',
    'supersuccess' => 'Папочка работает',
    'Adding a site' => 'Добавление сайта',
    'Generate code' => 'Сгенерировать код',
    'Next' => 'Далее',
    'Languages' => 'Языки',
    'Frameworks' => 'Фреймворки',
    'Submit site' => 'Добавить сайт',
    'Webmaster site' => 'Площадка',
    'Max links on levels 1/2/3/4' => 'Максимальное количество ссылок на страницах по уровням 1/2/3/4',
    'Submit' => 'Отправить',

    // ----------- GroupPrices ----------
    'Group setting prices' => 'Групповая настройка цен',
    'Price for 1 level page' => 'Цена страниц 1 уровня',
    'Price for 2 level page' => 'Цена страниц 2 уровня',
    'Price for 3 level page' => 'Цена страниц 3 уровня',
    'Price for 4 level page' => 'Цена страниц 4 уровня',
    
    // settings view
    'Settings' => 'Настройки',
    'Maximum links' => 'Максимальное количество ссылок',
    'Set prices for new pages' => 'Устанавливать цены для новых страниц',
    'Average' => 'Средние по сайту',
    'System average multiply on' => 'Средние по системе, умноженные на',
    'Set manually' => 'Заданные вручную',
    'Do not change' => 'Не устанавливать',
    'Set new prices on PR and Tits update' => 'Устанавливать новые цены автоматически при изменении ТИЦ и PR страниц',
    'Use stop-words lists' => 'Использовать списки "Стоп-слов" ',
    'Submit' => 'Отправить',
    
    //wmsite controller
    'Statistics'=>'Статистика',
    'Pages' => 'Страницы',
    'Black list' => 'Чёрный список',
    'Prices' => 'Цены',
    'Operation broken off' => 'Подтверждение обломалось',
    'Operation broken off. You don\'t have enough permission' => 'Подтверждение обломалось. Вы не имеете недостаточно прав для подтверждения запрошенных заявок.',
);

