<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 29.10.14
 * Time: 18:31
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>



<?php echo Html::errorSummary($model); ?>

<?php $form = ActiveForm::begin(['id' => 'SettingsForm', 'options' => ['enctype'=>'multipart/form-data']]); ?>
<div style="display:none"><input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" name="_csrf"></div>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Настройки')  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'email', ['class' => 'registration-form__label']); ?>
<?= Html::activeTextInput($model, 'email',
	['class' => 'form-control input-sm registration-form__input', 'style' => 'width: 200px' ]); ?>
<?= Html::endTag('fieldset'); ?>

<?php foreach($model->checkboxes as $name => $value): ?>
	<?= Html::checkbox("SettingsForm[checkboxes][{$name}]", $value, ['label' => $name, 'value' => '1', 'uncheck' => false]); ?>
<?php endforeach;?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Паспорт: ' . $model->passport )  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'passport1_scan', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'passport1_scan', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'passport2_scan', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'passport2_scan', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode(\Yii::t('profile','Insurance pension certificate') . ': ' . $model->ipc)  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'ipc_scan', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'ipc_scan', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode(\Yii::t('profile','Certificate on tax') . ': ' . $model->inn)  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'inn_scan', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'inn_scan', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

<?= Html::submitButton(\Yii::t('profile','Save'), ['class' => 'btn btn-primary registration-form__button', 'ng-submit' => 'sendFormSettings()']); ?>

<?php ActiveForm::end(); ?>