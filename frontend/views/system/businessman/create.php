<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 28.10.14
 * Time: 11:18
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Индивидуальный предпринематель') ?></h1></div>
<?php echo Html::errorSummary($model); ?>
<?php $form = ActiveForm::begin(['id' =>'Client_businessman',]); ?>
	<?= Html::beginTag('div', ['class' => 'registration-form__formgroup']); ?>
	<div style="display:none"><input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" name="_csrf"></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'lastname', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'lastname',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Lastname'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'firstname', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'firstname',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Firstname'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'middlename', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'middlename',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Middlename'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'contact_person', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'contact_person',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Contact person'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'phone', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'phone',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Phone number'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<div class="content-title"><h1 class="content-title__title"><?php echo \Yii::t('profile','Actual address') ?></h1></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'a_zip', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'a_zip',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Zip'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'a_country_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'a_country_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'a_region_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'a_region_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'a_city_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'a_city_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'a_address', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'a_address', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<div class="content-title"><h1 class="content-title__title"><?php echo \Yii::t('profile','Post address') ?></h1></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'l_zip', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model, 'l_zip',
		['class' => 'form-control input-sm registration-form__input',
			'placeholder' => \Yii::t('profile','Zip'),
			'required' => 'required']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'l_country_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'l_country_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'l_region_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'l_region_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'l_city_id', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'l_city_id', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'l_address', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'l_address', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>

	<div class="content-title"><h1 class="content-title__title"><?php echo \Yii::t('profile','Tax requisites') ?></h1></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'inn', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'inn', ['class' => 'form-control input-sm registration-form__input', 'placeholder' => \Yii::t('profile','Inn')]); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model,'ogrnip', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'ogrnip', ['class' => 'form-control input-sm registration-form__input', 'placeholder' => \Yii::t('profile','Ogrnip')]); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'ogrn_date', ['class' => 'registration-form__label']); ?>
	<?= Html::activeInput('date', $model, 'ogrn_date', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>


	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'taxation_form', ['class' => 'registration-form__label']); ?>
	<?= Html::activeRadioList($model,'taxation_form',
		['0' => \Yii::t('profile','Base'), '1' => \Yii::t('profile','Simplify')]); ?>
	<?= Html::endTag('fieldset'); ?>


	<div class="content-title"><h1 class="content-title__title"><?php echo \Yii::t('profile','Certificate on registration') ?></h1></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'certificate_reg_series', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'certificate_reg_series', ['class' => 'form-control input-sm registration-form__input', 'style'=>'width: 90px;', 'placeholder' => \Yii::t('profile','Series'),]); ?>
	<?= Html::activeTextInput($model,'certificate_reg_number', ['class' => 'form-control input-sm registration-form__input', 'style'=>'width: 120px;', 'placeholder' => \Yii::t('profile','Number')]); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'certificate_reg_date', ['class' => 'registration-form__label']); ?>
	<?= Html::activeInput('date', $model, 'certificate_reg_date', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>


	<div class="content-title"><h1 class="content-title__title"><?php echo \Yii::t('profile','Certificate on tax') ?></h1></div>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'certificate_tax_series', ['class' => 'registration-form__label']); ?>
	<?= Html::activeTextInput($model,'certificate_tax_series', ['class' => 'form-control input-sm registration-form__input', 'style'=>'width: 90px;', 'placeholder' => \Yii::t('profile','Series'),]); ?>
	<?= Html::activeTextInput($model,'certificate_tax_number', ['class' => 'form-control input-sm registration-form__input', 'style'=>'width: 120px;', 'placeholder' => \Yii::t('profile','Number')]); ?>
	<?= Html::endTag('fieldset'); ?>

	<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
	<?= Html::activeLabel($model, 'certificate_tax_date', ['class' => 'registration-form__label']); ?>
	<?= Html::activeInput('date', $model, 'certificate_tax_date', ['class' => 'form-control input-sm registration-form__input']); ?>
	<?= Html::endTag('fieldset'); ?>


	<?= Html::submitButton(\Yii::t('profile','Save'), ['class' => 'btn btn-primary registration-form__button', 'ng-submit' => 'sendFormRegistaration()']); ?>

	<?= Html::endTag('div'); ?>
<?php ActiveForm::end(); ?>
