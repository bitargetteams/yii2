<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 29.10.14
 * Time: 18:31
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php echo Html::errorSummary($model); ?>

<?php $form = ActiveForm::begin(['id' => 'SettingsForm', 'options' => ['enctype'=>'multipart/form-data']]); ?>
	<div style="display:none"><input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" name="_csrf"></div>
	<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Настройки')  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'email', ['class' => 'registration-form__label']); ?>
<?= Html::activeTextInput($model, 'email',
	['class' => 'form-control input-sm registration-form__input', 'style' => 'width: 200px' ]); ?>
<?= Html::endTag('fieldset'); ?>

<?php foreach($model->checkboxes as $name => $value): ?>
	<?= Html::checkbox("SettingsForm[checkboxes][{$name}]", $value, ['label' => $name, 'value' => '1', 'uncheck' => false]); ?>
<?php endforeach;?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode(\Yii::t('profile','Certificate on registration') . ': ' . $model->certificate_reg)  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'certificate_reg', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'certificate_reg', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

	<div class="content-title"><h1 class="content-title__title"><?= Html::encode(\Yii::t('profile','Certificate on tax') . ': ' . $model->certificate_tax)  ?></h1></div>

<?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
<?= Html::activeLabel($model, 'certificate_tax', ['class' => 'registration-form__label']); ?>
<?= Html::activeInput('file', $model, 'certificate_tax', ['class' => 'form-control input-sm registration-form__input']); ?>
<?= Html::endTag('fieldset'); ?>

<?= Html::submitButton(\Yii::t('profile','Save'), ['class' => 'btn btn-primary registration-form__button', 'ng-submit' => 'sendFormSettings()']); ?>

<?php ActiveForm::end(); ?>