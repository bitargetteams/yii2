<?php

use yii\helpers\Html;
?>
<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Платформы') ?></h1></div>
<div style="font-size: 20px;color:red;"><?= $error ?></div>
<div style="font-size: 20px;color:green;"><?= $success ?></div>

<article class="content">
<? foreach ($platforms as $platform): ?>
        <div style="font-size: 17px;">
                <?=$platform->title?>
        </div>   
<? endforeach; ?>
    
    <form action="/system/businessman/partner/<?=$id?>" method="POST"> 
        <input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" name="_csrf">     
        <input name="createPlatfrom" style="width: 250px;" class="btn btn-primary registration-form__button" title="Хочу платформу" type="submit" value="Создать платформу" />
    </form>

</article>
