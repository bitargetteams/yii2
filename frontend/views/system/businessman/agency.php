<?php
use yii\helpers\Html;
?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Выбрать тариф') ?></h1></div>
<div style="font-size: 20px;color:red;"><?=$error?></div>
<div style="font-size: 20px;color:green;"><?=$success?></div>

<article class="content">
    <?foreach($tarifs as $tarif):?>
        <div>
            <? ?>
            <form method="POST" name="tarif_<?=$tarif->id?>">
                <input type="hidden" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" name="_csrf">     
                <input type="hidden" name="tarif_id" value="<?=$tarif->id?>">
		<input name="subscribe" style="width: 250px;" class="btn btn-primary registration-form__button" title="Подписаться" type="submit" value="<?=$tarif->title?> | <?=$tarif->royalty?>% | <?=$tarif->price?>Р" />
            </form>
        </div>   
    <?  endforeach;?>
</article>