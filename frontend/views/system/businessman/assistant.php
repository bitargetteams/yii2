<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?> 
<?php echo Html::errorSummary($model); ?>
    <?php $form = ActiveForm::begin([
        'id' =>'Client_organization',
        ]); ?>
    <?= Html::beginTag('div', ['class' => 'registration-form__formgroup']); ?>
 

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'email', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model, 'email', 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => 'short name',
         'required' => 'required']); ?>   
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'role', ['class' => 'registration-form__label']); ?>
    <?= Html::activeDropDownList($model,'role', array_combine(array_values((array)$roles),array_values((array)$roles)), 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => \Yii::t('profile', 'Organization full name'),
         'required' => 'required',
        ]); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary registration-form__button',
        'ng-submit' => 'sendFormRegistaration()']); ?>

    <?= Html::endTag('div'); ?>
    <?php ActiveForm::end(); ?>

<div style="font-size: 17px;padding-bottom: 20px;">
<?  foreach ($assistants as $assistant):?>

    <?if($assistant->flag == 0):?>
        Пользователь еще не зарегистрировался
        <?=$assistant->email?>
        <?=$assistant->role?>
        Ожидает подтверждения
        <?=Html::a('Отменить приглашение',  yii\helpers\Url::to(['system/businessman/cancelinvite/','email'=>$assistant->email,'hash'=>$assistant->hash]),array('style'=>"font-size:17px;"))?>
    <?  endif;?>

    <?if($assistant->flag == 1):?>
        <?=$assistant->lastname.' '.$assistant->firstname.' '.$assistant->middlename?>
        <?=$assistant->email?>
        <?=$assistant->role?>
        Ожидает подтверждения
        <?=Html::a('Отменить приглашение',  yii\helpers\Url::to(['system/businessman/cancelinvite','email'=>$assistant->email,'hash'=>$assistant->hash]),array('style'=>"font-size:17px;"))?>
    <?  endif;?>
        
    <?if($assistant->flag == 2):?>
        <?=$assistant->lastname.' '.$assistant->firstname.' '.$assistant->middlename?>
        <?=$assistant->email?>
        <?=$assistant->role?>
        Принято
        <?=Html::a('Удалить роль',  yii\helpers\Url::to(['system/businessman/deleterole/'.$location_id,'user_id'=>$assistant->user_id]),array('style'=>"font-size:17px;"))?>
    <?  endif;?>
        <br />

<? endforeach; ?>
</div>
