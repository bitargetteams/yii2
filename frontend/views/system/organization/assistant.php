<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?> 
<?php echo Html::errorSummary($model); ?>
    <?php $form = ActiveForm::begin([
        'id' =>'Client_organization',
        ]); ?>
    <?= Html::beginTag('div', ['class' => 'registration-form__formgroup']); ?>
 

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'email', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model, 'email', 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => 'short name',
         'required' => 'required']); ?>   
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'role', ['class' => 'registration-form__label']); ?>
    <?= Html::activeDropDownList($model,'role', $roles, 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => \Yii::t('profile', 'Organization full name'),
         'required' => 'required',
        ]); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary registration-form__button',
        'ng-submit' => 'sendFormRegistaration()']); ?>
    <?  foreach ($assistants as $assistant):?>

        <?if($assistant->flag == 0):?>
            Пользователь еще не зарегистрировался
            <?=$assistant->email?>
            <?=$assistant->role?>
            Ожидает подтверждения
        <?  endif;?>

        <?if($assistant->flag == 1):?>
            <?=$assistant->lastname.' '.$assistant->firstname.' '.$assistant->middlename?>
            <?=$assistant->email?>
            <?=$assistant->role?>
            Ожидает подтверждения
        <?  endif;?>

        <?if($assistant->flag == 2):?>
            <?=$assistant->lastname.' '.$assistant->firstname.' '.$assistant->middlename?>
            <?=$assistant->email?>
            <?=$assistant->role?>
            Принято
        <?  endif;?>


    <? endforeach; ?>
            
    <?= Html::endTag('div'); ?>
    <?php ActiveForm::end(); ?>



