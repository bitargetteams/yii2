<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Организация') ?></h1></div>
<?php echo Html::beginForm(); ?>
<?php echo Html::dropDownList('organization', 0, $drop); ?>
<?php echo Html::endForm();?>

<div id="settings-form">
    
</div>


<script>
    $('[name=organization]').on('change', function(){
	$.ajax({
		type: "GET",
		url: "/ajax/organization?location_id="+$(this).val(),
		success: function(data) {
                    document.getElementById("settings-form").innerHTML = data;
		}
	});
    });
</script>