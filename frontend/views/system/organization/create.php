<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 28.10.14
 * Time: 11:18
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\widgets\geodata\GeodataWidget;

?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Организация') ?></h1></div>

    <?php echo Html::errorSummary($model); ?>
    <?php $form = ActiveForm::begin([
        'id' =>'Client_organization',
        ]); ?>
    <?= Html::beginTag('div', ['class' => 'registration-form__formgroup']); ?>
 
    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'name', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model, 'name', 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => 'short name',
         'required' => 'required']); ?>   
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'fullname', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'fullname', 
        ['class' => 'form-control input-sm registration-form__input',
         'placeholder' => \Yii::t('profile', 'Organization full name'),
         'required' => 'required',
        ]); ?>
    <?= Html::endTag('fieldset'); ?>

    <?php GeodataWidget::begin([
            'options' => [
                'id'=>'geodata',
                'Country'=>'Страна (Юридический адрес)',
                'Region'=>'Регион',
                'City'=>'Город',
            ],
            'model' => $model,
            'form' => $form,
            'template' => '@geodata/views/defaultActive.php',
            'prefix' => 'a',
        ])->end();
    ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'a_address', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'a_address', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'a_zip', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'a_zip', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?php GeodataWidget::begin([
            'options' => [
                    'id'=>'geogeodata',
                    'Country'=>'Страна (Фактический адрес)',
                    'Region'=>'Регион',
                    'City'=>'Город',
            ],
            'model' => $model,
            'form' => $form,
            'template' => '@geodata/views/defaultActive.php',
            'prefix' => 'l',
        ])->end();
    ?>
    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'l_address', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'l_address', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'l_zip', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'l_zip', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'director', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'director', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'accountant', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'accountant', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'contact_person', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'contact_person', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'phone', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'phone', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'inn', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'inn', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'kpp', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'kpp', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'ogrn', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'ogrn', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'ogrn_date', ['class' => 'registration-form__label']); ?>
    <?= Html::activeInput('date', $model,'ogrn_date', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'certificate_reg', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'certificate_reg', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'certificate_reg_date', ['class' => 'registration-form__label']); ?>
    <?= Html::activeInput('date', $model,'certificate_reg_date', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'certificate_tax', ['class' => 'registration-form__label']); ?>
    <?= Html::activeTextInput($model,'certificate_tax', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'certificate_tax_date', ['class' => 'registration-form__label']); ?>
    <?= Html::activeInput('date', $model,'certificate_tax_date', ['class' => 'form-control input-sm registration-form__input']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::beginTag('fieldset', ['class' => 'registration-form__fieldset']); ?>
    <?= Html::activeLabel($model,'taxation_form', ['class' => 'registration-form__label']); ?>
    <?= Html::activeRadioList($model,'taxation_form', 
        ['simple' => 'simple', 'full' => 'full']); ?>
    <?= Html::endTag('fieldset'); ?>

    <?= Html::submitButton('Login', ['class' => 'btn btn-primary registration-form__button',
        'ng-submit' => 'sendFormRegistaration()']); ?>

    <?= Html::endTag('div'); ?>
    <?php ActiveForm::end(); ?>

