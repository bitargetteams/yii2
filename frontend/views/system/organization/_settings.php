<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <?php echo Html::errorSummary($model); ?>
    <?php $form = ActiveForm::begin([
        'id' =>'Settings_organization',
        'action' => './settings',
        'method' => 'post',
        'options' => [
            'enctype' => 'multipart/form-data',
             ]
        ]); ?>

    <?= $form->field($model, 'email')->input('email'); ?>
    <?php if ($model->email_verified):?>
<p><?= \Yii::t('app', 'Your email is successfully confirmed'); ?>
    <?php else: ?>
<p><?= \Yii::t('app', 'Your email is needed to be confirmed'); ?>
    <?php endif; ?>
    <?php echo Html::activeHiddenInput($model, 'object_id');?>

    <?php foreach($model->checkboxes as $name => $value): ?>
    <?= Html::checkbox("SettingsForm[checkboxes][{$name}]", $value, ['label' => $name, 'uncheck' => false]); ?>
    
    <?php endforeach;?>
    
    <?= $form->field($model, 'ogrn'); ?>
    <?= $form->field($model, 'ogrn_date'); ?>
    <?php if ($model->ogrn_verified):?>
<p><?= \Yii::t('app', 'Your ogrn is successfully confirmed'); ?>
    <?php else: ?>
<p><?= \Yii::t('app', 'Your ogrn is needed to be confirmed'); ?>
   <?= $form->field($model, 'ogrn_scan')->fileInput(); ?>
    <?php endif; ?>
<br />
    <?= $form->field($model, 'certificate_reg'); ?>
    <?= $form->field($model, 'certificate_reg_date'); ?>
    <?php if ($model->certificate_reg_verified):?>
<p><?= \Yii::t('app', 'Your registration certificate is successfully confirmed'); ?>
    <?php else: ?>
<p><?= \Yii::t('app', 'Your registration certificate is needed to be confirmed'); ?>
   <?= $form->field($model, 'certificate_reg_scan')->fileInput(); ?>
    <?php endif; ?>
<br />
    <?= $form->field($model, 'certificate_tax'); ?>
    <?= $form->field($model, 'certificate_tax_date'); ?>
    <?php if ($model->certificate_tax_verified):?>
<p><?= \Yii::t('app', 'Your tax registration certificate is successfully confirmed'); ?>
    <?php else: ?>
<p><?= \Yii::t('app', 'Your tax registration certificate is needed to be confirmed'); ?>
   <?= $form->field($model, 'certificate_reg_scan')->fileInput(); ?>
    <?php endif; ?>

   <?= Html::submitButton(Yii::t('app', 'Отправить')); ?>

   <?php ActiveForm::end(); ?>
