<style>
	div.container-fluid.info-user{
		min-height: 640px;
	}
</style>
<div class="container-fluid info-user">
	<div class="row">
		<div class="col-md-2">
			{$this->render('@app/views/layouts/nav_panel.tpl')}
		</div>
		<div class="col-md-10">
			<div class="jumbotron">
				<h1>Статистика о пользователе</h1>
				<p>Главная для залогиненного пользователя</p>
				<p><a class="btn btn-primary btn-lg" role="button">Узнать больше</a></p>
			</div>
		</div>
	</div>
</div>