<?php
use yii\bootstrap\Nav;
use \app\modules\sections\models\Sections;
use \app\modules\sections\assets\AssetsSections;

AssetsSections::register($this);
?>
<style>
    div.container-fluid.info-user{
        min-height: 640px;
    }
</style>
<div class="container-fluid info-user">
    <div class="row">
        <div class="col-md-2">
            <div class="nav-title">Sections</div>
            <?php $bar = Nav::begin(['id'=>'navigation']); ?>
            <?php $bar->items = array_map(create_function('$model','{return $model->getUrl();}'),Sections::model());?>
            <?php $bar->end();?>
        </div>
        <div class="col-md-10">
            <div class="jumbotron">
                <h1>Статистика о пользователе</h1>
                <p>Главная для залогиненного пользователя</p>
                <p><a class="btn btn-primary btn-lg" role="button">Узнать больше</a></p>
            </div>
        </div>
    </div>
</div>