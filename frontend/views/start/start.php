<?php
/**
 * Created by PhpStorm.
 * User: ddo
 * Date: 24.10.14
 * Time: 19:00
 */

use yii\helpers\Html;
?>

<div class="content-title"><h1 class="content-title__title"><?= Html::encode('Стартовая страница') ?></h1></div>

<article class="content">
	<div><a href="/system/person/create">
		<input style="width: 250px;" class="btn btn-primary registration-form__button" type="submit" value="<?php echo \Yii::t('profile','Private person') ?>" />
	</a></div>
	<div><a href="/system/businessman/create">
		<input style="width: 250px;" class="btn btn-primary registration-form__button" type="submit" value="<?php echo \Yii::t('profile','Private businessman') ?>" />
	</a></div>
	<div><a href="/system/organization/create">
		<input style="width: 250px;" class="btn btn-primary registration-form__button" type="submit" value="<?php echo \Yii::t('profile','Organization') ?>" />
	</a></div>
	<div><a href="/system/person/quick">
		<input style="width: 250px;" class="btn btn-primary registration-form__button" type="submit" value="<?php echo \Yii::t('profile','Quick start') ?>" />
	</a></div>

</article>