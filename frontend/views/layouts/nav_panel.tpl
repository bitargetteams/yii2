
{use class="\app\modules\sections\models\Sections"}
{use class="\app\modules\sections\assets\AssetsSections"}
{AssetsSections::register($this)|void}

<div class="nav-title">Sections</div>
<ul id="navigation" class="nav">
	{foreach Sections::model() as $section}
		<li><a href="/section/{$section->slug}">{$section->title}</a></li>
	{/foreach}
</ul>