<?php
use \frontend\helpers\CHtml;
use \frontend\models\forms\TicketForm;
use yii\widgets\ActiveForm;
//var_dump($form);exit;
?>

<?php $model = ActiveForm::begin(['id'=>'', 'method'=>'post', 'options'=>['class' => 'ticket-form', 'enctype' => 'multipart/form-data']]); ?>
<fieldset class="left contact-form__fieldset">

    <?php echo CHtml::activeDropDownList($form, 'subject', TicketForm::getSubjects(), array(
            'class' => 'contact-form__select',
            'required'));?>
    <?php echo CHtml::activeTextArea($form, 'message', array(
            'class' => 'form-control contact-form__textarea'. CHtml::cssError($form,'message')
        ));?>
</fieldset>
<fieldset class="right contact-form__fieldset">
    <div class="contact-form__text">Вы можете прикрепить 6 изображений.
        <br>Общий размер не должен превышать 20мб.
    </div>
    <?= CHtml::activeFileInput($form, 'img[]', array(
            'class' => 'form-control contact-form__file', 'multiple' => true)); ?>
    <div class="form-group">
        <div data-provides="fileinput" class="fileinput fileinput-new">
                <span class="btn btn-primary btn-file contact-form__fileinput">
                    <span class="fileinput-new">
                        <span class="fui-upload"></span> Добавить файл
                    </span>
                    <span class="fileinput-exists">
                        <span class="fui-gear"></span> Изменить
                    </span>
                </span>
            <span class="fileinput-filename"></span>
            <a href="#" data-dismiss="fileinput" class="close fileinput-exists">Удалить</a>
        </div>
    </div>
</fieldset>
<?php
echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary contact-form__button'));
ActiveForm::end();
?>
