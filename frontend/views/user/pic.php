<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<link rel="stylesheet" src="/web/css/jquery.Jcrop.css" />
<?php \Yii::$app->assetManager->publish('/home/user/srv/front.dev/public/yii2/frontend/web/themes/flat/assets/scripts/jquery.Jcrop.min.js'); ?>
<?php \Yii::$app->assetManager->publish('/home/user/srv/front.dev/public/yii2/frontend/web/themes/flat/assets/scripts/jquery-1.11.0.min.js'); ?>
<?php $url = \Yii::$app->assetManager->getPublishedUrl('/home/user/srv/front.dev/public/yii2/frontend/web/themes/flat/assets/scripts/jquery-1.11.0.min.js'); ?>
<script src="<?=$url;?>"></script>
<?php $url = \Yii::$app->assetManager->getPublishedUrl('/home/user/srv/front.dev/public/yii2/frontend/web/themes/flat/assets/scripts/jquery.Jcrop.min.js'); ?>
<script src="<?=$url;?>"></script>
<script type="text/javascript">

    jQuery(function($){

      // Create variables (in this scope) to hold the API and image size
      var jcrop_api, boundx, boundy;

      $('#cropbox').Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;
      });

      function updatePreview(c)
      {

				$('#x').val(c.x);
				$('#y').val(c.y);
				$('#w').val(c.w);
				$('#h').val(c.h);
				if (parseInt(c.w) > 0)
        {
          var rx = 100 / c.w;
          var ry = 100 / c.h;

          $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
      };

			function checkCoords()
			{
				if (parseInt($('#w').val())) return true;
				alert('Please select a crop region then press submit.');
				return false;
			};

    });

  </script>
  <?php if (!empty($model->avatarFile)): ?>
  <img src=<?=$model->avatarFile;?> />
  <?php else: ?>
    <?php echo Html::errorSummary($model); ?>
    <?php if(isset($error)) echo $error; ?>
    <?php $form = ActiveForm::begin([
        'id' =>'UserpicForm',
        'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

    <?php if(isset($model->tmpFile)): ?>
        <img src=<?=$model->tmpFile;?>  id='cropbox'/>

    <!--div class="preview-container" style="width:80px;height:80px;">
      <img src="<?=$model->tmpFile;?>" class="jcrop-preview" alt="Preview" id="preview" />
    </div-->
        <?= Html::activeHiddenInput($model, 'x', ['id'=> 'x']);?>
        <?= Html::activeHiddenInput($model, 'y', ['id'=> 'y']);?>
        <?= Html::activeHiddenInput($model, 'w', ['id'=> 'w']);?>
        <?= Html::activeHiddenInput($model, 'h', ['id'=> 'h']);?>
    <?php else: ?>

    <?= $form->field($model,'rawfile')->fileInput(); ?>

    <?php endif; ?>

    <?= Html::submitButton(Yii::t('app', 'Отправить')); ?>

    <?php ActiveForm::end(); ?>
<?php endif;?>