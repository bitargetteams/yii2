<?php
use \frontend\helpers\CHtml;
use \frontend\models\forms\FeedBackForm;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>
<style>
</style>
<div class="content-title"><h1 class="content-title__title"><?= Yii::t('feedback', 'Feedback'); ?></h1></div>
<?php $model = ActiveForm::begin(['id'=>'', 'method'=>'post', 'options'=>['class' => 'contact-form', 'enctype' => 'multipart/form-data']]); ?>
    <fieldset class="left contact-form__fieldset">
        <?php echo CHtml::activeTextInput($form, 'name', array(
            'placeholder' => 'Ваше имя',
            'class' => 'form-control input-sm contact-form__input'. CHtml::cssError($form,'name'),
            'required'
        ));?>
        <?php echo CHtml::activeTextInput($form, 'e_mail', array(
            'placeholder' => 'Ваш e-mail',
            'class' => 'form-control input-sm contact-form__input'. CHtml::cssError($form,'e_mail'),
            'required'
        ));?>
        <?php /*echo CHtml::activeDropDownList($form, 'subject', FeedBackForm::getSubjects(), array(
            'class' => 'contact-form__select',
            'required')); */?>
        <?php echo CHtml::activeTextArea($form, 'text', array(
            'class' => 'form-control contact-form__textarea'. CHtml::cssError($form,'text')
        ));?>
    </fieldset>
    <fieldset class="right contact-form__fieldset">
        <div class="contact-form__text">Вы можете прикрепить 6 изображений.
            <br>Общий размер не должен превышать 20мб.
        </div>
        <?= CHtml::activeFileInput($form, 'img', array(
                'class' => 'form-control contact-form__file')); ?>
        <div class="form-group">
            <div data-provides="fileinput" class="fileinput fileinput-new">
                <span class="btn btn-primary btn-file contact-form__fileinput">
                    <span class="fileinput-new">
                        <span class="fui-upload"></span> Добавить файл
                    </span>
                    <span class="fileinput-exists">
                        <span class="fui-gear"></span> Изменить
                    </span><!--input type="file" name="FeedBackForm[img1]" id="feedbackform-img1"-->
                </span>
                <span class="fileinput-filename"></span>
                <a href="#" data-dismiss="fileinput" class="close fileinput-exists">Удалить</a>
            </div>
        </div>
    </fieldset>
    <div class="bottom contact-form__text">Введите капчу, и все готово к отправке!</div>
    <?php
    if (Captcha::checkRequirements()) {
        echo CHtml::activeLabel($form, 'verifyCode');
        echo Captcha::widget([
            'name' => 'verifyCode',
            'model' => $model,
            'template' => '{image}',
            'imageOptions' => ['class' => 'contact-form__img'],
            'captchaAction' => 'clients/site/captcha'
        ]);
        echo CHtml::activeTextInput($form, 'verifyCode', [
            'placeholder' => 'Проверочный код',
            'class' => 'form-control input-sm contact-form__input-cod'. CHtml::cssError($form,'verifyCode'),
            'template' => '{input}\n{error}',
            'required'
        ]);
    }
    echo CHtml::submitButton('Отправить', array('class' => 'btn btn-primary contact-form__button'));
    ActiveForm::end();
    ?>
<div class="contacts">
    <div class="emails">info@bitarget.ru<br>admin@bitarget.ru</div>
    <dl class="contacts__list">
        <dt class="contacts__list-name">ООО "НЭТ-Сервис"</dt>
        <dd class="contacts__list-item">
            <dl class="contacts__sub-list">
                <dt class="contacts__sub-list-name">Юридический адрес:</dt>
                <dd class="contacts__sub-list-item">115035, Москва, Космодамианская набережная, д.4/22, кор. Б,
                    помещение VIII, комн.6
                </dd>
                <br>
                <dt class="contacts__sub-list-name">Почтовый адрес:</dt>
                <dd class="contacts__sub-list-item">117105, Москва, 1-й Нагатинский пр-д., д. 2, стр. 7, офис 302
                </dd>
                <br>
                <dt class="contacts__sub-list-name">Телефон:</dt>
                <dd class="contacts__sub-list-item">(495) 995-34-05</dd>
            </dl>
        </dd>
    </dl>
    <dl class="contacts__list">
        <dt class="contacts__list-name">&nbsp;</dt>
        <dd class="contacts__list-item">
            <dl class="contacts__sub-list">
                <dt class="contacts__sub-list-name">ИНН / КПП:</dt>
                <dd class="contacts__sub-list-item">7705542340/770501001</dd>
                <br>
                <dt class="contacts__sub-list-name">ОГРН:</dt>
                <dd class="contacts__sub-list-item">1137746504345</dd>
                <br>
                <dt class="contacts__sub-list-name">ОКПО:</dt>
                <dd class="contacts__sub-list-item">17684356</dd>
            </dl>
        </dd>
    </dl>
    <dl class="contacts__list">
        <dt class="contacts__list-name">Банковские реквизиты:</dt>
        <dd class="contacts__list-item">
            <dl class="contacts__sub-list">
                <dt class="contacts__sub-list-name">ВТБ 24 (ЗАО)</dt>
                <dd class="contacts__sub-list-item">ИНН банка 7710353606, БИК 044525716</dd>
                <br>
                <dt class="contacts__sub-list-name">Р/сч</dt>
                <dd class="contacts__sub-list-item">40702810700000101063</dd>
                <br>
                <dt class="contacts__sub-list-name">Кор.сч.</dt>
                <dd class="contacts__sub-list-item">30101810100000000716 в Оперу Московского ГТУ Банка России</dd>
            </dl>
        </dd>
    </dl>
</div>