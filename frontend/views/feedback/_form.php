<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 08.10.14
 * Time: 19:15
 */
use \yii\helpers\Html as CHtml;
use \frontend\models\forms\FeedBackForm;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$cf = ActiveForm::begin();

$cf->attributes = ['template'=>''];
//$captcha = $cf->field($form,'verifyCode')->widget(Captcha::className());
//$captcha->template = '{input}';
//echo CHtml::activeTextInput($form,'verifyCode');


ActiveForm::end();

echo Captcha::widget([
    'name' => 'verifyCode',
    'model' => $form
]);