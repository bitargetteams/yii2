<?php
use \Yii;
use \yii\bootstrap\ActiveForm;
use \frontend\helpers\CHtml;
use \yii\captcha\Captcha;
?>
<?php $form = ActiveForm::begin(['id'=>'', 'method'=>'post']); ?>
<?= $form->field($model, 'oldPassword'); ?>
<?= $form->field($model, 'password'); ?>
<?= $form->field($model, 'confirmPassword'); ?>
<?php if (Captcha::checkRequirements()) {
    echo CHtml::activeLabel($model, 'verifyCode');
    echo Captcha::widget(
        [
            'captchaAction' => '/profile/captcha',
            'name' => 'verifyCode',
            'model' => $form,
            'template' => '{image}',
            'imageOptions' => ['class' => 'contact-form__img'],
        ]
    );
    echo CHtml::activeTextInput(
        $model, 'verifyCode', [
            'placeholder' => 'Проверочный код',
            'class' => 'form-control input-sm contact-form__input-cod'
                . CHtml::cssError($model, 'verifyCode'),
            'template' => '{input}\n{error}',
            'required'
        ]
    );
}
?>
<?= CHtml::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>