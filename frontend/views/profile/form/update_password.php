<?php
use \Yii;
use \yii\bootstrap\ActiveForm;
use \frontend\helpers\CHtml;
?>
<?php $form = ActiveForm::begin(['id'=>'', 'method'=>'post']); ?>
<?= $form->field($model, 'oldPassword'); ?>
<?= $form->field($model, 'password'); ?>
<?= $form->field($model, 'confirmPassword'); ?>

<?= CHtml::submitButton(Yii::t('registration', 'Send'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>