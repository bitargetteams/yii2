<?php
use \Yii;
use \yii\bootstrap\ActiveForm;
use \frontend\helpers\CHtml;
use \yii\captcha\Captcha;
?>
<?php $form = ActiveForm::begin(['id'=>'', 'method'=>'post']); ?>
<?= $form->field($model, 'phone_number'); ?>
<?= $form->field($model, 'password'); ?>
<?= $form->field($model, 'pin'); ?>
<?= CHtml::submitButton(Yii::t('share', 'Submit'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>