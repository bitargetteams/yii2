<?php
/**
 * Created by PhpStorm.
 * User: i_kononenko
 * Date: 31.10.14
 * Time: 13:28
 */
use \yii\bootstrap\Alert;
if($alert) {
    echo Alert::widget(
        [
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => Yii::t('user', $alert['target']) . Yii::t('user', 'successful'),
        ]
    );
}
?>
<style>
    .table-responsive{
        max-width: 70%;
        margin: 40px auto;
    }
</style>
<h5> Профиль пользователя</h5>
<div class="table-responsive">
<table class="table table-hover table-condensed">
    <tr>
        <td><?= Yii::t('profile','Lastname')?></td>
        <td colspan="2"><?= $model->lastname ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('profile','Firstname')?></td>
        <td colspan="2"><?= $model->firstname ?></td>
    </tr>
    <?php if ($model->middlename):?>
    <tr>
        <td><?= Yii::t('profile','Middlename')?></td>
        <td colspan="2"><?= $model->middlename ?></td>
    </tr>
    <?php endif;?>
    <tr>
        <td>E-mail</td>
        <td colspan="2"><?= $model->email ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('registration','Password')?></td>
        <td>&#149;&#149;&#149;&#149;&#149;&#149;</td>
        <td>
            <a href="/profile/update-password"><button type="button" class="btn btn-primary btn-xs">Редактировать</button>
        </td>
    </tr>
    <tr>
        <td><?= Yii::t('profile','Phone number')?></td>
        <td>
            <?php
            if($model->phone_number) {
                echo $model->phone_number;
                echo $model->phone_status ? Yii::t('profile', ' Подтвержден'):Yii::t('profile', ' Не подтвержден');
            }else
                echo "N/A";
            ?>
        </td>
        <td>
            <?php if($model->phone_number):?>
                <a href="/profile/edit-phone">
                    <button type="button" class="btn btn-primary btn-xs">Редактировать</button>
                </a>
                <?php if(!$model->phone_status):?>
                    <a href="/profile/confirm-phone">
                        <button type="button" class="btn btn-primary btn-xs">Подтвердить</button>
                    </a>
                <?php endif;?>
            <?php else :?>
                <a href="/profile/edit-phone">
                    <button type="button" class="btn btn-primary btn-xs">Подключить</button>
                </a>
            <?php endif;?>
        </td>
    </tr>

</table>

</div>