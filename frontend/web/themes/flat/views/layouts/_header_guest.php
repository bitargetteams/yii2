<?php
use \Yii;
$assetUrl = Yii::$app->get('curBundle')->baseUrl;
?>
<div class="logo">
    <a href="/" class="logo__link">
        <img src="<?= $assetUrl ?>/images/logo.png" alt="" class="logo__img">
    </a>
</div>
<div class="description"><span class="bolder">Bitarget</span> - это удобная единая Система интернет-рекламы,
    включающая в себя ряд
    уникальных сервисов.
</div>
<nav class="header-nav">
    <ul class="header-nav__list">
        <li class="header-nav__item"><a href="<?=Yii::$app->getUrlManager()->createUrl('page/about');?>" class="header-nav__link">О компании</a></li>
        <li class="header-nav__item"><a href="<?=Yii::$app->getUrlManager()->createUrl('news/');?>" class="header-nav__link">Новости</a></li>
        <li class="header-nav__item"><a href="<?=Yii::$app->getUrlManager()->createUrl('/');?>" class="header-nav__link">Форум</a></li>
        <li class="header-nav__item"><a href="<?=Yii::$app->getUrlManager()->createUrl('feedback/index');?>" class="header-nav__link">Обратная связь</a></li>
    </ul>
</nav>
<form novalidate="novalidate" class="authorization" action="/user/login" method="POST">
    <div style="visibility: hidden;">
        <input type="text" name="login[login]" placeholder="E-mail">
    </div>
    <input type="hidden" name="_csrf" value="<?= Yii::$app->getRequest()->getCsrfToken()?>" />
    <input type="text" name="login[email]" placeholder="E-mail" required="required" class="form-control input-sm authorization__input">
    <input type="password" name="login[password]" placeholder="Пароль" required="required" class="form-control input-sm authorization__input">
    <div class="trigger authorization__captcha">
        <img src="" alt="" width="120px" height="50px" class="authorization__captcha-img">
        <a href="#" class="authorization__captcha-link">Получить новый код</a>
        <input type="text" name="" placeholder="Введите код" class="form-control input-sm captcha-input authorization__input">
    </div>
    <label for="rememberMe" class="checkbox authorization__label" >
        <input name="login[rememberMe]" id="savepass" type="checkbox" class="authorization__checkbox">
        Запомнить пароль
    </label>
    <a href="<?=Yii::$app->getUrlManager()->createUrl('user/request-password-reset');?>" class="getpass authorization__link">Забыли пароль</a>
    <button type="submit" class="btn btn-primary authorization__button">Войти</button>
    <a href="/user/registration" class="btn btn-primary reg authorization__button">Регистрация</a>
</form>
