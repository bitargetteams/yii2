<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\di\ServiceLocator;

/* @var $this \yii\web\View */
/* @var $content string */

Yii::$app->set('curBundle',AppAsset::register($this));
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app>
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<?= Yii::$app->params['shortcut_icon'] ?>"/>
    <?php $this->head() ?>
</head>

<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <header>
        <?=Yii::$app->user->isGuest ? $this->render('_header_guest') : $this->render('_header_user')?>
    </header>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    <footer>
        <?= $this->render('_footer') ?>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
