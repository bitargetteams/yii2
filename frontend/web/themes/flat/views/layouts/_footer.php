﻿<?php
use \Yii;
use \yii\helpers\Html;
$assetUrl = Yii::$app->get('curBundle')->baseUrl;
?>
<div class="payment">
    <ul class="payment__list">
        <li class="payment__item"><a href="#" class="payment__link">
               <?= Html::img($assetUrl.'/images/qiwi.png',['alt'=>"","class"=>"payment__img"])?>
        </li>
        <li class="payment__item"><a href="#" class="payment__link">
                <?= Html::img($assetUrl.'/images/webmoney.png',['alt'=>"","class"=>"payment__img"])?>
        </li>
        <li class="payment__item"><a href="#" class="payment__link">
                <?= Html::img($assetUrl.'/images/visa.png',['alt'=>"","class"=>"payment__img"])?>
        </li>
        <li class="payment__item"><a href="#" class="payment__link">
                <?= Html::img($assetUrl.'/images/mastercard.png',['alt'=>"","class"=>"payment__img"])?>
        </li>
    </ul>
</div>
<div class="footer-text">ООО "Нэт-сервис"</div>