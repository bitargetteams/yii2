<?php
$assetUrl = \Yii::$app->get('curBundle')->baseUrl;

use yii\helpers\Html;
?>
<div class="innerpanel logo">
    <?= Html::a(Html::img($assetUrl."/images/logo.png", "", array("class" => 'logo__img')), '/', array('class' => 'logo_a')); ?>
</div>
<div class="user-inform">
    <div class="user-inform__avatar">
<?= Html::a('', Yii::$app->urlManager->createUrl('/profile'), array('class' => 'user-inform__avatar')); ?>
    </div>
    <div class="user-inform__user">
        <div class="user-inform__name-text">
            <?= Yii::t('share', 'You are logged in as'); ?>
        </div>
        <?= Html::a(Yii::$app->user->lastname.' '.Yii::$app->user->firstname, '/user/index', array('class' => 'user-inform__name')); ?>

        <div class="user-inform__date-visit">
            <?= Yii::t('registration', 'Your last signup'); ?>&nbsp;
            <?= date("d.m.Y", strtotime(Yii::$app->user->getLastSignup())); ?>
        </div>
        <?= Html::a(Yii::t('login', 'Logout'), '/user/logout', array('class' => 'user-inform__a')); ?>
        <?= Html::a(Yii::t('share', 'Settings'),'/profile/settings', array('class' => 'user-inform__a')); ?>
    </div>
    <div class="user-inform__balance">
        <div class="user-inform__balance-text"><?= Yii::t('share', 'Balance'); ?></div>
        <div class="user-inform__money"><?php //= (int)User::model()->getBalance(Yii::$app->user->id);?>0 руб.</div>
        <?= Html::a(Yii::t('share', 'Withdraw'), "#", array('class' => 'user-inform__a','style'=>'')); ?>
        <?= Html::a(Yii::t('share', 'Fill'), "#", array('class' => 'user-inform__a')); ?>
    </div>
</div>
<div class="documents">
    <?= Html::a(Yii::t('share', 'Documents'), "#", array('class' => 'group-a documents__title')); ?>
    <ul class="documents__list">
        <li class="documents__item">
            <?= Html::a(Yii::t('share', 'Contracts'), "#", array('class' => 'documents__a')); ?>
        </li>
        <li class="documents__item">
            <?= Html::a(Yii::t('share', 'Reporting'), "#", array('class' => 'documents__a')); ?>
        </li>
    </ul>
</div>
<div class="notification">
    <div class="notification__number">26</div>
    <?= Html::a(Yii::t('share', 'Notifications'), '#', array('class' => 'group-a notification__title')); ?>
    <ul class="notification__list">
        <li class="notification__item">
            <?= Html::a(Yii::t('share', 'Important'), '#', array('class' => 'notification__a')); ?>
        </li>
        <li class="notification__item">
            <?= Html::a(Yii::t('share', 'Information'), '#', array('class' => 'notification__a')); ?>
        </li>
        <li class="notification__item">
            <?= Html::a(Yii::t('share', 'Common'), '#', array('class' => 'notification__a')); ?>
        </li>
    </ul>
</div>
<nav class="innerpanel header-nav">
    <ul class="header-nav__list">
        <li class="header-nav__item">
            <?= Html::a(Yii::t('share', 'About company'), '#', array('class' => 'header-nav__a')); ?>
        </li>
        <li class="header-nav__item">
            <?= Html::a(Yii::t('share', 'News'), '/news', array('class' => 'header-nav__a')); ?>
        </li>
        <li class="header-nav__item">
            <?= Html::a(Yii::t('share', 'Forum'), 'http://forum.'.Yii::$app->request->getServerName().'/index.php?sid'.Yii::$app->session['forum_session_id'], array('class' => 'header-nav__a')); ?>
        </li>
        <li class="header-nav__item">
            <?= Html::a(Yii::t('share', 'Feedback'), '/feedback', array('class' => 'header-nav__a')); ?>
        </li>
    </ul>
</nav>
<div class="social-buttons">
    <ul class="social-buttons__list">
        <li class="social-buttons__item">
            <?= Html::a('', "http://vk.com/share.php?url=http://www.bitarget.ru", array('class' => 'vk social-buttons__a')); ?>
        </li>
        <li class="social-buttons__item">
            <?= Html::a('', "http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl=http://www.bitarget.ru", array('class' => 'ok social-buttons__a')); ?>
        </li>
        <li class="social-buttons__item">
            <?= Html::a('', "https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2www.bitarget.ru", array('class' => 'fb social-buttons__a')); ?>
        </li>
        <li class="social-buttons__item">
<!--            <a href="#" class="tw social-buttons__a"></a>-->
            <?= Html::a('', "https://twitter.com/intent/tweet?original_referer=&text=".Yii::$app->name."-&tw_p=tweetbutton&url=http%3A%2F%2Fwww.bitarget.ru", array('class' => 'tw social-buttons__a')); ?>
        </li>
    </ul>
</div>
