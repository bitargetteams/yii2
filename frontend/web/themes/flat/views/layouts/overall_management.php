<?php
    $controllerId = Yii::app()->getController()->id;
?>
<!doctype html>
<html id="ng-app" ng-app="bitarget" lang="<?php echo Yii::app()->language; ?>">
<head>
    <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
    <meta charset="UTF-8">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
<?php
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/styles/bootstrap.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/styles/flat-ui.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/styles/index.css');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/require.js', CClientScript::POS_HEAD, array('data-main' => Yii::app()->theme->baseUrl.'/scripts/main'));
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/jquery-1.11.0.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/jquery-ui-1.10.3.custom.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/jquery.ui.touch-punch.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/bootstrap.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/bootstrap-select.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/bootstrap-switch.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flatui-checkbox.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flatui-radio.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flatui-fileinput.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/jquery.tagsinput.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/jquery.placeholder.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/typeahead.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/application.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/scripts.js', CClientScript::POS_END);
?>
</head>
<body>
<div class="wrapper">
    <div class="top-block">
        <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_header_content'); ?>
    </div>
    <div class="center-block">
        <div class="left-panel">
            <?php $this->widget('application.widgets.statistics.Statistics'); ?>
            <div class="direction">
                <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.direction._directions_list'); ?>
            </div>
        </div>
        <div class="<?= $controllerId ?>">
            <?php echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.direction._tabs'); ?>
            <?php if(isset($this->module)) { ?>
            <div class="<?= $controllerId; ?>-top-nav">
                <?php if(CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) { ?>
                    <?php $this->widget('application.widgets.navigation.NavigationThirdLevel',
                                        array(
                                            'items' => $this->module->getCurrentThirdLevelMenu(),
//                                            'linkOptions' => array('class' => $controllerId.'-top-nav__link'),
                                            'itemCssClass' => $controllerId.'-top-nav__item',
                                            'htmlOptions' => array('class' => $controllerId.'-top-nav__list'),
                                        )); ?>
                <?php } ?>
            <?php } else { ?>
                <div class="<?= $controllerId; ?>-top-nav__sub-list">
                    <?php $this->widget('zii.widgets.CMenu',
                                        array(
                                            'items' => $this->getCurrentFourthLevelMenu(),
                                            'htmlOptions' => array('class' => 'navigation-fourth-level'),
                                        )); ?>
                </div>
            <?php } ?>
            </div>
            <div class="<?= $controllerId; ?>-table">
                <?= $content; ?>
            </div>
        </div>
    </div>
    <footer class="innerpanel">
        <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_footer_content'); ?>
    </footer>
</div>
</body>
</html>