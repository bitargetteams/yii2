<style type="text/css">
    div.dialog {
        width: 25em;
        padding: 0 4em;
        margin: 4em auto 0 auto;
        border: 1px solid #ccc;
        border-right-color: #999;
        border-bottom-color: #999;
    }

    div.dialog a{
        font-size: 15px;
    }

    div.dialog span{
        font-size: 5em;
    }
    h1 { font-size: 100%; color: #f00; line-height: 1.5em; }
</style>

<div class="dialog">
    <h1><span>404</span> Not found.</h1>
    <a href="<?= Yii::$app->homeUrl?>"> Go back</a>
</div>