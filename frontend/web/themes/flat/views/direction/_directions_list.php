<?= CHtml::dropDownList('direction', false,
                        array(
                            Yii::t('share', 'Links'),
                            Yii::t('share', 'Teletext'),
                            Yii::t('share', 'Audit'),
                            Yii::t('share', 'Context ads'),
                            Yii::t('share', 'Notes'),
                        ),
                        array(
                            'class' => 'direction__select',
                        ));
?>
