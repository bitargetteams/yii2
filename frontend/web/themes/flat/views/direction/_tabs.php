<?php
    $controllerId = Yii::app()->getController()->id;
?>
<nav class="user-type-nav">
    <ul class="user-type-nav__list">
        <li class="user-type-nav__item">
            <?= CHtml::link(Yii::t('share', 'For webmaster'), "/".Yii::app()->getController()->getModule()->id."/webmaster/",
                            array(
                                'class' => "user-type-nav__link " . ($controllerId == 'webmaster' ? 'active' : ''),
                            )); ?>
        </li>
        <li class="user-type-nav__item">
            <?= CHtml::link(Yii::t('share', 'For optimizer'), "/".Yii::app()->getController()->getModule()->id."/optimizer/",
                            array(
                                'class' => "user-type-nav__link " . ($controllerId == 'optimizer' ? 'active' : ''),
                            )); ?>
        </li>
        <li class="user-type-nav__item">
            <?= CHtml::link(Yii::t('share', 'For partner'), "/".Yii::app()->getController()->getModule()->id."/partner/",
                            array(
                                'class' => "user-type-nav__link " . ($controllerId == 'partner' ? 'active' : ''),
                            )); ?>
        </li>
    </ul>
</nav>
