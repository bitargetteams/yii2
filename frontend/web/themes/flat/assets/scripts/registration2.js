$("#birthday").datepicker({
    changeYear: true,
    changeMonth: true,
    dateFormat: "dd.mm.yy",
    maxDate: "-18y"
});
$("#passport-givenday").datepicker({
    changeYear: true,
    changeMonth: true,
    dateFormat: "dd.mm.yy",
    maxDate: "0y"
});
$("#ogrn-givenday2, #spnu-givenday2, #sr-givenday2, #ogrn-givenday3, #spnu-givenday3, #sr-givenday3, #ogrn-givenday4, #spnu-givenday4, #sr-givenday4").datepicker({
    changeYear: true,
    changeMonth: true,
    dateFormat: "dd.mm.yy",
    maxDate: "0y",
    minDate: "25.12.1991"
});
$(document).ready(function(){
    function radioCheck(){
        var checkbox1 = document.getElementById("user-type2-trigger");
        var checkbox2 = document.getElementById("user-type3-trigger");
        var checkbox3 = document.getElementById("user-type4-trigger");
        $('.radio-list2 :radio').radio('uncheck');
        if(checkbox1.checked){
            $('#nalog-normal2').radio('check');
        }
        if(checkbox2.checked){
            $('#nalog-normal3').radio('check');
        }
        if(checkbox3.checked){
            $('#nalog-normal4').radio('check');
        }
    }
    radioCheck();

    function usertype(){
        var radio = $(".user-type__radio");
        Array.prototype.forEach.call(radio, function(el){
            if (el.checked==true){
                document.getElementById("description"+el.value).classList.add("visible");
                document.getElementById("user-info"+el.value).classList.add("visible");
                var inputs = $("#user-info"+el.value+" input");
                Array.prototype.forEach.call(inputs, function(el){
                    el.disabled=false;
                });
            }else{
                document.getElementById("description"+el.value).classList.remove("visible");
                document.getElementById("user-info"+el.value).classList.remove("visible");
                var inputs = $("#user-info"+el.value+" input");
                Array.prototype.forEach.call(inputs, function(el){
                    el.disabled=true;
                });
            }
        });
    }
    usertype();

    var showErrMess = jQuery.Event("showErrMess"),
        showSuccessMess = jQuery.Event("showSuccessMess"),
        form = document.querySelector(".registration-form-2"),
        button = document.querySelector(".registration-form-2__button");

    function validForm(){
        form.checkValidity() ? button.disabled=false : button.disabled=true;
    }
    validForm();

    $(".registration-form-2").on('showErrMess',".user-info__error-item",function(event){
        event.target.classList.add("visible");
    }).on("input","input",function(event){
        var target = event.target,
            nextElem=target.nextElementSibling;
        Array.prototype.forEach.call(nextElem.children, function(el){
            el.classList.remove("visible");
            target.classList.remove("error");
        });
        if(!target.validity.valid){
            for(var errType in target.validity) {
                if(errType!="valid" && target.validity[errType]){
                    $(nextElem.querySelector("."+errType)).trigger('showErrMess');
                    target.classList.add("error");
                }
            }
        }
        validForm()
    }).on("change","input",function(){
        validForm()
    });

    $(".user-type").on('change',".user-type__radio",function(){
        usertype();
        radioCheck();
    });
});