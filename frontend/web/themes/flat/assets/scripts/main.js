require.config({
    paths: {
        'slick':'slick.min',
        'feedback_validation':'feedback-validation',
        'registration':'registration',
        'registration2':'registration2',
        'webmaster':'webmaster'
    }
});

//Слайдер на главной
if(document.querySelector(".single-item")){
    require(['slick'], function (){
        $('.single-item').slick();
    });
}

//Валидация формы обратной связи
if(document.querySelector(".contact-form")){
    require(['feedback-validation'], function(){});
}

//Регистрация
if(document.querySelector(".registration-form")){
    require(['registration'], function(){});
}

//Регистрация 2
if(document.querySelector(".registration-form-2")){
    require(['registration2'], function(){});
}

//Страница вебмастара
if(document.querySelector(".webmaster")){
    require(['webmaster'], function(){});
}