$(document).ready(function(){
    function consent(){
        var checkbox = document.getElementById("contract-trigger"),
            inputs = $(".registration-form__input"),
            button = document.querySelector(".registration-form__button");
        if (checkbox.checked) {
            Array.prototype.forEach.call(inputs, function (el) {
                el.disabled = false;
            });

        } else {
            Array.prototype.forEach.call(inputs, function (el) {
                el.disabled = true;
            });
            button.disabled = true;
        }
    }
    consent();

   $(".contract-block__checkbox").on('change',function(){
        consent();
   });

    //Валидация формы

    var showErrMess = jQuery.Event("showErrMess"),
        showSuccessMess = jQuery.Event("showSuccessMess"),
        form = document.querySelector(".registration-form"),
        button = document.querySelector(".registration-form__button"),
        pass1 = document.querySelector("#registration-pass-1"),
        pass2 = document.querySelector("#registration-pass-2"),
        email1 = document.querySelector("#registration-email-1"),
        email2 = document.querySelector("#registration-email-2");

    function validForm(){
        form.checkValidity() ? button.disabled=false : button.disabled=true;
    }
    $("#registration-pass-2").on('input',function(){
        if(!this.validity.valueMissing){
            if((pass1.value === pass2.value)&&(pass1.value != "")){
                this.setCustomValidity("");
            }else{
                this.setCustomValidity("error");
            }
        }else{
            this.setCustomValidity("");
        }
    });
    $("#registration-email-2").on('input',function(){
        if(!this.validity.valueMissing) {
            if ((email1.value === email2.value) && (email1.value != "")) {
                this.setCustomValidity("");
            } else {
                this.setCustomValidity("error");
            }
        }else{
            this.setCustomValidity("");
        }
    });
    $(".registration-form").on('showErrMess',".registration-form__error-item",function(event){
        event.target.classList.add("visible");
    }).on("input","input",function(event){
        var target = event.target,
            nextElem=target.nextElementSibling;
        Array.prototype.forEach.call(nextElem.children, function(el){
            el.classList.remove("visible");
            target.classList.remove("error");
        });
        if(!target.validity.valid){
            for(var errType in target.validity) {
                if(errType!="valid" && target.validity[errType]){
                    $(nextElem.querySelector("."+errType)).trigger('showErrMess');
                    target.classList.add("error");
                }
            }
        }
        validForm()
    });
});