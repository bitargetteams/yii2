$(':checkbox').checkbox();
$("select").selectpicker({style: 'btn-hg btn-primary', menuStyle: 'dropdown-inverse'});
$('.contact-form .contact-form__select .filter-option').text("Тематика сообщения");
$("#datepicker").datepicker(
	{
		dateFormat: "yy-mm-dd",
		onSelect: function(dateText, inst) {
			document.location.replace('/news/bydate/' + dateText);
		}
	}
);