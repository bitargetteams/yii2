//Валидация формы обратной связи

$('.contact-form .contact-form__select .filter-option').text("Тематика сообщения");
$(document).ready(function(){

    var subButton = document.querySelector(".contact-form__button"),
        contForm = document.querySelector(".contact-form"),
        showErrMess = jQuery.Event("showErrMess");

    function validForm(){
        if(contForm.checkValidity()){
            subButton.disabled=false;
        }else{
            subButton.disabled=true;
        }
    }
    validForm();

    $(".contact-form").on('showErrMess',".contact-form__error-item",function(event){
        event.target.classList.add("visible");
    }).on("input","input",function(event){

        var target = event.target,
            nextElem = target.nextElementSibling;

        Array.prototype.forEach.call(nextElem.children, function(el, i){
            el.classList.remove("visible");
            target.classList.remove("error");
        });

        if(!target.validity.valid){
            for(var errType in target.validity) {
                if(errType!="valid" && target.validity[errType]){
                    $(nextElem.querySelector("."+errType)).trigger('showErrMess');
                    target.classList.add("error");
                }
            }
        }
        validForm();

    }).on("input","textarea",function(event){
        var target = event.target,
            nextElem = target.nextElementSibling,
            textarea = document.querySelector(".contact-form textarea");

        if((textarea.value.length < 10)&&(textarea.value.length > 0)){
            textarea.setCustomValidity("error");
        }else{
            textarea.setCustomValidity("");
        }

        Array.prototype.forEach.call(nextElem.children, function(el, i){
            el.classList.remove("visible");
            target.classList.remove("error");
        });

        if(!target.validity.valid){
            for(var errType in target.validity) {
                if(errType!="valid" && target.validity[errType]){
                    $(nextElem.querySelector("."+errType)).trigger('showErrMess');
                    target.classList.add("error");
                }
            }
        }
        validForm();

    }).on("click",".select",function(){
        var liClass = document.querySelector(".select li[rel='0']").classList,
            custError = document.querySelector(".noValue");

        if(liClass == "selected"){
            custError.classList.add("visible");
        }
        validForm();

    }).on("click",".select",function(){
        var liClass = document.querySelector(".select li[rel='0']").classList,
            custError = document.querySelector(".noValue");

        if(liClass != "selected"){
            custError.classList.remove("visible");
        }else{
            $('.contact-form .contact-form__select .filter-option').text("Тематика сообщения");
        }
        validForm();
    })
});