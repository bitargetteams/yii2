

$(function() {
    $('#navigation-whois li').click(function() {
        $('#content .note .text').hide();
        $('#content .note .text-'+$(this).index()).show();
        $('li', $(this).parent()).removeClass('active');
        $(this).addClass('active');
       // $('#content .note').css('background', 'url(/themes/biblue/img/note-left-image-'+$(this).index()+'.png) no-repeat');   
        return false;
    });
    
    $('#content .note .next').click(function() {
        var index = $('#navigation-whois .active').index();
        if(index == $('#navigation-whois li:last-child').index()) {
            index = 0;
        } else {
            index += 1;
        }

        $('#navigation-whois li:eq('+index+')').trigger('click');
        
        return false;        
    });

});
