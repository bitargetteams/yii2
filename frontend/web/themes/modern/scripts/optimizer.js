

$(function() {
    var defaultSymbol = $('.toggle').eq(0).text();
    
    $('.toggle').each(function(index, element) {
        var parent = $(this).parent().parent();
        var parentIndex = $(parent).index();
        var grandpa = $(parent).parent();
        
        var data = $('> tr:eq('+(parentIndex+1)+')', grandpa);
        var length = $('.links-cell', data).length;
        
        if(length == 0) {
            $(this).addClass('unactive');
        }
    });

    $('.toggle').click(function() {
        if($(this).hasClass('unactive')) {
            return false;
        }
        var parent = $(this).parent().parent();
        var parentIndex = $(parent).index();
        var grandpa = $(parent).parent();

        var data = $('> tr:eq('+(parentIndex+1)+')', grandpa);
        var data = $('.links-cell', data);

        if($(data).css('display') == 'none') {
            $(this).text('-');
            data.show(100);
        } else {
            $(this).text(defaultSymbol);
            data.hide(100);
        }
        return false;
    });
});
