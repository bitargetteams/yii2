$(document).ready(function(){
    menu();
    $(window).on('resize', menu);   
});


function setMenuActive(){
    var hiddenElements = $('#more-list > li');
    for (var i = 0; i < hiddenElements.length; i++){
        if ( $(hiddenElements[i]).hasClass('active') ){
           $('#more').addClass('active');
        }
    }
}
function menu()
{
    var allListElements = $(".navigation-third-level > li");
    var allListElementsWidth = 0;
    var ulElementWidth = $(".navigation-third-level").width();
    var moreMenu = [];
    for (var i = 0; i < allListElements.length; i++){
        allListElementsWidth += allListElements[i].offsetWidth;
        if (ulElementWidth <= allListElementsWidth){
            if( $(allListElements[i]).attr('id') == 'more' ) {
                var elem = $(allListElements[i]).eq(-2);
            } else {
                var elem = $(allListElements[i]);
            }
            elem.attr("data-size", elem.width() );
            elem.detach();
            moreMenu.push(elem);
            if(!$('#more').length)
            {
                $(".navigation-third-level").append('<li id="more" ><a href="#" id="more-link">Ещё</a><ul id="more-list"></ul></li>');
                $("#more").css({'display':'block'});
                $('#more-link').bind("click", function callMenu() {
                                                var menu = $('#more-list');
                                                if(menu.css('display') == 'none') {
                                                    $('#more').addClass('active');
                                                    menu.slideDown(100);
                                                } else {
                                                    menu.slideUp(100);
                                                    $('#more').removeClass('active');
                                                    setMenuActive();
                                                }
                                            return false;
                                            });
            }
        } else {
            
        }
        $('#more-list').append(moreMenu.pop());
    }
    setMenuActive();
    
    var elementBackToMenu = $('#more-list > li:first-child');
    if (ulElementWidth > allListElementsWidth + elementBackToMenu.data('size')){
        elementBackToMenu.detach();
        $(elementBackToMenu).insertBefore("#more");
        if ( $("#more-list > li").length <= 0 ) {
                $("#more").detach();
        }
    }
}