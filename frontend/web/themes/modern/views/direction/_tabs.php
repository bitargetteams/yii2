        <?php
        
        if(isset($this->module) && CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) {
            $activity = $this->module->getUserActivity();
        } else {
            $activity = CommonHelpers::getLastValueFromUrl();
        }
        
        ?>
        <?php if(Yii::app()->controller->id != 'direction'){?>
             <ul class="tabs">
            <?php if($this->getUniqueId() == 'direction' || $this->getUniqueId() == 'news/default') { ?>
                <li class="title"><?php echo CHtml::link($this->pageTitle, '/'.$this->getUniqueId().'/'.$this->action->id); ?></li>
            <?php } ?>
            <?php if(isset($this->module) && $wmDefaultLink = $this->module->getWebmasterDefaultLink()) { ?>
                <li class="for-webmaster <?php echo $activity == 'wm' ? 'active' : ''; ?>">
                    <?php if(isset($this->module) && CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) { ?>
                        <?php echo $wmDefaultLink; ?>
                    <?php } else { ?>
                        <?php echo CHtml::link(Yii::t('share', 'For webmaster'), '/'.$this->getUniqueId().$this->action->id.'/wm'); ?>
                    <?php } ?>
                </li>
            <?php } ?>
            <?php if(isset($this->module) && $optDefaultLink = $this->module->getOptimizerDefaultLink())?>
            <li class="for-optimizer <?php echo $activity == 'opt' ? 'active' : ''; ?>">
                <?php if(isset($this->module) && CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) { ?>
                    <?php echo $optDefaultLink; ?>
                <?php } else { ?>
                    <?php echo CHtml::link(Yii::t('share', 'For optimizer'), '/'.$this->getUniqueId().$this->action->id.'/opt'); ?>
                <?php } ?>
            </li>
        </ul>
        <?php }?>
        <?php if(!Yii::app()->user->isGuest ){?>
        <div class="user-profile">
            <a id="user-profile-link" href="#">Ваш профиль</a>
            <ul class="settings-list">
                <li class="settings-list-item">
                    <a class="settings-list-link" href="/profile/default/index">Личная информация</a>
                </li>
                <li class="settings-list-item">
                    <a class="settings-list-link" href="/profile/documents/contracts">Документы</a>
                </li>
                <li class="settings-list-item">
                    <a class="settings-list-link" href="/profile/balance/add">Пополнение счёта</a>
                </li>
                <!--<li class="settings-list-item">
                    <a class="settings-list-link"  href="#">Вывод средств</a>
                </li>
                <li class="settings-list-item">
                    <a class="settings-list-link"  href="#">Статистика взаиморасчётов</a>
                </li>
                <li class="settings-list-item">
                    <a class="settings-list-link"  href="#">Настройки</a>
                </li>-->
            </ul>
        </div>
        <?php } else { ?> 
        <div class="user-profile">
            <a id="user-profile-link" href="#">Ваш профиль</a>
            <ul class="settings-list">
                <li class="settings-list-item">
                    <a class="settings-list-link" href="/user/login">Войти</a>
                </li>
                <li class="settings-list-item">
                    <a class="settings-list-link" href="/user/registration">Зарегистроваться</a>
                </li>
            </ul>
        </div>
        <?php }?>