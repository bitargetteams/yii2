<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/direction_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
?>

        <div class="about-picture">&nbsp;</div>
        <p>
            Одним из важнейших факторов, влияющих на позицию сайта в поисковых системах, 
            а значит и на его раскрутку, является количество и качество внешних ссылок на сайт. 
            Наряду с другими инструментами, максимальный эффект по наращиванию ссылочной массы сайта дает работа с такими биржами,
            как Рекламная Система Bitarget. Поскольку для получения ощутимого эффекта от раскрутки и продвижения сайтов требуется 
            достаточно большое количество внешних ссылок (от нескольких десятков до нескольких тысяч ссылок), пользователям необходим 
            действенный инструмент для автоматизации размещения ссылок. Таким механизмом является Направление «ССЫЛКИ» в Рекламной Системе Bitarget.     
        </p>
        
<div class="wm-picture-border">
        <img class="picture-border-tl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/2.png';?>">
        <div class="wm-picture-header"></div>
        <img class="picture-border-tr" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/1.png';?>">
        <div class="description-wrapper wm">
            <div class="description">
                <p>Владельцы сайтов, желающие хорошо на них зарабатывать,
                    не тратя при этом свое время на рутину, получат уникальную возможность делать это, 
                    работая через Рекламную Систему Bitarget. С помощью всего лишь пары кликов бизнес начинает приносить 
                    Вебмастеру стабильный и постоянно растущий доход, не отнимая его время. 
                    Рекламная Система Bitarget работает за Вебмастера, продавая и покупая в его интересах ссылки с главных и внутренних страниц сайтов. 
                    Ресурсы Системы Bitarget практически безграничны. Направление «ССЫЛКИ» Рекламной Системы Bitarget дает Вебмастеру возможность участвовать 
                    в Системе стабильного заработка денег с помощью его сайта путем продажи мест под ссылки с сайта Вебмастера на сайты Оптимизаторов. 
                    За размещение таких ссылок на своем сайте Вебмастер получает стабильный и растущий доход.   
                </p>
                
            </div>
        </div>
        <img class="picture-border-bl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/3.png';?>">
        <img class="picture-border-br" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/4.png';?>">
</div>

<div class="opt-picture-border">
        <img class="picture-border-tl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/2.png';?>">
        <div class="wm-picture-header"></div>
        <img class="picture-border-tr" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/1.png';?>">
        <div class="description-wrapper opt">
            <div class="description">
                 <p> Оптимизаторы, работающие через Рекламную Систему Bitarget, получат большой объем ссылочной массы на свои сайты. 
                     Закупка Оптимизаторами ссылок с других сайтов на свои площадки с целью улучшения позиций своих сайтов в поисковых системах и увеличения их посещаемости,
                     является распространенным методом работы Оптимизатора. Оптимизаторы, выбравшие в качестве площадки для работы Рекламную Систему Bitarget, 
                     имеют право выбора тематики и категории сайтов, на которых размещаются ссылки на их сайты. Кроме того, направление «ССЫЛКИ» Рекламной Системы Bitarget
                     дает Оптимизатору право и возможность выбора различных дополнительных параметров сайтов, на которых будут размещены обратные ссылки.
                 </p>
            </div>
        </div>
        <img class="picture-border-bl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/3.png';?>">
        <img class="picture-border-br" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/4.png';?>">
</div>

