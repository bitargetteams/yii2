<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/direction_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
?>

        <div class="about-picture">&nbsp;</div>
        <p>
            Заметки являются новым уникальным рекламным направлением, представленным в Рекламной Системе Bitarget.  Рекламное Направление Заметки это указание на особо важное место на сайте или в тексте, которые могут быть использованы для привлечения внимания посетителей сайта, а также для направления посетителя сайта к рекламируемому сайту. Заметки могут быть расположены в любом месте интернет-страницы. Заметки написаны рукописным шрифтом, что, в свою очередь, также работает в целях привлечения внимания посетителей сайта к тому или иному контенту. В Рекламной Системе Bitarget реализованы все необходимые удобные инструменты для работы с рекламным направлением Заметки.      
        </p>
        
<div class="wm-picture-border">
        <img class="picture-border-tl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/2.png';?>">
        <div class="wm-picture-header"></div>
        <img class="picture-border-tr" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/1.png';?>">
        <div class="description-wrapper wm">
            <div class="description">
                <p>Вебмастер получает возможность предоставления своих интернет-сайтов для размещения уникальных и креативных рекламных заметок. Вебмастер самостоятельно определяет количество размещаемых на одной странице заметок, выбирая при этом их виды и цветовую гамму. Заметки, являясь новым и уникальным рекламным направлением, дают Вебмастерам возможность для привлечения новых рекламодателей и, таким образом, получения дополнительной прибыли.  
 
                </p>
                
            </div>
        </div>
        <img class="picture-border-bl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/3.png';?>">
        <img class="picture-border-br" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/webmaster/4.png';?>">
</div>

<div class="opt-picture-border">
        <img class="picture-border-tl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/2.png';?>">
        <div class="wm-picture-header"></div>
        <img class="picture-border-tr" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/1.png';?>">
        <div class="description-wrapper opt">
            <div class="description">
                 <p> Благодаря Рекламной Системе Bitarget Оптимизаторы получают возможность работы с новым рекламным направлением - Заметки. Оптимизатор, работающий с помощью направления Заметки в Рекламной Системе Bitarget, получает возможность мгновенного размещения указывающих Заметок на сотнях тысяч сайтов. Данный вид рекламы интересен Оптимизаторам тем, что рекламные Заметки, расположенные в разных частях страницы, бросаются в глаза и привлекают внимание посетителей сайтов, на которых они расположены. Помимо этого, Оптимизатору также дается право на выбор различных критериев поиска и подбора сайтов, на которых необходимо расположить рекламные Заметки. Данное рекламное направление, используемое наряду с другими видами рекламы, позволяет дополнительно увеличить трафик и, соответственно, ускорить продвижение рекламируемого сайта. 
                 </p>
            </div>
        </div>
        <img class="picture-border-bl" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/3.png';?>">
        <img class="picture-border-br" src="<?php echo Yii::app()->theme->baseUrl.'/img/directions/corners/optimizer/4.png';?>">
</div>

