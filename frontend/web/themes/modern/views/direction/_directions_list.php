<h2 class="title">Рекламные направления<span class="arrow">&#9660;</span></h2>
<?php
$audit = ((Yii::app()->getController()->id == 'direction')) ? '/audit/' : '/audit/default/index';
$direction = Yii::app()->getController()->id == 'direction' ? 'direction' : '';

$navigation = array(
    'items'=>array(
        array('label'=>Yii::t('share', 'Links'), 'url'=>array($direction.'/links/')),
        array('label'=>Yii::t('share', 'Teletext'), 'url'=>array($direction.'/teletext/')),
        array('label'=>Yii::t('share', 'Audit'), 'url'=>array($direction.$audit)),
        array('label'=>'Контекстная реклама', 'url'=>array('/contextad/')),
        array('label'=>'Заметки', 'url'=>array('/notes/')),
//        array('label'=>Yii::t('share', 'Context ads'), 'url'=>array($direction.'/contextads/')),
//        array('label'=>Yii::t('share', 'Notes'), 'url'=>array($direction.'/notes/')),
//         array('label'=>Yii::t('share', 'Articles'), 'url'=>array($direction.'/paudit/')),
//         array('label'=>Yii::t('share', 'Blogs'), 'url'=>array($direction.'/gaudit/')),
//         array('label'=>Yii::t('share', 'Articles'), 'url'=>array($direction.'/anudit/')),
//         array('label'=>Yii::t('share', 'Announcing'), 'url'=>array($direction.'/naudit/')),
    ),
    'htmlOptions' => array(
    ),
);

if((Yii::app()->controller->module) && Yii::app()->controller->module->name == 'help') {
    array_unshift($navigation['items'], array('label'=>Yii::t('share', 'For optimizer'), 'url'=>array($direction.'/foroptimizer/')));
    array_unshift($navigation['items'], array('label'=>Yii::t('share', 'For webmaster'), 'url'=>array($direction.'/forwebmaster/')));
    array_unshift($navigation['items'], array('label'=>Yii::t('share', 'Private data'), 'url'=>array($direction.'/profile/')));
    array_unshift($navigation['items'], array('label'=>Yii::t('share', 'Share'), 'url'=>array($direction.'/share/')));
}

$this->widget('application.widgets.SideBarNavigation', $navigation);
