
<a href="/" class="logo-bitarget"></a>

<?php if(Yii::app()->user->isGuest) { ?>
<div class="note">
	Bitarget - это удобная единая Система<br /> интернет-рекламы, включающая<br />
	в себя ряд уникальных сервисов.
</div>
<?php } else { ?>
<dl class="info">
    <?php if(isset(Yii::app()->user->last_signup)){?>
        <dt class="notice-text"><?php echo Yii::t('registration', 'Your last signup'); ?>:&nbsp;</dt>
        <dd class="notice-value"><?= Yii::app()->locale->dateFormatter->format("dd MMMM yyyy, HH:mm", Yii::app()->user->last_signup); ?></dd>
    <?php }
    if(isset(Yii::app()->user->registered)&& Yii::app()->user->registered==TRUE){
        $user= new User();

    ?>
        <dt class="notice-text"><?php echo Yii::t('share', 'Balance'); ?>:&nbsp;</dt>
        <dd class="notice-value money"><?=CHtml::link((float)$user->getBalance(Yii::app()->user->id), '/profile/balance/add');?></dd><div class="icon-ruble"></div>                            
    <?php }?>
</dl>
<?php } ?>
<div class="registration">
      <?php if(Yii::app()->user->isGuest) { ?>
          <div class="login">
            <a class="login-links" href="/user/registration">Регистрация</a>
            <span>|</span>
            <a class="login-links" href="/user/login">Вход</a>
         </div>
                    <?php } else { ?>
                        <div class="logout">
		<div class="row">
			<span class="entered-notice">
                                    <?php echo Yii::t('share', 'You are logged in as'); ?>
                                    <a class="entered-username"
				href="/user/index">
                                        <?php echo Yii::app()->user->name; ?>
                                    </a>
			</span>
            <?php
            echo CHtml::beginForm('/user/logout', 'post', array('id' => "logout-form", 'accept-charset' => "utf-8"));
            echo CHtml::submitButton("Выйти", array('class' => "send_form_submit_button"));
            echo CHtml::endForm();
            ?>
		</div>
		<div class="row">
			<a href="/user/index"> <img width="50" height="50"
                                                    src="<?php echo Yii::app()->theme->baseUrl;?>/img/avatar.jpg" alt="Avatar" />
			</a>
		</div>
	</div>
                    <?php } ?>
                </div>
    
    <div class="line-top"></div>
    <div class="line-bottom"></div>
    <div class="bottom">
        <div class="language">
        	<a href="<? echo Yii::app()->createUrl('user/changelanguage/', array('lang'=>'ru')); ?>" class="icons-flag-russia <?php echo Yii::app()->language == 'ru' ? 'flag-russia-active' : '' ;?> "></a> 
            <a href="<? echo Yii::app()->createUrl('user/changelanguage/', array('lang'=>'en_us')); ?>" class="icons-flag-britain <?php echo Yii::app()->language == 'en_us' ? 'flag-britain-active'  : '' ;?> "></a>
        </div>

        <ul class="navigation">
            <li><a href="/news">Новости</a></li>
<!--            <li><a href="/blog">Блог</a></li>-->
            <li><a href="http://forum.<?php echo Yii::app()->request->getServerName(); ?>/index.php?sid=<?php echo Yii::app()->session['forum_session_id'];?>">Форум</a></li>
            <li><a href="/feedback">Обратная связь</a></li>
<!--             <li><a href="">Контакты</a></li> -->
            <li><a href="/help">Помощь</a></li>
<!--             <li><a href="">Видеоуроки</a></li> -->
        </ul>
                   
         <div class="social-buttons">
             <a href="http://vk.com/share.php?url=http://www.bitarget.ru" target="_blank" class="social-icons-vk"></a>
             <a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl=http://www.bitarget.ru" class="social-icons-od"></a>
             <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.bitarget.ru" target="_blank" class="social-icons-f"></a>
             <a href="https://twitter.com/intent/tweet?original_referer=<?php echo Yii::app()->request->urlReferrer;?>&text=<?php echo Yii::app()->name;?>-&tw_p=tweetbutton&url=http%3A%2F%2Fwww.bitarget.ru" class="social-icons-t" data-url="http://www.bitarget.ru" data-lang="ru" target="_blank" data-count="none"></a>
         </div>
    </div>
