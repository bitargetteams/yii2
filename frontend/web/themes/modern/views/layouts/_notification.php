<?php
$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/css/notification.css');
Yii::app()->clientScript->registerCssFile($assetsUrl);

Yii::app()->clientScript->registerPackage('angular.resource');
Yii::app()->clientScript->registerScriptFile('/scripts/common/notification/controllers.js');
Yii::app()->clientScript->registerScriptFile('/scripts/common/notification/services.js');
?>
<table id="notifications" ng-controller="UserNotifications" ng-init="showUserNotifications(1)">
    <tr>
        <td id="notification-navicon">
            <div class="notification-navicon" ng-click="expandUserNotifications()"></div>
        </td>
        <td id="notification-items">
            <ul>
                <li class="notification-item" ng-show="($index==0) || ($index>0 && expanded==true)" ng-repeat="notification in notifications">
                    <div class="notification-{{notification.type}}"></div>
                    <div class="notification-text" ng-bind="notification.text"></div>
                    <div 
                        ng-show="(notification.type=='<?php echo Notification::TYPE_INFO; ?>' || notification.type=='<?php echo Notification::TYPE_ADMIN; ?>')" 
                        ng-class="{
                         'notification-hide-small': expanded==true, 
                         'notification-hide-big': expanded==false
                         }" 
                         ng-click="hideUserNotification($index)">
                    </div>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="3" id="notifications-all" ng-show="expanded==true">
            <a href="/notification">все уведомления</a>
        </td>
    </tr>
</table>