<!doctype html>
<html id="ng-app" ng-app="bitarget" lang="<?php echo Yii::app()->language; ?>">
<head>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui');

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.scripts') . DIRECTORY_SEPARATOR . 'global.js');
    Yii::app()->clientScript->registerScriptFile($assetsUrl);

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/scripts/overall_management.js');
    Yii::app()->clientScript->registerScriptFile($assetsUrl);

    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery.ui.menu.css');

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/css/global_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/css/overall_management.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/css/profile_new.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot') . Yii::app()->theme->baseUrl . '/css/help.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
    ?>
    <link href='http://fonts.googleapis.com/css?family=Marck+Script&subset=cyrillic,latin' rel='stylesheet'
          type='text/css'>

    <meta charset="UTF-8"/>
    <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
</head>
<body>
<div id="root">
    <div class="top-spacer">
        <div class="misc-links">

        </div>
    </div>
    <div class="bg">
        <div id="header">
            <?php $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts._layout_header_content'); ?>
        </div>
        <div id="messages">
            <?php echo Yii::t('HelpModule.default', 'Module'); ?>
        </div>
        <div id="content">
            <div class="content-inner">
                <div class="column">
                    <?php if (isset($this->module)) { ?>
                    <div
                        class="inner <?php echo CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior') ? $this->module->getUserActivity() : ''; ?>">
                        <?php if (CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) { ?>
                            <?php $this->widget('application.widgets.navigation.NavigationThirdLevel',
                                array(
                                    'items' => $this->module->getCurrentThirdLevelMenu(),
                                    'htmlOptions' => array('class' => 'profile-tabs'),
                                )); ?>
                        <?php } ?>
                        <?php } ?>
                        <div class="blue_container">
                            <?php
                            $this->widget('HelpMenu', array('items' => $this->items_menu_third_level,
                                'htmlOptions' => array('class' => 'navigation-third-level')));
                            $this->widget('HelpMenu', array('items' => $this->items_menu_fourth_level,
                                'htmlOptions' => array('class' => 'navigation-fourth-level')));
                            ?>
                            <div class="grey_container">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='sidebar'>
                <div class='content'>
                    <?php $this->widget('HelpMenu', array('items' => $this->items_sidebar)); ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="root_footer"></div>
</div>
<div id="footer">
    <div class="footer-inner">
        <?php $this->renderPartial('webroot.themes.' . Yii::app()->theme->name . '.views.layouts._layout_footer_content'); ?>
    </div>
</div>
</body>
</html>