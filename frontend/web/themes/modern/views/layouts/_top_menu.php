

<?php
    $this->widget(
        'zii.widgets.CMenu',
        array(
            'items' => array(
                array(
                    'url' => array('/news'),
                    'label' => 'Новости',
                ),
                array(
                    'url' => array('/blog'),
                    'label' => 'Блог',
                ),
            ),
            'htmlOptions' => array(
                'class' => 'navigation',
            ),
        ));
?>