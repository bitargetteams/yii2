<!doctype html>
<html lang="<?= Yii::app()->language ?>">
<?php
$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/global_layout.css');
Yii::app()->clientScript->registerCssFile($assetsUrl);
?>
<?php
$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/front_page_layout.css');
Yii::app()->clientScript->registerCssFile($assetsUrl);
?>
<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<?php
$assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.scripts').DIRECTORY_SEPARATOR.'global.js');
Yii::app()->clientScript->registerScriptFile($assetsUrl);
?>
<head>
	<meta charset="UTF-8" />
	<title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
</head>
<body>
      <div id='root'>
          <div class="bg">
            <div id='header'>
                <?php $this->renderPartial('//layouts/_layout_header_content'); ?>
            </div>
              <?= $this->renderPartial('//direction/_begin_note'); ?>
            <div id="content">
                <?php echo $content; ?>
            </div>
        </div>
        <div id='root_footer'></div>
      </div>
      <div id='footer'>
        <div class="footer-inner">
            <?php $this->renderPartial('//layouts/_layout_footer_content'); ?>
        </div>
      </div>
</body>
</html>

