<!doctype html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/global_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/overall_management.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
?>
	<meta charset="UTF-8" />
	<title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
</head>
<body>
  <div id="root">
    <div class="bg">
        <div id="header">
            <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_header_content'); ?>
        </div>
        <div id="messages">
            Уведомления
        </div>
        <div id="content">
            <div class="content-inner">
                <div class="column">
                    <?php echo $content;?>
                </div>
            </div>
            <div class="sidebar">
                <?php $this->widget('application.widgets.statistics.Statistics'); ?>
                <div class="content">
                    <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.direction._directions_list'); ?>
                </div>
            </div>
            <div class="clearfix"></div> 
        </div>        
    </div>
    <div id="root_footer"></div>
    <div id='scroll-up'><span>&#8593</span></div>
  </div>
  <div id="footer">
      <div class="footer-inner">
        <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_footer_content'); ?>
      </div>
  </div>
</body>
</html>