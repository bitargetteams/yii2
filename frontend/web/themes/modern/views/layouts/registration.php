<!doctype html>
<html id="ng-app" ng-app="bitarget" lang="<?php echo Yii::app()->language; ?>">
<head>
<?php
    Yii::app()->clientScript->registerCoreScript('jquery.ui');

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.scripts').DIRECTORY_SEPARATOR.'global.js');
    Yii::app()->clientScript->registerScriptFile($assetsUrl);

    Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.menu.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.button.css');

    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/global_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
    
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/overall_management.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
    
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/registration.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl); 
?>
	<meta charset="UTF-8" />
	<title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
</head>
<body>
  <div id="root">
    <div class="bg">
        <div id="header">
            <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_header_content'); ?>
        </div>
        <div id="messages">
            <?php echo isset($this->messageHeader) ? $this->messageHeader : Yii::app()->name; ?>
        </div>
        <div id="content">
            <div class="content-inner">
                <?php if ($this->getUniqueId() == 'registration'):?>
                <ul id="registration-second-navigation">
                    <li class="registration-second-navigation-li <?php if ($this->action->id == 'step1') echo 'current';?>"><?php echo Yii::t('registration', 'Step {step}', array('{step}' => 1));?></li>
                    <li class="registration-second-navigation-li <?php if ($this->action->id == 'step2') echo 'current';?>"><?php echo Yii::t('registration', 'Step {step}', array('{step}' => 2));?></li>
                    <li class="registration-second-navigation-li <?php if ($this->action->id == 'step3') echo 'current';?>"><?php echo Yii::t('registration', 'Step {step}', array('{step}' => 3));?></li>
                    <li class="registration-second-navigation-li <?php if ($this->action->id == 'step4') echo 'current';?>"><?php echo Yii::t('registration', 'Step {step}', array('{step}' => 4));?></li>
                </ul>
                <?php endif; ?>
                <?php echo $content; ?>
            </div>
        </div>
    </div>
    <div id="root_footer"></div>
  </div>
  <div id="footer">
      <div class="footer-inner">
        <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_footer_content'); ?>
      </div>
  </div>
</body>
</html>