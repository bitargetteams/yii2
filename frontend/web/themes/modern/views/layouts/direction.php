<!doctype html>
<html id="ng-app" ng-app="bitarget" lang="<?php echo Yii::app()->language; ?>">
<head>
<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.scripts').DIRECTORY_SEPARATOR.'global.js');
    Yii::app()->clientScript->registerScriptFile($assetsUrl);
?>
<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/scripts/overall_management.js');
    Yii::app()->clientScript->registerScriptFile($assetsUrl);
?>
<?php Yii::app()->clientScript->registerCssFile(
    Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery.ui.menu.css'); ?>
<?php 
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/global_layout.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
?>
<?php
    $assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('webroot').Yii::app()->theme->baseUrl.'/css/overall_management.css');
    Yii::app()->clientScript->registerCssFile($assetsUrl);
?>
    <link href='http://fonts.googleapis.com/css?family=Marck+Script&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

	<meta charset="UTF-8" />
	<title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/img/favicon_32_32.ico"/>
</head>
<body>
  <div id="root">
    <div class="bg">
        <div id="header">
            <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_header_content'); ?>
        </div>
        <?= $this->renderPartial('_begin_note'); ?>
        <div id="content">
            <div class="content-inner">
                <div class="column">
                    <?php echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.direction._tabs'); ?>
                    <?php if(isset($this->module)) { ?>
                        <div class="inner <?php echo CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior') ? $this->module->getUserActivity() : ''; ?>">
                        <?php if(CommonHelpers::isBehaviorEnabled($this->module, 'BInformerAboutModuleBehavior')) { ?>
                            <?php $this->widget('application.widgets.navigation.NavigationThirdLevel',
                                            array(
                                                'items' => $this->module->getCurrentThirdLevelMenu(),
                                                'htmlOptions' => array('class' => 'navigation-third-level'),
                                            )); ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="inner <?php if(CommonHelpers::getLastValueFromUrl() == 'opt') echo 'opt';
                                            else if(CommonHelpers::getLastValueFromUrl() == 'wm') echo 'wm'; ?>">
                    <?php } ?>
                    <div class="clearfix"></div>                   
                    <div class="direction-description"><?php echo $content; ?></div>
                    </div>
                </div>
            </div>
            <div class="sidebar">
                <div class="content">
                    <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.direction._directions_list'); ?>
                </div>
            </div>
            <div class="clearfix"></div> 
        </div>        
    </div>
    <div id="root_footer"></div>
  </div>
  <div id="footer">
      <div class="footer-inner">
        <?php $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts._layout_footer_content'); ?>
      </div>
  </div>
</body>
</html>