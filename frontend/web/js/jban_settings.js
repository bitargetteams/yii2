function loadTmpl(id) {
    loadedTmpl = $('#form_tmpl_' + id) || false;
    if (loadedTmpl.length == 0) {
        $.ajax('/jban/optimizer/gettmpl/' + id,
                {
                    method: 'GET',
                    success: function (data) {
                        $('#container').append(data);
                        if (window.viewTmplId) {
                            $('#form_tmpl_' + window.viewTmplId).css('display', 'none');
                        }
                        window.viewTmplId = id;
                    }
                });
    } else {
        $('#form_tmpl_' + id).css('display', 'block');
        $('#form_tmpl_' + window.viewTmplId).css('display', 'none');
        window.viewTmplId = id;
    }
    window.color = '#000';
}