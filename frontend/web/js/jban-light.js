function jbanLight(options) {
    var bannerId = options.bannerId || 0;
    var colorMain = options.colorMain || '#fff';
    var colorSecondary = options.colorSecondary || '#fff';

    var wrapper = $('#jban-' + bannerId)[0];
    var jh = $('#j-hint-' + bannerId);

    var edges = [
        [[0, 0, 0], [50, 0, 0], [50, 50, 0], [0, 50, 0]],
        [[0, 0, 0], [0, 50, 0], [0, 50, 50], [0, 0, 50]],
        [[0, 0, 50], [50, 0, 50], [50, 50, 50], [0, 50, 50]],
        [[50, 0, 0], [50, 0, 50], [50, 50, 50], [50, 50, 0]],
        [[0, 0, 50], [50, 0, 50], [50, 0, 0], [0, 0, 0]],
        [[0, 50, 50], [50, 50, 50], [50, 50, 0], [0, 50, 0]]
    ];
    var groupSides = [[0, 2], [1, 3], [4, 5]];
    var centre = [25, 25, 25];
    var canvas = Raphael(wrapper, 100, 100);
    canvas.canvas.style.top = '-25px';
    canvas.canvas.style.left = '-25px';

    var elements = [];
    var animationComplete = true;
    var mouseInHint = false;
    var mouseInTarget = false;
    var neededBeHidden = false;

    (function () {
        drawBox();
        var opts = {
            amountX: 1,
            amountY: 0.4,
            delay: 40,
            duration: 800,
            delta: linear
        };
        animateRotate(opts);
        showHint();


    })();

    function showHint() {
        if (!animationComplete) {
            return false;
        }
        animationComplete = false;
        jh.css('display', 'block');
        animateProperty({
            obj: jh,
            to: {
                left: '-335px',
                width: '300px',
                top: '-100px',
                opacity: '1'
            },
            duration: 250,
            delay: 25,
            complete: function () {
                animationComplete = true;
                setTimeout(function () {
                    if (neededBeHidden && !mouseInTarget && !mouseInHint) {
                        hideHint();
                    }
                }, 150);
            }
        });

        return true;
    }

    function addEvent(elem, evType, fn) {
        if (elem.addEventListener) {
            elem.addEventListener(evType, fn, false);
        }
        else if (elem.attachEvent) {
            elem.attachEvent('on' + evType, fn);
        }
        else {
            elem['on' + evType] = fn;
        }
    }

    function animate(opts) {
        var start = new Date;
        var delta = opts.delta || linear;

        var timer = setInterval(function () {
            var progress = (new Date - start) / opts.duration;
            if (progress > 1)
                progress = 1;

            opts.step(delta(progress));

            if (progress == 1) {
                clearInterval(timer);
                opts.complete && opts.complete();
            }
        }, opts.delay || 20);

        return timer;
    }

    function animateRotate(opts)
    {
        var xRad = 0;
        var yRad = 0;
        var amountRadX = opts.amountX || 0;
        var amountRadY = opts.amountY || 0;

        var stepRadX = 0, stepRadY = 0;
        opts.step = function (delta) {
            stepRadX = (amountRadX * delta) - xRad;
            stepRadY = (amountRadY * delta) - yRad;
            xRad += stepRadX;
            yRad += stepRadY;

            rotate('x', stepRadX);
            rotate('y', stepRadY);
        };

        animate(opts);
    }

    function animateProperty(opts) {
        var start = {};
        for (var property in opts.to) {
            start[property] = parseInt(opts.obj.css(property)) || 0;
        }

        opts.step = function (delta) {
            for (var p in opts.to) {
                var str = opts.to[p] + '';
                var value = parseInt(str);
                var res = str.match(/[^\d-]+/ig);
                var units = res ? res[0] : '';
                opts.obj.css(p, (start[p] - (start[p] - value) * delta) + units);
            }
        };

        animate(opts);
    }

    function drawBox()
    {
        for (var i = 0; i < edges.length; i++)
        {
            var path = null;
            edges[i].forEach(function (point) {
                path = path ? path + 'L' : 'M';
                path += point[0] + ' ' + point[1];
            });
            if (elements[i]) {
                elements[i].remove();
            }
            elements[i] = canvas.path(path + 'Z');
            elements[i].attr({'fill': '25-' + colorMain + ':0-' + colorSecondary + ':100', opacity: 1, stroke: colorMain, 'stroke-width': 1});
            elements[i].translate(20, 20);
        }

        overlap();
    }



    function overlap()
    {
        groupSides.forEach(function (group) {
            var elem1 = {
                index: group[0],
                sum: function () {
                    var total = 0;
                    edges[group[0]].forEach(function (currentValue) {
                        total = total + currentValue[2];
                    });
                    return total;
                }()
            }
            var elem2 = {
                index: group[1],
                sum: function () {
                    var total = 0;
                    edges[group[1]].forEach(function (currentValue) {
                        total = total + currentValue[2];
                    });
                    return total;
                }()
            }
            if (elem1.sum < elem2.sum) {
                elements[elem1.index].hide();
                elements[elem2.index].show();
            } else {
                elements[elem1.index].show();
                elements[elem2.index].hide();
            }

        });
    }

    function rotate(axis, radians) {
        axis = (axis == 'x') ? 1 : 0;
        for (var i = 0; i < edges.length; i++)
        {
            edges[i].forEach(function (points, index) {
                var coordAxis = points[axis] - centre[axis];
                var coordZ = points[2] - centre[2];
                var d = Math.sqrt(coordAxis * coordAxis + coordZ * coordZ);
                var theta = Math.atan2(coordAxis, coordZ) + radians;
                edges[i][index][axis] = centre[axis] + d * Math.sin(theta);
                edges[i][index][2] = centre[2] + d * Math.cos(theta);
            });
        }
        drawBox();
    }

    function linear(progress) {
        return progress;
    }

    function shadeColor(hex_color, percent) {

        var colorInt = parseInt(hex_color.substring(1), 16);
        var R = (colorInt & 0xFF0000) >> 16;
        var G = (colorInt & 0x00FF00) >> 8;
        var B = (colorInt & 0x0000FF) >> 0;

        R = (R != 0) ? R : 1;
        G = (G != 0) ? G : 1;
        B = (B != 0) ? B : 1;

        var k = (R + G + B) / (3 * 255);
        var percent = percent / k;

        // var kr = R / 255;


        R = R + Math.floor((percent / 255) * R);
        R = R < 255 ? R : 255;
        R = R.toString(16);
        R = (R.length == 1) ? 0 + '' + R : R;


        G = G + Math.floor((percent / 255) * G);
        G = G < 255 ? G : 255;
        G = G.toString(16);
        G = (G.length == 1) ? 0 + '' + G : G;


        B = B + Math.floor((percent / 255) * B);
        B = B < 255 ? B : 255;
        B = B.toString(16);
        B = (B.length == 1) ? 0 + '' + B : B;

        var newColorStr = '#' + '' + R + '' + G + '' + B;
        return newColorStr;
    }

    return {
        setColor: function (color) {
            color = '#' + color.replace('#', '');
            colorMain = color;
            colorSecondary = shadeColor(color, 35);
            drawBox();
            jh.find('.j-header').css('background', colorMain);
            jh.find('.j-body').css('background', colorSecondary);
            jh.find('.j-pointer').css('border-left-color', colorSecondary)
                    .css('border-left-width', "20px")
                    .css('border-left-style', "solid");
            jh.find('.j-button').css('border-color', colorSecondary);
        },
        setColorText: function (colorText) {
            colorText = '#' + colorText.replace('#', '');
            jh.find('.j-color').css('color', colorText);
        },
    }

}