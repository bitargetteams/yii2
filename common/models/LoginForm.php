<?php
namespace common\models;

use Yii;
use yii\base\Model;
use frontend\models\User;
use app\components\Api;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $login;
    public $email;
    public $password;
    public $rememberMe = false;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', function($attribute, $params){
                if (!empty($this->$attribute)) {
                    $this->addError($attribute, 'YOU ROBOT!');
                }
            }],
            [['email', 'password'], 'required'],
            ['email','email'],
            ['rememberMe','filter','filter'=>function($value){
                return $value == 'on' ? true : false;
            }],
            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        $childLabels = [
            'email' => Yii::t('login', 'Username'),
            'password' => Yii::t('login', 'Password'),
            'rememberMe' => Yii::t('login', 'Remember me'),
        ];
        return $childLabels + parent::attributeLabels();
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && ($userModel = $this->getUser())) {
            return Yii::$app->user->login($userModel, $this->rememberMe ? 3600 * 24 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Api::resource('user/login')->post(['password'=>  $this->password,'email'=>  $this->email]);
        }
        return (!isset($this->_user->errors) && isset($this->_user->items[0]) && $this->_user->items->id)  ? $this->_user->items : false;
    }
    
}
